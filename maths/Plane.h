/* 
 * File:   Plane.h
 * Author: thegiallo
 *
 * Created on 26 marzo 2011, 17.37
 */

#ifndef PLANE_H
#define	PLANE_H

#include "../maths/Vec3.h"

#ifndef ABS
#define ABS(a) ((a)<0?-(a):(a))
#endif
#ifndef SQRT2
#define SQRT2 1.414213562f
#endif

class Plane
{
public:
    Vec3 normal;
    Vec3 point;
    float d;
    Plane();
    Plane(Vec3 normal, Vec3 point);
    Plane(Vec3 normal, float d);
    Plane(Vec3 p1, Vec3 p2, Vec3 p3);
    void calc(Vec3 p1, Vec3 p2, Vec3 p3);
    void calcPoint();
    void calcD();
    float dist(Vec3 p);
};

#endif	/* PLANE_H */

