/* 
 * File:   PerlinNoise.h
 * Author: thegiallo
 *
 * Created on 5 ottobre 2011, 20.56
 */

#ifndef PERLINNOISE_H
#define	PERLINNOISE_H

#include "../maths/Vec2.h"
#include "../maths/Vec3.h"

class PerlinNoise {
public:
    PerlinNoise(float persistence=0.25, int number_of_octaves=4, int seed = 0);
    
    const static long int a[];
    const static long int b[];
    const static long int c[];
    
    float persistence;
    int number_of_octaves;
    int seed;
    
    float noise(int x, int n=0);
    float smoothedNoise(int x, int n=0);
    float cosineInterpolate(float a, float b, float x);
    float interpolatedNoise(float x, int n=0);
    float perlinNoise1D(float x);
    
    float noise2D(int x, int y, int i=0);
    float smoothNoise2D(Vec2 v, int i=0);
    float interpolatedNoise2D(Vec2 v, int i=0);
    float perlinNoise2D(Vec2 v);
    
    float noise3D(int x, int y, int z, int i=0);
    float smoothNoise3D(Vec3 v, int i=0);
    float interpolatedNoise3D(Vec3 v, int i=0);
    float perlinNoise3D(Vec3 v);
private:
    int internal_seed;
};

#endif	/* PERLINNOISE_H */

