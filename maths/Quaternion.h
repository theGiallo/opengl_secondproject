/* 
 * File:   Transformation.h
 * Author: thegiallo
 *
 *  code taken from http://gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
 *
 * Created on 29 gennaio 2011, 15.20
 */

#ifndef TRANSFORMATION_H
#define	TRANSFORMATION_H

class Quaternion;

#include "Vec3.h"
#include "Matrix4.h"

class Quaternion
{
protected:
    float w,x,y,z;

public:
    Quaternion();
    Quaternion(float x, float y, float z, float w);
    ~Quaternion();

    void normalize();
    Quaternion getConjugate();
    Quaternion operator* (const Quaternion &rq);
    Vec3 operator* (Vec3 vec);
    void FromAxis( Vec3 v, float angle);
    void FromEuler(float pitch, float yaw, float roll);
    Matrix4 getMatrix() const;
    void getAxisAngle(Vec3 *axis, float *angle);
};

#endif	/* TRANSFORMATION_H */

