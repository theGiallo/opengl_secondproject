#include "Plane.h"
#include <iostream>

Plane::Plane()
{
    normal = Vec3(0,0,1);
    d = 0;
    point = Vec3();
}

Plane::Plane(Vec3 normal, float d)
{
    this->normal = normal;
    this->d = d;
    calcPoint();
}
Plane::Plane(Vec3 normal, Vec3 point)
{
    this->normal = normal;
    this->point = point;
    calcD();
}

void Plane::calcPoint()
{
    point = normal*d;
}
void Plane::calcD()
{
    d =  -(normal.x*point.x+normal.y*point.y+normal.z*point.z);
}
Plane::Plane(Vec3 p1, Vec3 p2, Vec3 p3)
{
    calc(p1,p2,p3);
}

float Plane::dist(Vec3 p)
{
//    Vec3 dv = point-p;
//    Vec3 f = Vec3(dv.x/normal.x, dv.y/normal.y, dv.z/normal.z);
//    float min = ABS(f.x)<ABS(f.y)?
//                        (ABS(f.x) < ABS(f.z) ?
//                                    f.x
//                                    : (ABS(f.z)<ABS(f.y)? f.z : f.y))
//                        : (ABS(f.y)<ABS(f.z) ? f.y : f.z);
//    return min;
//    std::cout<<"d= "<<d<<" dist from: ";p.print();
//    normal.print();
//    point.print();
    return normal.x*p.x+normal.y*p.y+normal.z*p.z+d;
}

void Plane::calc(Vec3 p1, Vec3 p2, Vec3 p3)
{
    Vec3 v1 = p1-p2;
    Vec3 v2 = p3-p2;
    this->normal = (v1^v2).getNormalized();
//    std::cout<<"calcolo: ";p1.print();p2.print();p3.print();
//    std::cout<<"normal: "; normal.print();
    point = p1;
    calcD();
}