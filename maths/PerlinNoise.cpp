/* 
 * File:   PerlinNoise.cpp
 * Author: thegiallo
 * 
 * Created on 5 ottobre 2011, 20.56
 */

#include "PerlinNoise.h"
#include <cmath>
#include <climits>
#include <iostream>

#define PRIMES_ARRAY_LENGTH 11

const long int PerlinNoise::a[PRIMES_ARRAY_LENGTH] = {15731,     16127,      18397,    2113,        18181,  7,          5039,      28657,     433494437,         27056,  60493};
const long int PerlinNoise::b[PRIMES_ARRAY_LENGTH] = {789221,    1046527,    115249,   29947,       108881, 127,        39916801,  514229,    2971215073,        106033, 19990303};
const long int PerlinNoise::c[PRIMES_ARRAY_LENGTH] = {1376312589,1073676287, 27644437, 68718952447, 181081, 2147483647, 479001599, 433494437, 99194853094755497, 999331, 1376312589};

PerlinNoise::PerlinNoise(float persistence, int number_of_octaves, int seed)
{
    this->persistence = persistence;
    this->number_of_octaves = number_of_octaves;
    this->seed = seed;
}

float PerlinNoise::noise(int x, int n)
{    
    x = x*1619 + internal_seed*1013;
    n &= 0x7fffffff;
    x = (x>>13) ^ x;
    return ( (x * (x * x * a[n] +b[n]) +c[n]) & 0x7fffffff) /(float)INT_MAX;
//    return ( 1.0 - ( (x * (x * x * a[n] +b[n]) +c[n]) & 0x7fffffff) / 1073741824.0f);
}
float PerlinNoise::smoothedNoise(int x, int n)
{
    return noise(x,n)/2.0f  +  noise(x-1,n)/4.0f  +  noise(x+1,n)/4.0f;
}
float PerlinNoise::cosineInterpolate(float a, float b, float x)
{
    float  ft = x * 3.1415927f;
    float f = (1 - cos(ft)) * 0.5f;

    return  a*(1-f) + b*f;
}
float PerlinNoise::interpolatedNoise(float x, int n)
{
    int  integer_X = (int)x;
    float fractional_X = x - integer_X;

    float v1 = smoothedNoise(integer_X, n);
    float v2 = smoothedNoise(integer_X + 1, n);

    return cosineInterpolate(v1 , v2 , fractional_X);
}
float PerlinNoise::perlinNoise1D(float x)
{
    int i;
    float total = 0;
    float p = persistence;
    int n = number_of_octaves - 1;
    float frequency, amplitude;

    for (i=0; i<n ; i++ )
    {
        frequency = pow((float)2,i);
        amplitude = pow((float)p,i);
          
        internal_seed = (seed + i) & 0xffffffff;
    
        total = total + interpolatedNoise(x * frequency) * amplitude;
    }
    return total;
}

float PerlinNoise::noise2D(int x, int y, int i)
{
    int n = (x*1619 + y * 31337 + internal_seed*1013) & 0x7fffffff;
    n = (n>>13) ^ n;
    return ( (n * (n * n * a[i] +b[i]) +c[i]) & 0x7fffffff) /(float)INT_MAX;
//    return ( 1.0 - ( (n * (n * n * a[i] +b[i]) +c[i]) & 0x7fffffff) / 1073741824.0f);
}
float PerlinNoise::smoothNoise2D(Vec2 v, int i)
{
    float corners = ( noise2D(v.x-1, v.y-1, i)+noise2D(v.x+1, v.y-1, i)+noise2D(v.x-1, v.y+1, i)+noise2D(v.x+1, v.y+1, i) ) / 16.0f;
    float sides   = ( noise2D(v.x-1, v.y, i)  +noise2D(v.x+1, v.y, i)  +noise2D(v.x,   v.y-1, i)+noise2D(v.x,   v.y+1, i) ) /  8.0f;
    float center  =   noise2D(v.x,   v.y, i) / 4.0f;
    return corners + sides + center;
}
float PerlinNoise::interpolatedNoise2D(Vec2 v, int i)
{
    int integer_X    = (int)v.x;
    float fractional_X = v.x - (float)integer_X;

    int integer_Y    = (int)v.y;
    float fractional_Y = v.y - (float)integer_Y;

    float v1 = smoothNoise2D(Vec2(integer_X,     integer_Y    ), i);
    float v2 = smoothNoise2D(Vec2(integer_X + 1, integer_Y    ), i);
    float v3 = smoothNoise2D(Vec2(integer_X,     integer_Y + 1), i);
    float v4 = smoothNoise2D(Vec2(integer_X + 1, integer_Y + 1), i);

    float i1 = cosineInterpolate(v1 , v2 , fractional_X);
    float i2 = cosineInterpolate(v3 , v4 , fractional_X);

    return cosineInterpolate(i1 , i2 , fractional_Y);
}
float PerlinNoise::perlinNoise2D(Vec2 v)
{
      float total = 0;
      float p = persistence;
      int n = number_of_octaves - 1;

      for (int i=0 ; i<n ; i++)
      {
          float frequency = pow((float)2,i);
          float amplitude = pow((float)p,i);
          
          internal_seed = (seed + i) & 0xffffffff;

          total = total + interpolatedNoise2D(v*frequency, 0) * amplitude;
      }
      return total;
}


float PerlinNoise::noise3D(int x, int y, int z, int i)
{
    int n = x*1619 + y * 31337 + z * 6971 + internal_seed*1013;
    n &= 0x7fffffff;
    n = (n>>13) ^ n;
    return ( (n * (n * n * a[i] +b[i]) +c[i]) & 0x7fffffff) /(float)INT_MAX;
//    return ( 1.0 - ( (n * (n * n * a[i] +b[i]) +c[i]) & 0x7fffffff) / 1073741824.0f);
}
float PerlinNoise::smoothNoise3D(Vec3 v, int i)
{
    float corners = ( noise3D(v.x+1, v.y+1, v.z+1, i)+
                      noise3D(v.x+1, v.y+1, v.z-1, i)+
                      noise3D(v.x+1, v.y-1, v.z+1, i)+
                      noise3D(v.x+1, v.y-1, v.z-1, i)+
                      noise3D(v.x-1, v.y+1, v.z+1, i)+
                      noise3D(v.x-1, v.y+1, v.z-1, i)+
                      noise3D(v.x-1, v.y-1, v.z+1, i)+
                      noise3D(v.x-1, v.y-1, v.z-1, i) ) / 24.0f;
    float sides   = ( noise3D(v.x+1, v.y, v.z, i)+
                      noise3D(v.x-1, v.y, v.z, i)+
                      noise3D(v.x, v.y+1, v.z, i)+
                      noise3D(v.x, v.y-1, v.z, i)+
                      noise3D(v.x, v.y, v.z+1, i)+
                      noise3D(v.x, v.y, v.z-1, i) ) /  18.0f;
    float center  =  noise3D(v.x, v.y, v.z, i)/3.0f;
    return corners + sides + center;
}
float PerlinNoise::interpolatedNoise3D(Vec3 v, int i)
{
    int integer_X    = int(v.x);
    int fractional_X = v.x - integer_X;
    int integer_Y    = int(v.y);
    int fractional_Y = v.y - integer_Y;
    int integer_Z    = int(v.z);
    int fractional_Z = v.z - integer_Z;
    
    
    // TODO fare come si deve

    float v1 = smoothNoise3D(Vec3(integer_X,     integer_Y, integer_Z), i);
    float v2 = smoothNoise3D(Vec3(integer_X + 1, integer_Y, integer_Z), i);
    float v3 = smoothNoise3D(Vec3(integer_X,     integer_Y + 1, integer_Z), i);
    float v4 = smoothNoise3D(Vec3(integer_X,     integer_Y, integer_Z + 1 ), i);
    float v5 = smoothNoise3D(Vec3(integer_X + 1, integer_Y + 1, integer_Z), i);
    float v6 = smoothNoise3D(Vec3(integer_X, integer_Y + 1, integer_Z+1), i);
    float v7 = smoothNoise3D(Vec3(integer_X + 1, integer_Y, integer_Z+1), i);

    float i1 = cosineInterpolate(v1 , v2 , fractional_X);
    float i2 = cosineInterpolate(v3 , v4 , fractional_X);
    float i3 = cosineInterpolate(v5 , v6 , fractional_Y);
    float i4 = cosineInterpolate(i3 , v7 , fractional_Z);
    float i5 = cosineInterpolate(i1 , i2 , fractional_Y);

    return cosineInterpolate(i4 , i5 , fractional_Z);
}
float PerlinNoise::perlinNoise3D(Vec3 v)
{
      float total = 0;
      float p = persistence;
      int n = number_of_octaves - 1;

      for (int i=0 ; i<n ; i++)
      {
          float frequency = 2*i;
          float amplitude = p*i;
          
          internal_seed = (seed + i) & 0xffffffff;

          total = total + interpolatedNoise3D(Vec3(v.x*frequency, v.y*frequency, v.z*frequency)) * amplitude;
      }
      return total;
}