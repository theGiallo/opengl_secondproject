/* 
 * File:   Vec2.h
 * Author: thegiallo
 *
 * Created on 28 gennaio 2011, 1.33
 */

#ifndef VEC2_H
#define	VEC2_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <cstdint>

#include "Vec3.h"

#ifndef X_COMPONENT
#define X_COMPONENT 0
#endif
#ifndef Y_COMPONENT
#define Y_COMPONENT 1
#endif

class Vec2
{
public:
    float x,y;

    Vec2();
    Vec2(float x, float y);
    float getModule(void) const;
    float getSin(Vec2 v) const;
    float getCos(Vec2 v) const;
    float getAngle(Vec2 v) const;
    void normalize(void);
    Vec2 getNormalized(void) const;
    void add(Vec2 v);
    Vec2 operator +(Vec2 v) const;
    Vec2 operator +=(Vec2 v);
    void sub(Vec2 v);
    Vec2 operator -(void) const;
    Vec2 operator -(Vec2 v) const;
    Vec2 operator -=(Vec2 v);
    void add(uint8_t i, float f); /// adds as if it was an array {x,y}
    void add(float k);
    Vec2 operator +(float k) const;
    Vec2 operator +=(float k);
    void sub(float k);
    Vec2 operator -(float k) const;
    Vec2 operator -=(float k);
    void mult(uint8_t i, float f); /// multiply as if it was an array {x,y}
    void mult(float k);
    Vec2 operator *(float k) const;
    Vec2 operator *=(float k);
    void div(uint8_t i, float f); /// divides as if it was an array {x,y}
    void div(float k);
    Vec2 operator/(float k) const;
    Vec2 operator/=(float k);
    float scalar(Vec2 v) const;
    float operator *(Vec2 v) const;
//    Vec2 getVectorial(Vec2 v);
    Vec3 operator ^(Vec2 v) const;
//    Vec2 operator ^=(Vec2 v);
    bool isEqual(Vec2 v) const;
    bool isEqual(Vec2 v, float epsilon) const;
    bool operator ==(Vec2 v) const;
    bool operator !=(Vec2 v) const;
    void clone(Vec2 v);
    Vec2 getClone(void) const;
    Vec2 operator =(Vec2 v);
    float get(uint8_t i) const; /// returns as if it was an array {x,y}
    float getX(void) const;
    float getY(void) const;
    void set(uint8_t i, float f); /// sets as if it was an array {x,y}
    void setX(float x);
    void setY(float y);
    void rotate(float rot);
    Vec2 getRotated(float rot) const;
    void rotate(float rot, Vec2 center);
    Vec2 getRotated(float rot, Vec2 center) const;
    void print(void) const;
};

void buildMatrixRotFromUpVector(Vec2 up, GLfloat* matrix);

#endif	/* VEC2_H */

