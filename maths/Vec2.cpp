#define _USE_MATH_DEFINES
#include <cmath>
#include <cassert>
#include <iostream>

#include "Vec2.h"

Vec2::Vec2()
{
    x = y = 0;
}
Vec2::Vec2(float x, float y)
{
    this->x = x;
    this->y = y;
}
float Vec2::getModule(void) const
{
    return sqrt(x*x+y*y);
}
float Vec2::getSin(Vec2 v) const
{
    Vec2 v1 = this->getNormalized();
    Vec2 v2 = v.getNormalized();
    return v1.x*v2.y-v1.y*v2.x;
}
float Vec2::getCos(Vec2 v) const
{
    return this->getNormalized()*v.getNormalized();
}
float Vec2::getAngle(Vec2 v) const
{
    Vec2 v1 = this->getNormalized();
    Vec2 v2 = v.getNormalized();
	float s = v1.x*v2.y-v1.y*v2.x;
	float c = v1*v2;
	if (c>0)
	{
		return std::asin(s);
	}
	if (s>0)
	{
		return std::acos(c);
	}
	return std::acos(s)+M_PI;
}
void Vec2::normalize(void)
{
    float l = this->getModule();
    if ( l != 0 )
    {
        *this /= l;
    }
}
Vec2 Vec2::getNormalized(void) const
{
    Vec2 tmp = getClone();
    tmp.normalize();
    return tmp;
}
void Vec2::add(Vec2 v)
{
    x+=v.x;
    y+=v.y;
}
Vec2 Vec2::operator +(Vec2 v) const
{
    Vec2 tmp = this->getClone();
    tmp.add(v);
    return tmp;
}
Vec2 Vec2::operator +=(Vec2 v)
{
    this->add(v);
    return *this;
}
void Vec2::sub(Vec2 v)
{
    x-=v.x;
    y-=v.y;
}
Vec2 Vec2::operator -(void) const
{
	return *this*-1;
}
Vec2 Vec2::operator -(Vec2 v) const
{
    Vec2 tmp = this->getClone();
    tmp.sub(v);
    return tmp;
}
Vec2 Vec2::operator -=(Vec2 v)
{
    this->sub(v);
    return *this;
}
void Vec2::add(uint8_t i, float f)
{
    switch(i%2)
    {
        case X_COMPONENT:
            x += f;
            break;
        case Y_COMPONENT:
            y += f;
            break;
    }
}
void Vec2::add(float k)
{
    x+=k;
    y+=k;
}
Vec2 Vec2::operator +(float k) const
{
    Vec2 tmp = this->getClone();
    tmp.add(k);
    return tmp;
}
Vec2 Vec2::operator +=(float k)
{
    this->add(k);
    return *this;
}
void Vec2::sub(float k)
{
    x-=k;
    y-=k;
}
Vec2 Vec2::operator -(float k) const
{
    Vec2 tmp = this->getClone();
    tmp.sub(k);
    return tmp;
}
Vec2 Vec2::operator -=(float k)
{
    this->sub(k);
    return *this;
}
void Vec2::mult(uint8_t i, float f)
{
    switch(i%2)
    {
        case X_COMPONENT:
            x *= f;
            break;
        case Y_COMPONENT:
            y *= f;
            break;
    }
}
void Vec2::mult(float k)
{
    x*=k;
    y*=k;
}
Vec2 Vec2::operator *(float k) const
{
    Vec2 tmp = this->getClone();
    tmp.mult(k);
    return tmp;
}
Vec2 Vec2::operator *=(float k)
{
    this->mult(k);
    return *this;
}
Vec3  Vec2::operator ^(Vec2 v) const
{
	return Vec3(x,y,0)^Vec3(v.x,v.y,0);
}
void Vec2::div(uint8_t i, float f)
{
    switch(i%2)
    {
        case X_COMPONENT:
            x /= f;
            break;
        case Y_COMPONENT:
            y /= f;
            break;
    }
}
void Vec2::div(float k)
{
    if (k==0)
        return;
    x/=k;
    y/=k;
}
Vec2 Vec2::operator/(float k) const
{
    Vec2 tmp = this->getClone();
    tmp.div(k);
    return tmp;
}
Vec2 Vec2::operator/=(float k)
{
    this->div(k);
    return *this;
}
float Vec2::scalar(Vec2 v) const
{
    return (x*v.x+y*v.y);
}
float Vec2::operator *(Vec2 v) const
{
    return this->scalar(v);
}
//Vec2 Vec2::getVectorial(Vec2 v)
//{
//    return Vec2( y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x );
//}
//Vec2 Vec2::operator ^(Vec2 v)
//{
//    return this->getVectorial(v);
//}
//Vec2 Vec2::operator ^=(Vec2 v)
//{
//    *this = this->getVectorial(v);
//}
bool Vec2::isEqual(Vec2 v) const
{
    return x==v.x&&
                y==v.y;
}
bool Vec2::isEqual(Vec2 v,float epsilon) const
{
    return fabs(x - v.x) < epsilon &&
                fabs(y - v.y) < epsilon;
}
bool  Vec2::operator ==(Vec2 v) const
{
    return this->isEqual(v);
}
bool  Vec2::operator !=(Vec2 v) const
{
    return !(this->isEqual(v));
}
void  Vec2::clone(Vec2 v)
{
    x = v.x;
    y = v.y;
}
Vec2  Vec2::getClone(void) const
{
    return Vec2(x,y);
}
Vec2  Vec2::operator =(Vec2 v)
{
    this->clone(v);
    return *this;
}
float Vec2::get(uint8_t i) const
{
    switch(i%2)
    {
        case X_COMPONENT:
            return x;
        case Y_COMPONENT:
            return y;
        default:
        	assert(false);
        	return 0.0f;
    }
}
float  Vec2::getX(void) const
{
    return x;
}
float  Vec2::getY(void) const
{
    return y;
}
void Vec2::set(uint8_t i, float f)
{
    switch(i%2)
    {
        case X_COMPONENT:
            x = f;
            break;
        case Y_COMPONENT:
            y = f;
            break;
    }
}
void  Vec2::setX(float x)
{
    this->x = x;
}
void  Vec2::setY(float y)
{
    this->y = y;
}
void Vec2::rotate(float rot)
{
    float s = sin(rot);
    float c = cos(rot);
    float xx = x*c - y*s;
    float yy = x*s + y*c;
    x=xx;
    y=yy;
}
Vec2 Vec2::getRotated(float rot) const
{
    Vec2 res = *this;
    res.rotate(rot);
    return res;
}
void Vec2::rotate(float rot, Vec2 center)
{
    *this-=center;
    this->rotate(rot);
    *this+=center;
}
Vec2 Vec2::getRotated(float rot, Vec2 center) const
{
    Vec2 res = *this;
    res.rotate(rot, center);
    return res;
}
void Vec2::print() const
{
    std::cout<<"("<<x<<", "<<y<<")"<<std::endl;
}

/**
 * the up vector (0,1) is considered the zero rotation
 * so x and y are swapped with respect to a normal rotation
 **/
void buildMatrixRotFromUpVector(Vec2 up, GLfloat* matrix)
{
	matrix[0] = up.y;
	matrix[1] = up.x;
	matrix[2] = 0;
	matrix[3] = 0;

	matrix[4] = -up.x;
	matrix[5] = up.y;
	matrix[6] = 0;
	matrix[7] = 0;

	matrix[8] = 0;
	matrix[9] = 0;
	matrix[10] = 1;
	matrix[11] = 0;

	matrix[12] = 0;
	matrix[13] = 0;
	matrix[14] = 0;
	matrix[15] = 1;
}
