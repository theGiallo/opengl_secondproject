#include "Trigger2D.h"
#include <iostream>
Trigger2D::Trigger2D(Bounder2D* bounder, Collider2D* collider)// : bounder(bounder), collider(collider)
{
    // 
    active = check_collisions = intersectable = visible = true;
    //
    
    this->bounder = bounder;
    this->collider = collider;
    
    // rendered yellow
    color[0] = 1.0f;
    color[1] = 1.0f;
    color[2] = 0.0f;
    color[3] = 1.0f;
}

Trigger2D::~Trigger2D()
{
    delete bounder;
    delete collider;
    // TODO questa cosa andrebbe costituita con un uso dei puntatori std
}

void Trigger2D::setBounder(Bounder2D* bounder)
{
    this->bounder = bounder;
    
    if (!bounder)
    {
        return;
    }
    pos = bounder->getPos();
    bounder->check_collisions=bounder->intersectable=bounder->visible=true;
    bounder->setColor(color);
}

void Trigger2D::render(void)
{
    if (!bounder)
    {
        return;
    }
    bounder->render();
}
