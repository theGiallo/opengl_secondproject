/** 
 * File:   LD25Hero.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 16 December 2012
 * 
 */
#ifndef LD25HERO_H
#define	LD25HERO_H


#include "LD25Player.h"
#include "LD25World.h"

class LD25Hero : public LD25Player
{
protected:
    int max_evilness;
    int villains[LD25World::num_players];
    int villains_count;
    int villain_coming_to_id;
    bool coming;
    bool fighting;
    bool going_away;
    bool purifying;
    bool waiting;
    bool going_center;
    uint32_t time_to_fight;
    uint32_t time_to_wait;
    void (LD25Hero::*after_wait_func)();
    BoundingCircle* away_bounder;
    BoundingCircle* center_bounder;
    void targetVillain(void);
    void goToCenter(void);
    void goAway(void);
public:
    
    LD25Hero();
    ~LD25Hero();
    
    static const uint32_t TIME_HERO_PERIOD_MS;
    static const float coming_velocity;
    static const Vec2 away_pos;
    uint32_t time_until_come_ms;
    
    /**
     * @override from PhysicalObject2D
     */ 
    virtual void updatePhysics(Uint32 elapsedTime){PhysicalObject2D::updatePhysics(elapsedTime);};
    
    /**
     * @override from PositionableObject2D
     */
    virtual void setPos(const Vec2& pos){PositionableObject2D::setPos(pos);};
    virtual void setUp(const Vec2& up){PositionableObject2D::setUp(up);};
    virtual void setScale(const Vec2& scale){PositionableObject2D::setScale(scale);};
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime){};
    
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection){throw false;};

};

#endif /* LD25HERO_H */
