/* 
 * File:   VoxelAtom.h
 * Author: thegiallo
 *
 * Created on 20 marzo 2011, 15.47
 */

#ifndef VOXELATOM_H
#define	VOXELATOM_H
#include "../maths/Vec3.h"
class VoxelAtom;
#include "AtomFace_VoxelAtom.h"
// SPOSTATO IN AtomFace_VoxelAtom.h
//class VoxelAtom
//{
//public:
//    const static float side = 0.1;
//    Vec3 pos;
//    GLfloat color[4];
//    AtomFace faces[6];
//
//    VoxelAtom();
//    VoxelAtom(Vec3 pos, GLfloat color[4]);
//    void setColor(GLfloat color[4]);
//};

#endif	/* VOXELATOM_H */

