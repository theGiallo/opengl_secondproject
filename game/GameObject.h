/* 
 * File:   GameObject.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 17 marzo 2011, 21.23
 */

#ifndef GAMEOBJECT_H
#define	GAMEOBJECT_H

#include <stdlib.h>
#include <list>
#include <SDL/SDL.h>

class GameObject
{
public:
    Uint32 id;
    bool active;

	GameObject()
	{
		active=true;
	}
//    std::list<Behaviour> behaviours;

    virtual void update(Uint32 elapsedTime)=0;
    virtual void lateUpdate(Uint32 elapsedTime)=0;

};

#endif	/* GAMEOBJECT_H */

