#include "Loopable2DLD25.h"
#include "TestCollider2D.h"
#include "../graphics/RegularPolygonBounder.h"
#include "../graphics/BoundingCircle.h"
#include "LD25World.h"
#include "LD25MOB.h"
#include "LD25Hero.h"
#include <cstdlib>
#include <ctime>

#include "../graphics/OGLColors.h"

#include <cassert>

using namespace std;

Loopable2DLD25::~Loopable2DLD25()
{
    if(LD25World::hero)
    {
        delete LD25World::hero;
    }
}

void Loopable2DLD25::myGlewInit()
{
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if ( GLEW_OK!=err)
    {
        cout<<"error initializing GLEW: "<<glewGetErrorString(err)<<endl;
    }
    if (glewIsSupported("GL_VERSION_1_3"))
    {
        cout<<"OpenGL 1.3 supported"<<endl;
    }
    if (GLEW_VERSION_1_3)
    {
        cout<<"GLEW 1.3 supported"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_1"))
    {
        cout<<"OpenGL 2.1"<<endl;
    }
    if (glewIsSupported("GL_VERSION_4_0"))
    {
        cout<<"OpenGL 4.0"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_0"))
    {
        printf("Ready for OpenGL 2.0\n");
    } else
    {
        printf("OpenGL 2.0 not supported\n");
        //exit(1);
    }
}
/**
 * @override from IWindowResizeHandler
 **/
void Loopable2DLD25::onResize(int w, int h)
{
    if (w==width && h==height)
    {
            return;
    }
    float ar = (float)width/(float)height;
    cout<<"ar: "<<ar<<endl;
    cout<<"width: "<<width<<" w: "<<w<<endl;
    cout<<"height: "<<height<<" h: "<<h<<endl;
    width=MAX(w, 1);
    height=MAX(h, 1);
    float nar = (float)width/(float)height;
#ifdef WIN32
			int real_width = width;
			int real_height = height;
#endif
    if (nar >= ar)
    {
        if (ar<1)
        {
            height = ((float)width)/ar;
        } else
        {
            width = ((float)height)*ar;
        }
    } else
    {
        if (ar>=1)
        {
            height = ((float)width)/ar;
        } else
        {
            width = ((float)height)*ar;
        }
    }
#ifdef WIN32
			int space_w=0, space_h=0;
			if (real_width!=width)
				space_w = (real_width-width)/2;
			if (real_height!=height)
				space_h  = (real_height-height)/2;
			glViewport(space_w, space_h, width, height);
#endif
	char c_title[500];
#ifdef WIN32
	sprintf_s(c_title, "%s %dx%d", title.c_str(), width, height);
#else
        sprintf(c_title, "%s %dx%d", title.c_str(), width, height);
#endif
    window.setTitle(c_title);
#ifndef WIN32
	window.resize(width, height);
#endif
    //camera->setAspectRatio((float)width/(float)height);
}

/**
 * keys pointer has not to be allocated
 **/
const Uint8* Loopable2DLD25::getKeysMap_const(void) const
{
    return keys;
}
Uint8* Loopable2DLD25::getKeysMap(void)
{
    return keys;
}
void Loopable2DLD25::setKeysMap(Uint8* keys)
{
    this->keys = keys;
}

void Loopable2DLD25::onEvent(SDL_Event* event)
{
    eventHandler->onEvent(event);
    switch ( event->type )
    {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_VIDEORESIZE:
            if ( !window.resize(event->resize.w, event->resize.h) )
            {
                cout<<"Oh no! Resize failed!"<<endl;
            }
            break;
    }
}
void Loopable2DLD25::keyChecks(void) /// checks the keymap
{
    if ( keys[SDLK_F1] )
    {
            draw_interface = !draw_interface;
            keys[SDLK_F1]=false;
    }
    if ( keys[SDLK_F2] )
    {
            GameManager2D::renderBounders = !GameManager2D::renderBounders;
            keys[SDLK_F2]=false;
    }
    if ( keys[SDLK_F3] )
    {
            GameManager2D::renderNotBounders = !GameManager2D::renderNotBounders;
            keys[SDLK_F3]=false;
    }
    if ( keys[SDLK_F4] )
    {
            draw_axes = !draw_axes;
            keys[SDLK_F4]=false;
    }
    if ( keys[SDLK_F5] )
    {
            GameManager2D::do_collide = !GameManager2D::do_collide;
            keys[SDLK_F5]=false;
    }
    if ( keys[SDLK_F6] )
    {
            GameManager2D::do_collision_react = !GameManager2D::do_collision_react;
            keys[SDLK_F6]=false;
    }
    if ( keys[SDLK_F7] )
    {
            GameManager2D::do_update = !GameManager2D::do_update;
            keys[SDLK_F7]=false;
    }
    if ( keys[SDLK_F8] )
    {
            GameManager2D::do_late_update = !GameManager2D::do_late_update;
            keys[SDLK_F8]=false;
    }
    if ( keys[SDLK_F9] )
    {
            GameManager2D::do_update_physics = !GameManager2D::do_update_physics;
            keys[SDLK_F9]=false;
    }
    
    /**
     * camera movement
     * should be done automatically (camera should be renderable, posizionable and physical)
     **/
    short int x=0,y=0;
    if ( keys[SDLK_LEFT] )
    {
        x--;
    }
    if ( keys[SDLK_RIGHT] )
    {
        x++;
    }
    if ( keys[SDLK_DOWN] )
    {
        y--;
    }
    if ( keys[SDLK_UP] )
    {
        y++;
    }
    if (x||y)
    {
        camera_new_pos_dir = Vec2(x,y).getNormalized()*camera_velocity*(keys[SDLK_LSHIFT]||keys[SDLK_RSHIFT]?camera_velocity_multiplier:1.0f);
        camera_changed_pos=true;
    }
    /**
     * camera zoom
     */
    x=0;
    if ( keys[SDLK_PAGEUP] )
    {
        x++;
    }
    if ( keys[SDLK_PAGEDOWN] )
    {
        x--;
    }
    if (keys[SDLK_EQUALS] || (keys[SDLK_0]&&(keys[SDLK_LSHIFT]||keys[SDLK_RSHIFT])))
    {
        camera.setZoom(1.0f);
    } else
    if (x)
    {
        camera_new_zoom_fact = x*camera_scale_factor;
        camera_changed_zoom = true;
    }
    playerControl(LD25World::players[0], LD25World::wasd[0], LD25World::action[0]);
    playerControl(LD25World::players[1], LD25World::wasd[1], LD25World::action[1]);
}
void Loopable2DLD25::playerControl(LD25Player& player, uint8_t wasd[4], uint8_t* action)
{
    /**
     * player movement
     **/
    int x=0,y=0;
    if ( keys[wasd[1]] )
    {
        x--;
    }
    if ( keys[wasd[3]] )
    {
        x++;
    }
    if ( keys[wasd[2]] )
    {
        y--;
    }
    if ( keys[wasd[0]] )
    {
        y++;
    }
    if (player.canMove())
    {
        player.desired_vel = Vec2(Vec2(x,y).getNormalized()*player.getVelocityModule());
    } else
    {
        player.desired_vel.x=0.0f;
        player.desired_vel.y=0.0f;
    }
    
    /**
     * Player action
     */
    if ( keys[action[0]] )
    {
        bool b = player.act();
        keys[action[0]] = false;
    }
    if ( keys[action[1]] )
    {
        bool b = player.act(true);
        keys[action[1]] = false;
    }
}

void Loopable2DLD25::onLoop(Uint32 elapsedTime)
{
    if (camera_changed_pos)
    {
        camera.setPos(camera.getPos()+camera_new_pos_dir*elapsedTime/camera.getZoom());
    }
    if (camera_changed_zoom)
    {
        camera.setZoom(camera.getZoom()*(1.0f+camera_new_zoom_fact*elapsedTime));
    }
    if (camera_changed_pos||camera_changed_zoom)
    {
        camera.updateData();
        camera_changed_pos=false;
        camera_changed_zoom = false;
    }
    GameManager2D::update(elapsedTime);
}
void Loopable2DLD25::onRender(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glShadeModel(GL_FLAT);
    //glEnable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);
    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glHint (GL_POINT_SMOOTH_HINT, GL_DONT_CARE);
    //glEnable(GL_POLYGON_SMOOTH); ///it causes the line between the background triangles to be visible
	glEnable (GL_LINE_SMOOTH);
	glEnable (GL_POINT_SMOOTH);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0f,10.0f);

    camera.setGLCamera();
    
#if 1 //background white
    float xmin,xmax,ymin,ymax;
    xmin = camera.getPos().getX()-camera.getActualWidth()/2.0f;
    xmax = camera.getPos().getX()+camera.getActualWidth()/2.0f;
    ymin = camera.getPos().getY()-camera.getActualHeight()/2.0f;
    ymax = camera.getPos().getY()+camera.getActualHeight()/2.0f;

//    glMatrixMode(GL_PROJECTION);
//    glPushMatrix();
//    glLoadIdentity();
    glPushAttrib(GL_ENABLE_BIT);
    glPushAttrib(GL_POLYGON_BIT);
    glPushAttrib(GL_CURRENT_BIT);
        glDisable(GL_DEPTH_TEST);
        glPolygonOffset(1.0f, 15.0f);
        glDisable(GL_BLEND);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_NONE);
        
        glColor3fv(OGLColors::white);
        glEdgeFlag(GL_FALSE);
            glBegin(GL_TRIANGLE_STRIP);
                glVertex2f(xmin,ymin);
                glVertex2f(xmin,ymax);
                glVertex2f(xmax,ymin);
                glVertex2f(xmax, ymax);
            glEnd();
        glPopAttrib();
        glPopAttrib();
        glPopAttrib();
//    glPopMatrix();
#endif

//    glPointSize(10);
//    glColor3fv(OGLColors::brown);
//    glBegin(GL_POINTS);
//        glVertex3f(camera.getPos().getX(),camera.getPos().getY(),1.0f);
//    glEnd();
    
    GameManager2D::render();
    
    if (draw_axes)
    {
        glPushAttrib(GL_ENABLE_BIT);
        glPushAttrib(GL_POLYGON_BIT);
        glPushAttrib(GL_CURRENT_BIT);
        glPushAttrib(GL_POINT_BIT);
            glPointSize(10);
            glPolygonOffset(1.0f, 1.0f);
            glBegin(GL_LINES);
                glColor3fv(OGLColors::red);
                glVertex2f(-10,0);
                glVertex2f(10,0);
                glColor3fv(OGLColors::green);
                glVertex2f(0,-10);
                glVertex2f(0,10);
            glEnd();
            glBegin(GL_POINTS);
                glColor3fv(OGLColors::red);
                glVertex2f(10,0);
                glColor3fv(OGLColors::green);
                glVertex2f(0,10);
            glEnd();
        glPopAttrib();
        glPopAttrib();
        glPopAttrib();
        glPopAttrib();
    }

#if 0
    const_cast<BoundingRectangle&>(camera.getBoundingRectangle()).render();
#endif

    /**
     * Interface
     **/
    text.color = OGLColors::white;
    FPS_tt.check(MIDDLE_CHECK);
    deltaTimeFPSCalc += FPS_tt.last_delta;
    if ( deltaTimeFPSCalc >= 1000000)
    {
        rendered_frames_in_last_sec++;
        displayedFPS = (float)(rendered_frames_in_last_sec*1000000.0)/deltaTimeFPSCalc;
        rendered_frames_in_last_sec=0;
        deltaTimeFPSCalc = 0;
    } else
    {
        rendered_frames_in_last_sec++;
    }
    if (draw_interface)
    {
        char text_c[200];
        string text_string;
        float txtsize = 1.0f;

        sprintf(text_c, "FPS:%.3f", displayedFPS);
        text_string = text_c;
        text.print(4,4, text_string, txtsize, (float)width, (float)height);
#if 0
        sprintf(text_c, "Camera: pos= %f, %f   zoom= %f", camera.getPos().getX(), camera.getPos().getY(), camera.getZoom());
        text_string = text_c;
        text.print(16,/*window.getHeight()*/height-52, text_string, txtsize, (float)width, (float)height);
#endif
        for (int i=0 ; i<LD25World::num_players ; i++)
        {
            GLfloat tc[4];
            text.color = LD25World::players[i].getColor(tc);
            text.color[3]=0.5f;
            
            sprintf(text_c, "P%d: evilness=%d", i+1, LD25World::players[i].getEvilness());
            text_string = text_c;
            text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-22, text_string, txtsize*1.2f, (float)width, (float)height);
            sprintf(text_c, "%09d point%s", LD25World::players[i].points, LD25World::players[i].points==1?"":"s");
            text_string = text_c;
            text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-42, text_string, txtsize*1.2f, (float)width, (float)height);
#if 0
            LD25Player::PlayerCollider* c = dynamic_cast<LD25Player::PlayerCollider*>(LD25World::players[i].collider);
            sprintf(text_c, "Player: collided=[UP:%u DOWN:%u LEFT:%u RIGHT:%u]",
                                            c->collided(LD25Player::PlayerCollider::UP), 
                                            c->collided(LD25Player::PlayerCollider::DOWN),
                                            c->collided(LD25Player::PlayerCollider::LEFT),
                                            c->collided(LD25Player::PlayerCollider::RIGHT));
            text_string = text_c;
            text.print(16,/*window.getHeight()*/height-92, text_string, txtsize, (float)width, (float)height);
#endif
            if (LD25World::players[i].isStunned())
            {
                sprintf(text_c, "Stunned!");
                text_string = text_c;
                text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-64, text_string, txtsize, (float)width, (float)height);
            } else
            if (LD25World::players[i].isActing())
            {
                LD25Player* target = LD25World::players[i].getTarget();
                uint8_t action = LD25World::players[i].getAction();
                sprintf(text_c, "%s!", LD25World::players[i].actionToString(action));
                text_string = text_c;
                text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-64, text_string, txtsize, (float)width, (float)height);
                if (action==LD25Player::NUMACTIONTYPES)
                {
                    sprintf(text_c, "Player %d", (i+1)%2+1);
                    text_string = text_c;
                    text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-84, text_string, txtsize, (float)width, (float)height);
                } else
                {
                    sprintf(text_c, "Target=%s", (target==&LD25World::players[i]?" yourself :(":dynamic_cast<LD25MOB*>(target)->getTypeName()));
                    text_string = text_c;
                    text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-84, text_string, txtsize, (float)width, (float)height);
                }
//                if (target==NULL)
//                {
//                    target = &(LD25World::players[i]);
//                }
                /**
                 * Draw red dot on target acted
                 */
                glPushAttrib(GL_ENABLE_BIT);
                glPushAttrib(GL_POLYGON_BIT);
                glPushAttrib(GL_CURRENT_BIT);
                    glPolygonOffset(1.0f,5.0f);
                    glPointSize(10);
                    glColor3fv(OGLColors::red);
                    glBegin(GL_POINTS);
                        glVertex2f(target->getPos().getX(),target->getPos().getY());
                    glEnd();
                glPopAttrib();
                glPopAttrib();
                glPopAttrib();
            } else
            {
                LD25MOB* targetmob=NULL;
                LD25Player* targetpl=NULL;
                uint8_t action = LD25World::players[i].getClosestAction(targetmob, targetpl);
                
                sprintf(text_c, "%s?", LD25World::players[i].actionToString(action));
                text_string = text_c;
                text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-64, text_string, txtsize, (float)width, (float)height);
                sprintf(text_c, "Target=%s", (targetmob==NULL?" yourself":targetmob->getTypeName()));
                text_string = text_c;
                text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-84, text_string, txtsize, (float)width, (float)height);
                
                /**
                 * Draw lines and dots to targets and
                 */
                if (targetpl!=NULL)
                {
                    sprintf(text_c, "Stun player %d?", (i+1)%2+1);
                    text_string = text_c;
                    text.print(16+i*/*window.getWidth()*/width/2,/*window.getHeight()*/height-104, text_string, txtsize, (float)width, (float)height);
                    
                    glPushAttrib(GL_ENABLE_BIT);
                    glPushAttrib(GL_POLYGON_BIT);
                    glPushAttrib(GL_CURRENT_BIT);
                    glPushAttrib(GL_LINE_BIT);
                        glPolygonOffset(1.0f,5.0f);
//                        glDisable(GL_DEPTH_TEST);
//                        glPointSize(10);
                        glLineWidth(2);
                        glEnable(GL_LINE_STIPPLE);
                        glLineStipple(1,0xAAAA);
//                        glColor3fv(tc);
//                        glBegin(GL_POINTS);
//                            glVertex2f(targetpl->getPos().getX(),targetpl->getPos().getY());
//                        glEnd();
                        glColor4fv(tc);
                        glBegin(GL_LINES);
                            glVertex2f(LD25World::players[i].getPos().getX(),LD25World::players[i].getPos().getY());
                            glVertex2f(targetpl->getPos().getX(),targetpl->getPos().getY());
                        glEnd();
                    glPopAttrib();
                    glPopAttrib();
                    glPopAttrib();
                    glPopAttrib();
                }
                if (targetmob!=NULL)
                {
                    glPushAttrib(GL_ENABLE_BIT);
                    glPushAttrib(GL_POLYGON_BIT);
                    glPushAttrib(GL_CURRENT_BIT);
                    glPushAttrib(GL_LINE_BIT);
                        glPolygonOffset(1.0f,4.0f);
//                        glDisable(GL_DEPTH_TEST);
//                        glPointSize(10);
                        glLineWidth(2);
                        glEnable(GL_LINE_STIPPLE);
                        glLineStipple(1,0xAAAA);
//                        glColor3fv(tc);
//                        glBegin(GL_POINTS);
//                            glVertex2f(targetmob->getPos().getX(),targetmob->getPos().getY());
//                        glEnd();
                        glColor4fv(tc);
                        glBegin(GL_LINES);
                            glVertex2f(LD25World::players[i].getPos().getX(),LD25World::players[i].getPos().getY());
                            glVertex2f(targetmob->getPos().getX(),targetmob->getPos().getY());
                        glEnd();
                    glPopAttrib();
                    glPopAttrib();
                    glPopAttrib();
                    glPopAttrib();
                }
            }
        }
        
        /**
         * Hero Timer
         */
        text.color = OGLColors::gold;
        text.color[3] = 0.3f;
        sprintf(text_c, "Hero Coming in");
        text_string = text_c;
        long int t = LD25World::hero->time_until_come_ms;
        text.print(/*window.getWidth()*/width/2-(txtsize*text.fontSize*(float)strlen(text_c))/2.0f,/*window.getHeight()*/height-120, text_string, txtsize, (float)width, (float)height);
        sprintf(text_c, "%02ld:%02ld:%02ld", t/60000, (t%60000)/1000, (t%1000)/10);
        text_string = text_c;
        text.print((/*window.getWidth()*/width-txtsize*text.fontSize*(float)strlen(text_c))/2.0f,/*window.getHeight()*/height-140, text_string, txtsize, (float)width, (float)height);
                
        
    }

    SDL_GL_SwapBuffers();
}
void Loopable2DLD25::init(void)
{
    if ( SDL_Init(SDL_INIT_EVERYTHING) !=0 )
    {
        exit(1);
    }
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
	width=800;
	height=600;
    window.initGL(width,height,32,SDL_RESIZABLE);
    window.setTitle(title);
    window.setResizeCallbackHolder(this);
    myGlewInit();
    
//    red[0] = red[3] = 1;
//    red[1] = red[2] = 0;
//    blue[0] = blue[1] = 0;
//    blue[2] = blue[3] = 1;
//    green[0] = green[2] = 0;
//    green[1] = green[3] = 1;
//    white[0] = white[1] = white[2] = white[3] = 1;
//    yellow[0] = yellow[1] = yellow[3] = 1;
//    yellow[2] = 0;
    
    camera.setBaseWidthHeight(80,60);
    camera_velocity = 0.01f;
    camera_velocity_multiplier = 2.0f;
    camera_scale_factor = 0.001f;

    //if (!text.loadFontTexture("./resources/fonts/fontHand16_debug.png"))
    if (!text.loadFontTexture("./resources/fonts/LiberationMono16.png"))
    {
        cout<<"font non caricato!"<<std::endl;
    } else
    {
        text.BuildFont();
        cout<<"font texID = "<<text.font->texId<<" isTexture: "<<(glIsTexture(text.font->texId)?"yes":"no")<<" type "<<(text.font->type==GL_RGB?"GL_RGB":"GL_RGBA")<<endl;
    }

    draw_axes = false;
    draw_interface = true;
    
    GameManager2D::renderBounders = false;
    GameManager2D::activeCamera = &camera;
    
    /**
     * Game borders instantiation
     */
    float thickness = 10.0f, ht = thickness/2.0f, hw = LD25World::width/2.0f, hh = LD25World::height/2.0f;
    LD25World::up_bounder.setBounder(new BoundingRectangle(Vec2(.0f,hh+ht), LD25World::width*2.0f, thickness));
    LD25World::down_bounder.setBounder(new BoundingRectangle(Vec2(.0f,-hh-ht), LD25World::width*2.0f, thickness));
    LD25World::left_bounder.setBounder(new BoundingRectangle(Vec2(-hw-ht,.0f), thickness, LD25World::height*2.0f));
    LD25World::right_bounder.setBounder(new BoundingRectangle(Vec2(hw+ht,.0f), thickness, LD25World::height*2.0f));
    assert(GameManager2D::registerGameObject(&LD25World::up_bounder));
    assert(GameManager2D::registerGameObject(&LD25World::down_bounder));
    assert(GameManager2D::registerGameObject(&LD25World::left_bounder));
    assert(GameManager2D::registerGameObject(&LD25World::right_bounder));

    /**
     * Player instanciation
     **/
    for (int i=0 ; i<LD25World::num_players ; i++)
    {
        LD25World::players[i].initActionArea();
        GameManager2D::registerGameObject(&LD25World::players[i]);
        GameManager2D::registerGameObject(LD25World::players[i].action_area);
        LD25World::players[i].setPos(LD25World::startingPos[i]);
    }
    
    /**
     * Hero instanciation
     */
    LD25World::hero = new LD25Hero();
    GameManager2D::registerGameObject(LD25World::hero);
    
    LD25World::mobs_count = 5*LD25MOB::NUMTYPES;
    LD25World::mobs = new LD25MOB*[LD25World::mobs_count];
    
    float hwm = LD25World::width*0.45;
    float hhm = LD25World::height*0.45;
    srand(time(NULL));
    for (int i=0 ; i<LD25World::mobs_count ; i++)
    {
        float x = (rand()/(float)RAND_MAX) * (LD25World::width*0.9f) -hwm;
        float y = (rand()/(float)RAND_MAX) * (LD25World::height*0.9f) -hhm;
        uint8_t type;
        switch (i%(2*LD25MOB::NUMTYPES-1))
        {
            case 0:
            case 1:
                type = LD25MOB::LOLLYPOPCHILDM;
                break;
            case 2:
            case 3:
                type = LD25MOB::LOLLYPOPCHILDF;
                break;
            case 4:
            case 5:
                type = LD25MOB::MAN;
                break;
            case 6:
            case 7:
                type = LD25MOB::WOMAN;
                break;
            case 8:
                type = LD25MOB::GRANNY;
                break;
            default:
                assert(false);
                break;
        }
        LD25World::mobs[i] = new LD25MOB(Vec2(x,y), type);
        GameManager2D::registerGameObject(LD25World::mobs[i]);
    }
    
    glClearColor(0,0,0,1);

    eventHandler = new TestEvent();
    dynamic_cast<TestEvent*>(eventHandler)->holder = this;
    running = true;
}
void Loopable2DLD25::cleanUp(void)
{
    SDL_Quit();
}
bool Loopable2DLD25::isLoopable(void) /// if false this won't loop any more
{
    return running;
}

void Loopable2DLD25::TestEvent::onExit()
{
     holder->running=false;
     cout<<"esceee!"<<endl;
}
void Loopable2DLD25::TestEvent::onResize(int w, int h)
{
         cout<<"ridimensionamento!"<<endl;
}
void Loopable2DLD25::TestEvent::onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
}
void Loopable2DLD25::TestEvent::onMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle)
{
}
void Loopable2DLD25::TestEvent::onLButtonDown(int mX, int mY)
{
}
void Loopable2DLD25::TestEvent::onRButtonDown(int mX, int mY)
{
}
