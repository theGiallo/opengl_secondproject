/* 
 * File:   OctTree.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 26 settembre 2011, 16.28
 */

#ifndef OCTTREE_H
#define	OCTTREE_H
//#include "../generalinclude.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include <climits>
#include <SDL/SDL.h>
#include "../maths/Vec3.h"
#include "../graphics/Texture.h"
#include "../graphics/BoundingBox.h"    
#ifdef DEBUG
#include "../tools/TimeTool.h"
#endif

#ifndef SQRT3_F
#define SQRT3_F 1.7320508075688772935274463415059f
#endif

#ifndef uint
typedef unsigned int uint;
#endif

#define VARRAY_MAX_FACES 1000000
#define FACE_SIZE 32//20

typedef Uint8 RemoveRetType;
#define NOT_REMOVED 0
#define REMOVED 1
#define REMOVE_ME 2

typedef Uint8 OrientationType;
#define MX 0
#define PX 1
#define MY 2
#define PY 3
#define MZ 4
#define PZ 5
#define OPPOSITE_ORIENTATION(a) (a%2==0?(a)+1:(a)-1)

/**
 * Leaf bit structure
 *               42        36           12 8 4 0
 * |  1   |3----2|    6    | 4|4|4|4|4|4 |4|4|4|
 * |isAtom|21free|neighbour|6 faces light|R G B|
 * 
 * New version to work with 32bit architectures
 *               42            18       12 8 4 0
 * |  1   |3----2| 4|4|4|4|4|4 |    6    |4|4|4|
 * |isAtom|21free|6 faces light|neighbour|R G B|
 */
#define NFB 12
#define IS_THERE_NEIGHBOUR(p,n) ((unsigned long int)(p)&((unsigned long int)0x1<<((n)+NFB)))
#define SET_NEIGHBOUR(p,n,b) ((p)=(OctTree*)((b)?(unsigned long int)(p)&~((unsigned long int)0x1<<((n)+NFB)):(unsigned long int)(p)|((unsigned long int)0x1<<((n)+NFB))))

#define GET_COLOR_R(p) (((unsigned long int)(p)&((unsigned long int)0xF00))>>8)
#define GET_COLOR_G(p) (((unsigned long int)(p)&((unsigned long int)0xF0))>>4)
#define GET_COLOR_B(p) ((unsigned long int)(p)&((unsigned long int)0xF))
#define SET_COLOR_R(p,r) ((p)=(OctTree*)((unsigned long int)(p)&~0xF00)|0x((unsigned long int)r)<<8)
#define SET_COLOR_G(p,g) ((p)=(OctTree*)((unsigned long int)(p)&~0xF00)|0x((unsigned long int)g)<<4)
#define SET_COLOR_B(p,b) ((p)=(OctTree*)((unsigned long int)(p)&~0xF00)|0x(unsigned long int)b)

//#define LFB 18
//#define GET_LIGHT(p,id) ((unsigned long int)(p)&((unsigned long int)0xF<<(LFB+4*id))
//#define SET_LIGHT(p,id,l) ((p)|=((unsigned long int)l<<(LFB+4*id))

#define IS_ATOM(p) ((unsigned long int)(p)&((unsigned long int)0x1<<(sizeof(OctTreeNode*)*CHAR_BIT-1)))
#define SET_ATOM(p) ((p)=(OctTree*)((unsigned long int)0x1<<(sizeof(OctTreeNode*)*CHAR_BIT-1)))
#define IS_CHILD_ABSENT(a) ((a)==NULL)
#define IS_OUTSIDE(myC, C, hS) ((C).x<((myC).x-(int)(hS)) || (C).x>((myC).x+(int)(hS)) || \
                                (C).y<((myC).y-(int)(hS)) || (C).y>((myC).y+(int)(hS)) || \
                                (C).z<((myC).z-(int)(hS)) || (C).z>((myC).z+(int)(hS)) )
#define IS_THIS(mC,C) ((mC)==(C))
//#define CAN_BE_RENDERED_AS_CUBE(ot, s) ((ot)->getContainedAtomsCount()>pow((2.0f*s)/atom_side,3)/2.0)
#define ID_X(id) ((id)&0x1)
#define ID_Y(id) ((id)&0x2)
#define ID_Z(id) ((id)&0x4)
#define SET_ID_X(id,v) ((id)= v?(id)|0x1:(id)&0xFE)
#define SET_ID_Y(id,v) ((id)= v?(id)|0x2:(id)&0xFD)
#define SET_ID_Z(id,v) ((id)= v?(id)|0x4:(id)&0xFB)
#define CALC_CHILD_CENTER(cC,mC,chS,id) (cC).x = ID_X(id)?mC.x+chS:mC.x-chS;\
                                     (cC).y = ID_Y(id)?mC.y+chS:mC.y-chS;\
                                     (cC).z = ID_Z(id)?mC.z+chS:mC.z-chS
#define CALC_ID(id,mC,C) if (C.x>mC.x)\
                        {\
                            SET_ID_X(id,1);\
                        }\
                        if (C.y>mC.y)\
                        {\
                            SET_ID_Y(id,1);\
                        }\
                        if (C.z>mC.z)\
                        {\
                            SET_ID_Z(id,1);\
                        }
#define IMPORTANCE(side,center) (((GameManager::activeCamera->near*((float)(side))*SQRT3_F)/(((Vec3)(center))-GameManager::activeCamera->pos).getModule())/GameManager::activeCamera->width)

class OctTreeNode;
typedef OctTreeNode OctTree;
typedef OctTreeNode* OctTreeLeaf;

class OctTreeNode
{
public:
    const static float importance_limit;
    const static uint atom_side;

    OctTreeNode* childs[8];
    OctTreeNode();
    void render(Vec3_int my_center, uint my_half_side);
    void renderCube(OctTreeNode* node, Vec3_int my_center, uint my_half_side);
    OctTreeNode** voxel(Vec3_int my_center, uint my_half_side, Vec3_int center, bool* isOutside);
    RemoveRetType remove(Vec3_int my_center, uint my_half_side, Vec3_int center);
    bool add(Vec3_int my_center, uint my_half_side, Vec3_int center);
    OctTreeLeaf* select(Vec3 start, Vec3 dir, float* dist, Vec3* inter, Vec3_int* center, Vec3_int my_center, uint my_half_side);
    unsigned long int getContainedAtomsCount();
    Vec3_int getNeighbourCenter(Vec3_int my_center, uint my_half_side, OrientationType o);
    
    static bool render_structure;
    /**
     * face number * number of faces in the array
     * */
    static GLfloat vArray[20*VARRAY_MAX_FACES];
    /**
     * number of faces in the array
     * */
    static unsigned long n_in_vArray;
    static void render_array();
    static Texture* tex_atom_face;
    static BoundingBox bounding_box;
    static OctTreeNode* root;
    static Vec3_int root_center;
    static uint root_half_side;
    static Uint8 root_level;
    static RemoveRetType remove_from_root(Vec3_int center);
    static bool add_to_root(Vec3_int center);
    static void render_root();
    static OctTreeLeaf* select_from_root(Vec3 start, Vec3 dir, float* dist, Vec3* inter, Vec3_int* center);
    static bool compareIntersections(std::pair<float, Uint8> a, std::pair<float, Uint8> b);
    static unsigned long int active_faces;
    
#ifdef DEBUG
    static TimeTool render_array_tt,
                    render_tt,
                    render_single_tt,
                    casual_tt;
    static Uint32 render_calls;
#endif
};
#endif	/* OCTTREE_H */
