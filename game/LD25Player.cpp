#include "LD25Player.h"
#include "LD25World.h"
#include "../graphics/OGLColors.h"
#include "LD25MOB.h"
#include <iostream>
#include <float.h>
#include <cassert>

char* LD25Player::action_strings[] = { "Throw a granny",
                                        "Steal a lollypop",
                                        "Make bad noises",

                                        "Stun rival",
                                            };
const int LD25Player::evil_bonus[] = {500, // THROW_GRANNY
                                      200, // STEALLOLLYPOP
                                      100, // FARTORBURP

                                      -100 // NUMACTIONTYPES (stun rival)
                                      };
 
const float LD25Player::action_area_radius = 2.0f;
const float LD25Player::player_side = 2.0f;

LD25Player::LD25Player(uint8_t num_player)
{
    // 
    active = check_collisions = intersectable = visible = true;
    //
    bounce_on_world_bounders = false;
    
    
    evilness = 0;
    points = 0;
    velocity_module = 10.0f;
    this->bounder = new BoundingCircle(player_side/2.0f); // new BoundingRectangle(pos, player_side, player_side);
    this->collider = new PlayerCollider(*this);
    
    action_area = NULL;
    _act = _stunning = acting = false;
    time_to_act=0;
    
    switch (num_player)
    {
        case 0:
            this->setColor(OGLColors::red);
            break;
        case 1:
            this->setColor(OGLColors::green);
            break;
        default:
            assert(false);
            break;
    }
    color[3] = 0.3;
}
LD25Player::~LD25Player(void)
{
    delete action_area;
}

bool LD25Player::act(bool stun_pl)
{
    if (!acting && !_act && !stunned)
    {
        _act=true;
        _stunning = stun_pl;
        return true;
    }
    return false;
}
bool LD25Player::isActing(void)
{
    return acting;
}
bool LD25Player::isStunning(void)
{
    return _stunning;
}
bool LD25Player::isStunned(void)
{
    return stunned;
}
void LD25Player::beStunned(uint32_t time_to_be_stunned)
{
    stunned = true;
    _act = acting = false;
    target = NULL;
    this->time_to_be_stunned = time_to_be_stunned;
    acc = vel = Vec2(.0f,.0f);
    rot_acc = .0f;
}
LD25Player* LD25Player::getTarget(void)
{
    return target;
}
uint8_t LD25Player::getAction(void)
{
    return action_carrying_out;
}
char* LD25Player::actionToString(uint8_t action)
{
    return action_strings[action];
}

uint8_t LD25Player::getClosestAction(LD25MOB*& targetmob, LD25Player*& targetpl)
{
    float mind_mob = FLT_MAX;
    float mind_pl = FLT_MAX;
    action_area->calcDistances();
    Renderable2D** ts = action_area->getInSightThings();
    LD25MOB *mob=NULL; targetmob=NULL;
    LD25Player *pl=NULL; targetpl=NULL;
    for (int i=0 ; i<action_area->getCountThingsSeen() ; i++)
    {
        mob = dynamic_cast<LD25MOB*>(ts[i]);
        pl = dynamic_cast<LD25Player*>(ts[i]);
        if (ts[i]!=&*this &&
            mob!=NULL &&
            !mob->acted &&
            !mob->throwed &&
            (
                ((mob->getType()==LD25MOB::LOLLYPOPCHILDM || mob->getType()==LD25MOB::LOLLYPOPCHILDF) && mob->has_lollypop) ||
                (mob->getType()!=LD25MOB::LOLLYPOPCHILDM && mob->getType()!=LD25MOB::LOLLYPOPCHILDF)
            ) &&
            action_area->distances[i]<mind_mob
           )
        {
            mind_mob = action_area->distances[i];
            targetmob = mob;
        } else
        if (ts[i]!=&*this && mob==NULL && pl!=NULL && !pl->stunned && action_area->distances[i]<mind_pl)
        {
            mind_pl = action_area->distances[i];
            targetpl = pl;
        }
    }
    if (targetmob!=NULL)
    {
        switch (targetmob->getType())
        {
            case LD25MOB::GRANNY:
                return THROW_GRANNY;
                break;
            case LD25MOB::LOLLYPOPCHILDM:
            case LD25MOB::LOLLYPOPCHILDF:
                return STEALLOLLYPOP;
                break;
            case LD25MOB::MAN:
            case LD25MOB::WOMAN:
                // TODO check if loving
                return FARTORBURP;
                break;
        }
    }
    return FARTORBURP;
}

void LD25Player::initActionArea(void)
{
    if (action_area!=NULL)
    {
        return;
    }
    action_area = new LD25Sight(action_area_radius, &*this);
}

bool LD25Player::canMove(void) const
{
    // TODO implement check that is not acting
    return !acting && !stunned;
}


/**
 * @override from PositionableObject2D
 */
void LD25Player::setPos(const Vec2& pos)
{
    PositionableObject2D::setPos(pos);
    if (action_area)
    {
        action_area->setPos(pos);
    }
}
void LD25Player::setUp(const Vec2& up)
{
    PositionableObject2D::setUp(up);
    if (action_area)
    {
        action_area->setUp(up);
    }
}
void LD25Player::setScale(const Vec2& scale)
{
    PositionableObject2D::setScale(scale);
    if (action_area)
    {
        action_area->setScale(scale);
    }
}

/**
 * @override from Renderable2D
 */
void LD25Player::render(void)
{
    // TODO implement rectangle2D + texture
        float xmin,xmax,ymin,ymax;
        float hside = player_side/2.0f;
        xmin = getPos().getX()-hside;
        xmax = getPos().getX()+hside;
        ymin = getPos().getY()-hside;
        ymax = getPos().getY()+hside;
        
        glPushAttrib(GL_ENABLE_BIT);
        glPushAttrib(GL_POLYGON_BIT);
        glPushAttrib(GL_CURRENT_BIT);
//        glDisable(GL_DEPTH_TEST);
//        glDisable(GL_BLEND);
            glEnable(GL_BLEND);
            glPolygonMode(GL_FRONT, GL_FILL);
            glPolygonMode(GL_BACK, GL_NONE);
            glColor4fv(this->color);
    //            glEdgeFlag(GL_FALSE);
                glBegin(GL_TRIANGLE_STRIP);
                    glVertex2f(xmin,ymin);
                    glVertex2f(xmin,ymax);
                    glVertex2f(xmax,ymin);
                    glVertex2f(xmax, ymax);
                glEnd();
                glEdgeFlag(GL_TRUE);
        glPopAttrib();
        glPopAttrib();
        glPopAttrib();
}
void LD25Player::update(Uint32 elapsedTime)
{
    /**
     * Correct collision with world bounders
     */
    bool not_limited = true;
    PlayerCollider* collider = dynamic_cast<PlayerCollider*>(this->collider);
    uint8_t up,down,left,right;
    up = collider->collided(PlayerCollider::UP);
    down = collider->collided(PlayerCollider::DOWN);
    left = collider->collided(PlayerCollider::LEFT);
    right = collider->collided(PlayerCollider::RIGHT);
    
    if (((up==COLLISION_STAY||up==COLLISION_ENTER)&&vel.getY()>0.0f) ||
        ((down==COLLISION_STAY||down==COLLISION_ENTER)&&vel.getY()<0.0f) ||
        (up==COLLISION_EXIT&&vel.getY()>0.0f) ||
        (down==COLLISION_EXIT&&vel.getY()<0.0f)
        )
    {
        float ts = elapsedTime/1000.0f;
        setPos(pos+Vec2(.0f,vel.getY()*-1.0f)*ts);
        vel = Vec2(vel.getX(),bounce_on_world_bounders?-(vel.getY()):.0f);
        not_limited = false;
    }
    if (((left==COLLISION_STAY||left==COLLISION_ENTER)&&vel.getX()<0.0f) ||
        ((right==COLLISION_STAY||right==COLLISION_ENTER)&&vel.getX()>0.0f) ||
        (left==COLLISION_EXIT&&vel.getX()<0.0f) ||
        (right==COLLISION_EXIT&&vel.getX()>0.0f))
    {
        float ts = elapsedTime/1000.0f;
        setPos(pos+Vec2(vel.getX()*-1.0f,.0f)*ts);
        vel = Vec2(bounce_on_world_bounders?-(vel.getX()):.0f,vel.getY());
        not_limited = false;
    }
    
    if (not_limited)
    {
        vel = desired_vel;
    }
    
    if (stunned)
    {
        if (time_to_be_stunned<elapsedTime)
        {
            stunned =false;
        } else
        {
            time_to_be_stunned -= elapsedTime;
        }
        return;
    }
    
    /**
     * carry out action
     */
    if (acting)
    {
        LD25MOB* targetmob = dynamic_cast<LD25MOB*>(this->target);
        if (time_to_act<elapsedTime)
        {
            /**
             * action completed
             */
            evilness += evil_bonus[action_carrying_out];
            acting = false;
            _stunning = false;
            if (targetmob!=NULL)
            {
                if (action_carrying_out == STEALLOLLYPOP)
                {
                    targetmob->has_lollypop = false;
                    targetmob->time_until_lollypop = 2000;
                    targetmob->stunned = true;
                    targetmob->time_to_be_stunned = 2000;
                }
                targetmob->acted = false;
                targetmob->throwed=false;
            }
            this->target = NULL;
        } else
        {
            if (action_carrying_out==THROW_GRANNY && time_to_act < 1500 && !targetmob->throwed)
            {
                /**
                 * throwing
                 */
                targetmob->throwed=true;
                targetmob->setVel((target->getPos()-pos).getNormalized()*10.f);
                targetmob->setAcc(Vec2(.0f,.0f));
                targetmob->setRot(0);
                targetmob->setRotAcc(0);
            }
            if (action_carrying_out==THROW_GRANNY && time_to_act < 500 && targetmob->acted)
            {
                /**
                 * granny is landed, and I laught
                 */
                targetmob->stunned = true;
                targetmob->time_to_be_stunned = 3000;
                targetmob->setVel(Vec2(.0f,.0f));
                targetmob->acted = false;
            }
            if (action_carrying_out==FARTORBURP && time_to_act<500 && !target->stunned)
            {
                target->beStunned(700);
            }
            
            time_to_act-=elapsedTime;
        }
        return;
    }
    /**
     * begin action
     */
    if (_act)
    {
        LD25MOB* targetmob;
        LD25Player* targetpl;
        uint8_t action = getClosestAction(targetmob, targetpl);
        /**
         * stun rival
         */
        if (_stunning)
        {
            if (targetpl==NULL)
            {
                _act = _stunning = false;
                return;
            }
            action_carrying_out = NUMACTIONTYPES;
            time_to_act = 250;
            this->target = targetpl;
            targetpl->beStunned(700);
        } else
        {
            if (targetmob!=NULL)
            {
                this->target = targetmob;
            } else
            {
                /**
                 * fart on yourself :(
                 */
                this->target = &*this;
            }
            /**
             * act on somebody or yourself
             */
            switch(action)
            {
                case THROW_GRANNY:
                    time_to_act = 2100;
                    targetmob->acted = true;
                    {Vec2 gp(1.0f,.0f);
                    gp.rotate(2.0f*M_PI*(rand()/(float)RAND_MAX));
                    targetmob->setPos(pos+gp);}
                    targetmob->setVel(Vec2(.0f,.0f));
                    targetmob->setAcc(Vec2(.0f,.0f));
                    targetmob->setRot(0);
                    targetmob->setRotAcc(0);
                    break;
                case STEALLOLLYPOP:
                    time_to_act = 1400;
                    targetmob->acted = true;
                    targetmob->setVel(Vec2(.0f,.0f));
                    targetmob->setAcc(Vec2(.0f,.0f));
                    targetmob->setRot(0);
                    targetmob->setRotAcc(0);
                    break;
                case FARTORBURP:
                    time_to_act = 700;
                    break;
                default:
                    assert(false);
                    break;
            }
            action_carrying_out = action;
        }
        acting = true;
        _act = false;
    }
}

LD25Player::PlayerCollider::PlayerCollider(LD25Player& player) : player(player)
{
}

void LD25Player::PlayerCollider::onCollision(uint8_t* states, Renderable2D** renderables, int num)
{
    for (int i=0 ; i<NUMDIRECTIONS ; i++)
    {
        if (_collided[i]==COLLISION_EXIT)
        {
            _collided[i] = COLLISION_NONE;
        }
    }
    if (num==0)
    {
        return;
    }
    for (int i=0 ; i<num ; i++)
    {
        GameObject* go;
        if ((go=dynamic_cast<GameObject*>(renderables[i])))
        {
            if (go->id == LD25World::up_bounder.id)
            {
                _collided[UP] = states[i];
            } else
            if (go->id == LD25World::down_bounder.id)
            {
                _collided[DOWN] = states[i];
            } else
            if (go->id == LD25World::left_bounder.id)
            {
                _collided[LEFT] = states[i];
            } else
            if (go->id == LD25World::right_bounder.id)
            {
                _collided[RIGHT] = states[i];
            }
        }
    }
}
uint8_t LD25Player::PlayerCollider::collided(int direction) const
{
    return _collided[direction];
}