/** 
 * File:   LD25Player.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 15 December 2012
 * 
 */
#ifndef LD25PLAYER_H
#define	LD25PLAYER_H

#include "PhysicalObject2D.h"
#include "../graphics/Renderables2D.h"
#include "../graphics/CollisionSolver2D.h"
#include "../game/GameObject.h"
#include "../game/GameManager2D.h"
#include "../game/LD25Sight.h"
#include <cstdlib>

class LD25MOB;

class LD25Player : public PhysicalObject2D
{
protected:
    float velocity_module;
    
    bool _act;
    bool _stunning;
    bool acting;
    uint32_t time_to_act;
    uint8_t action_carrying_out;
    LD25Player* target;
    bool stunned;
    uint32_t time_to_be_stunned;
public:
    int evilness;
    int points;
    
    static const float player_side;
    
    bool bounce_on_world_bounders;
    
    static const float action_area_radius;
    LD25Sight* action_area; //has to be initialized calling initActionArea
    
    
    
    Vec2 desired_vel;
    
    class PlayerCollider : public Collider2D
    {
    public:
        enum en_direction
        {
            UP,DOWN,LEFT,RIGHT,NUMDIRECTIONS
        };
    protected:
        uint8_t _collided[NUMDIRECTIONS];
    public:
        PlayerCollider(LD25Player& player);
        
        uint8_t collided(int direction) const ;
        
        LD25Player& player;

        /**
         * @override from Collider2D
         */ 
        virtual void onCollision(uint8_t* states, Renderable2D** renderables, int num);
    };
    
    enum en_action_types
    {
        THROW_GRANNY,
        STEALLOLLYPOP,
        FARTORBURP,
        NUMACTIONTYPES
    };
    static char* action_strings[];
    static const int evil_bonus[];
    
    LD25Player(uint8_t num_player=0);
    ~LD25Player(void);
    
    void initActionArea(void);
    
    uint8_t getClosestAction(LD25MOB*& targetmob, LD25Player*& targetpl);
    
    const int& getEvilness(void) const
    {
        return evilness;
    }
    void setEvilness(int evilness)
    {
        this->evilness = evilness;
    }
    const float& getVelocityModule(void) const
    {
        return velocity_module;
    }
    void setVelocityModule(const float& velocity_module)
    {
        this->velocity_module = velocity_module;
    }
    
    bool canMove(void) const;
    bool act(bool stun_pl=false);
    bool isActing(void);
    bool isStunning(void);
    bool isStunned(void);
    void beStunned(uint32_t time_to_be_stunned);
    LD25Player* getTarget(void);
    uint8_t getAction(void);
    
    char* actionToString(uint8_t action);
    
    /**
     * @override from PhysicalObject2D
     */ 
    virtual void updatePhysics(Uint32 elapsedTime){PhysicalObject2D::updatePhysics(elapsedTime);};
    
    /**
     * @override from PositionableObject2D
     */
    virtual void setPos(const Vec2& pos);
    virtual void setUp(const Vec2& up);
    virtual void setScale(const Vec2& scale);
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime){};
    
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection){throw false;};

};

#endif /* LD25PLAYER_H */