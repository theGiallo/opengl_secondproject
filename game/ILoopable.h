#ifndef ILOOPABLE_H
#define ILOOPABLE_H

#include <SDL/SDL.h>

class ILoopable
{
public:
	/**
	 * keys pointer has not to be allocated
	 **/
	virtual const Uint8* getKeysMap_const(void) const =0;
	virtual Uint8* getKeysMap(void) =0;
	virtual void setKeysMap(Uint8* keys) =0;
	
	virtual void onEvent(SDL_Event* event) =0;
	virtual void keyChecks(void) =0; /// checks the keymap
	virtual void onLoop(Uint32 elapsedTime) =0;
	virtual void onRender(void) =0;
	virtual void init(void) =0;
	virtual void cleanUp(void) =0;
	virtual bool isLoopable(void) =0; /// if false this won't loop any more

	virtual ~ILoopable(void){};
};

#endif /* ILOOPABLE_H */
