#include "LivingEntity.h"
#include <cfloat>
#include <ctime>

#include <iostream>

#ifndef ABS
#define ABS(a) ((a)>=0?(a):-(a))
#endif

#ifndef MAX
#define MAX(a,b) ((a)>(b)?(a):(b))
#endif

RegularPolygon2D LivingEntity::livingentity_appearence(SIDES_OF_LE);

float LivingEntity::SpeciesCharacteristics::standard_genes[] = {
                                    10.0f, //max_vel,
                                    4.0f, //acceleration,
                                    10.0f, //sight, // ray of circle sight
                                    5.0f, //min_mass, // if mass is less than this the LE dies
                                    15.0f, //max_mass,
                                    50.0f, //fertility, // percentage; bigger = more offspring (with fertility==1 LE could die)
                                    50.0f, //offspring_vigour, // percentage
                                    1.0f, //reproduction_speed, // mass per second accumulated for the reproduction
                                    50.0f, //reproduction_mass_percentage_trigger, // if mass is this*max_mass the LE starts the reproduction
                                    5.0f, //mutation_min,
                                    20.0f, //mutation_max, // offspring has characteristics mutated of a percentage between min and max i.e. min<=(parent-offspring)/parent<=max
                                    70.0f, //own_kind_tolerance, // percentage of difference under which the LE considers another LE of its kind (and do not eats it) (=percentage difference abs average)
                                    30.0f //reflexes, // times per minute; frequency of ambient scan (max 3600 = 60/s)
                                };
LivingEntity::SpeciesCharacteristics LivingEntity::SpeciesCharacteristics::giveMutatedFromStandard(void)
{
    SpeciesCharacteristics sc;
    memcpy(&sc.genes[0], &standard_genes[0], sizeof(float)*GENES_COUNT);
    sc.mutate();
    return sc;
}
LivingEntity::SpeciesCharacteristics LivingEntity::SpeciesCharacteristics::getMutated(void) const
{
    SpeciesCharacteristics res = *this;
    res.mutate();
    return res;
}
float LivingEntity::SpeciesCharacteristics::getAMutationPercentage()
{
    srand(LivingEntity::random_seed++);
    float r = rand();
    r/=RAND_MAX;
    float span = (mutation_max-mutation_min);
    return 1.0f-span+2.0f*span*r;
}
void LivingEntity::SpeciesCharacteristics::mutate(void)
{
    for (int i=0 ; i<GENES_COUNT ; i++)
    {
        genes[i]*=getAMutationPercentage();
    }
}

LivingEntity::StoringCollider::StoringCollider()
{
    count_collided_things = 0;
}
void LivingEntity::StoringCollider::onCollision(uint8_t* states, Renderable2D** renderables, int num)
{
    for (int i=0 ; i<num ; i++)
    {
        collided_things[i] = renderables[i];
        collision_states[i] = states[i];
    }
    count_collided_things = num;
}
int LivingEntity::StoringCollider::getCountCollidedThings(void)
{
    return count_collided_things;
}

LivingEntity::Sight::Sight(float sight, LivingEntity* owner_living_entity)
{
    // 
    active = check_collisions = intersectable = visible = true;
    //
    collider = new StoringCollider();
    /*RegularPolygonBounder* b = new RegularPolygonBounder(calcSides(sight));
    b->setScale(Vec2(sight,sight));
    this->bounder = b;*/
    BoundingCircle* bc = new BoundingCircle(sight);
    this->bounder = bc;
    this->owner_living_entity = owner_living_entity;
    color[0] = color[1] = color[2] = 0.3;
    color[3] = 0.1;
}
void LivingEntity::Sight::calcDistances()
{
    StoringCollider* sc = reinterpret_cast<StoringCollider*>(collider);
    int count = sc->getCountCollidedThings();
    for (int i=0 ; i<count ; i++)
    {
        CollisionSolver2D::collide(*owner_living_entity->bounder, *sc->collided_things[i]->bounder, distances[i]);
    }
}
int LivingEntity::Sight::getCountThingsSeen(void)
{
    return reinterpret_cast<StoringCollider*>(collider)->getCountCollidedThings();
}
void LivingEntity::Sight::render(void)
{
    float radius = reinterpret_cast<BoundingCircle*>(bounder)->getRadius_const();
    livingentity_appearence.setColor(color);
    LivingEntity::livingentity_appearence.Tline_Ffill = true;
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslatef(pos.x,pos.y,0);
        glScalef(scale.x*radius, scale.y*radius, 1);
        livingentity_appearence.render();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
float LivingEntity::Sight::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    return FLT_MAX;
}

unsigned int LivingEntity::random_seed = time(NULL);
int LivingEntity::calcSides(float radius)
{
    return MAX(3,radius*EDGES_PER_RADIUS);
}
float LivingEntity::calcRadius(float mass)
{
    return sqrt(mass/MASS_DENSITY/M_PI);
}
    
LivingEntity::LivingEntity(const SpeciesCharacteristics& genes, float mass)
{
    // 
    active = check_collisions = intersectable = visible = true;
    //
    
    color[0] = 0.8f;
    color[1] = 0.1f;
    color[2] = 0.2f;
    color[3] = 0.8f;
    
    this->genes = genes;
    this->mass = this->new_mass = mass;
    this->sight_bounder = new LivingEntity::Sight(genes.genes[SpeciesCharacteristics::sight], &*this);
    this->scan_period = 1000.0f/genes.reflexes*60.0f;
    this->reproduction_mass_trigger = genes.genes[SpeciesCharacteristics::max_vel]*genes.genes[SpeciesCharacteristics::reproduction_mass_percentage_trigger];
    this->reproduction_current_mass = 0.0f;
    this->reproduction_target_mass = 0.0f;
    this->reproducting = false;
    checkIfIsAlive();
    
    float radius = calcRadius(mass);
    /*RegularPolygonBounder* b = new RegularPolygonBounder(calcSides(radius));
    b->setScale(Vec2(radius,radius));*/
    BoundingCircle* bc = new BoundingCircle(radius);
    this->bounder = bc;
    
    this->collider = new StoringCollider();
    
    GameManager2D::registerGameObject(&*this);
    GameManager2D::registerGameObject(this->sight_bounder);
}
LivingEntity::~LivingEntity(void)
{
    GameManager2D::deleteGameObject(&*this);
    GameManager2D::deleteGameObject(this->sight_bounder);
}
void LivingEntity::checkIfIsAlive(void)
{
    alive = mass>=genes.min_mass;
}
void LivingEntity::generateOffspring(void)
{
    float m;
    int n;
    m = genes.min_mass+(genes.max_mass-genes.min_mass)*genes.offspring_vigour;
    n = mass*genes.fertility/m;
    RegularPolygonBounder rpb(n);
    rpb.setScale(UNITARY_SCALE*calcRadius(mass));
    rpb.setPos(pos);
    for (int i=0 ; i<n ; i++)
    {
        float newmass = mass-m;
        if (newmass>=0)
        {
            mass = newmass;
        } else
        {
            mass = 0;
            m+=newmass;
        }
        LivingEntity* offspring = new LivingEntity(genes.getMutated(),m);
        offspring->pos = Vec2(rpb.getVArray()[i*2],rpb.getVArray()[i*2+1]);
    }
}
void LivingEntity::reproductionUpdate(uint32_t elapsedTime)
{
    if (reproduction_current_mass==reproduction_target_mass)
    {
        generateOffspring();
        reproducting = false;
        return;
    }
    float delta_mass = genes.reproduction_speed*elapsedTime/1000.0f;
    reproduction_current_mass += delta_mass;
    if (reproduction_target_mass>reproduction_current_mass)
    {
        delta_mass-=reproduction_current_mass-reproduction_target_mass;
        reproduction_current_mass = reproduction_target_mass;
    }
    mass -= delta_mass;
}
void LivingEntity::checkReproductivity(void)
{
    if (reproducting)
    {
        return;
    }
    reproducting = mass>=reproduction_mass_trigger;
}

bool LivingEntity::isNotOfMyKind(LivingEntity* le)
{
    float differences = 0;
    for (int i=0 ; i<SpeciesCharacteristics::GENES_COUNT ; i++)
    {
        differences += ABS(le->genes.genes[i]-genes.genes[i])/genes.genes[i];
    }
    return differences/(float)SpeciesCharacteristics::GENES_COUNT < genes.genes[SpeciesCharacteristics::own_kind_tolerance];
}

void LivingEntity::setPos(const Vec2& pos)
{
    PositionableObject2D::setPos(pos);
    sight_bounder->setPos(pos);
}
void LivingEntity::setUp(const Vec2& up)
{
    PositionableObject2D::setUp(up);
}
void LivingEntity::setScale(const Vec2& scale)
{
    PositionableObject2D::setScale(scale);
}

void LivingEntity::updatePhysics(Uint32 elapsedTime)
{
    float ts = elapsedTime/1000.0f;
    vel += acc*ts;
    if (vel.getModule()>genes.genes[SpeciesCharacteristics::max_vel])
    {
        vel.normalize();
        vel*=genes.genes[SpeciesCharacteristics::max_vel];
    }
    setPos(pos+vel*ts);
    rot_vel += rot_acc*ts;
    setRot(rot+rot_vel*ts);
}
void LivingEntity::update(Uint32 elapsedTime)
{
    // eat
    StoringCollider* sc = reinterpret_cast<StoringCollider*>(collider);
    for (int i=0 ; i<sc->getCountCollidedThings() ; i++)
    {
        LivingEntity* le = dynamic_cast<LivingEntity*>(sc->collided_things[i]);
        if (le)
        {
            if (le->mass < this->mass && isNotOfMyKind(le))
            {
                float eaten = MASS_EATEN_PER_SECOND*(float)elapsedTime/1000.0f;
                float nlem = le->new_mass-eaten;
                if (nlem<0)
                {
                    eaten+=nlem;
                    nlem = 0;
                }
                le->new_mass = nlem;
                this->new_mass+= eaten;
            }
        }
    }
}
void LivingEntity::lateUpdate(Uint32 elapsedTime)
{
    mass = new_mass;
    float radius = calcRadius(mass);
    setScale(Vec2(radius,radius));
    if (mass==0)
    {
        //I'm dead
        delete this;
        return;
    }
    if (alive)
    {
        checkIfIsAlive();
    }
    if (vel.getModule()<=0.01)
    {
        //acc = vel = Vec2();
    }
    if (!alive)
    {
        return;
    }
    /*if (alive)
    {
        checkIfIsAlive();
        if (!alive && vel!=Vec2())
        {
            acc=vel.getNormalized()*mass*-FRICTION_COEFFICIENT;
        }
    } else
    {
        if (acc!=Vec2() && (acc.getNormalized()-vel.getNormalized()).getModule()>1.9f)//if acc is accelerating vel
        {
            acc = Vec2();
        }
    }*/
    checkReproductivity();
    if (reproducting)
    {
        reproductionUpdate(elapsedTime);
    }
    elapsed_time_last_scan+=elapsedTime;
    if (elapsed_time_last_scan>=scan_period)
    {
        elapsed_time_last_scan-=scan_period;
        // commented! now we use calcRadius(mass)!! sight_bounder->calcDistances();
        
        // decide where to go
        StoringCollider* sight_sc = reinterpret_cast<StoringCollider*>(sight_bounder->collider);
        int count = sight_sc->getCountCollidedThings();
        Vec2 direction;
        for (int i=0 ; i<count ; i++)
        {
            LivingEntity* le = dynamic_cast<LivingEntity*>(sight_sc->collided_things[i]);
            if (le && isNotOfMyKind(le))
            {
                Vec2 cc = (le->pos-pos);
                direction+=cc/(cc.getModule()-calcRadius(mass)-calcRadius(le->mass))*(mass<le->mass?-1:2);
            }
        }
        if (direction==Vec2())
        {
            direction-=pos;
            //direction.rotate(rand()/(float)RAND_MAX*);
        }
        direction.normalize();
        setAcc(direction*genes.genes[SpeciesCharacteristics::acceleration]);
    }
    Vec2 fr = (vel.getNormalized()*mass*-FRICTION_COEFFICIENT);
    if (fr.getModule()>acc.getModule())
    {
        fr = -acc;
    }
    setAcc(acc+fr);
}

bool LivingEntity::isAlive(void)
{
    return alive;
}
const LivingEntity::SpeciesCharacteristics& LivingEntity::getGenes(SpeciesCharacteristics& genes_out)
{
    genes_out = genes;
    return genes_out;
}

void LivingEntity::render(void)
{
    float radius = reinterpret_cast<BoundingCircle*>(bounder)->getRadius_const();
    livingentity_appearence.setColor(color);
    LivingEntity::livingentity_appearence.Tline_Ffill = false;
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslatef(pos.x,pos.y,0);
        glScalef(scale.x*radius, scale.y*radius, 1);
        livingentity_appearence.render();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
float LivingEntity::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    throw false;
}
