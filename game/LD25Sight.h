/** 
 * File:   LD25Sight.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 15 December 2012
 * 
 */
#ifndef LD25SIGHT_H
#define	LD25SIGHT_H

#include "../game/PhysicalObject2D.h"
#include "../graphics/Renderables2D.h"
#include "../graphics/CollisionSolver2D.h"
#include "../game/GameObject.h"
#include "../game/GameManager2D.h"
#include "../game/StoringCollider.h"
#include "../graphics/Renderables2D.h"

#include <cstdlib>

class LD25Sight : public GameObject, public PositionableObject2D
{
protected:
    PhysicalObject2D* owner;
public:
    LD25Sight(float sight, PhysicalObject2D* owner);
    
    static const int SIDES_OF_CIRCLE;
    static RegularPolygon2D circle;

    float distances[MAX_COLLISIONS_PER_OBJ];
    void calcDistances(void);
    int getCountThingsSeen(void);
    
    Renderable2D** getInSightThings(void);


    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime){}
    virtual void lateUpdate(Uint32 elapsedTime){}
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};

#endif /* LD25SIGHT_H */