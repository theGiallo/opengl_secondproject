/** 
 * File:   Loopable2DLD24.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 26 August 2012
 **/

#ifndef LOOPABLE_2DLD24_H
#define LOOPABLE_2DLD24_H

#include "ILoopable.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL/SDL.h>
#include <list>

#include "../game/GameObject.h"
#include "../IO/Event.h"
#include "../graphics/Window.h"
#include "../graphics/TextureManager.h"
#include "../graphics/Texture.h"
#include "../graphics/Text.h"
#include "../graphics/GLDebug.h"

#include "../game/GameManager2D.h"
#include "../game/PhysicalObject2D.h"
#include "../game/LivingEntity.h"
#include "../graphics/Renderables2D.h"
#include "../graphics/Camera2D.h"
#include "../graphics/RegularPolygon.h"
#include "../graphics/RegularPolygonBounder.h"
#include "../graphics/GJK2D.h"
#define MAX(a,b) (a>b?a:b)
#include "../graphics/IResizableWindowCallbackHolder.h"
#include "../tools/TimeTool.h"

class Loopable2DLD24 : public ILoopable, IWindowResizeHandler
{
protected:
    Text text;
    Camera2D camera;
    float camera_velocity; /// units per ms
    float camera_velocity_multiplier;
    float camera_scale_factor; /// mHz
    float camera_new_zoom_fact;
    bool camera_changed_zoom;
    Vec2 camera_new_pos_dir;
    bool camera_changed_pos;
    int width,height;
    Window window;
    bool running;
    Uint8* keys;
    Event* eventHandler;
    bool draw_axes, draw_interface;
    std::string title;

    /**
     * Time Calc
     **/
    int64_t deltaTimeFPSCalc;
    int rendered_frames_in_last_sec;
    float displayedFPS;
    TimeTool FPS_tt;

    GLfloat red[4];// = {1,0,0,1};
    GLfloat blue[4];// = {0,0,1,1};
    GLfloat green[4];// = {0,1,0,1};
    GLfloat yellow[4];// = {1,1,0,1};
    GLfloat white[4];// = {1,1,1,1};
    
    void myGlewInit();
public:
    /**
     * @override from IWindowResizeHandler
     **/
    void onResize(int w, int h);
    
    /**
     * keys pointer has not to be allocated
     **/
    const Uint8* getKeysMap_const(void) const;
    Uint8* getKeysMap(void);
    void setKeysMap(Uint8* keys);

    void onEvent(SDL_Event* event);
    void keyChecks(void); /// checks the keymap
    void onLoop(Uint32 elapsedTime);
    void onRender(void);
    void init(void);
    void cleanUp(void);
    bool isLoopable(void); /// if false this won't loop any more
    ~Loopable2DLD24(){};
    
    class TestEvent : public Event
    {
    public:
        void onExit();
        void onResize(int w, int h);
        void onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
        void onMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle);
        void onLButtonDown(int mX, int mY);
        void onRButtonDown(int mX, int mY);
        
        Loopable2DLD24* holder;
    };
};

#endif /* LOOPABLE_2DLD24_H */
