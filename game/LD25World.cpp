#include "LD25World.h"
#include "SDL/SDL.h"
#include "LD25Hero.h"


Trigger2D LD25World::up_bounder = Trigger2D();
Trigger2D LD25World::down_bounder = Trigger2D();
Trigger2D LD25World::left_bounder = Trigger2D();
Trigger2D LD25World::right_bounder = Trigger2D();
float LD25World::width=80.0f;
float LD25World::height=60.0f;

LD25Player LD25World::players[2] = { LD25Player(0), LD25Player(1)};
uint8_t LD25World::wasd[2][4] = {{SDLK_w, SDLK_a, SDLK_s, SDLK_d}, 
                                 {SDLK_i, SDLK_j, SDLK_k, SDLK_l}};
uint8_t LD25World::action[2][2] = {{SDLK_q, SDLK_e}, {SDLK_u, SDLK_o}};
//const uint8_t LD25World::num_players = 2;
Vec2 LD25World::startingPos[2] = {Vec2(-20.0f,.0f),Vec2(20.0f,.0f)};

LD25Hero* LD25World::hero = NULL;
LD25MOB** LD25World::mobs = NULL;
uint16_t LD25World::mobs_count = 0;