#include "Loopable2DTest.h"
#include "TestCollider2D.h"
#include "../graphics/RegularPolygonBounder.h"
#include "../graphics/BoundingCircle.h"

using namespace std;

void Loopable2DTest::myGlewInit()
{
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if ( GLEW_OK!=err)
    {
        cout<<"error initializing GLEW: "<<glewGetErrorString(err)<<endl;
    }
    if (glewIsSupported("GL_VERSION_1_3"))
    {
        cout<<"OpenGL 1.3 supported"<<endl;
    }
    if (GLEW_VERSION_1_3)
    {
        cout<<"GLEW 1.3 supported"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_1"))
    {
        cout<<"OpenGL 2.1"<<endl;
    }
    if (glewIsSupported("GL_VERSION_4_0"))
    {
        cout<<"OpenGL 4.0"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_0"))
    {
        printf("Ready for OpenGL 2.0\n");
    } else
    {
        printf("OpenGL 2.0 not supported\n");
        //exit(1);
    }
}
/**
 * @override from IWindowResizeHandler
 **/
void Loopable2DTest::onResize(int w, int h)
{
    if (w==width && h==height)
    {
            return;
    }
    float ar = (float)width/(float)height;
    cout<<"ar: "<<ar<<endl;
    cout<<"width: "<<width<<" w: "<<w<<endl;
    cout<<"height: "<<height<<" h: "<<h<<endl;
    width=MAX(w, 1);
    height=MAX(h, 1);
    float nar = (float)width/(float)height;
#ifdef WIN32
			int real_width = width;
			int real_height = height;
#endif
    if (nar >= ar)
    {
        if (ar<1)
        {
            height = ((float)width)/ar;
        } else
        {
            width = ((float)height)*ar;
        }
    } else
    {
        if (ar>=1)
        {
            height = ((float)width)/ar;
        } else
        {
            width = ((float)height)*ar;
        }
    }
#ifdef WIN32
			int space_w=0, space_h=0;
			if (real_width!=width)
				space_w = (real_width-width)/2;
			if (real_height!=height)
				space_h  = (real_height-height)/2;
			glViewport(space_w, space_h, width, height);
#endif
	char c_title[500];
#ifdef WIN32
	sprintf_s(c_title, "%s %dx%d", "Flatland: un contest al quadrato", width, height);
#else
        sprintf(c_title, "%s %dx%d", "Flatland: un contest al quadrato", width, height);
#endif
    window.setTitle(c_title);
#ifndef WIN32
	window.resize(width, height);
#endif
    //camera->setAspectRatio((float)width/(float)height);
}

/**
 * keys pointer has not to be allocated
 **/
const Uint8* Loopable2DTest::getKeysMap_const(void) const
{
    return keys;
}
Uint8* Loopable2DTest::getKeysMap(void)
{
    return keys;
}
void Loopable2DTest::setKeysMap(Uint8* keys)
{
    this->keys = keys;
}

void Loopable2DTest::onEvent(SDL_Event* event)
{
    eventHandler->onEvent(event);
    switch ( event->type )
    {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_VIDEORESIZE:
            if ( !window.resize(event->resize.w, event->resize.h) )
            {
                cout<<"Oh no!"<<endl;
            }
            break;
    }
}
void Loopable2DTest::keyChecks(void) /// checks the keymap
{
    if ( keys[SDLK_F1] )
    {
            draw_interface = !draw_interface;
            keys[SDLK_F1]=false;
    }
    if ( keys[SDLK_F2] )
    {
            GameManager2D::renderBounders = !GameManager2D::renderBounders;
            keys[SDLK_F2]=false;
    }
    if ( keys[SDLK_F3] )
    {
            GameManager2D::renderNotBounders = !GameManager2D::renderNotBounders;
            keys[SDLK_F3]=false;
    }
    if ( keys[SDLK_F4] )
    {
            draw_axes = !draw_axes;
            keys[SDLK_F4]=false;
    }
    if ( keys[SDLK_a] )
    {
        camera.setPos(camera.getPos()+Vec2(-1,0));
    }
    if ( keys[SDLK_d] )
    {
        camera.setPos(camera.getPos()+Vec2(1,0));
    }
    if ( keys[SDLK_s] )
    {
        camera.setPos(camera.getPos()+Vec2(0,-1));
    }
    if ( keys[SDLK_w] )
    {
        camera.setPos(camera.getPos()+Vec2(0,1));
    }
}
void Loopable2DTest::onLoop(Uint32 elapsedTime)
{
    GameManager2D::update(elapsedTime);
}
void Loopable2DTest::onRender(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);
    glEnable(GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glHint (GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glHint (GL_POINT_SMOOTH_HINT, GL_DONT_CARE);
    //glEnable(GL_POLYGON_SMOOTH); ///it causes the line between the background triangles to be visible
	glEnable (GL_LINE_SMOOTH);
	glEnable (GL_POINT_SMOOTH);
    glDisable(GL_TEXTURE_2D);

    camera.setGLCamera();
#if 1
    float xmin,xmax,ymin,ymax;
    xmin = camera.getPos().getX()-camera.getActualWidth()/2.0f;
    xmax = camera.getPos().getX()+camera.getActualWidth()/2.0f;
    ymin = camera.getPos().getY()-camera.getActualHeight()/2.0f;
    ymax = camera.getPos().getY()+camera.getActualHeight()/2.0f;
    glDisable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_BACK, GL_FILL);
	glColor3f(1,1,1);
        glEdgeFlag(GL_FALSE);
        glBegin(GL_TRIANGLE_STRIP);
            glVertex2f(xmin,ymin);
            glVertex2f(xmin,ymax);
            glVertex2f(xmax,ymin);
            glVertex2f(xmax, ymax);
        glEnd();
        glEdgeFlag(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glPolygonMode(GL_FRONT, GL_LINE);
#endif
    glPointSize(5);
    glColor3fv(blue);
    GameManager2D::render();
    if (draw_axes)
    {
        glDisable(GL_DEPTH_TEST);
        glBegin(GL_LINES);
            glColor3fv(red);
            glVertex2f(-10,0);
            glVertex2f(10,0);
            glColor3fv(green);
            glVertex2f(0,-10);
            glVertex2f(0,10);
        glEnd();
        glBegin(GL_POINTS);
            glColor3fv(red);
            glVertex2f(10,0);
            glColor3fv(green);
            glVertex2f(0,10);
        glEnd();
        glEnable(GL_DEPTH_TEST);
    }
	

    /**
     * Interface
     **/
    FPS_tt.check(MIDDLE_CHECK);
    deltaTimeFPSCalc += FPS_tt.last_delta;
    if ( deltaTimeFPSCalc >= 1000000)
    {
        rendered_frames_in_last_sec++;
        displayedFPS = (float)(rendered_frames_in_last_sec*1000000.0)/deltaTimeFPSCalc;
        rendered_frames_in_last_sec=0;
        deltaTimeFPSCalc = 0;
    } else
    {
        rendered_frames_in_last_sec++;
    }
    if (draw_interface)
    {
        char text_c[200];
        string text_string;

        sprintf(text_c, "FPS:%.3f", displayedFPS);
        text_string = text_c;
        text.print(16,window.getHeight()-32, text_string, 1, (float)width, (float)height);

        sprintf(text_c, "po_2: acc(%.3f,%.3f) vel(%.3f,%.3f) pos(%.3f,%.3f) up(%.3f,%.3f)(module:%.3f)", po_2->getAcc().x, po_2->getAcc().y, po_2->getVel().x, po_2->getVel().y, po_2->getPos().x, po_2->getPos().y, po_2->getUp().x, po_2->getUp().y, po_2->getUp().getModule());
        text_string = text_c;
        text.print(16,window.getHeight()-52, text_string, 1, (float)width, (float)height);

        sprintf(text_c, "po_1: acc(%.3f,%.3f) vel(%.3f,%.3f) pos(%.3f,%.3f) up(%.3f,%.3f)(module:%.3f)", po_1->getAcc().x, po_1->getAcc().y, po_1->getVel().x, po_1->getVel().y, po_1->getPos().x, po_1->getPos().y, po_1->getUp().x, po_1->getUp().y, po_1->getUp().getModule());
        text_string = text_c;
        text.print(16,window.getHeight()-72, text_string, 1, (float)width, (float)height);

        sprintf(text_c, "GameObjects:%03d Render Bounders:%d  Render:%d", GameManager2D::getGameObjectsCount(), GameManager2D::renderBounders, GameManager2D::renderNotBounders);
        text_string = text_c;
        text.print(16,window.getHeight()-92, text_string, 1, (float)width, (float)height);
        
        //sprintf(text_c, "distance: %.3f)", GJK2D::distance(RegularPolygon::varrays[dynamic_cast<RegularPolygon*>(po_1->getAppearence())->getNSides()], dynamic_cast<RegularPolygon*>(po_1->getAppearence())->getNSides(),
        //												   RegularPolygon::varrays[dynamic_cast<RegularPolygon*>(po_2->getAppearence())->getNSides()], dynamic_cast<RegularPolygon*>(po_2->getAppearence())->getNSides());
        //text_string = text_c;
        //text.print(16,window.getHeight()-72, text_string, 1, (float)width, (float)height);
    }

    SDL_GL_SwapBuffers();
}
void Loopable2DTest::init(void)
{
    if ( SDL_Init(SDL_INIT_EVERYTHING) !=0 )
    {
        exit(1);
    }
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);
	width=800;
	height=600;
    window.initGL(width,height,32,SDL_RESIZABLE);
    window.setTitle("Flatland: un contest al quadrato");
    window.setResizeCallbackHolder(this);
    myGlewInit();
    
    red[0] = red[3] = 1;
    red[1] = red[2] = 0;
    blue[0] = blue[1] = 0;
    blue[2] = blue[3] = 1;
    green[0] = green[2] = 0;
    green[1] = green[3] = 1;
    white[0] = white[1] = white[2] = white[3] = 1;
    yellow[0] = yellow[1] = yellow[3] = 1;
    yellow[2] = 0;
    camera.setBaseWidthHeight(80,60);

    //if (!text.loadFontTexture("./resources/fonts/fontHand16_debug.png"))
    if (!text.loadFontTexture("./resources/fonts/LiberationMono16.png"))
    {
        cout<<"font non caricato!"<<std::endl;
    } else
    {
        text.BuildFont();
        cout<<"font texID = "<<text.font->texId<<" isTexture: "<<(glIsTexture(text.font->texId)?"yes":"no")<<" type "<<(text.font->type==GL_RGB?"GL_RGB":"GL_RGBA")<<endl;
    }

    draw_axes = draw_interface = true;
    
    GameManager2D::renderBounders = true;
    GameManager2D::activeCamera = &camera;

    /**
     * GameObjects instanciation
     **/
    po_1 = new PhysicalObject2D();
    po_1->setAppearence(new RegularPolygon2D(5));
    //po_2->setColor(blue);
    po_1->bounder = new RegularPolygonBounder(5);
    po_1->bounder->setColor(green);
    po_1->setScale(Vec2(2.0f,2.0f));
    po_1->setPos(Vec2(5,5));
    po_1->setRotAcc(0.3f);
    po_1->active = po_1->intersectable = po_1->check_collisions = true;
    po_1->collider = new TestCollider2D(red);
    dynamic_cast<TestCollider2D*>(po_1->collider)->setOwner(po_1);
    //GameManager2D::registerGameObject(po_1);
    
    po_2 = new PhysicalObject2D();
    po_2->setAppearence(new RegularPolygon2D(6));
    po_2->bounder = new RegularPolygonBounder(6);
    po_2->bounder->setColor(yellow);
    po_2->setAcc(Vec2(0.1f,0.1f));
    po_2->active = po_2->intersectable = po_2->check_collisions = true;
    //po_2->active = false;
    po_2->collider = new TestCollider2D(red);
    dynamic_cast<TestCollider2D*>(po_2->collider)->setOwner(po_2);
    //GameManager2D::registerGameObject(po_2);
    
    po_3_circular = new PhysicalObject2D();
    po_3_circular->setAppearence(new RegularPolygon2D(30));
    po_3_circular->bounder = new BoundingCircle();// new RegularPolygonBounder(15);
    po_3_circular->bounder->setColor(blue);
    po_3_circular->setPos(Vec2(-7,-6));
    po_3_circular->setVel(Vec2(1,0));
    po_3_circular->active = po_3_circular->intersectable = po_3_circular->check_collisions = true;
    po_3_circular->collider = new TestCollider2D(red);
    dynamic_cast<TestCollider2D*>(po_3_circular->collider)->setOwner(po_3_circular);
    //GameManager2D::registerGameObject(po_3_circular);
    
    po_4_circular = new PhysicalObject2D();
    po_4_circular->setAppearence(new RegularPolygon2D(20));
    po_4_circular->bounder = new BoundingCircle();// RegularPolygonBounder(10);
    po_4_circular->bounder->setColor(green);
    po_4_circular->setPos(Vec2(7,-1.8));
    po_4_circular->setVel(Vec2(-1,0));
    po_4_circular->active = po_4_circular->intersectable = po_4_circular->check_collisions = po_4_circular->visible = true;
    po_4_circular->collider = new TestCollider2D(red);
    dynamic_cast<TestCollider2D*>(po_4_circular->collider)->setOwner(po_4_circular);
    GameManager2D::registerGameObject(po_4_circular);
    
    po_5_rectangle = new PhysicalObject2D();
    po_5_rectangle->bounder = new BoundingRectangle(Vec2(),1,1);//RegularPolygonBounder(4);
    po_5_rectangle->setAppearence(new BoundingRectangle(Vec2(),1,1)); //new RegularPolygon2D(4));
    //po_5_rectangle->bounder->setColor(yellow);
    po_5_rectangle->setPos(Vec2(3,-4));
    //po_5_rectangle->setRotVel(0.5);
    po_5_rectangle->active = po_5_rectangle->intersectable = po_5_rectangle->check_collisions = true;
    po_5_rectangle->collider = new TestCollider2D(red);
    dynamic_cast<TestCollider2D*>(po_5_rectangle->collider)->setOwner(po_5_rectangle);
    po_5_rectangle->setScale(Vec2(4,4));
    GameManager2D::registerGameObject(po_5_rectangle);
    
    glClearColor(0,0,0,1);

    eventHandler = new TestEvent();
    dynamic_cast<TestEvent*>(eventHandler)->holder = this;
    running = true;
}
void Loopable2DTest::cleanUp(void)
{
    SDL_Quit();
}
bool Loopable2DTest::isLoopable(void) /// if false this won't loop any more
{
    return running;
}

void Loopable2DTest::TestEvent::onExit()
{
     holder->running=false;
     cout<<"esceee!"<<endl;
}
void Loopable2DTest::TestEvent::onResize(int w, int h)
{
         cout<<"ridimensionamento!"<<endl;
}
void Loopable2DTest::TestEvent::onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
{
}
void Loopable2DTest::TestEvent::onMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle)
{
}
void Loopable2DTest::TestEvent::onLButtonDown(int mX, int mY)
{
}
void Loopable2DTest::TestEvent::onRButtonDown(int mX, int mY)
{
}