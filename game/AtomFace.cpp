#include <GL/glew.h>
#include <iostream>
#include "AtomFace_VoxelAtom.h"
#include "../graphics/TextureManager.h"
GLuint AtomFace::baseList = 0;
Texture* AtomFace::terrain_atlas = NULL;
void AtomFace::initializeRenderingProcedure()
{
    float hs = VoxelAtom::side/2.0f;
    AtomFace::baseList = glGenLists(6);
    std::cout<<"baseList: "<<baseList<<std::endl;
    glNewList(baseList+PX, GL_COMPILE);
//        glNormal3f(1,0,0);
        glTexCoord2d(0, 1);
        glVertex3f(hs,-hs,hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(hs,-hs,-hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(hs,hs,-hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(hs,hs,hs);
    glEndList();
    glNewList(baseList+MX, GL_COMPILE);
//        glNormal3f(-1,0,0);
        glTexCoord2d(0, 1);
        glVertex3f(-hs,hs,hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(-hs,hs,-hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(-hs,-hs,-hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(-hs,-hs,hs);
    glEndList();
    glNewList(baseList+PY, GL_COMPILE);
//        glNormal3f(0,1,0);
        glTexCoord2d(0, 1);
        glVertex3f(hs,hs,hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(hs,hs,-hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(-hs,hs,-hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(-hs,hs,hs);
    glEndList();
    glNewList(baseList+MY, GL_COMPILE);
//        glNormal3f(0,-1,0);
        glTexCoord2d(0, 1);
        glVertex3f(-hs,-hs,hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(-hs,-hs,-hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(hs,-hs,-hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(hs,-hs,hs);
    glEndList();
    glNewList(baseList+PZ, GL_COMPILE);
//        glNormal3f(0,0,1);
        glTexCoord2d(0, 1);
        glVertex3f(-hs,hs,hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(-hs,-hs,hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(hs,-hs,hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(hs,hs,hs);
    glEndList();
    glNewList(baseList+MZ, GL_COMPILE);
//        glNormal3f(0,0,-1);
        glTexCoord2d(0, 1);
        glVertex3f(hs,hs,-hs);
        glTexCoord2d(0, 1-0.0625);
        glVertex3f(hs,-hs,-hs);
        glTexCoord2d(0.0625, 1-0.0625);
        glVertex3f(-hs,-hs,-hs);
        glTexCoord2d(0.0625, 1);
        glVertex3f(-hs,hs,-hs);
    glEndList();
}
void AtomFace::initializeTexture()
{
//    if (!terrain_atlas.load("./resources/terrain_textures/terrain_atlas.png"))
//    {
//        std::cerr<<"WARNING!! - could not load terrain atlas!!"<<std::endl;
//    } else
//    {
//        std::cout<<"terrain atlas texture id = "<<terrain_atlas.texId<<std::endl;
//    }

    AtomFace::terrain_atlas = TextureManager::loadTexture("./resources/terrain_textures/terrain_atlas.png");
    if (AtomFace::terrain_atlas==NULL)
    {
        std::cerr<<"terrain atlas == NULL!!!"<<std::endl;
        
    } else
    {
        std::cout<<"terrain atlas txt id = "<<AtomFace::terrain_atlas->texId<<std::endl;
    }
}

void AtomFace::renderNoTexture()
{
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_TEXTURE_2D);
//    std::cout<<"rendering a face...  pos:"<<owner->pos.x<<", "<<owner->pos.y<<", "<<owner->pos.z<<"  ("<<baseList+orientation<<")"<<std::endl;
//    std::cout<<"color: "<<color[0]<<" "<<color[1]<<" "<<color[2]<<" "<<color[3]<<std::endl;
//    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_FRONT, GL_LINE);
    glPolygonMode(GL_BACK, GL_NONE);
//    glEnable(GL_POLYGON_OFFSET_LINE); // Avoid Stitching!
//    glPolygonOffset(0.01,0.01);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslated(owner->pos.x, owner->pos.y, owner->pos.z);
        glBegin(GL_QUADS);
            glColor4fv(color);
            glCallList(baseList+orientation);
        glEnd();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopAttrib();
}

AtomFace::AtomFace()
{
    owner = NULL;
    orientation = PX;
    active = false;
}

AtomFace::AtomFace(VoxelAtom* owner, OrientationType orientation, GLfloat color[4])
{
    this->owner = owner;
    this->orientation = orientation;
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
    active = false;
}

void AtomFace::setColor(GLfloat color[4])
{
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
}

VoxelAtom * AtomFace::selectionNoCheck(Vec3 start, Vec3 dir, float* in_dist)
{
    float d = VoxelAtom::side/2.0f;
    float s = 1;
    Vec3 inter;
    switch (orientation)
    {
        case MX:
            s =-1;
        case PX:
            inter = start+dir*((owner->pos.x+d*s-start.x)/dir.x);
            if (inter.y<=owner->pos.y+d && inter.y> owner->pos.y-d &&
                inter.z<=owner->pos.z+d && inter.z>owner->pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case MY:
            s = -1;
        case PY:
            inter = start+dir*((owner->pos.y+d*s-start.y)/dir.y);
            if (inter.x<=owner->pos.x+d && inter.x> owner->pos.x-d &&
                inter.z<=owner->pos.z+d && inter.z>owner->pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case MZ:
            s = -1;
        case PZ:
            inter = start+dir*((owner->pos.z+d*s-start.z)/dir.z);
            if (inter.x<=owner->pos.x+d && inter.x> owner->pos.x-d &&
                inter.y<=owner->pos.y+d && inter.y>owner->pos.y-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
    }
    return NULL;
}