/* 
 * File:   StaticObject.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 06 November 2011
 */

#ifndef PHYSICALOBJECT2D_H
#define	PHYSICALOBJECT2D_H

#include "../graphics/Renderables2D.h"
#include "../game/GameObject.h"
#include "../maths/Vec2.h"

class PhysicalObject2D : public PositionableObject2D, public GameObject
{
protected:
    Vec2 vel;
    Vec2 acc;
    float rot_vel, rot_acc;
    Renderable2D * appearence;
public:
    virtual ~PhysicalObject2D(){};
    PhysicalObject2D();
    PhysicalObject2D(Renderable2D* appearence);
    Renderable2D* getAppearence(void);
    virtual void setAppearence(Renderable2D* appearence);
    Vec2 getVel(void);
    virtual void setVel(Vec2 vel);
    Vec2 getAcc(void);
    virtual void setAcc(Vec2 acc);
    float getRotVel(void);
    virtual void setRotVel(float rot_vel);
    float getRotAcc(void);
    virtual void setRotAcc(float rot_acc);

    virtual void updatePhysics(Uint32 elapsedTime);
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime);

    /**
     * @override from Renderable2D
     */ 
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};

#endif	/* PHYSICALOBJECT2D_H */
