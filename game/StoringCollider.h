/** 
 * File:   StoringCollider.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 15 December 2012
 * 
 */
#ifndef STORINGCOLLIDER_H
#define	STORINGCOLLIDER_H

#include "PhysicalObject2D.h"
#include "../graphics/Renderables2D.h"
#include "../graphics/CollisionSolver2D.h"
#include "../game/GameObject.h"
#include "../game/GameManager2D.h"
#include <cstdlib>

class StoringCollider : public Collider2D
{
protected:
    int count_collided_things;

public:
    Renderable2D* collided_things[MAX_COLLISIONS_PER_OBJ];
    uint8_t collision_states[MAX_COLLISIONS_PER_OBJ];

    StoringCollider();
    int getCountCollidedThings(void);

    /**
     * @override from Collider2D
     */ 
    virtual void onCollision(uint8_t* states, Renderable2D** renderables, int num);
};
#endif /* STORINGCOLLIDER_H */