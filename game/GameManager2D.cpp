#include <stdint.h>
#include "../game/GameManager2D.h"
#include "../graphics/Renderables2D.h"
#include "../game/Collider2D.h"
#include "../graphics/CollisionSolver2D.h"
#include <iostream>
#include <limits>

#define N_REND_AND_GO 500000
#define COLLISIONS_IDS_SIZE MAX_COLLISIONS_PER_OBJ

//#define DEBUG_COLLISION_MANAGEMENT

#ifdef WIN32
	#undef max
#endif
uint8_t GameManager2D::collision_buffer_id = 0;
Camera2D* GameManager2D::activeCamera = NULL;
uint32_t GameManager2D::maxFreeID=0;
//Uint32 GameManager2D::maxFreeRenderableIndex=0;
bool GameManager2D::renderBounders = false;
bool GameManager2D::renderNotBounders = true;
bool GameManager2D::do_update = true;
bool GameManager2D::do_update_physics = true;
bool GameManager2D::do_late_update = true;
bool GameManager2D::do_collision_react = true;
bool GameManager2D::do_collide = true;
int GameManager2D::gameobjects_count=0;
Renderable2D** initRends()
{
    Renderable2D** tmp = new Renderable2D*[N_REND_AND_GO];
    memset(tmp, NULL, N_REND_AND_GO);
	for (int i=0 ; i<N_REND_AND_GO ; i++)
	{
		if (tmp[i]!=NULL)
		{
			//std::cerr<<"tmp["<<i<<"]="<<(unsigned int)tmp[i]<<std::endl;
			tmp[i] = NULL;
		}
	}
    return tmp;
}
#ifdef WIN32
Renderable2D** GameManager2D::renderables = initRends();
#else
Renderable2D** GameManager2D::renderables(initRends());
#endif
GameObject** initGOs()
{
    GameObject** tmp = new GameObject*[N_REND_AND_GO];
    memset(tmp, NULL, N_REND_AND_GO);
	for (int i=0 ; i<N_REND_AND_GO ; i++)
	{
		if (tmp[i]!=NULL)
		{
			//std::cerr<<"tmp["<<i<<"]="<<(unsigned int)tmp[i]<<std::endl;
			tmp[i] = NULL;
		}
	}
    return tmp;
}
#ifdef WIN32
GameObject** GameManager2D::game_objects = initGOs();
#else
GameObject** GameManager2D::game_objects(initGOs());
#endif
PhysicalObject2D** initPhyOs()
{
	PhysicalObject2D** tmp = new PhysicalObject2D*[N_REND_AND_GO];
    memset(tmp, NULL, N_REND_AND_GO);
	for (int i=0 ; i<N_REND_AND_GO ; i++)
	{
		if (tmp[i]!=NULL)
		{
			//std::cerr<<"tmp["<<i<<"]="<<(unsigned int)tmp[i]<<std::endl;
			tmp[i] = NULL;
		}
	}
    return tmp;
}
#ifdef WIN32
PhysicalObject2D** GameManager2D::physical_objects = initPhyOs();
#else
PhysicalObject2D** GameManager2D::physical_objects(initPhyOs());
#endif
uint8_t*** initCollStates()
{
	uint8_t*** tmp = new uint8_t**[2];
	for (int j=0 ; j<2 ; j++)
	{
		tmp[j] = new uint8_t*[N_REND_AND_GO];
		for (int i=0 ; i<N_REND_AND_GO ; i++)
		{
			tmp[j][i] = new uint8_t[COLLISIONS_IDS_SIZE];
			memset(tmp[j][i], COLLISION_NONE, COLLISIONS_IDS_SIZE);
		}
	}
    return tmp;
}
#ifdef WIN32
uint8_t*** GameManager2D::collision_states = initCollStates();
#else
uint8_t*** GameManager2D::collision_states(initCollStates());
#endif
unsigned int*** initCollIDs()
{
	unsigned int*** tmp = new unsigned int**[2];
	for (int j=0 ; j<2 ; j++)
	{
		tmp[j] = new unsigned int*[N_REND_AND_GO];
		for (int i=0 ; i<N_REND_AND_GO ; i++)
		{
			tmp[j][i] = new unsigned int[COLLISIONS_IDS_SIZE];
		}
	}
    return tmp;
}
#ifdef WIN32
unsigned int*** GameManager2D::collisionsIDs = initCollIDs();
#else
unsigned int*** GameManager2D::collisionsIDs(initCollIDs());
#endif
uint8_t** initNCollIDs()
{
    uint8_t** tmp = new uint8_t*[2];
    for (int i=0 ; i<2 ; i++)
    {
        tmp[i] = new uint8_t[N_REND_AND_GO];
        memset(tmp[i], 0, N_REND_AND_GO);
    }
    return tmp;
}
#ifdef WIN32
uint8_t** GameManager2D::n_collisionsIDs = initNCollIDs();
#else
uint8_t** GameManager2D::n_collisionsIDs(initNCollIDs());
#endif
std::list<Uint32> initFIL_2D()
{
    std::list<Uint32> tmp;
    return tmp;
}
#ifdef WIN32
std::list<Uint32> GameManager2D::freed_ids = initFIL_2D();
#else
std::list<Uint32> GameManager2D::freed_ids(initFIL_2D());
#endif
int GameManager2D::getGameObjectsCount(void)
{
    return GameManager2D::gameobjects_count;
}

void GameManager2D::solveCollisions(void)
{
    /**
     * solve collisions
     **/
#ifdef DEBUG_COLLISION_MANAGEMENT
    std::cout<<std::endl<<"Collision solving..."<<std::endl;
#endif
    for (int i=0 ; i<maxFreeID ; i++ )
    {
        if (GameManager2D::renderables[i]!=NULL && GameManager2D::renderables[i]->check_collisions && GameManager2D::renderables[i]->bounder!=NULL)
        {
            for (int j=i+1 ; j<maxFreeID ; j++ )
            {
                if (GameManager2D::renderables[j]!=NULL && GameManager2D::renderables[j]->check_collisions && GameManager2D::renderables[j]->bounder!=NULL)
                {
                    // solve collision
                    float dist;
                    int coll = CollisionSolver2D::collide(*renderables[i]->bounder, *renderables[j]->bounder, dist);
#ifdef DEBUG_COLLISION_MANAGEMENT
                    std::cout<<"coll("<<i<<", "<<j<<") = "<<coll<<std::endl;
#endif
                    // find j in [i]
                    bool already_i_in_j, already_j_in_i;
                    already_i_in_j = already_j_in_i = false;
                    int jj;
                    for (jj=0 ; jj<n_collisionsIDs[collision_buffer_id][i] ; jj++)
                    {
                        if (collisionsIDs[collision_buffer_id][i][jj]==j)
                        {
                            already_j_in_i = true;
                            break;
                        }
                    }
                    // find i in [j]
                    int ii;
                    for (ii=0 ; ii<n_collisionsIDs[collision_buffer_id][j] ; ii++)
                    {
                        if (collisionsIDs[collision_buffer_id][j][ii]==i)
                        {
                            already_i_in_j = true;
                            break;
                        }
                    }
                    if (!already_j_in_i && (coll!=COLLISION_OUT||already_i_in_j))
                    // j not found in [i]
                    {
                        // search first slot free
                        for (jj=0 ; jj<n_collisionsIDs[collision_buffer_id][i] ; jj++)
                        {
                            if (collision_states[collision_buffer_id][i][jj]==COLLISION_NONE)
                            {
                                break;
                            }
                        }
                        if ((jj==n_collisionsIDs[collision_buffer_id][i] && n_collisionsIDs[collision_buffer_id][i]<COLLISIONS_IDS_SIZE) ||
                            jj!=n_collisionsIDs[collision_buffer_id][i]
                           )
                            // we can use a free slot or append a new id
                        {
                            collisionsIDs[collision_buffer_id][i][jj] = j;
                            if (already_i_in_j)
                            // if i found we correct a lost collision
                            {
                                if (collision_states[collision_buffer_id][j][ii] == COLLISION_ENTER ||
                                    collision_states[collision_buffer_id][j][ii] == COLLISION_STAY
                                   )
                                {
                                    if (coll!=COLLISION_OUT)
                                    {
                                        collision_states[collision_buffer_id][i][jj] =
                                        collision_states[collision_buffer_id][j][ii] = COLLISION_STAY;
                                    } else
                                    {
                                        collision_states[collision_buffer_id][i][jj] =
                                        collision_states[collision_buffer_id][j][ii] = COLLISION_EXIT;
                                    }
                                } else
                                if (collision_states[collision_buffer_id][j][ii] == COLLISION_EXIT ||
                                    collision_states[collision_buffer_id][j][ii] == COLLISION_NONE
                                   )
                                {
                                    if (coll!=COLLISION_OUT)
                                    {
                                        collision_states[collision_buffer_id][i][jj] =
                                        collision_states[collision_buffer_id][j][ii] = COLLISION_ENTER;
                                    } else
                                    {
                                        collision_states[collision_buffer_id][i][jj] =
                                        collision_states[collision_buffer_id][j][ii] = COLLISION_NONE;
                                    }
                                }
                            } else
                            // !already_i_in_j
                            {
                                if (coll!=COLLISION_OUT)
                                {
                                    collision_states[collision_buffer_id][i][jj] = COLLISION_ENTER;
                                }
                                /**
                                 * Now we insert the id i in [j]
                                 **/
                                //if (!already_i_in_j) true
                                // i not found in [j]
                                // search first slot free
                                for (ii=0 ; ii<n_collisionsIDs[collision_buffer_id][j] ; ii++)
                                {
                                    if (collision_states[collision_buffer_id][j][ii]==COLLISION_NONE)
                                    {
                                        break;
                                    }
                                }
                                if ((ii==n_collisionsIDs[collision_buffer_id][j] && n_collisionsIDs[collision_buffer_id][j]<COLLISIONS_IDS_SIZE) ||
                                    ii!=n_collisionsIDs[collision_buffer_id][j]
                                   )
                                // we can use a free slot or append a new id
                                {
                                    // if there is a collision
                                    if (coll!=COLLISION_OUT)
                                    {
                                        collisionsIDs[collision_buffer_id][j][ii] = i;
                                        collision_states[collision_buffer_id][j][ii] = COLLISION_ENTER;
                                        if (ii==n_collisionsIDs[collision_buffer_id][j])
                                        {
#ifdef DEBUG_COLLISION_MANAGEMENT
                                            std::cout<<"n_collisionsIDs[collision_buffer_id][j="<<j<<"]++  line "<<__LINE__<<std::endl;
#endif
                                            n_collisionsIDs[collision_buffer_id][j]++;
                                        }
                                    }
                                } else
                                // all the array is full
                                {
                                    std::cerr<<"WARNING! - collision lost! ("<<i<<", "<<j<<")"<<std::endl;
                                }
                            }
                            if (jj==n_collisionsIDs[collision_buffer_id][i])
                            // if we has appended
                            {
#ifdef DEBUG_COLLISION_MANAGEMENT
                                std::cout<<"n_collisionsIDs[collision_buffer_id][i="<<i<<"]++  line "<<__LINE__<<std::endl;
#endif
                                n_collisionsIDs[collision_buffer_id][i]++;
                            }

                        } else
                        // all the array is full
                        {
                            std::cerr<<"WARNING! - collision lost! ("<<i<<", "<<j<<")"<<std::endl;
                        }
                    } else
                    // j found in [i]
                    {
                        if (!already_i_in_j)
                        // i not found in [j]
                        {
                            // search first slot free
                            for (ii=0 ; ii<n_collisionsIDs[collision_buffer_id][j] ; ii++)
                            {
                                if (collision_states[collision_buffer_id][j][ii]==COLLISION_NONE)
                                {
                                    break;
                                }
                            }
                        }
                        // inset j in [i]
                        if (coll!=COLLISION_OUT)
                        // there's a collision now
                        {
                            switch (collision_states[collision_buffer_id/*(collision_buffer_id+1)%2*/][i][jj])
                            {
                            case COLLISION_ENTER:
                            case COLLISION_STAY:
                                collision_states[collision_buffer_id][i][jj] = COLLISION_STAY;
                                break;
                            case COLLISION_EXIT:
                                collision_states[collision_buffer_id][i][jj] = COLLISION_ENTER;
                                break;
                            }
                        } else
                        // no collision now
                        {
                            switch (collision_states[collision_buffer_id/*(collision_buffer_id+1)%2*/][i][jj])
                            {
                            case COLLISION_ENTER:
                            case COLLISION_STAY:
                                collision_states[collision_buffer_id][i][jj] = COLLISION_EXIT;
                                break;
                            case COLLISION_EXIT:
                                collision_states[collision_buffer_id][i][jj] = COLLISION_NONE;
                                if (jj==n_collisionsIDs[collision_buffer_id][i]-1)
                                {
#ifdef DEBUG_COLLISION_MANAGEMENT
                                    std::cout<<"n_collisionsIDs[collision_buffer_id][i="<<i<<"]--  line "<<__LINE__<<std::endl;
#endif
                                    n_collisionsIDs[collision_buffer_id][i]--;
                                }
                                break;
                            case COLLISION_NONE:
                                //it's already this! collision_states[collision_buffer_id][i][jj] = COLLISION_NONE;
                                break;
                            }
                        }
                        // insert i in [j]
                        /*if ( (ii==n_collisionsIDs[collision_buffer_id][j] && n_collisionsIDs[collision_buffer_id][j]<COLLISIONS_IDS_SIZE) ||
                             ii!=n_collisionsIDs[collision_buffer_id][j]
                           )
                        {*/
                        if (already_i_in_j)
                        {
                            if (collision_states[collision_buffer_id][i][jj]==COLLISION_NONE && ii==n_collisionsIDs[collision_buffer_id][j]-1)
                            {
#ifdef DEBUG_COLLISION_MANAGEMENT
                                std::cout<<"n_collisionsIDs[collision_buffer_id][j="<<j<<"]--  line "<<__LINE__<<std::endl;
#endif
                                n_collisionsIDs[collision_buffer_id][j]--;
                            }
                            collision_states[collision_buffer_id][j][ii] = collision_states[collision_buffer_id][i][jj];
                        } else
                        if (ii!=n_collisionsIDs[collision_buffer_id][j])
                        // insert i in a free slot
                        {
                            collisionsIDs[collision_buffer_id][j][ii] = j;
                            collision_states[collision_buffer_id][j][ii] = collision_states[collision_buffer_id][i][jj];
                        } else
                        if (ii==n_collisionsIDs[collision_buffer_id][j] && n_collisionsIDs[collision_buffer_id][j]<COLLISIONS_IDS_SIZE)
                        // insert at the end
                        {
                            if (collision_states[collision_buffer_id][i][jj]!=COLLISION_NONE)
                            {
                                collision_states[collision_buffer_id][j][ii] = collision_states[collision_buffer_id][i][jj];
#ifdef DEBUG_COLLISION_MANAGEMENT
                                std::cout<<"n_collisionsIDs[collision_buffer_id]["<<j<<"]++"<<std::endl;
#endif
                                n_collisionsIDs[collision_buffer_id][j]++;
                            }
                        } else
                        //} else
                        // all the array is full
                        {
                            std::cerr<<"WARNING! - collision lost! ("<<i<<", "<<j<<")"<<std::endl;
                        }
                    }
                }
            }
        }
    }
}
void GameManager2D::render()
{
    for (int i=0 ; i<N_REND_AND_GO ; i++ )
    {
        if (GameManager2D::renderables[i]!=NULL && (GameManager2D::renderables[i]->bounder==NULL || GameManager2D::renderables[i]->bounder->collide(*activeCamera)!=COLLISION_OUT) )
        {
            if (GameManager2D::renderBounders && GameManager2D::renderables[i]->bounder!=NULL)
            {
                GameManager2D::renderables[i]->bounder->render();
            }
            if (GameManager2D::renderNotBounders && GameManager2D::renderables[i]->visible)
            {
                GameManager2D::renderables[i]->render();
            }
        }
    }
}

void GameManager2D::update(Uint32 elapsedTime)
{
    uint8_t old_bufid = collision_buffer_id;
    collision_buffer_id++;
    collision_buffer_id%=2;
    
    if (do_update_physics)
    {
        /**
         * update objects physics
         **/
        for (int i=0 ; i<maxFreeID ; i++ )
        {
            if (GameManager2D::physical_objects[i]!=NULL && GameManager2D::physical_objects[i]->active)
            {
                    GameManager2D::physical_objects[i]->updatePhysics(elapsedTime);
            }
        }
    }
    
    if (do_collide)
    {
        /**
         * solve collisions
         **/
        solveCollisions();
    }
    
    /*
            c'è realmente la necessità di mantenere una lista di renderables completamente
            separata dai GO? ci sarà mai un renderable che non deve aggiornarsi e che non deve collidere?
            uno sfondo ad esempio, � uno pazienza se faccio una call a update() a vuoto
                            NO.
    */
    /*
            hei ma ci sono anche GO che non sono renderable e di cui bisogna controllare le collisioni, come i trigger!
            ma quindi non ha senso controllare le collisioni sui renderables, si dovrebbe fare sui GO. Se un renderable interagisce col resto delle cose perchè dovrebbe collidere?

            vorrei ricordare che anche i trigger devono essere renderable, altrimenti come debugghi??
    */
    
    if (do_collision_react)
    {
        /**
         * collision reactions
         **/
        for (int i=0 ; i<maxFreeID ; i++ )
        {
            Renderable2D* colliding[COLLISIONS_IDS_SIZE];
            uint8_t states[COLLISIONS_IDS_SIZE];
            int n_cols=0;
            if (GameManager2D::n_collisionsIDs[collision_buffer_id][i]!=0)
            {
                if (renderables[i]->collider!=NULL)// && n_collisionsIDs[collision_buffer_id][i]!=0)
                {
                    n_cols=0;
                    for (int j=0 ; j<n_collisionsIDs[collision_buffer_id][i] ; j++)
                    {
                        if (collision_states[collision_buffer_id][i][j]!=COLLISION_NONE || collision_states[old_bufid][i][j]==COLLISION_EXIT)
                        {
                            colliding[n_cols] = renderables[collisionsIDs[collision_buffer_id][i][j]];
                            states[n_cols] = collision_states[collision_buffer_id][i][j];
                            n_cols++;
                        }
                    }
                    /*if (n_cols!=0) this is commented because is important to make objs aware there are no collisions
                    {*/
#ifdef DEBUG_COLLISION_MANAGEMENT
                        std::cout<<i<<" collider->onCollision "<<n_cols<<std::endl;
#endif
                        renderables[i]->collider->onCollision(states, colliding, n_cols);
                    //}
                }
            } else
            {
                // TODO questo è un tappullo, in realtà dovrebbe partire una chiamata con dei NONE il giro dopo gli  EXIT, ma non parte, bisogna rivedere il solver
                renderables[i]->collider->onCollision(states, colliding, 0);
            }
        }
    }
    if (do_update)
    {
        /**
         * update
         **/
        for (int i=0 ; i<maxFreeID ; i++ )
        {
            if (GameManager2D::game_objects[i]!=NULL && GameManager2D::game_objects[i]->active)
            {
                GameManager2D::game_objects[i]->update(elapsedTime);
            }
        }
    }
    if (do_late_update)
    {
        /**
         * late update
         **/
        for (int i=0 ; i<maxFreeID ; i++ )
        {
            if (GameManager2D::game_objects[i]!=NULL && GameManager2D::game_objects[i]->active)
            {
                GameManager2D::game_objects[i]->lateUpdate(elapsedTime);
            }
        }
    }
}

bool GameManager2D::registerGameObject(GameObject * go)
{
    //TODO dovrebbe essere UINT32_MAX ma non mi va quindi per prova metto un valore io
    if (GameManager2D::maxFreeID==N_REND_AND_GO)
    {
        std::list<Uint32>::iterator it=GameManager2D::freed_ids.begin();
        if(it!=GameManager2D::freed_ids.end())
        {
            go->id=*it;
            GameManager2D::freed_ids.pop_front();
        } else
        {
            return false;
        }
    } else
    {
        go->id=maxFreeID++;
    }
    GameManager2D::game_objects[go->id]=go;
    PhysicalObject2D* po = dynamic_cast<PhysicalObject2D*>(go);
    if (po)
    {
            GameManager2D::physical_objects[go->id]=po;
    }
    Renderable2D* r = dynamic_cast<Renderable2D*>(go);
    if (r)
    {
            GameManager2D::renderables[go->id]=r;
    }
    GameManager2D::gameobjects_count++;
    return true;
}

//bool GameManager2D::registerRenderable(Renderable2D * r)
//{
//    //TODO la registrazione dei renderable � diversa da quella dei game object
//    if (GameManager2D::maxFreeRenderableIndex<N_REND_AND_GO)
//    {
//        GameManager2D::renderables[GameManager2D::maxFreeRenderableIndex++]=r;
//        std::cout<<"renderable registered"<<std::endl;
//        return true;
//    }
//    return false;
//}

void GameManager2D::deleteGameObject(GameObject * go)
{
    if (GameManager2D::game_objects[go->id]==NULL)
    {
        return;
    }
    GameManager2D::game_objects[go->id]=NULL;
    GameManager2D::physical_objects[go->id]=NULL;
    GameManager2D::renderables[go->id]=NULL;
    GameManager2D::n_collisionsIDs[0][go->id] = GameManager2D::n_collisionsIDs[1][go->id] = 0;
    freed_ids.push_back(go->id);
    GameManager2D::gameobjects_count--;
}

//void GameManager2D::deleteRenderable(Renderable2D * r)
//{
//    for (int i=0; i<N_REND_AND_GO ; i++ )
//    {
//        if (GameManager2D::renderables[i]==r)
//        {
//            GameManager2D::renderables[i]=NULL;
//        }
//    }
//}

bool GameManager2D::rayCast(Vec2 start, Vec2 dir, Vec2 * intersection, Renderable2D**intersected)
{
    float tmp_dist, min_dist = std::numeric_limits<float>::max();
    Vec2 tmp_inter;
    for (int i=0 ; i<N_REND_AND_GO ; i++ )
    {
        if (    GameManager2D::renderables[i]!=NULL &&
                GameManager2D::renderables[i]->visible &&
                GameManager2D::renderables[i]->intersectable &&
                (tmp_dist=GameManager2D::renderables[i]->rayIntersect(start, dir, &tmp_inter))>0 &&
                tmp_dist<min_dist )
        {
            min_dist = tmp_dist;
            *intersection = tmp_inter;
            *intersected = GameManager2D::renderables[i];
        }
    }
    return min_dist!=std::numeric_limits<float>::max();
}
