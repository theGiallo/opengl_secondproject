#include <GL/glew.h>

#include "VoxelTerrainChunk.h"
#include <float.h>
#include <iostream>
#include "../graphics/BoundingBox.h"

VoxelTerrainChunk::VoxelTerrainChunk()
{
    for (int i=0 ; i<VoxelTerrainChunk::chunkSide ; i++ )
    {
        for (int j=0 ; j<VoxelTerrainChunk::chunkSide ; j++ )
        {
            for (int k=0 ; k<VoxelTerrainChunk::chunkSide ; k++ )
            {
                matrix[i][j][k] = NULL;
            }
        }
    }
    /**
     * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide = faces on a plane of the chunk
     * VoxelTerrainChunk::chunkSide = planes on a dimension of the chunk
     * 3 = dimensions of the plane
     * ------------------------------------------------------------------------------------------
     * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*3 = max numbers of faces rendered per chunk
     * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*6 = numbers of the faces of all cubes in the chunk
     * 20 = numbers of floats necessary to render a face (4*(3+2)), although normals could be useless
     */
    std::cout<<"size of GLfloat: "<<sizeof(GLfloat)<<std::endl;
    std::cout<<"requested "<<(size_t)(sizeof(GLfloat)*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*3*20)<<" bytes"<<std::endl;
    vArray = (GLfloat*)malloc(sizeof(GLfloat)*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*3*20);
    if (vArray==NULL)
    {
        std::cerr<<"Malloc fallita per vArray in VoxelTerrainChunk. Richiesti "<<(size_t)(sizeof(GLfloat)*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*3*20)<<"bytes"<<std::endl;
        exit(1);
    }
    nActiveFaces = 0;
    float side = VoxelTerrainChunk::chunkSide*VoxelAtom::side;
    this->bounder = new BoundingBox(origin+side/2.0f, side);
    for (int i=0 ; i<6 ; i++ )
    {
        adjacents[i] = NULL;
    }
}

void VoxelTerrainChunk::setOrigin(Vec3 origin)
{
    this->origin = origin;
    dynamic_cast<BoundingBox*>(this->bounder)->center = origin+VoxelTerrainChunk::chunkSide*VoxelAtom::side/2.0f;
}
Vec3 VoxelTerrainChunk::getOrigin()
{
    return origin;
}

void VoxelTerrainChunk::initializeStructure()
{
    std::cout<<"Inizializzo un chunk..."<<std::endl;
    std::cout<<"VoxelTerrainChunk::chunkSide: "<<VoxelTerrainChunk::chunkSide<<std::endl;
    std::cout<<"matrix[0][0][0]: "<<matrix[0][0][0]<<std::endl;
    for (int i=0 ; i<VoxelTerrainChunk::chunkSide ; i++ )
    {
        for (int j=0 ; j<VoxelTerrainChunk::chunkSide ; j++ )
        {
            for (int k=0 ; k<VoxelTerrainChunk::chunkSide ; k++ )
            {
                if (i<VoxelTerrainChunk::chunkSide-1)
                {
                    if (matrix[i][j][k]==NULL)
                    {
                        if (matrix[i+1][j][k]!=NULL)
                        {
                            matrix[i+1][j][k]->faces[MX].active = true;
                            activeFaces[MX].push_back(&(matrix[i+1][j][k]->faces[MX]));
                            nActiveFaces++;
                        }
                    } else
                    {
                        if (matrix[i+1][j][k]==NULL)
                        {
                            matrix[i][j][k]->faces[PX].active = true;
                            activeFaces[PX].push_back(&(matrix[i][j][k]->faces[PX]));
                            nActiveFaces++;
                        }
                        if (i==0)
                        {
                            matrix[i][j][k]->faces[MX].active = true;
                            activeFaces[MX].push_back(&(matrix[i][j][k]->faces[MX]));
                            nActiveFaces++;
                        }
                    }
                } else
                {
                    if (matrix[i][j][k]!=NULL)
                    {
                        //TODO dovrebbe chiedere al chunk adiacente
                        matrix[i][j][k]->faces[PX].active = true;
                        activeFaces[PX].push_back(&(matrix[i][j][k]->faces[PX]));
                        nActiveFaces++;
                    }
                }
                if (j<VoxelTerrainChunk::chunkSide-1)
                {
                    if (matrix[i][j][k]==NULL)
                    {
                        if (matrix[i][j+1][k]!=NULL)
                        {
                            matrix[i][j+1][k]->faces[MY].active = true;
                            activeFaces[MY].push_back(&(matrix[i][j+1][k]->faces[MY]));
                            nActiveFaces++;
                        }
                    } else
                    {
                        if (matrix[i][j+1][k]==NULL)
                        {
                            matrix[i][j][k]->faces[PY].active = true;
                            activeFaces[PY].push_back(&(matrix[i][j][k]->faces[PY]));
                            nActiveFaces++;
                        }
                        if (j==0)
                        {
                            matrix[i][j][k]->faces[MY].active = true;
                            activeFaces[MY].push_back(&(matrix[i][j][k]->faces[MY]));
                            nActiveFaces++;
                        }
                    }
                } else
                {
                    if (matrix[i][j][k]!=NULL)
                    {
                        //TODO dovrebbe chiedere al chunk adiacente
                        matrix[i][j][k]->faces[PY].active = true;
                        activeFaces[PY].push_back(&(matrix[i][j][k]->faces[PY]));
                            nActiveFaces++;
                    }
                }
                if (k<VoxelTerrainChunk::chunkSide-1)
                {
                    if (matrix[i][j][k]==NULL)
                    {
                        if (matrix[i][j][k+1]!=NULL)
                        {
                            matrix[i][j][k+1]->faces[MZ].active = true;
                            activeFaces[MZ].push_back(&(matrix[i][j][k+1]->faces[MZ]));
                            nActiveFaces++;
                        }
                    } else
                    {
                        if (matrix[i][j][k+1]==NULL)
                        {
                            matrix[i][j][k]->faces[PZ].active = true;
                            activeFaces[PZ].push_back(&(matrix[i][j][k]->faces[PZ]));
                            nActiveFaces++;
                        }
                        if (k==0)
                        {
                            matrix[i][j][k]->faces[MZ].active = true;
                            activeFaces[MZ].push_back(&(matrix[i][j][k]->faces[MZ]));
                            nActiveFaces++;
                        }
                    }
                } else
                {
                    if (matrix[i][j][k]!=NULL)
                    {
                        //TODO dovrebbe chiedere al chunk adiacente
                        matrix[i][j][k]->faces[PZ].active = true;
                        activeFaces[PZ].push_back(&(matrix[i][j][k]->faces[PZ]));
                        nActiveFaces++;
                    }
                }
            }
        }
    }
}
AtomFace * VoxelTerrainChunk::selection(Vec3 start, Vec3 dir, float * dist)
{
    int n;
    std::list<AtomFace*> *candidates;
    if (dir.y == 0)
    {
        if (dir.z == 0)
        {
            n = 1;
            candidates = new std::list<AtomFace*>[n];
            if (dir.x>0)
            {
                candidates[0] = activeFaces[MX];
            } else
            {
                candidates[0] = activeFaces[PX];
            }
        } else if (dir.x==0)
        {
            n = 1;
            candidates = new std::list<AtomFace*>[n];
            if (dir.z>0)
            {
                candidates[0] = activeFaces[MZ];
            } else
            {
                candidates[0] = activeFaces[PZ];
            }
        } else
        {
            n=2;
            candidates = new std::list<AtomFace*>[n];
            if (dir.x>0)
            {
                candidates[0] = activeFaces[MX];
            } else
            {
                candidates[0] = activeFaces[PX];
            }
            if (dir.z>0)
            {
                candidates[1] = activeFaces[MZ];
            } else
            {
                candidates[1] = activeFaces[PZ];
            }
        }
    } else if (dir.x==0)
    {
        if (dir.z==0)
        {
            n=1;
            candidates = new std::list<AtomFace*>[n];
            if (dir.y>0)
            {
                candidates[0] = activeFaces[MY];
            } else
            {
                candidates[0] = activeFaces[PY];
            }
        } else
        {
            n=2;
            candidates = new std::list<AtomFace*>[n];
            if (dir.y>0)
            {
                candidates[0] = activeFaces[MY];
            } else
            {
                candidates[0] = activeFaces[PY];
            }
            if (dir.z>0)
            {
                candidates[1] = activeFaces[MZ];
            } else
            {
                candidates[1] = activeFaces[PZ];
            }
        }
    } else
    {
        if (dir.z==0)
        {
            n=2;
            candidates = new std::list<AtomFace*>[n];
            if (dir.x>0)
            {
                candidates[0] = activeFaces[MX];
            } else
            {
                candidates[0] = activeFaces[PX];
            }
            if (dir.y>0)
            {
                candidates[1] = activeFaces[MY];
            } else
            {
                candidates[1] = activeFaces[PY];
            }
        } else
        {
            n=3;
            candidates = new std::list<AtomFace*>[n];
            if (dir.x>0)
            {
                candidates[0] = activeFaces[MX];
            } else
            {
                candidates[0] = activeFaces[PX];
            }
            if (dir.y>0)
            {
                candidates[1] = activeFaces[MY];
            } else
            {
                candidates[1] = activeFaces[PY];
            }
            if (dir.z>0)
            {
                candidates[2] = activeFaces[MZ];
            } else
            {
                candidates[2] = activeFaces[PZ];
            }
        }
    }
    GLfloat tmp_dist, min_dist = FLT_MAX;
    VoxelAtom* tmp_va = NULL;
    AtomFace* res = NULL;
    for (int i=0 ; i<n ; i++)
    {
        for (std::list<AtomFace*>::iterator it=candidates[i].begin(); it!=candidates[i].end(); ++it)
        {
            tmp_va = (*it)->selectionNoCheck(start, dir, &tmp_dist);
            if (tmp_va != NULL && tmp_dist>0 && tmp_dist<min_dist)
            {
                res = *it;
                min_dist = tmp_dist;
            }
        }
    }
    delete [] candidates;
    *dist = min_dist;
    return res;
}
void VoxelTerrainChunk::removeVoxelAtom(VoxelAtom * va)
{
    Vec3 rel = va->pos-origin;
    if (rel.x > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.y > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.z > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.x <= 0 ||
        rel.y <= 0 ||
        rel.z <= 0
        )
    {
        std::cerr<<"WARNING!! - trying to delete a VoxelAtom from a Chunk that do not contains it!!"<<std::endl;
        return;
    }
    int a,b,c;
    a = rel.x/VoxelAtom::side;
    b = rel.y/VoxelAtom::side;
    c = rel.z/VoxelAtom::side;
    VoxelAtom * tmp = matrix[a][b][c];
    if ( tmp == va )
    {
        matrix[a][b][c] = NULL;
        for (int i=0 ; i<6 ; i++ )
        {
            if (va->faces[i].active)
            {
                activeFaces[i].remove(&(va->faces[i]));
                nActiveFaces--;
            }
        }
        if (a>0)
        {
            if ( (tmp = matrix[a-1][b][c])!=NULL && !tmp->faces[PX].active )
            {
                tmp->faces[PX].active = true;
                activeFaces[PX].push_back(&(tmp->faces[PX]));
                nActiveFaces++;
            }
        }
        if (a<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a+1][b][c])!=NULL && !tmp->faces[MX].active )
            {
                tmp->faces[MX].active = true;
                activeFaces[MX].push_back(&(tmp->faces[MX]));
                nActiveFaces++;
            }
        }
        if (b>0)
        {
            if ( (tmp = matrix[a][b-1][c])!=NULL && !tmp->faces[PY].active )
            {
                tmp->faces[PY].active = true;
                activeFaces[PY].push_back(&(tmp->faces[PY]));
                nActiveFaces++;
            }
        }
        if (b<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a][b+1][c])!=NULL && !tmp->faces[MY].active )
            {
                tmp->faces[MY].active = true;
                activeFaces[MY].push_back(&(tmp->faces[MY]));
                nActiveFaces++;
            }
        }
        if (c>0)
        {
            if ( (tmp = matrix[a][b][c-1])!=NULL && !tmp->faces[PZ].active )
            {
                tmp->faces[PZ].active = true;
                activeFaces[PZ].push_back(&(tmp->faces[PZ]));
                nActiveFaces++;
            }
        }
        if (c<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a][b][c+1])!=NULL && !tmp->faces[MZ].active )
            {
                tmp->faces[MZ].active = true;
                activeFaces[MZ].push_back(&(tmp->faces[MZ]));
                nActiveFaces++;
            }
        }
    }
    //TODO qui non cancello dalla memoria l'atomo rimosso
//    delete [] tmp->faces;
//    delete tmp;
//    std::cout<<"!ASDDA"<<std::endl;
}

void VoxelTerrainChunk::addVoxelAtom(VoxelAtom * va)
{
    Vec3 rel = va->pos-origin;
    if (rel.x > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.y > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.z > VoxelTerrainChunk::chunkSide*VoxelAtom::side ||
        rel.x <= 0 ||
        rel.y <= 0 ||
        rel.z <= 0
        )
    {
        std::cerr<<"WARNING!! - trying to create a VoxelAtom in a Chunk outside of it!!"<<std::endl;
        return;
    }
    int a,b,c;
    a = rel.x/VoxelAtom::side;
    b = rel.y/VoxelAtom::side;
    c = rel.z/VoxelAtom::side;
    VoxelAtom * tmp;
    if (matrix[a][b][c]==NULL)
    {
        matrix[a][b][c] = va;
        if (a>0)
        {
            if ( (tmp = matrix[a-1][b][c])!=NULL)
            {
                if ( tmp->faces[PX].active )
                {
                    tmp->faces[PX].active = false;
                    activeFaces[PX].remove(&(tmp->faces[PX]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[MX].active = true;
                activeFaces[MX].push_back(&(va->faces[MX]));
                nActiveFaces++;
            }
        }
        if (a<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a+1][b][c])!=NULL)
            {
                if ( tmp->faces[MX].active )
                {
                    tmp->faces[MX].active = false;
                    activeFaces[MX].remove(&(tmp->faces[MX]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[PX].active = true;
                activeFaces[PX].push_back(&(va->faces[PX]));
                nActiveFaces++;
            }
        }
        if (a==0)
        {
            va->faces[MX].active = true;
            activeFaces[MX].push_back(&(va->faces[MX]));
            nActiveFaces++;
        } else
        if (a==VoxelTerrainChunk::chunkSide-1)
        {
            va->faces[PX].active = true;
            activeFaces[PX].push_back(&(va->faces[PX]));
            nActiveFaces++;
        }
        if (b>0)
        {
            if ( (tmp = matrix[a][b-1][c])!=NULL)
            {
                if ( tmp->faces[PY].active )
                {
                    tmp->faces[PY].active = false;
                    activeFaces[PY].remove(&(tmp->faces[PY]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[MY].active = true;
                activeFaces[MY].push_back(&(va->faces[MY]));
                nActiveFaces++;
            }
        }
        if (b<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a][b+1][c])!=NULL)
            {
                if ( tmp->faces[MY].active )
                {
                    tmp->faces[MY].active = false;
                    activeFaces[MY].remove(&(tmp->faces[MY]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[PY].active = true;
                activeFaces[PY].push_back(&(va->faces[PY]));
                nActiveFaces++;
            }
        }
        if (b==0)
        {
            va->faces[MY].active = true;
            activeFaces[MY].push_back(&(va->faces[MY]));
            nActiveFaces++;
        } else
        if (b==VoxelTerrainChunk::chunkSide-1)
        {
            va->faces[PY].active = true;
            activeFaces[PY].push_back(&(va->faces[PY]));
            nActiveFaces++;
        }
        if (c>0)
        {
            if ( (tmp = matrix[a][b][c-1])!=NULL)
            {
                if ( tmp->faces[PZ].active )
                {
                    tmp->faces[PZ].active = false;
                    activeFaces[PZ].remove(&(tmp->faces[PZ]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[MZ].active = true;
                activeFaces[MZ].push_back(&(va->faces[MZ]));
                nActiveFaces++;
            }
        }
        if (c<VoxelTerrainChunk::chunkSide-1)
        {
            if ( (tmp = matrix[a][b][c+1])!=NULL)
            {
                if ( tmp->faces[MZ].active )
                {
                    tmp->faces[MZ].active = false;
                    activeFaces[MZ].remove(&(tmp->faces[MZ]));
                    nActiveFaces--;
                }
            } else
            {
                va->faces[PZ].active = true;
                activeFaces[PZ].push_back(&(va->faces[PZ]));
                nActiveFaces++;
            }
        }
        if (c==0)
        {
            va->faces[MZ].active = true;
            activeFaces[MZ].push_back(&(va->faces[MZ]));
            nActiveFaces++;
        } else
        if (c==VoxelTerrainChunk::chunkSide-1)
        {
            va->faces[PZ].active = true;
            activeFaces[PZ].push_back(&(va->faces[PZ]));
            nActiveFaces++;
        }
    } else
    {
        std::cerr<<"WARINIG!! - trying to add a VoxelAtom where already exists one!!"<<std::endl;
    }
}

void VoxelTerrainChunk::update(Uint32 elapsedTime)
{
    //per ora non fa nulla
}
void VoxelTerrainChunk::render()
{
    if (false)
    {
        glPushAttrib(GL_ENABLE_BIT);
    //    glDisable(GL_TEXTURE_2D);
    //    std::cout<<"rendering a face...  pos:"<<owner->pos.x<<", "<<owner->pos.y<<", "<<owner->pos.z<<"  ("<<baseList+orientation<<")"<<std::endl;
    //    std::cout<<"color: "<<color[0]<<" "<<color[1]<<" "<<color[2]<<" "<<color[3]<<std::endl;
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_LINE);
    //    glEnable(GL_POLYGON_OFFSET_LINE); // Avoid Stitching!
    //    glPolygonOffset(0.01,0.01);
    //    std::cout<<"render sul chunk"<<std::endl;
        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, AtomFace::terrain_atlas->texId);
    //    glColor4f(1,1,1,1);
        for (int i=0 ; i<6 ; i++ )
        {
            for (std::list<AtomFace*>::iterator it=activeFaces[i].begin(); it!=activeFaces[i].end(); ++it)
            {
    //            std::cout<<"chiamo un render"<<std::endl;
    //            (*it)->renderNoTexture(); //commentato per mettere il codice OGL qua
                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                    glTranslated((*it)->owner->pos.x, (*it)->owner->pos.y, (*it)->owner->pos.z);
                    glBegin(GL_QUADS);
    //                    glColor4fv((*it)->color);
                        glCallList(AtomFace::baseList+(*it)->orientation);
                    glEnd();
                glMatrixMode(GL_MODELVIEW);
                glPopMatrix();
    //            std::cout<<"--------"<<std::endl;
            }
        }
        glMatrixMode(GL_PROJECTION);
        glPopAttrib();
    } else
    {
        /**
         * Test rendering with array
         */
        //std::cout<<this->nActiveFaces<<" active faces"<<std::endl;
        float hs = VoxelAtom::side/2.0f;
        int i=0;
        for (int dir=0 ; dir<6 ; dir++ )
        {
            for (std::list<AtomFace*>::iterator it=activeFaces[dir].begin(); it!=activeFaces[dir].end(); ++it)
            {
                //std::cout<<"building "<<i<<" face"<<std::endl;
                int j = i*20;
                int d=0;
                switch (dir)
                {
                    case 0: //PX
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;
                        break;
                    case 1: //MX
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;
                        break;
                    case 2: // PY
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;
                        break;
                    case 3: //MY
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
//                        vArray[j+d++]= 0;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;
                        break;
                    case 4: //PZ
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 1;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z+hs;
                        break;
                    case 5: //MZ
                        vArray[j+d++]=0;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
                        vArray[j+d++]=(*it)->owner->pos.x+hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1-0.0625;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y-hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;

                        vArray[j+d++]=0.0625;
                        vArray[j+d++]=1;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= 0;
//                        vArray[j+d++]= -1;
                        vArray[j+d++]=(*it)->owner->pos.x-hs;
                        vArray[j+d++]=(*it)->owner->pos.y+hs;
                        vArray[j+d++]=(*it)->owner->pos.z-hs;
                        break;
                    default:
                        std::cerr<<"Errore!! - trying to render a face with dir indicator: "<<dir<<std::endl;
                        break;
                }
                i++;
            }
        }
//        std::cout<<i<<" faces builded"<<std::endl;
        glPushAttrib(GL_ENABLE_BIT);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_LINE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, AtomFace::terrain_atlas->texId);
        //glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
//        glInterleavedArrays( GL_T2F_N3F_V3F,8*sizeof(GLfloat),&vArray[0]);
        glInterleavedArrays( GL_T2F_V3F,5*sizeof(GLfloat),&vArray[0]);
        glDrawArrays(GL_QUADS, 0, /*this->nActiveFaces*/i*4);
        glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
        glPopAttrib();
    }
}
