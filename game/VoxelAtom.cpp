#include "AtomFace_VoxelAtom.h"

const float VoxelAtom::side = 0.1f;

VoxelAtom::VoxelAtom()
{
    color[0]=color[1]=color[2]=color[3]=1;
    faces[PX] = AtomFace(this, PX, color);
    faces[MX] = AtomFace(this, MX, color);
    faces[PY] = AtomFace(this, PY, color);
    faces[MY] = AtomFace(this, MY, color);
    faces[PZ] = AtomFace(this, PZ, color);
    faces[MZ] = AtomFace(this, MZ, color);
}
VoxelAtom::VoxelAtom(Vec3 pos, GLfloat color[4])
{
    this->pos = pos;
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
    faces[PX] = AtomFace(this, PX, color);
    faces[MX] = AtomFace(this, MX, color);
    faces[PY] = AtomFace(this, PY, color);
    faces[MY] = AtomFace(this, MY, color);
    faces[PZ] = AtomFace(this, PZ, color);
    faces[MZ] = AtomFace(this, MZ, color);

}
void VoxelAtom::setColor(GLfloat color[4])
{
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
    faces[PX].setColor(color);
    faces[MX].setColor(color);
    faces[PY].setColor(color);
    faces[MY].setColor(color);
    faces[PZ].setColor(color);
    faces[MZ].setColor(color);
}