#include <stdint.h>

#include "GameManager.h"
#include "../graphics/Bounder.h"
#include "../game/VoxelTerrainChunkOfChunks.h"
#include <iostream>
#include <limits>

#define N_REND_AND_GO 500000

#ifdef WIN32
	#undef max
#endif

PerspectiveCamera* GameManager::activeCamera = NULL;
Uint32 GameManager::maxFreeID=0;
Uint32 GameManager::maxFreeRenderableIndex=0;
bool GameManager::renderBounders = false;
Renderable** initRL()
{
    Renderable** tmp = new Renderable*[N_REND_AND_GO];
    memset(tmp, 0, N_REND_AND_GO);
    return tmp;
}
#ifdef WIN32
Renderable** GameManager::renderables = initRL();
#else
Renderable** GameManager::renderables(initRL());
#endif
GameObject** initGOL()
{
    GameObject** tmp = new GameObject*[N_REND_AND_GO];
    memset(tmp, NULL, N_REND_AND_GO);
    return tmp;
}
#ifdef WIN32
GameObject** GameManager::game_objects = initGOL();
#else
GameObject** GameManager::game_objects(initGOL());
#endif
std::list<Uint32> initFIL()
{
    std::list<Uint32> tmp;
    return tmp;
}
#ifdef WIN32
std::list<Uint32> GameManager::freed_ids = initFIL();
#else
std::list<Uint32> GameManager::freed_ids(initFIL());
#endif

void GameManager::render()
{
    for (int i=0 ; i<N_REND_AND_GO ; i++ )
    {
//        if (GameManager::renderables[i]!=NULL)
//            std::cout<<GameManager::renderables[i]->bounder->collide(&(activeCamera->frustum))<<std::endl;
//        std::cout<<"renderables["<<i<<"]: ";//<<renderables[i]<<std::endl;
//        std::cout<<"maxFreeRenderableIndex: "<<maxFreeRenderableIndex<<std::endl;
//        std::cout<<"level of this: "<<dynamic_cast<VoxelTerrainChunkOfChunks*>(renderables[i])->getLevel()<<std::endl;
//        std::cout<<"renderables["<<i<<"]->bounder: "<<renderables[i]->bounder<<std::endl;
        if (GameManager::renderables[i]!=NULL && GameManager::renderables[i]->visible && (GameManager::renderables[i]->bounder==NULL || GameManager::renderables[i]->bounder->collide(&(activeCamera->frustum))!=COLLISION_OUT) )
        {
            GameManager::renderables[i]->render();
            if (GameManager::renderBounders && GameManager::renderables[i]->bounder!=NULL)
            {
                GameManager::renderables[i]->bounder->render();
            }
        }
    }
}

void GameManager::update(Uint32 elapsedTime)
{
    for (int i=0 ; i<N_REND_AND_GO ; i++ )
    {
        if (GameManager::game_objects[i]!=NULL && GameManager::game_objects[i]->active)
        {
            GameManager::game_objects[i]->update(elapsedTime);
        }
    }
}

bool GameManager::registerGameObject(GameObject * go)
{
    //TODO dovrebbe essere UINT32_MAX ma non mi va quindi per prova metto un valore io
    if (GameManager::maxFreeID==N_REND_AND_GO)
    {
        std::list<Uint32>::iterator it=GameManager::freed_ids.begin();
        if(it!=GameManager::freed_ids.end())
        {
            go->id=*it;
            GameManager::freed_ids.pop_front();
        } else
        {
            return false;
        }
    } else
    {
        go->id=maxFreeID++;
    }
    GameManager::game_objects[go->id]=go;
    return true;
}

bool GameManager::registerRenderable(Renderable * r)
{
    //TODO la registrazione dei renderable è diverad da quella dei game object
    if (GameManager::maxFreeRenderableIndex<N_REND_AND_GO)
    {
        GameManager::renderables[GameManager::maxFreeRenderableIndex++]=r;
        std::cout<<"renderable registered"<<std::endl;
        return true;
    }
    return false;
}

void GameManager::deleteGameObject(GameObject * go)
{
    GameManager::game_objects[go->id]=NULL;
    freed_ids.push_back(go->id);
}

void GameManager::deleteRenderable(Renderable * r)
{
    for (int i=0; i<N_REND_AND_GO ; i++ )
    {
        if (GameManager::renderables[i]==r)
        {
            GameManager::renderables[i]=NULL;
        }
    }
}

bool GameManager::rayCast(Vec3 start, Vec3 dir, Vec3 * intersection, Renderable**intersected)
{
    float tmp_dist, min_dist = std::numeric_limits<float>::max();
    Vec3 tmp_inter;
    for (int i=0 ; i<N_REND_AND_GO ; i++ )
    {
        if (    GameManager::renderables[i]!=NULL &&
                GameManager::renderables[i]->visible &&
                GameManager::renderables[i]->intersectable &&
                (tmp_dist=GameManager::renderables[i]->rayIntersect(start, dir, &tmp_inter))>0 &&
                tmp_dist<min_dist )
        {
            min_dist = tmp_dist;
            *intersection = tmp_inter;
            *intersected = GameManager::renderables[i];
        }
    }
    return min_dist!=std::numeric_limits<float>::max();
}
