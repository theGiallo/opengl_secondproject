/* 
 * File:   GameManager.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 17 marzo 2011, 21.46
 */

#ifndef GAMEMANAGER_H
#define	GAMEMANAGER_H

#include <stdlib.h>
#include <list>
#include "../game/GameObject.h"
#include "../graphics/Renderable.h"
#include "../graphics/Camera.h"
#include <SDL/SDL.h>

class GameManager
{
protected:
    static GameObject** game_objects;
    static std::list<Uint32> freed_ids;
    static Uint32 maxFreeID;
    static Uint32 maxFreeRenderableIndex;
    static Renderable** renderables; // per debug TODO mettere public
public:
    static bool renderBounders;
    static PerspectiveCamera* activeCamera;
    static void update(Uint32 elapsedTime);
    static void render(void);
    static bool registerGameObject(GameObject * go);
    static bool registerRenderable(Renderable * r);
    static void deleteGameObject(GameObject * go);
    static void deleteRenderable(Renderable * r);
    static bool rayCast(Vec3 start, Vec3 dir, Vec3 * intersection, Renderable**intersected);
};

#endif	/* GAMEMANAGER_H */

