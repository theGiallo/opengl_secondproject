#include "VoxelCube.h"
#include <iostream>
VoxelCube::VoxelCube()
{
    GLfloat color[4] = {1,1,1,1};
    *this = VoxelCube(Vec3(), 1.0, color);
}

VoxelCube::VoxelCube(Vec3 pos, float side, GLfloat color[4])
{
    this->cube = Cube(pos, side, color);
    this->visible = true;
}

void VoxelCube::update(Uint32 elapsedTime)
{

}
void VoxelCube::render()
{
    cube.render();
}