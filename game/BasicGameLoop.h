#ifndef BAISCGAMELOOP_H
#define BAISCGAMELOOP_H

#include "IGameLoop.h"

class BasicGameLoop : public IGameLoop
{
protected:
    ILoopable& loopable;
public:
    BasicGameLoop(ILoopable& loopable);
    void setLoopable(ILoopable& loopable);
    void init(void);
    void run(void);
};

#endif /* BAISCGAMELOOP_H */