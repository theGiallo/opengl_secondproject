/*
 * LoopableTest3DModels.h
 *
 *  Created on: 21/apr/2013
 *      Author: thegiallo
 */

#ifndef LOOPABLETEST3DMODELS_H_
#define LOOPABLETEST3DMODELS_H_

#include "ILoopable.h"

class LoopableTest3DModels: public ILoopable
{
public:
	LoopableTest3DModels();
	virtual ~LoopableTest3DModels();
	virtual const Uint8* getKeysMap_const(void) const;
	virtual Uint8* getKeysMap(void);
	virtual void setKeysMap(Uint8* keys);

	virtual void onEvent(SDL_Event* event);
	virtual void keyChecks(void); /// checks the keymap
	virtual void onLoop(Uint32 elapsedTime);
	virtual void onRender(void);
	virtual void init(void);
	virtual void cleanUp(void);
	virtual bool isLoopable(void); /// if false this won't loop any more
};

#endif /* LOOPABLETEST3DMODELS_H_ */
