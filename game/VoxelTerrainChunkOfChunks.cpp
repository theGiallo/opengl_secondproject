/* 
 * File:   VoxelTerrainChunkOfChunks.cpp
 * Author: thegiallo
 * Directory: game
 * 
 * Created on 8 settembre 2011, 17.05
 */

#include "VoxelTerrainChunkOfChunks.h"
#include "../graphics/BoundingBox.h"
#include "../graphics/TextureManager.h"
#include <iostream>
#include <cfloat>
#include <algorithm>
#include "../game/GameManager.h"

#define POSTOXINDEX(v) ((int)(((v).x-origin.x)/(side/(float)chunkSide)))
#define POSTOYINDEX(v) ((int)(((v).y-origin.y)/(side/(float)chunkSide)))
#define POSTOZINDEX(v) ((int)(((v).z-origin.z)/(side/(float)chunkSide)))

#define IS_CHUNK_FREE(a,b,c) (matrix[a][b][c]==NULL)
#define IS_CHUNK_OUTSIDE(v) ((v).x-origin.x<0 || (v).x-origin.x>this->getSide() || (v).y-origin.y<0 || (v).y-origin.y>this->getSide() || (v).z-origin.z<0 || (v).z-origin.z>this->getSide() )

GLuint ChunkFace::baseList = 0;
Texture* ChunkFace::terrain_atlas = NULL;
///**
// * non usato
// */
//void ChunkFace::initializeRenderingProcedure()
//{
//    float hs = VoxelAtom::side/2.0;
//    ChunkFace::baseList = glGenLists(6);
//    std::cout<<"baseList: "<<baseList<<std::endl;
//    glNewList(baseList+PX, GL_COMPILE);
////        glNormal3f(1,0,0);
//        glTexCoord2d(0, 1);
//        glVertex3f(hs,-hs,hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(hs,hs,-hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(hs,hs,hs);
//    glEndList();
//    glNewList(baseList+MX, GL_COMPILE);
////        glNormal3f(-1,0,0);
//        glTexCoord2d(0, 1);
//        glVertex3f(-hs,hs,hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(-hs,hs,-hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(-hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(-hs,-hs,hs);
//    glEndList();
//    glNewList(baseList+PY, GL_COMPILE);
////        glNormal3f(0,1,0);
//        glTexCoord2d(0, 1);
//        glVertex3f(hs,hs,hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(hs,hs,-hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(-hs,hs,-hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(-hs,hs,hs);
//    glEndList();
//    glNewList(baseList+MY, GL_COMPILE);
////        glNormal3f(0,-1,0);
//        glTexCoord2d(0, 1);
//        glVertex3f(-hs,-hs,hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(-hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(hs,-hs,hs);
//    glEndList();
//    glNewList(baseList+PZ, GL_COMPILE);
////        glNormal3f(0,0,1);
//        glTexCoord2d(0, 1);
//        glVertex3f(-hs,hs,hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(-hs,-hs,hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(hs,-hs,hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(hs,hs,hs);
//    glEndList();
//    glNewList(baseList+MZ, GL_COMPILE);
////        glNormal3f(0,0,-1);
//        glTexCoord2d(0, 1);
//        glVertex3f(hs,hs,-hs);
//        glTexCoord2d(0, 1-0.0625);
//        glVertex3f(hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1-0.0625);
//        glVertex3f(-hs,-hs,-hs);
//        glTexCoord2d(0.0625, 1);
//        glVertex3f(-hs,hs,-hs);
//    glEndList();
//}
void ChunkFace::initializeTexture()
{
//    if (!terrain_atlas.load("./resources/terrain_textures/terrain_atlas.png"))
//    {
//        std::cerr<<"WARNING!! - could not load terrain atlas!!"<<std::endl;
//    } else
//    {
//        std::cout<<"terrain atlas texture id = "<<terrain_atlas.texId<<std::endl;
//    }

    ChunkFace::terrain_atlas = TextureManager::loadTexture("./resources/terrain_textures/terrain_atlas.png");
    if (ChunkFace::terrain_atlas==NULL)
    {
        std::cerr<<"terrain atlas == NULL!!!"<<std::endl;

    } else
    {
        std::cout<<"terrain atlas txt id = "<<ChunkFace::terrain_atlas->texId<<std::endl;
    }
}
///**
// * non usato
// */
//void ChunkFace::renderNoTexture()
//{
//    if (owner==NULL)
//    {
//        std::cerr<<"ERROR!"<<std::endl<<"\ttrying to render a face with no owner";
//    }
//    Vec3 pos = owner->getPos();
//    glPushAttrib(GL_ENABLE_BIT);
//    glDisable(GL_TEXTURE_2D);
////    std::cout<<"rendering a face...  pos:"<<pos.x<<", "<<pos.y<<", "<<pos.z<<"  ("<<baseList+orientation<<")"<<std::endl;
////    std::cout<<"color: "<<color[0]<<" "<<color[1]<<" "<<color[2]<<" "<<color[3]<<std::endl;
////    glPolygonMode(GL_FRONT, GL_FILL);
//    glPolygonMode(GL_FRONT, GL_LINE);
//    glPolygonMode(GL_BACK, GL_NONE);
////    glEnable(GL_POLYGON_OFFSET_LINE); // Avoid Stitching!
////    glPolygonOffset(0.01,0.01);
//    glMatrixMode(GL_MODELVIEW);
//    glPushMatrix();
//        glTranslated(pos.x, pos.y, pos.z);
//        glBegin(GL_QUADS);
//            glColor4fv(color);
//            glCallList(baseList+orientation);
//        glEnd();
//    glMatrixMode(GL_MODELVIEW);
//    glPopMatrix();
//    glMatrixMode(GL_PROJECTION);
//    glPopAttrib();
//}

ChunkFace::ChunkFace()
{
    owner = NULL;
    orientation = PX;
    active = false;
//    this->sideFacesCount = 1;
}

ChunkFace::ChunkFace(GenericVoxelTerrainChunk* owner, OrientationType orientation/*, GLfloat color[4]*/)
{
    this->owner = owner;
    this->orientation = orientation;
//    this->color[0] = color[0];
//    this->color[1] = color[1];
//    this->color[2] = color[2];
//    this->color[3] = color[3];
    active = false;
//    this->sideFacesCount = owner->getSide()/VoxelAtom::side;
}

//void ChunkFace::setColor(GLfloat color[4])
//{
//    this->color[0] = color[0];
//    this->color[1] = color[1];
//    this->color[2] = color[2];
//    this->color[3] = color[3];
//}

GenericVoxelTerrainChunk * ChunkFace::selectionNoCheck(Vec3 start, Vec3 dir, float* in_dist)
{
    if (owner==NULL)
    {
        std::cerr<<"ERROR!"<<std::endl<<"\ttesting selection on a face with no owner";
    }
    Vec3 pos = owner->getPos();
    float d = owner->getSide()/2.0f;
    float s = 1;
    Vec3 inter;
    /**
     * code is repeted to avoid the border case
     */
    switch (orientation)
    {
        case MX:
            s =-1;
            inter = start+dir*((pos.x+d*s-start.x)/dir.x);
            if (inter.y<=pos.y+d && inter.y>pos.y-d &&
                inter.z<=pos.z+d && inter.z>pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case PX:
            inter = start+dir*((pos.x+d*s-start.x)/dir.x);
            if (inter.y<pos.y+d && inter.y>=pos.y-d &&
                inter.z<pos.z+d && inter.z>=pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case MY:
            s = -1;
            inter = start+dir*((pos.y+d*s-start.y)/dir.y);
            if (inter.x<pos.x+d && inter.x>= pos.x-d &&
                inter.z<=pos.z+d && inter.z>pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case PY:
            inter = start+dir*((pos.y+d*s-start.y)/dir.y);
            if (inter.x<=pos.x+d && inter.x> pos.x-d &&
                inter.z<pos.z+d && inter.z>=pos.z-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case MZ:
            s = -1;
            inter = start+dir*((pos.z+d*s-start.z)/dir.z);
            if (inter.x<pos.x+d && inter.x>=pos.x-d &&
                inter.y<pos.y+d && inter.y>=pos.y-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        case PZ:
            inter = start+dir*((pos.z+d*s-start.z)/dir.z);
            if (inter.x<=pos.x+d && inter.x> pos.x-d &&
                inter.y<=pos.y+d && inter.y>pos.y-d)
            {
                *in_dist = (inter-start).getModule();
                return owner;
            }
            break;
        default:
            std::cerr<<"ERROR!"<<std::endl<<"\ttesting selection with unexistent orientation";
            break;
    }
    return NULL;
}

const float VoxelAtom::side = 0.1f;

VoxelAtom::VoxelAtom()
{
//    color[0]=color[1]=color[2]=color[3]=1;
    faces[PX] = ChunkFace(this, PX/*, color*/);
    faces[MX] = ChunkFace(this, MX/*, color*/);
    faces[PY] = ChunkFace(this, PY/*, color*/);
    faces[MY] = ChunkFace(this, MY/*, color*/);
    faces[PZ] = ChunkFace(this, PZ/*, color*/);
    faces[MZ] = ChunkFace(this, MZ/*, color*/);
    can_br_as_cube = true;
    level = 0;
}
VoxelAtom::VoxelAtom(Vec3 pos/*, GLfloat color[4]*/)
{
    this->pos = pos;
//    this->color[0] = color[0];
//    this->color[1] = color[1];
//    this->color[2] = color[2];
//    this->color[3] = color[3];
    faces[PX] = ChunkFace(this, PX/*, color*/);
    faces[MX] = ChunkFace(this, MX/*, color*/);
    faces[PY] = ChunkFace(this, PY/*, color*/);
    faces[MY] = ChunkFace(this, MY/*, color*/);
    faces[PZ] = ChunkFace(this, PZ/*, color*/);
    faces[MZ] = ChunkFace(this, MZ/*, color*/);
    can_br_as_cube = true;
    level = 0;
}
//void VoxelAtom::setColor(GLfloat color[4])
//{
//    this->color[0] = color[0];
//    this->color[1] = color[1];
//    this->color[2] = color[2];
//    this->color[3] = color[3];
//    faces[PX].setColor(color);
//    faces[MX].setColor(color);
//    faces[PY].setColor(color);
//    faces[MY].setColor(color);
//    faces[PZ].setColor(color);
//    faces[MZ].setColor(color);
//}

//VoxelTerrainChunkOfChunks::VoxelTerrainChunkOfChunks()
//{
//    for (int i=0 ; i<VoxelTerrainChunkOfChunks::chunkSide ; i++ )
//    {
//        for (int j=0 ; j<VoxelTerrainChunkOfChunks::chunkSide ; j++ )
//        {
//            for (int k=0 ; k<VoxelTerrainChunkOfChunks::chunkSide ; k++ )
//            {
//                matrix[i][j][k] = NULL;
//            }
//        }
//    }
//    GLfloat color[4] = {1,1,1,1};
//    faces[PX] = ChunkFace(this, PX, color);
//    faces[MX] = ChunkFace(this, MX, color);
//    faces[PY] = ChunkFace(this, PY, color);
//    faces[MY] = ChunkFace(this, MY, color);
//    faces[PZ] = ChunkFace(this, PZ, color);
//    faces[MZ] = ChunkFace(this, MZ, color);
//    nActiveFaces = 0;
//    float side = getSide();
//    this->bounder = new BoundingBox(origin+side/2.0, side);
//    for (int i=0 ; i<6 ; i++ )
//    {
//        adjacentChunks[i] = NULL;
//    }
//    level = 1;
//    father = NULL;
////    if (father==NULL && masterFather==NULL)
////    {
////        masterFather = this;
////    }
//}
VoxelTerrainChunkOfChunks::VoxelTerrainChunkOfChunks(unsigned int level, Vec3 origin, VoxelTerrainChunkOfChunks* father)
{
    for (int i=0 ; i<VoxelTerrainChunkOfChunks::chunkSide ; i++ )
    {
        for (int j=0 ; j<VoxelTerrainChunkOfChunks::chunkSide ; j++ )
        {
            for (int k=0 ; k<VoxelTerrainChunkOfChunks::chunkSide ; k++ )
            {
                matrix[i][j][k] = NULL;
            }
        }
    }
    GLfloat color[4] = {1,1,1,1};
    faces[PX] = ChunkFace(this, PX/*, color*/);
    faces[MX] = ChunkFace(this, MX/*, color*/);
    faces[PY] = ChunkFace(this, PY/*, color*/);
    faces[MY] = ChunkFace(this, MY/*, color*/);
    faces[PZ] = ChunkFace(this, PZ/*, color*/);
    faces[MZ] = ChunkFace(this, MZ/*, color*/);
    nActiveFaces = 0;
    can_br_as_cube = false;
    nChunksMaybeAsCube = 0;
    this->level = level;
    side = calcSide();
    this->bounder = new BoundingBox(origin+side/2.0f, side);
    active = visible = intersectable = true;
    for (int i=0 ; i<6 ; i++ )
    {
        adjacentChunks[i] = NULL;
    }
    this->father = father;
    this->setOrigin(origin);
    if (father==NULL && masterFather==NULL)
    {
        setMasterFather(this);
    }
}

//moved to header const unsigned int VoxelTerrainChunkOfChunks::chunkSide = 10;
const float VoxelTerrainChunkOfChunks::levelOneSide = VoxelTerrainChunkOfChunks::chunkSide*VoxelAtom::side;

bool VoxelTerrainChunkOfChunks::canBeRenderedAsCube()
{
    //TODO stabilire un modo per determinarlo con meno cubi del 100%
    return nChunksMaybeAsCube>=chunkSide*chunkSide*chunkSide*0.5;
}

void VoxelTerrainChunkOfChunks::setOrigin(Vec3 origin)
{
    this->origin = origin;
    pos = origin+this->getSide()/2.0f;
    dynamic_cast<BoundingBox*>(this->bounder)->center = pos;
}

void VoxelTerrainChunkOfChunks::setPos(Vec3 pos)
{
    this->origin = pos-this->getSide()/2.0f;
    this->pos = pos;
    dynamic_cast<BoundingBox*>(this->bounder)->center = pos;
}

ChunkModEnum VoxelTerrainChunkOfChunks::removeChunk(GenericVoxelTerrainChunk * ck)
{
    //TODO removeChunk
    //TODO se viene rimosso un chunk con dentro della roba non aggiorna le facce
    if (ck==NULL)
    {
        std::cerr<<"ERROR!"<<std::endl<<"NULL passed to addChunk"<<std::endl;
        return NOMOD;
    }
    unsigned int mID[3] = {POSTOXINDEX(ck->getPos()),POSTOYINDEX(ck->getPos()),POSTOZINDEX(ck->getPos())};
    if (IS_CHUNK_OUTSIDE(ck->getPos()) || IS_CHUNK_FREE(mID[0], mID[1], mID[2]))
    {
        return NOMOD;
    }
    ChunkModEnum ret;
    if (ck->getLevel()==level-1)
    {
        //TODO aggiungere le facce dei sottociank adiacenti
        checkNearbyFaces(mID, false, matrix[mID[0]][mID[1]][mID[2]]->canBeRenderedAsCube());
        if (ck->canBeRenderedAsCube()) //TODo verificare/fare in modo che chiami il metodo di più alto livello
        {
            std::cout<<"ccbrac removed"<<std::endl;
            nChunksMaybeAsCube--;
            if (can_br_as_cube && !this->canBeRenderedAsCube())
            {
            std::cout<<"mod_cas!"<<std::endl;
                can_br_as_cube = false;
                ret =  MOD_CAS;
            }
        } else
        {
            ret = MOD_NOCAS;
        }
        delete matrix[mID[0]][mID[1]][mID[2]];
        matrix[mID[0]][mID[1]][mID[2]] = NULL;
    } else
    {
        bool wasRenderableAsChunk = matrix[mID[0]][mID[1]][mID[2]]->canBeRenderedAsCube();
        ret = dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[mID[0]][mID[1]][mID[2]])->removeChunk(ck);
        if (ret==MOD_CAS)
        {
            nChunksMaybeAsCube--;
            checkNearbyFaces(mID, false, wasRenderableAsChunk);
            if (can_br_as_cube && !this->canBeRenderedAsCube())
            {
                can_br_as_cube = false;
                ret =  MOD_CAS;
            }
        }
    }
    return ret;
}
ChunkModEnum VoxelTerrainChunkOfChunks::addChunk(GenericVoxelTerrainChunk * ck)
{
    //TODO se viene aggiunto un chunk con dentro della roba non aggiorna le facce
    if (ck==NULL)
    {
        std::cerr<<"ERROR!"<<std::endl<<"NULL passed to addChunk"<<std::endl;
        return NOMOD;
    }
    if (IS_CHUNK_OUTSIDE(ck->getPos()))
    {
        std::cout<<"outside! "; ck->getPos().print();
        if (this!=VoxelTerrainChunkOfChunks::masterFather)
        {
            std::cerr<<"ERROR!"<<std::endl<<"trying to add a chunk outside of a chunk != masterFather"<<std::endl;
            return NOMOD;
        }
        setMasterFather(new VoxelTerrainChunkOfChunks(level+1, origin-(chunkSide/2)*side));
        this->father = masterFather;
        dynamic_cast<VoxelTerrainChunkOfChunks*>(this->father)->addChunk(this);
        return dynamic_cast<VoxelTerrainChunkOfChunks*>(this->father)->addChunk(ck);
    }
    unsigned int mID[3] = {POSTOXINDEX(ck->getPos()),POSTOYINDEX(ck->getPos()),POSTOZINDEX(ck->getPos())};
    if (IS_CHUNK_FREE(POSTOXINDEX(ck->getPos()), POSTOYINDEX(ck->getPos()), POSTOZINDEX(ck->getPos())))
    {
        if (level==1)
        {
            if (!dynamic_cast<VoxelAtom*>(ck))
            {
                std::cerr<<"ERROR!"<<std::endl<<"trying to add a VoxelTerrainChunkOfChunks to a level 1"<<std::endl;
                return NOMOD;
            }
            matrix[mID[0]][mID[1]][mID[2]] = ck;
            ck->father = this;
            nChunksMaybeAsCube++;
            checkNearbyFaces(mID, true);
            if (!can_br_as_cube && this->canBeRenderedAsCube())
            {
//            std::cout<<"MOD_CAS"<<std::endl;
                can_br_as_cube = true;
                return MOD_CAS;
            }
//            std::cout<<"MOD_NOCAS"<<std::endl;
            return MOD_NOCAS;
        }
        if (ck->getLevel()<level-1) //if is smaller chunk
        {
            matrix[mID[0]][mID[1]][mID[2]]
                    = new VoxelTerrainChunkOfChunks(level-1, origin+Vec3(POSTOXINDEX(ck->getPos())*this->calcSide(level-1), POSTOYINDEX(ck->getPos())*this->calcSide(level-1), POSTOZINDEX(ck->getPos())*this->calcSide(level-1)), this);
            checkNearbyFaces(mID, true);
            ChunkModEnum res = dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[mID[0]][mID[1]][mID[2]])->addChunk(ck);
            if (res==NOMOD || res==MOD_NOCAS)
            {
                return res;
            }
            if (res==MOD_CAS)
            {
                nChunksMaybeAsCube++;
                checkNearbyFaces(mID, true);
                if (!can_br_as_cube && this->canBeRenderedAsCube())
                {
                    can_br_as_cube = true;
                    return MOD_CAS;
                }
                return MOD_NOCAS;
            }
        } else
        {
            matrix[mID[0]][mID[1]][mID[2]] = ck;
            ck->father = this;
            checkNearbyFaces(mID, true);
            if (ck->canBeRenderedAsCube())
            {
                nChunksMaybeAsCube++;
            }
            if (!can_br_as_cube && this->canBeRenderedAsCube())
            {
                can_br_as_cube = true;
                return MOD_CAS;
            }
            return MOD_NOCAS;
        }
    } else
    {
        if (ck->getLevel()<level && level!=1) //if is smaller chunk
        {
//            std::cout<<"to a smaller one"<<std::endl;
            ChunkModEnum res = dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[mID[0]][mID[1]][mID[2]])->addChunk(ck);
            if (res==NOMOD || res==MOD_NOCAS)
            {
                return res;
            }
            if (res==MOD_CAS)
            {
                nChunksMaybeAsCube++;
                checkNearbyFaces(mID, true);
                if (!can_br_as_cube && this->canBeRenderedAsCube())
                {
                    can_br_as_cube = true;
                    return MOD_CAS;
                }
                return MOD_NOCAS;
            }
        }
        std::cerr<<"ERROR!"<<std::endl<<"trying to add a chunk in an occupied slot!"<<std::endl;
        return NOMOD;
    }
    return 666;
}

void VoxelTerrainChunkOfChunks::checkNearbyFaces(unsigned int mID[3], bool added, bool wasRenderableAsChunk)
{
    for (int i=0 ; i<3 ; i++)
    {
        if (mID[i] >= chunkSide || mID[i] < 0)
        {
            return;
        }
    }
    GenericVoxelTerrainChunk** nears = matrix[mID[0]][mID[1]][mID[2]]->adjacentChunks;
    if (added)
    {
        // X
        if (mID[0]+1== chunkSide)
        {
            if (this->adjacentChunks[PX]==NULL)
            {
                nears[PX] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PX]))
            {
                nears[PX] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PX])->matrix[0][mID[1]][mID[2]];
            }
            nears[MX] = matrix[mID[0]-1][mID[1]][mID[2]];
        } else
        if (mID[0] == 0)
        {
            if (this->adjacentChunks[MX]==NULL)
            {
                nears[MX] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MX]))
            {
                nears[MX] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MX])->matrix[chunkSide-1][mID[1]][mID[2]];
            }
            nears[PX] = matrix[mID[0]+1][mID[1]][mID[2]];
        } else
        {
            nears[MX] = matrix[mID[0]-1][mID[1]][mID[2]];
            nears[PX] = matrix[mID[0]+1][mID[1]][mID[2]];
        }
        // Y
        if (mID[1]+1== chunkSide)
        {
            if (this->adjacentChunks[PY]==NULL)
            {
                nears[PY] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PY]))
            {
                nears[PY] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PY])->matrix[mID[0]][0][mID[2]];
            }
            nears[MY] = matrix[mID[0]][mID[1]-1][mID[2]];
        } else
        if (mID[1] == 0)
        {
            if (this->adjacentChunks[MY]==NULL)
            {
                nears[MY] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MY]))
            {
                nears[MY] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MY])->matrix[mID[0]][chunkSide-1][mID[2]];
            }
            nears[PY] = matrix[mID[0]][mID[1]+1][mID[2]];
        } else
        {
            nears[MY] = matrix[mID[0]][mID[1]-1][mID[2]];
            nears[PY] = matrix[mID[0]][mID[1]+1][mID[2]];
        }
        // Z
        if (mID[2]+1== chunkSide)
        {
            if (this->adjacentChunks[PZ]==NULL)
            {
                nears[PZ] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PZ]))
            {
                nears[PZ] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[PZ])->matrix[mID[0]][mID[1]][0];
            }
            nears[MZ] = matrix[mID[0]][mID[1]][mID[2]-1];
        } else
        if (mID[2] == 0)
        {
            if (this->adjacentChunks[MZ]==NULL)
            {
                nears[MZ] = NULL;
            } else
            if (dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MZ]))
            {
                nears[MZ] = dynamic_cast<VoxelTerrainChunkOfChunks*>(this->adjacentChunks[MZ])->matrix[mID[0]][mID[1]][chunkSide-1];
            }
            nears[PZ] = matrix[mID[0]][mID[1]][mID[2]+1];
        } else
        {
            nears[MZ] = matrix[mID[0]][mID[1]][mID[2]-1];
            nears[PZ] = matrix[mID[0]][mID[1]][mID[2]+1];
        }
    }
//    if (added)
//    {
//        for (int i=0 ; i<6 ; i++)
//        {
//            if (nears[i]!=NULL)
//            {
//                nears[i]->adjacentChunks[OPPOSITE_ORIENTATION(i)] = matrix[mID[0]][mID[1]][mID[2]];
//            }
//        }
//    }

    if (added?matrix[mID[0]][mID[1]][mID[2]]->canBeRenderedAsCube():wasRenderableAsChunk)
    {
        for (int i=0 ; i<6 ; i++)
        {
            if (nears[i]==NULL)
            {
                if (added)
                {
                    activeFaces[i].push_back(&matrix[mID[0]][mID[1]][mID[2]]->faces[i]);
                } else
                {
                    activeFaces[i].remove(&matrix[mID[0]][mID[1]][mID[2]]->faces[i]);
                }
                continue;
            }
            if (added)
            {
                if (nears[i]->canBeRenderedAsCube())
                {
                    dynamic_cast<VoxelTerrainChunkOfChunks*>(nears[i]->father)->activeFaces[OPPOSITE_ORIENTATION(i)].remove(&nears[i]->faces[OPPOSITE_ORIENTATION(i)]);
                } else
                {
                    activeFaces[i].push_back(&matrix[mID[0]][mID[1]][mID[2]]->faces[i]);
                }
                nears[i]->adjacentChunks[OPPOSITE_ORIENTATION(i)] = matrix[mID[0]][mID[1]][mID[2]];

            } else
            {
                if (nears[i]->canBeRenderedAsCube())
                {
                    dynamic_cast<VoxelTerrainChunkOfChunks*>(nears[i]->father)->activeFaces[OPPOSITE_ORIENTATION(i)].push_back(&nears[i]->faces[OPPOSITE_ORIENTATION(i)]);
                } else
                {
                    activeFaces[i].remove(&matrix[mID[0]][mID[1]][mID[2]]->faces[i]);
                }
                nears[i]->adjacentChunks[OPPOSITE_ORIENTATION(i)] = NULL;
            }
        }
    }
}

ChunkFace * VoxelTerrainChunkOfChunks::selection(Vec3 start, Vec3 dir, float * dist, unsigned int level)
{    
    GLfloat tmp_dist, min_dist = FLT_MAX;
    GenericVoxelTerrainChunk* tmp_va = NULL;
    std::pair<float, ChunkFace*> interFaces[3*chunkSide-2];
    int n_inters=0;
    ChunkFace* res = NULL;
    OrientationType orientations[3];
    int n;
    std::list<ChunkFace*>* candidates;
    bool this_level = this->level <= level+1;
//    std::cout<<"selection on level "<<this->level<<std::endl;
    if (dir.y == 0)
    {
        if (dir.z == 0)
        {
            n = 1;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.x>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MX];
                } else
                {
                    orientations[0] = MX;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PX];
                } else
                {
                    orientations[0] = PX;
                }
            }
        } else if (dir.x==0)
        {
            n = 1;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.z>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MZ];
                } else
                {
                    orientations[0] = MZ;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PZ];
                } else
                {
                    orientations[0] = PZ;
                }
            }
        } else
        {
            n = 2;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.x>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MX];
                } else
                {
                    orientations[0] = MX;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PX];
                } else
                {
                    orientations[0] = PX;
                }
            }
            if (dir.z>0)
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[MZ];
                } else
                {
                    orientations[1] = MZ;
                }
            } else
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[PZ];
                } else
                {
                    orientations[1] = PZ;
                }
            }
        }
    } else if (dir.x==0)
    {
        if (dir.z==0)
        {
            n = 1;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.y>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MY];
                } else
                {
                    orientations[0] = MY;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PY];
                } else
                {
                    orientations[0] = PY;
                }
            }
        } else
        {
            n = 2;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.y>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MY];
                } else
                {
                    orientations[0] = MY;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PY];
                } else
                {
                    orientations[0] = PY;
                }
            }
            if (dir.z>0)
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[MZ];
                } else
                {
                    orientations[1] = MZ;
                }
            } else
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[PZ];
                } else
                {
                    orientations[1] = PZ;
                }
            }
        }
    } else
    {
        if (dir.z==0)
        {
            n = 2;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.x>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MX];
                } else
                {
                    orientations[0] = MX;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PX];
                } else
                {
                    orientations[0] = PX;
                }
            }
            if (dir.y>0)
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[MY];
                } else
                {
                    orientations[1] = MY;
                }
            } else
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[PY];
                } else
                {
                    orientations[1] = PY;
                }
            }
        } else
        {
            n = 3;
            if (this_level)
            {
                candidates = new std::list<ChunkFace*>[n];
            }
            if (dir.x>0)
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[MX];
                } else
                {
                    orientations[0] = MX;
                }
            } else
            {
                if (this_level)
                {
                    candidates[0] = activeFaces[PX];
                } else
                {
                    orientations[0] = PX;
                }
            }
            if (dir.y>0)
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[MY];
                } else
                {
                    orientations[1] = MY;
                }
            } else
            {
                if (this_level)
                {
                    candidates[1] = activeFaces[PY];
                } else
                {
                    orientations[1] = PY;
                }
            }
            if (dir.z>0)
            {
                if (this_level)
                {
                    candidates[2] = activeFaces[MZ];
                } else
                {
                    orientations[2] = MZ;
                }
            } else
            {
                if (this_level)
                {
                    candidates[2] = activeFaces[PZ];
                } else
                {
                    orientations[2] = PZ;
                }
            }
        }
    }

    if (this_level)
    {
        for (int i=0 ; i<n ; i++)
        {
            for (std::list<ChunkFace*>::iterator it=candidates[i].begin(); it!=candidates[i].end(); ++it)
            {
                tmp_va = (*it)->selectionNoCheck(start, dir, &tmp_dist);
                if (tmp_va != NULL && tmp_dist>0 && tmp_dist<min_dist)
                {
                    res = *it;
                    min_dist = tmp_dist;
                }
            }
        }
        delete [] candidates;
        *dist = min_dist;
    } else
    {
        for (int i=0 ; i<chunkSide ; i++)
        {
            for (int j=0 ; j<chunkSide ; j++)
            {
                for (int k=0 ; k<chunkSide ; k++)
                {
                    if (matrix[i][j][k]==NULL)
                    {
                        continue;
                    }
                    for (int f=0 ; f<n ; f++)
                    {
                        tmp_va = matrix[i][j][k]->faces[orientations[f]].selectionNoCheck(start, dir, &tmp_dist);
                        if (tmp_va != NULL && tmp_dist>0)
                        {
                            interFaces[n_inters++] = std::pair<float, ChunkFace*>(tmp_dist,&matrix[i][j][k]->faces[orientations[f]]);
                        }
                    }
                }
            }
        }
        std::sort(interFaces, &interFaces[n_inters], compareIntersections);
        for (int i=0 ; i<n_inters ; i++)
        {
            VoxelTerrainChunkOfChunks* tmpVTCCp;
            tmpVTCCp=dynamic_cast<VoxelTerrainChunkOfChunks*>(interFaces[i].second->owner);
            if (!tmpVTCCp)
            {
                res = interFaces[i].second;
                *dist = interFaces[i].first;
                i = n_inters;
                continue;
            }
            ChunkFace* tmpCFp = tmpVTCCp->selection(start, dir, dist, level);
            if (tmpCFp!=NULL)
            {
                res = tmpCFp;
                i = n_inters;
                continue;
            }
        }
    }
    
    return res;
}

bool GenericVoxelTerrainChunk::compareIntersections(std::pair<float, ChunkFace*> a, std::pair<float, ChunkFace*> b)
{
    return a.first<b.first;
}

void VoxelTerrainChunkOfChunks::update(Uint32 elapsedTime)
{
    //TODO update
}
void VoxelTerrainChunkOfChunks::render()
{
//    std::cout<<"render at level: "<<level<<std::endl;
    float rasterized = GameManager::activeCamera->near*(side/(float)chunkSide)*SQRT2/(pos-GameManager::activeCamera->pos).getModule();
    float importance = rasterized/GameManager::activeCamera->width;
//    if (level!=1)
//    {
//        std::cout<<"rasterized: "<<rasterized<<std::endl;
//        std::cout<<"width: "<<GameManager::activeCamera->width<<std::endl;
//        std::cout<<"importance: "<<importance<<std::endl;
//    }
//    std::cout<<"debug -"<<std::endl;
    if (level==1 || importance<1/50.0 )
    {
//        if (level!=1)std::cout<<" level: "<<level<<std::endl;
        float hs = side/(float)(2.0*chunkSide);
        int i=0;
        for (int dir=0 ; dir<6 ; dir++ )
        {
            for (std::list<ChunkFace*>::iterator it=activeFaces[dir].begin(); it!=activeFaces[dir].end(); ++it)
            {
                Vec3 pos = (*it)->owner->getPos();
                int j = i*20;
                int d=0;
                /**
                 * testure coords with atlas:
                 *  X: 0 <-> 0.0625
                 *  Y: 1-0.0625 <-> 1
                 */
                GLfloat nAtomSide = pow((float)chunkSide, (int)level-1);
                switch (dir)
                {
                    case MX: //MX
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;
                        break;
                    case PX: //PX
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;
                        break;
                    case MY: //MY
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;
                        break;
                    case PY: // PY
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
    //                        vArray[j+d++]= 0;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;
                        break;
                    case MZ: //MZ
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z-hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= -1;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z-hs;
                        break;
                    case PZ: //PZ
                        vArray[j+d++]=0;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=0;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
                        vArray[j+d++]=pos.x-hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y-hs;
                        vArray[j+d++]=pos.z+hs;

                        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]=nAtomSide;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 0;
    //                        vArray[j+d++]= 1;
                        vArray[j+d++]=pos.x+hs;
                        vArray[j+d++]=pos.y+hs;
                        vArray[j+d++]=pos.z+hs;
                        break;
                    default:
                        std::cerr<<"Errore!! - trying to render a face with dir indicator: "<<dir<<std::endl;
                        break;
                }
                i++;
            }
        }
        VoxelTerrainChunkOfChunks::totalActiveFaces += i;
        glPushAttrib(GL_ENABLE_BIT);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_LINE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, ChunkFace::terrain_atlas->texId);
        //glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //        glInterleavedArrays( GL_T2F_N3F_V3F,8*sizeof(GLfloat),&vArray[0]);
        glInterleavedArrays( GL_T2F_V3F,5*sizeof(GLfloat),&vArray[0]);
        glDrawArrays(GL_QUADS, 0, /*this->nActiveFaces*/i*4);
        glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
        glPopAttrib();
    } else
    {
        for (int i=0 ; i<chunkSide ; i++)
        {
            for (int j=0 ; j<chunkSide ; j++)
            {
                for (int k=0 ; k<chunkSide ; k++)
                {
                    // frustum culling
                    if (matrix[i][j][k] != NULL &&
                            dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[i][j][k])->bounder!=NULL &&
                            dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[i][j][k])->bounder->collide(&(GameManager::activeCamera->frustum))!=COLLISION_OUT)
                    {
                        dynamic_cast<VoxelTerrainChunkOfChunks*>(matrix[i][j][k])->render();
                    }
                }
            }
        }
    }
}

float VoxelTerrainChunkOfChunks::distanceFrom(Vec3 point)
{
    float r = VoxelTerrainChunkOfChunks::chunkSide*VoxelAtom::side*SQRT2*0.5f;
    float dist = ((origin+r)-point).getModule();
    return dist<r?-dist:dist;
}
Vec3 VoxelTerrainChunkOfChunks::getOrigin()
{
    return origin;
}

int VoxelTerrainChunkOfChunks::getNActiveFaces()
{
    return nActiveFaces;
}
unsigned int VoxelTerrainChunkOfChunks::getLevel()
{
    return level;
}
GenericVoxelTerrainChunk* VoxelTerrainChunkOfChunks::getFather()
{
    return father;
}
float VoxelTerrainChunkOfChunks::getSide()
{
    return side;
}
float VoxelTerrainChunkOfChunks::calcSide(int level)
{
    if (level==-1)
    {
        level = this->level;
    }
    return pow((float)chunkSide, (int)level)*VoxelAtom::side;
}
VoxelTerrainChunkOfChunks* VoxelTerrainChunkOfChunks::masterFather = NULL;
int VoxelTerrainChunkOfChunks::totalActiveFaces = 0;
GLfloat VoxelTerrainChunkOfChunks::vArray[chunkSide*chunkSide*chunkSide*3*20];
VoxelTerrainChunkOfChunks* VoxelTerrainChunkOfChunks::getMasterFather()
{
    return masterFather;
}
float VoxelTerrainChunkOfChunks::rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection)
{
    float d=-1;
    selection(start, dir, &d);
    *intersection = start+dir*d;
    return d;
}
void VoxelTerrainChunkOfChunks::setMasterFather(VoxelTerrainChunkOfChunks* masterFather)
{
    VoxelTerrainChunkOfChunks::masterFather = masterFather;
    VoxelTerrainWrapper::instance->bounder = masterFather->bounder;
    VoxelTerrainWrapper::instance->visible = masterFather->visible;
    VoxelTerrainWrapper::instance->intersectable = masterFather->intersectable;
    VoxelTerrainWrapper::instance->active = masterFather->active;
}
VoxelTerrainWrapper::VoxelTerrainWrapper()
{
}
VoxelTerrainWrapper* VoxelTerrainWrapper::instance=NULL;
void VoxelTerrainWrapper::initialize()
{
    VoxelTerrainWrapper::instance = new VoxelTerrainWrapper();
}
void VoxelTerrainWrapper::render()
{
    VoxelTerrainChunkOfChunks::getMasterFather()->render();
}
void VoxelTerrainWrapper::update(Uint32 elapsedTime)
{
    VoxelTerrainChunkOfChunks::getMasterFather()->update(elapsedTime);
}
float VoxelTerrainWrapper::rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection)
{
    return VoxelTerrainChunkOfChunks::getMasterFather()->rayIntersect(start, dir, intersection);
}
