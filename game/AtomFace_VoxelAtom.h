/* 
 * File:   AtomFace.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 20 marzo 2011, 15.58
 */

#ifndef ATOMFACE_VOXELATOM_H
#define	ATOMFACE_VOXELATOM_H
//class AtomFace;
class VoxelAtom;
//#include "VoxelAtom.h"
#include "../maths/Vec3.h"
#include "../graphics/Texture.h"
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/gl.h>

#define PX 0
#define MX 1
#define PY 2
#define MY 3
#define PZ 4
#define MZ 5

typedef Uint8 OrientationType;

class AtomFace
{
public:
    OrientationType orientation;
    VoxelAtom* owner;
    GLfloat color[4];
    bool active;

    AtomFace();
    AtomFace(VoxelAtom* owner, OrientationType orientation, GLfloat color[4]);
    void setColor(GLfloat color[4]);
    
    static void initializeRenderingProcedure();
    static void initializeTexture();
    void renderNoTexture();

    VoxelAtom * selectionNoCheck(Vec3 start, Vec3 dir, float* in_dist);

    static GLuint baseList;
    static Texture* terrain_atlas;
};


class VoxelAtom
{
public:
	static const float side; //TODO: in VS non gli piace float
    Vec3 pos;
    GLfloat color[4];
    AtomFace faces[6];

    VoxelAtom();
    VoxelAtom(Vec3 pos, GLfloat color[4]);
    void setColor(GLfloat color[4]);
};

#endif	/* ATOMFACE_H */

