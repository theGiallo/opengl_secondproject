/* 
 * File:   VoxelTerrainChunkOfChunks.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 8 settembre 2011, 17.05
 */

#ifndef VOXELTERRAINCHUNKOFCHUNKS_H
#define	VOXELTERRAINCHUNKOFCHUNKS_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <cmath>
#include "../maths/Vec3.h"
#include "../graphics/Texture.h"
#include "../game/GameObject.h"
#include "../graphics/Renderable.h"
#ifdef WIN32
	#include <windows.h>
#endif
#include <iostream>

typedef Uint8 OrientationType;
#define MX 0
#define PX 1
#define MY 2
#define PY 3
#define MZ 4
#define PZ 5
#define OPPOSITE_ORIENTATION(a) (a%2==0?(a)+1:(a)-1)

/**
 * NOMOD: no modify
 * MOD_NOCAS: modify and no can as cube
 * MOD_CAS: modify and can as cube
 **/
typedef Uint8 ChunkModEnum;
#define NOMOD 0
#define MOD_NOCAS 1
#define MOD_CAS 2

#ifndef SQRT2
#define SQRT2 1.414213562f
#endif

class GenericVoxelTerrainChunk;

class ChunkFace
{
public:
    OrientationType orientation;
    GenericVoxelTerrainChunk* owner;
//    GLfloat color[4];
    bool active;
//    unsigned int sideFacesCount; ///per poter renderizzare facce di chunk con texture ripetuta

    ChunkFace();
    ChunkFace(GenericVoxelTerrainChunk* owner, OrientationType orientation/*, GLfloat color[4]*/);
//    void setColor(GLfloat color[4]);

//    static void initializeRenderingProcedure(); //not used
    static void initializeTexture();
//    void renderNoTexture(); //not used

    GenericVoxelTerrainChunk * selectionNoCheck(Vec3 start, Vec3 dir, float* in_dist);

    static GLuint baseList; //not used
    static Texture* terrain_atlas;
};
class GenericVoxelTerrainChunk
{
protected:
    Vec3 pos;
    bool can_br_as_cube;
    unsigned int level;
    static bool compareIntersections(std::pair<float, ChunkFace*> a, std::pair<float, ChunkFace*> b);
public:
    ChunkFace faces[6];
    GenericVoxelTerrainChunk* adjacentChunks[6];
    GenericVoxelTerrainChunk* father;
    GenericVoxelTerrainChunk(){can_br_as_cube=true;}
    virtual bool canBeRenderedAsCube(){std::cout<<"merda"<<std::endl;return can_br_as_cube;}
    virtual void setPos(Vec3 pos)
    {
        this->pos = pos;
    }
    Vec3 getPos()
    {
        return pos;
    }
    virtual float getSide(){return 0;}
    unsigned int getLevel(){return level;}
};
class VoxelAtom : public GenericVoxelTerrainChunk
{
public:
    static const float side;
//    GLfloat color[4];

    VoxelAtom();
    VoxelAtom(Vec3 pos/*, GLfloat color[4]*/);
//    void setColor(GLfloat color[4]);
    virtual bool canBeRenderedAsCube()
    {
        return true;
    }
    virtual float getSide(){return VoxelAtom::side;}
};
class VoxelTerrainChunkOfChunks : public GenericVoxelTerrainChunk, public GameObject, public Renderable
{
public:
    const static unsigned int chunkSide = 10;
    static int totalActiveFaces;
protected:
    //?? a che servirebbe? static float baseSideLength;
    static VoxelTerrainChunkOfChunks* masterFather;
    /**
    * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide = faces on a plane of the chunk
    * VoxelTerrainChunk::chunkSide = planes on a dimension of the chunk
    * 3 = dimensions of the plane
    * ------------------------------------------------------------------------------------------
    * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*3 = max numbers of faces rendered per chunk
    * VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*VoxelTerrainChunk::chunkSide*6 = numbers of the faces of all cubes in the chunk
    * 20 = numbers of floats necessary to render a face (4*(3+2)), although normals could be useless
    */
    static GLfloat vArray[chunkSide*chunkSide*chunkSide*3*20];

    GenericVoxelTerrainChunk* matrix[chunkSide][chunkSide][chunkSide];
    std::list<ChunkFace*> activeFaces[6]; ///active faces of subchunks(sons)
    int nActiveFaces;
    int nChunksMaybeAsCube;
    Vec3 origin;
    void checkNearbyFaces(unsigned int matrixID[3], bool added, bool wasRenderableAsChunk=true);
    float side;
public:
    //VoxelTerrainChunkOfChunks();
    VoxelTerrainChunkOfChunks(unsigned int level=1, Vec3 origin=Vec3(), VoxelTerrainChunkOfChunks* father=NULL);

    virtual bool canBeRenderedAsCube();

    const static float levelOneSide; /// = VoxelTerrainChunk::chunkSide*VoxelAtom::side;

    static VoxelTerrainChunkOfChunks* getMasterFather();
    static void setMasterFather(VoxelTerrainChunkOfChunks* masterFather);

//    float getSide(int level)
//    {
//        return pow(VoxelTerrainChunkOfChunks::levelOneSide, level);
//    }

    virtual float calcSide(int level = -1); /// default value is -1
    virtual float getSide();

    virtual void setPos(Vec3 pos);
    void setOrigin(Vec3 origin); /// this does not do any test and changes on the sons' position
    Vec3 getOrigin();

    int getNActiveFaces();
    unsigned int getLevel();
    GenericVoxelTerrainChunk* getFather();

    void initializeStructure(); //TODO initializeStructure implementare e aggiornare alla versione chunk generalizzata

    /**
     * @brief: Selects the face of a chunk pointed by the ray.
     * The level refers to the chunks DS: with level=0(default value) selects the face
     * of a chunk of the smallest type(VoxelAtom), with level=1 a father of one of
     * the smallest type, and so on.
     **/
    ChunkFace * selection(Vec3 start, Vec3 dir, float * dist, unsigned int level=0);
    ChunkModEnum removeChunk(GenericVoxelTerrainChunk * ck);
    ChunkModEnum addChunk(GenericVoxelTerrainChunk * ck);

     //TODO???vbool isInFrustum(const Frustum* & frustum);
	
    virtual void update(Uint32 elapsedTime);
	virtual void lateUpdate(Uint32 elapsedTime){};
    virtual void render();
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection);

    /**
     * @brief returns the distance from the circumscribed sphere
     * @param point
     * @return
     */
    float distanceFrom(Vec3 point);
};

class VoxelTerrainWrapper : public GameObject, public Renderable
{
private:
    VoxelTerrainWrapper();
public:
    static void initialize();
    static VoxelTerrainWrapper* instance;
    virtual void render();
    virtual void update(Uint32 elapsedTime);
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection);
	virtual void lateUpdate(Uint32){};
};

#endif	/* VOXELTERRAINCHUNKOFCHUNKS_H */

