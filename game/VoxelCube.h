/* 
 * File:   VoxelCube.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 18 marzo 2011, 21.41
 */

#ifndef VOXELCUBE_H
#define	VOXELCUBE_H

#include "../graphics/Cube.h"
#include "../game/GameObject.h"
#include "../graphics/Renderable.h"

class VoxelCube : public GameObject, public Renderable
{
public:
    Cube cube;

    VoxelCube();
    VoxelCube(Vec3 pos, float side, GLfloat color[4]);
	
	virtual void lateUpdate(Uint32 elapsedTime){};
    virtual void update(Uint32 elapsedTime);
    virtual void render(void);
};

#endif	/* VOXELCUBE_H */

