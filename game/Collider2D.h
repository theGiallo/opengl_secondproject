/* 
 * File:   Collider2D.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 25 August 2012
 */

#ifndef COLLIDER2D_H
#define	COLLIDER2D_H

#include <cstdint>

class Renderable2D;

class Collider2D
{
public:
    virtual void onCollision(uint8_t* states, Renderable2D** renderables, int num)=0;
};

#endif /* COLLIDER_H */