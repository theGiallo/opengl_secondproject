/* 
 * File:   GameManager2D.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 06 November 2011
 */

#ifndef GAMEMANAGER2D_H
#define	GAMEMANAGER2D_H

#include <cstdint>
#include <stdlib.h>
#include <list>
#include "../game/GameObject.h"
#include "../graphics/Renderables2D.h"
#include "../game/PhysicalObject2D.h"
#include "../graphics/Camera2D.h"
#include <SDL/SDL.h>

#define COLLISION_STATES
#define COLLISION_NONE 0x0
#define COLLISION_ENTER 0x1
#define COLLISION_STAY  0x2
#define COLLISION_EXIT  0x3

#define MAX_COLLISIONS_PER_OBJ 100

class GameManager2D
{
protected:
    static GameObject** game_objects;
    static PhysicalObject2D** physical_objects;
    /**
     * Indicates with which objs the obj collides(for each obj an array of 30 obj ids).
     * Thus for each collision there are two ids memorized.
     * The state of the collision is memorized in the arrays collision_states.
     **/
    static unsigned int*** collisionsIDs; ///double bufferedmatrix (N_REND_AND_GO*COLLISIONS_IDS_SIZE)
    /**
     * Indicates the number of ids in collisionsIDs entries
     **/
    static uint8_t** n_collisionsIDs; ///double buffered array N_REND_AND_GO
    static uint8_t*** collision_states; ///double buffered matrix (N_REND_AND_GO*COLLISIONS_IDS_SIZE)
    static uint8_t collision_buffer_id;
    static std::list<Uint32> freed_ids;
    static uint32_t maxFreeID;
    //static Uint32 maxFreeRenderableIndex;
    static Renderable2D** renderables; // per debug TODO mettere public
    
    static int gameobjects_count;
    
    static void solveCollisions(void);
public:
    static int getGameObjectsCount(void);
    static bool renderBounders, renderNotBounders, do_collision_react, do_update_physics, do_update, do_late_update, do_collide;
    static Camera2D* activeCamera;
    static void update(Uint32 elapsedTime);
    static void render(void);
    static bool registerGameObject(GameObject * go);
    //static bool registerRenderable(Renderable2D * r);
    static void deleteGameObject(GameObject * go);
    //static void deleteRenderable(Renderable2D * r);
    static bool rayCast(Vec2 start, Vec2 dir, Vec2 * intersection, Renderable2D**intersected);
};

#endif	/* GAMEMANAGER2D_H */

