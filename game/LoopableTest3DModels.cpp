/*
 * LoopableTest3DModels.cpp
 *
 *  Created on: 21/apr/2013
 *      Author: thegiallo
 */

#include "LoopableTest3DModels.h"
#include "../graphics/Model.h"

LoopableTest3DModels::LoopableTest3DModels() {
	// TODO Auto-generated constructor stub

}

LoopableTest3DModels::~LoopableTest3DModels() {
	// TODO Auto-generated destructor stub
}
const Uint8* LoopableTest3DModels::getKeysMap_const(void) const
{

}
Uint8* LoopableTest3DModels::getKeysMap(void)
{

}
void LoopableTest3DModels::setKeysMap(Uint8* keys)
{

}

void LoopableTest3DModels::onEvent(SDL_Event* event)
{

}
void LoopableTest3DModels::keyChecks(void) /// checks the keymap
{

}
void LoopableTest3DModels::onLoop(Uint32 elapsedTime)
{

}
void LoopableTest3DModels::onRender(void)
{

}
void LoopableTest3DModels::init(void)
{
    if ( SDL_Init(SDL_INIT_EVERYTHING) !=0 )
    {
        exit(1);
    }
    // TODO set a window, a resize callback, etc.
	Model model;
	model.load("./resources/3ds/samples/house/medieval_house.3ds");
}
void LoopableTest3DModels::cleanUp(void)
{

}
bool LoopableTest3DModels::isLoopable(void)
{
	return true;
}

