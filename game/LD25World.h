/* 
 * File:   LD25World.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 25 December 2012
 */

#ifndef LD25WORLD_H
#define	LD25WORLD_H

#include "../maths/Vec2.h"
#include "Trigger2D.h"
#include "LD25Player.h"

class LD25MOB;
class LD25Hero;

class LD25World
{
public:
    static Trigger2D up_bounder,
                     down_bounder,
                     left_bounder,
                     right_bounder;
    static float width, height;
    
    static LD25Player players[2];
    static LD25Hero* hero;
    static LD25MOB** mobs;
	static uint16_t mobs_count;
    static uint8_t wasd[2][4];
    static uint8_t action[2][2];
    static const uint8_t num_players = 2;
    static Vec2 startingPos[2];
};

#endif /* LD25WORLD_H */