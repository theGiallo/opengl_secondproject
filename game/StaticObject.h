/* 
 * File:   StaticObject.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 18 marzo 2011, 1.29
 */

#ifndef STATICOBJECT_H
#define	STATICOBJECT_H

#include "../game/GameObject.h"
#include "../graphics/Renderable.h"
#include "../maths/Vec3.h"
#include "../graphics/Model.h"

class StaticObject : public GameObject, public Renderable
{
private:
    Model *model;
    Vec3 pos;
    Vec3 rot;
public:
    StaticObject();
    void setModel(Model *m);
    Model *getModel(void);
    Vec3 getPos(void);
    void setPos(Vec3 p);
    Vec3 getRot(void);
    void setRot(Vec3 r);

    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime);
    virtual void render(void);
};

#endif	/* STATICOBJECT_H */

