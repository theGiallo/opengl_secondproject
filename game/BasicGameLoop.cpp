#include "BasicGameLoop.h"
#include "SDL/SDL.h"
#include "LoopableVoxelTest.h"

BasicGameLoop::BasicGameLoop(ILoopable& loopable) : loopable(loopable)
{
    this->loopable = loopable;
}
void BasicGameLoop::init()
{
    loopable.init();
}
void BasicGameLoop::run()
{
    SDL_Event event;
    Uint32 currentTicks, lastTick, elapsedTicks;
    lastTick = SDL_GetTicks();
    while (loopable.isLoopable())
    {
        Uint32 currentTicks;
        currentTicks = SDL_GetTicks();
        elapsedTicks = currentTicks - lastTick;
        lastTick = currentTicks;
        while (SDL_PollEvent(&event))
        {
            loopable.onEvent(&event);
        }
        loopable.setKeysMap(SDL_GetKeyState(NULL));
        loopable.keyChecks();
        loopable.onLoop(elapsedTicks);
        loopable.onRender();
    }
    loopable.cleanUp();
}
void BasicGameLoop::setLoopable(ILoopable& loopable)
{
    this->loopable = loopable;
}