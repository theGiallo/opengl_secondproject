/* 
 * File:   OctTree.cpp
 * Author: thegiallo
 * Directory: game
 * 
 * Created on 26 settembre 2011, 16.28
 */

//#include "../generalinclude.h"
#include "../game/OctTree.h"
#include "../game/GameManager.h"
#include <iostream>
#include <algorithm>

#include <cfloat>

#define ATOM_SIDE OctTreeNode::atom_side

OctTreeNode::OctTreeNode()
{
    for (Uint8 i=0 ; i<8 ; i++)
    {
        childs[i] = NULL;
    }
}
void OctTreeNode::render_array()
{
#ifdef DEBUG
    render_array_tt.check(START_CHECK);
#endif
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_ENABLE_BIT);
    glColor4f(1,1,1,1);
    glPolygonMode(GL_FRONT, GL_FILL);
    glPolygonMode(GL_BACK, GL_LINE);
    glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glBindTexture(GL_TEXTURE_2D, tex_atom_face->texId);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glInterleavedArrays( GL_T2F_N3F_V3F,8*sizeof(GLfloat),&vArray[0]);
    //glInterleavedArrays( GL_T2F_V3F,5*sizeof(GLfloat),&vArray[0]);
    glDrawArrays(GL_QUADS, 0, /*this->nActiveFaces*/n_in_vArray*4);
    glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
    glPopAttrib();
    glPopAttrib();
    active_faces += n_in_vArray;
    n_in_vArray = 0;
#ifdef DEBUG
    render_array_tt.check(STOP_CHECK);
#endif
}
void OctTreeNode::renderCube(OctTreeNode* node, Vec3_int my_center, uint my_half_side)
{
    unsigned int j = n_in_vArray*FACE_SIZE;
    Uint8 d = 0;
	uint my_side = my_half_side<<1;
    GLfloat nAtomSide = my_side;
    nAtomSide /= ATOM_SIDE;
    Vec3_int near_center;
    bool isOutside, nearCond,isAtom;
    OctTreeNode**tv;
    isAtom = IS_ATOM(node) || my_side==ATOM_SIDE;
    int mhs_int = (int)my_half_side;
    near_center = getNeighbourCenter(my_center, my_half_side, MX);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,MX):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///render face MX
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
    near_center = getNeighbourCenter(my_center, my_half_side, PX);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,PX):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///<render face PX>
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
    near_center = getNeighbourCenter(my_center, my_half_side, MY);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,MY):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///<render face MY>
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
    near_center = getNeighbourCenter(my_center, my_half_side, PY);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,PY):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///<render face PY>
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
                        vArray[j+d++]= 0;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
    near_center = getNeighbourCenter(my_center, my_half_side, MZ);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,MZ):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///<render face MZ>
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= -1;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z-mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
    near_center = getNeighbourCenter(my_center, my_half_side, PZ);
    nearCond = isAtom?IS_THERE_NEIGHBOUR(node,PZ):((!(tv=root->voxel(root_center, root_half_side, near_center,&isOutside))||!(*tv)) && !isOutside);
    if (nearCond || (my_side>ATOM_SIDE && IMPORTANCE(my_side,near_center)>=importance_limit))
    {
        ///<render face PZ>
        vArray[j+d++]=0;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=0;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
        vArray[j+d++]=my_center.x-mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y-mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;

        vArray[j+d++]=nAtomSide;
        vArray[j+d++]=nAtomSide;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 0;
                        vArray[j+d++]= 1;
        vArray[j+d++]=my_center.x+mhs_int;
        vArray[j+d++]=my_center.y+mhs_int;
        vArray[j+d++]=my_center.z+mhs_int;
        
        n_in_vArray++;
        if (n_in_vArray==VARRAY_MAX_FACES)
        {
            render_array();
        }
    }
}
void OctTreeNode::render(Vec3_int my_center, uint my_half_side)
{
#ifdef DEBUG
    if (my_half_side==root_half_side)
    {
        render_tt.check(START_CHECK);
        render_single_tt.check(START_CHECK);
    }
    render_calls++;
#endif
    if (render_structure)
    {
        bounding_box.side = my_half_side<<1;
        bounding_box.center = my_center;
        bounding_box.render();
    }
    
    uint child_half_side = my_half_side>>1;
    for (Uint8 i=0; i<8 ; i++)
    {
        if (!IS_CHILD_ABSENT(childs[i]) )// && (IS_ATOM(childs[i]) ||  CAN_BE_RENDERED_AS_CUBE(childs[i], child_half_side)))
        {
            Vec3_int child_center = Vec3_int();
            if (child_half_side==0)
            {
                CALC_CHILD_CENTER(child_center,my_center,(int)ATOM_SIDE/2,i);
            } else
            {
                CALC_CHILD_CENTER(child_center,my_center,(int)child_half_side,i);
            }
            if (GameManager::activeCamera->frustum.collideBox(child_center, my_half_side)!=COLLISION_OUT)
            {
                float importance;
                importance = IMPORTANCE((float)my_half_side, child_center);
#ifdef DEBUG
				/*if (my_half_side <=2 )
				{
					std::cout<<"importance: "<<importance<<std::endl<<"my_half_side: "<<my_half_side<<std::endl<<std::endl;
				}*/
                casual_tt.check(STOP_CHECK);
#endif
                if (importance < importance_limit || my_half_side==ATOM_SIDE || IS_ATOM(childs[i]))
                {
#ifdef DEBUG
                    render_tt.check(PAUSE_CHECK);
                    if (my_half_side==root_half_side)
                    {
                            render_single_tt.check(PAUSE_CHECK);
                    }
#endif
                    if (child_half_side==0)
                    {
                        renderCube(childs[i], child_center, ATOM_SIDE/2);
                    } else
                    {
                        renderCube(childs[i], child_center, child_half_side);
                    }
#ifdef DEBUG
                    render_tt.check(UNPAUSE_CHECK);
                    if (my_half_side==root_half_side)
                    {
                            render_single_tt.check(UNPAUSE_CHECK);
                    }
#endif
                } else
                {
#ifdef DEBUG
                    if (my_half_side==root_half_side)
                    {
                            render_single_tt.check(PAUSE_CHECK);
                    }
#endif
                    if (child_half_side==0)
                    {
                        childs[i]->render(child_center, ATOM_SIDE/2);
                    } else
                    {
                        childs[i]->render(child_center, child_half_side);
                    }
#ifdef DEBUG
                    if (my_half_side==root_half_side)
                    {
                            render_single_tt.check(UNPAUSE_CHECK);
                    }
#endif
                }
            }
        }
    }
#ifdef DEBUG
    if (my_half_side==root_half_side)
    {
        render_tt.check(STOP_CHECK);
        render_single_tt.check(STOP_CHECK);
    }
#endif
}
void OctTreeNode::render_root()
{
#ifdef DEBUG
    casual_tt.check(START_CHECK);
    render_calls = 0;
#endif
    n_in_vArray = active_faces = 0;
//    glMatrixMode(GL_MODELVIEW);
//    glPushMatrix();
//    GLfloat scalefactor = 1/(GLfloat)((int)ATOM_SIDE*10);
//    std::cout<<"scalefactor: "<<scalefactor<<std::endl;
//    glScalef(scalefactor, scalefactor, scalefactor);
    root->render(root_center, root_half_side);
    if (n_in_vArray!=0)
    {
        render_array();
    }
//    glPopMatrix();
#ifdef DEBUG
    casual_tt.check(STOP_CHECK);
#endif
}

/**
 * 
 * @param my_center
 * @param my_half_side
 * @param center
 * @return NULL if is absent, 1 if is outside, else the pointer to the node
 */
OctTreeNode** OctTreeNode::voxel(Vec3_int my_center, uint my_half_side, Vec3_int center, bool* isOutside)
{
    if (IS_OUTSIDE(my_center, center, my_half_side))
    {
        *isOutside = true;
        return NULL;
    }
    Uint8 id = 0;
    CALC_ID(id,my_center,center);
    if (IS_CHILD_ABSENT(childs[id]))
    {
        *isOutside = false;
        return NULL;
    }
    if (IS_ATOM(childs[id]) || my_half_side==ATOM_SIDE)
    {
        *isOutside = false;
        return &childs[id];
    }
    Vec3_int child_center = Vec3_int();
    uint child_half_side = my_half_side>>1;
    if (child_half_side==0)
    {
        CALC_CHILD_CENTER(child_center,my_center,ATOM_SIDE/2,id);
    } else
    {
        CALC_CHILD_CENTER(child_center,my_center,child_half_side,id);
    }
    if (IS_THIS(child_center, center))
    {
        *isOutside = false;
        return &childs[id];
    }
    *isOutside = false;
    return childs[id]->voxel(child_center, child_half_side, center, isOutside);
}
RemoveRetType OctTreeNode::remove(Vec3_int my_center, uint my_half_side, Vec3_int center)
{
    if (IS_THIS(my_center, center))
    {
        return REMOVE_ME;
    }
    if (IS_OUTSIDE(my_center, center, my_half_side))
    {
        return NOT_REMOVED;
    }
    Uint8 id = 0;
    CALC_ID(id,my_center,center);
    if (IS_CHILD_ABSENT(childs[id]))
    {
        return REMOVED;
    }
    Vec3_int child_center = Vec3_int();
    uint child_half_side = my_half_side>>1;
    if (child_half_side==0)
    {
        CALC_CHILD_CENTER(child_center,my_center,ATOM_SIDE/2,id);
    } else
    {
        CALC_CHILD_CENTER(child_center,my_center,child_half_side,id);
    }
    RemoveRetType res;
    bool rr = IS_ATOM(childs[id]) || my_half_side==ATOM_SIDE;
    if (!rr)
    {
        if (child_half_side==0)
        {
            res=childs[id]->remove(child_center, ATOM_SIDE/2, center);
        } else
        {
            res=childs[id]->remove(child_center, child_half_side, center);
        }
        rr= res==REMOVE_ME;
    }
    if (rr)
    {
        if (!(IS_ATOM(childs[id]) || my_half_side==ATOM_SIDE))
        {
            delete childs[id];
        } else
        {
            bool isOutside;
            OctTreeLeaf* neighbour;
            for (OrientationType i=0 ; i<6 ; i++)
            {
//                if (child_half_side==0)
//                {
//                    neighbour=root->voxel(root_center, root_half_side, getNeighbourCenter(child_center,0.5,i),&isOutside);
//                } else
//                {
                    neighbour=root->voxel(root_center, root_half_side, getNeighbourCenter(child_center,child_half_side,i),&isOutside);
//                }
                if (neighbour)
                {
                    SET_NEIGHBOUR(*neighbour,OPPOSITE_ORIENTATION(i),0);
                }
            }
        }
        childs[id] = NULL;
        Uint8 n=0;
        for (id=0 ; id<8 ; id++)
        {
            if (IS_CHILD_ABSENT(childs[id]))
            {
                n++;
            }
        }
        if (n==8)
        {
            return REMOVE_ME;
        }
        return REMOVED;
    }
    return res;
}
bool OctTreeNode::add(Vec3_int my_center, uint my_half_side, Vec3_int center)
{
    if (my_half_side==0)
    {
        std::cerr<<"my_half_side==0"<<std::endl;
        throw false;
    }
    if (IS_THIS(my_center, center))
    {
        return false;
    }
    Uint8 id = 0;
    CALC_ID(id,my_center,center);
    if (IS_ATOM(childs[id]) || (my_half_side==ATOM_SIDE && !IS_CHILD_ABSENT(childs[id])) )
    {
        return false;
    }
    Vec3_int child_center = Vec3_int();
    uint child_half_side = my_half_side>>1;
    if (child_half_side==0)
    {
        CALC_CHILD_CENTER(child_center,my_center,ATOM_SIDE/2,id);
    } else
    {
        CALC_CHILD_CENTER(child_center,my_center,child_half_side,id);
    }
    if (IS_CHILD_ABSENT(childs[id]))
    {
        if (my_half_side==ATOM_SIDE || IS_THIS(child_center, center))
        {
            SET_ATOM(childs[id]);
            bool isOutside;
            OctTreeLeaf* neighbour;
            for (OrientationType i=0 ; i<6 ; i++)
            {
//                if (child_half_side==0)
//                {
//                    neighbour=root->voxel(root_center, root_half_side, getNeighbourCenter(child_center,0.5f,i),&isOutside);
//                } else
//                {
                    neighbour=root->voxel(root_center, root_half_side, getNeighbourCenter(child_center,child_half_side,i),&isOutside);
//                }
                if (neighbour)
                {
                    SET_NEIGHBOUR(*neighbour,OPPOSITE_ORIENTATION(i),1);
                    SET_NEIGHBOUR(childs[id],i,1);
                } else
                {
                    SET_NEIGHBOUR(childs[id],i,0);
                }
            }
            return true;
        } else
        {
            childs[id] = new OctTreeNode();
            if (IS_ATOM(childs[id]))
            {
                std::cerr<<"node pointer would be considered an atom! "<<childs[id]<<std::endl;
            }
        }
    }
    if (!(IS_THIS(child_center, center)))
    {
            return childs[id]->add(child_center, child_half_side, center);
    }
    return false;
}
OctTreeLeaf* OctTreeNode::select(Vec3 start, Vec3 dir, float* dist, Vec3* inter, Vec3_int* center, Vec3_int my_center, uint my_half_side)
{
    float tmp_dist;
    Vec3_int centers[8];
    Vec3 intersPoints[8];
    std::pair<float, Uint8> inters[8];
    Uint8 ni=0;
    uint child_half_side = my_half_side>>1;
    Vec3 tmp_start = start-dir*(my_half_side<<1); /// this is to avoid the false negative in the case start is inside of the tested node (but it's not inside of all his atoms obviously)
    for (Uint8 id=0 ; id<8 ; id++)
    {
        if (!IS_CHILD_ABSENT(childs[id]))
        {
            if (child_half_side==0)
            {
                CALC_CHILD_CENTER(centers[id],my_center,ATOM_SIDE/2,id);
            } else
            {
                CALC_CHILD_CENTER(centers[id],my_center,child_half_side,id);
            }
            bounding_box.center = centers[id];
            bounding_box.side = my_half_side;
            tmp_dist = bounding_box.rayIntersect(tmp_start, dir, inter);
            if (tmp_dist>0)
            {
                inters[ni].first = tmp_dist;
                inters[ni].second = id;
                intersPoints[id] = *inter;
                ni++;
            }
        }
    }
    std::sort(inters, &inters[ni], compareIntersections);
    if ((my_half_side==ATOM_SIDE || IS_ATOM(childs[inters[0].second])) && ni!=0)
    {
        *dist = inters[0].first;
        *inter = intersPoints[inters[0].second];
        *center = centers[inters[0].second];
        return &childs[inters[0].second];
    }
    OctTreeLeaf* tmp_res;
    for (Uint8 i=0 ; i<ni ; i++)
    {
        tmp_res = childs[inters[i].second]->select(start,dir,dist,inter,center,centers[inters[i].second],child_half_side);
        if (tmp_res!=NULL)
        {
            return tmp_res;
        }
    }
    return NULL;
}
OctTreeLeaf* OctTreeNode::select_from_root(Vec3 start, Vec3 dir, float* dist, Vec3* inter, Vec3_int* center)
{
    bounding_box.center = root_center;
    bounding_box.side = root_half_side<<1;
    *dist = bounding_box.rayIntersect(start, dir, inter);
    if (dist<0)
    {
        std::cout<<"ray ourside of root"<<std::endl;
        return NULL;
    }
    return root->select(start, dir, dist, inter, center, root_center, root_half_side);
}
bool OctTreeNode::compareIntersections(std::pair<float, Uint8> a, std::pair<float, Uint8> b)
{
    return a.first<b.first;
}
unsigned long int OctTreeNode::getContainedAtomsCount()
{
    unsigned long n=0;
    for (Uint8 i=0 ; i<8 ; i++)
    {
        if (IS_ATOM(childs[i]))
        {
            n++;
        } else
        if (!IS_CHILD_ABSENT(childs[i]))
        {
            n+=childs[i]->getContainedAtomsCount();
        }
    }
    return n;
}
RemoveRetType OctTreeNode::remove_from_root(Vec3_int center)
{
    Uint8 res;
    Uint8 n=0;
    if (root_half_side==ATOM_SIDE)
    {
        for (Uint8 i=0 ; i<8 ; i++)
        {
            if (!IS_CHILD_ABSENT(root->childs[i]))
            {
                n++;
            }
        }
        if (n==1)
        {
            return NOT_REMOVED;
        }
    }
    if ((res=root->remove(root_center, root_half_side, center)))
    {
        OctTreeNode* tmp;
        Uint8 id;
        while (root_half_side!=ATOM_SIDE)
        {
            n=0;
            for (Uint8 i=0 ; i<8 ; i++)
            {
                if (!IS_CHILD_ABSENT(root->childs[i]))
                {
                    n++;
                    if (n==2)
                    {
                        break;
                    }
                    tmp = root->childs[i];
                    id = i;
                }
            }
            if (n==1)
            {
                root->childs[id] = NULL;
                delete root;
                root = tmp;
                Vec3_int tmp_center;
                root_half_side>>=1;
                CALC_CHILD_CENTER(tmp_center,root_center,root_half_side,id);
                root_center = tmp_center;
                root_level--;
            } else
            {
                break;
            }
        }
    }
    return res==NOT_REMOVED?res:REMOVED;
}
bool OctTreeNode::add_to_root(Vec3_int center)
{
//    std::cout<<"add_to_root";center.print();
//    std::cout<<" root_center";root_center.print();
//    std::cout<<" root_half_side: "<<root_half_side<<" root_level: "<<(uint)root_level<<std::endl;
//    std::cout<<"        root_center.x-root_half_side="<<root_center.x-root_half_side<<std::endl;
    if (IS_OUTSIDE(root_center, center, root_half_side))
    {
        Uint8 id=0;
        Vec3_int new_root_center = center-root_center;
        if (new_root_center.x>0)
        {
            new_root_center.x = root_center.x+root_half_side;
        } else
        {
            new_root_center.x = root_center.x-root_half_side;
        }
        if (new_root_center.y>0)
        {
            new_root_center.y = root_center.y+root_half_side;
        } else
        {
            new_root_center.y = root_center.y-root_half_side;
        }
        if (new_root_center.z>0)
        {
            new_root_center.z = root_center.z+root_half_side;
        } else
        {
            new_root_center.z = root_center.z-root_half_side;
        }
        CALC_ID(id, new_root_center, root_center);
        OctTree* tmp = root;
        root = new OctTreeNode();
        if (IS_ATOM(root))
        {
            std::cerr<<"root node pointer would be considered an atom! "<<root<<std::endl;
        }
        root->childs[id] = tmp;
        root_center = new_root_center;
        root_level++;
        root_half_side<<=1;
        return add_to_root(center);
    } else
    {
        return root->add(root_center, root_half_side, center);
    }
}
Vec3_int OctTreeNode::getNeighbourCenter(Vec3_int my_center, uint my_half_side, OrientationType o)
{
    switch(o)
    {
        case MX:
            return Vec3_int(my_center.x-(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)), my_center.y, my_center.z);
            break;
        case PX:
            return Vec3_int(my_center.x+(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)), my_center.y, my_center.z);
            break;
        case MY:
            return Vec3_int(my_center.x, my_center.y-(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)), my_center.z);
            break;
        case PY:
            return Vec3_int(my_center.x, my_center.y+(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)), my_center.z);
            break;
        case MZ:
            return Vec3_int(my_center.x, my_center.y, my_center.z-(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)));
            break;
        case PZ:
            return Vec3_int(my_center.x, my_center.y, my_center.z+(my_half_side==0?ATOM_SIDE/2:(my_half_side<<1)));
            break;
        default:
            return my_center;
    }
}
unsigned long OctTreeNode::n_in_vArray=0;
GLfloat OctTreeNode::vArray[20*VARRAY_MAX_FACES];
BoundingBox OctTreeNode::bounding_box = BoundingBox();
OctTreeNode* OctTreeNode::root = NULL;
Texture* OctTreeNode::tex_atom_face = NULL;
Vec3_int OctTreeNode::root_center = Vec3_int();
uint OctTreeNode::root_half_side = 0;
Uint8 OctTreeNode::root_level = 0;
bool OctTreeNode::render_structure = false;
unsigned long int OctTreeNode::active_faces = 0;
const float OctTreeNode::importance_limit=0.025f;
const uint OctTreeNode::atom_side=2;
#ifdef DEBUG
TimeTool OctTreeNode::render_array_tt = TimeTool();
TimeTool OctTreeNode::render_tt = TimeTool();
TimeTool OctTreeNode::render_single_tt = TimeTool();
TimeTool OctTreeNode::casual_tt = TimeTool();
Uint32 OctTreeNode::render_calls = 0;
#endif
