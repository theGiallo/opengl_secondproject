/** 
 * File:   LivingEntity.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 26 August 2012
 * 
 */
#ifndef LIVINGENTIY_H
#define	LIVINGENTIY_H

#include "PhysicalObject2D.h"
#include "../graphics/Renderables2D.h"
#include "../graphics/CollisionSolver2D.h"
#include "../game/GameObject.h"
#include "../game/GameManager2D.h"
#include <cstdlib>

#define MASS_DENSITY 8.0f
#define EDGES_PER_RADIUS 15
#define MASS_EATEN_PER_SECOND 2.0f
#define FRICTION_COEFFICIENT 0.01f // [acc]/[mass]
#define SIDES_OF_LE 50

class LivingEntity : public PhysicalObject2D
{
public:
    class SpeciesCharacteristics
    {
    protected:
        float getAMutationPercentage(void);
    public:
        enum en_genes
        {
            max_vel,
            acceleration,
            sight, // ray of circle sight
            min_mass, // if mass is less than this the LE dies
            max_mass,
            fertility, // percentage; bigger = more offspring (with fertility==1 LE could die)
            offspring_vigour, // percentage
            reproduction_speed, // mass per second accumulated for the reproduction
            reproduction_mass_percentage_trigger, // if mass is this*max_mass the LE starts the reproduction
            mutation_min,
            mutation_max, // offspring has characteristics mutated of a percentage between min and max i.e. min<=(parent-offspring)/parent<=max
            own_kind_tolerance, // percentage of difference under which the LE considers another LE of its kind (and do not eats it) (=percentage difference abs average)
            reflexes, // times per minute; frequency of ambient scan (max 3600 = 60/s)
            
            GENES_COUNT
        };
        float genes[GENES_COUNT];
        static float standard_genes[GENES_COUNT];
        static SpeciesCharacteristics giveMutatedFromStandard(void);
        
        SpeciesCharacteristics getMutated(void) const;
        void mutate(void);
    };
    class StoringCollider : public Collider2D
    {
    protected:
        int count_collided_things;
        
    public:
        Renderable2D* collided_things[MAX_COLLISIONS_PER_OBJ];
        uint8_t collision_states[MAX_COLLISIONS_PER_OBJ];

        StoringCollider();
        int getCountCollidedThings(void);

        /**
         * @override from Collider2D
         */ 
        virtual void onCollision(uint8_t* states, Renderable2D** renderables, int num);
    };
    class Sight : public GameObject, public PositionableObject2D
    {
    protected:
        LivingEntity* owner_living_entity;
    public:
        Sight(float sight, LivingEntity* owner_living_entity);
        
        float distances[MAX_COLLISIONS_PER_OBJ];
        void calcDistances(void);
        int getCountThingsSeen(void);
        
        
        /**
         * @override from GameObject
         */ 
        virtual void update(Uint32 elapsedTime){}
        virtual void lateUpdate(Uint32 elapsedTime){}
        /**
         * @override from Renderable2D
         */
        virtual void render(void);
        virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
    };
    
protected:
    SpeciesCharacteristics genes;
    float mass, new_mass; // they are always the same before and after the update loop
    Sight* sight_bounder;
    bool reproducting;
    float reproduction_target_mass;
    float reproduction_current_mass;
    float reproduction_mass_trigger; // derived from reproduction_mass_percentage_trigger
    bool alive;
    
    uint32_t scan_period; //ms
    uint32_t elapsed_time_last_scan; //ms
    
    void checkIfIsAlive(void);
    void generateOffspring(void);
    void reproductionUpdate(uint32_t elapsedTime);
    void checkReproductivity(void);
    static int calcSides(float radius);
    static float calcRadius(float mass);
public:
    LivingEntity(const SpeciesCharacteristics& genes, float mass);
    ~LivingEntity(void);
    
    bool isNotOfMyKind(LivingEntity* le);
    bool isAlive(void);
    const SpeciesCharacteristics& getGenes(SpeciesCharacteristics& genes_out);
    const float& getMass(void)
    {
        return mass;
    }
    
    /**
     * @override from PhysicalObject2D
     */ 
    virtual void updatePhysics(Uint32 elapsedTime);
    /**
     * @override from PositionableObject2D
     */
    virtual void setPos(const Vec2& pos);
    virtual void setUp(const Vec2& up);
    virtual void setScale(const Vec2& scale);
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime);
    
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
    
    
    static unsigned int random_seed;
    static RegularPolygon2D livingentity_appearence;
};

#endif /* LIVINGENTIY_H */