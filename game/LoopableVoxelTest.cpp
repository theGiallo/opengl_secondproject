#include "LoopableVoxelTest.h"
#ifndef MAX
#define MAX(a,b) (a>b?a:b)
#endif

using namespace std;

void LoopableVoxelTest::keyChecks()
{
    Vec3 delta;
    if ( keys[SDLK_SPACE] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveY(0.1);
    }
    if ( keys[SDLK_c] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveY(-0.1);
    }
    if ( keys[SDLK_w] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveZ(0.1);
    }
    if ( keys[SDLK_s] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveZ(-0.1);
    }
    if ( keys[SDLK_d] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveX(0.1);
    }
    if ( keys[SDLK_a] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->moveX(-0.1);
    }
    if ( keys[SDLK_q] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->rotateY(1);
    }
    if ( keys[SDLK_e] )
    {
        dynamic_cast<PerspectiveCamera*>(camera)->rotateY(-1);
    }
     if ( keys[SDLK_x] )
     {
         Quaternion q;
         q.FromAxis(camera->vertical,-0.005);
         dynamic_cast<PerspectiveCamera*>(camera)->setPosSameTarget(q*(camera->pos-camera->target) + camera->target);
     }
     if ( keys[SDLK_v] )
     {
         Quaternion q;
         q.FromAxis(camera->vertical,0.005);
         dynamic_cast<PerspectiveCamera*>(camera)->setPosSameTarget(q*(camera->pos-camera->target) + camera->target);
     }
     if ( keys[SDLK_b] )
     {
        GameManager::renderBounders = !GameManager::renderBounders;
        keys[SDLK_b] = false;
     }
    if ( keys[SDLK_F2] )
    {
        anaglyph = (anaglyph+1)%3;
        if (anaglyph==0)
        {
            dynamic_cast<StereoPerspectiveCamera*>(camera)->stereo = false;
        } else if (anaglyph==1)
        {
            dynamic_cast<StereoPerspectiveCamera*>(camera)->stereo = true;
        }
        keys[SDLK_F2] = false;
    }
    if ( keys[SDLK_F3] )
    {
        OctTree::render_structure = !OctTree::render_structure;
        keys[SDLK_F3] = false;
    }
}
void LoopableVoxelTest::myGlewInit()
{
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if ( GLEW_OK!=err)
    {
        cout<<"error initializing GLEW: "<<glewGetErrorString(err)<<endl;
    }
    if (glewIsSupported("GL_VERSION_1_3"))
    {
        cout<<"OpenGL 1.3 supported"<<endl;
    }
    if (GLEW_VERSION_1_3)
    {
        cout<<"GLEW 1.3 supported"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_1"))
    {
        cout<<"OpenGL 2.1"<<endl;
    }
    if (glewIsSupported("GL_VERSION_4_0"))
    {
        cout<<"OpenGL 4.0"<<endl;
    }
    if (glewIsSupported("GL_VERSION_2_0"))
    {
        printf("Ready for OpenGL 2.0\n");
    } else
    {
        printf("OpenGL 2.0 not supported\n");
        //exit(1);
    }
}

void LoopableVoxelTest::init()
{
    if ( SDL_Init(SDL_INIT_EVERYTHING) !=0 )
    {
        exit(1);
    }
    rendered_frames_in_last_sec = 0;
    deltaTimeFPSCalc = 0;
    SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);
    window.initGL(640,480,32,SDL_RESIZABLE);
    window.setTitle("OpenGL Second Project");
    window.setResizeCallbackHolder(this);
    myGlewInit();
    Cube::initCubeList();
//    ChunkFace::initializeRenderingProcedure();
//    ChunkFace::initializeTexture();
    Cube::initializeTexture();
//    camera = new PerspectiveCamera();
    camera = new StereoPerspectiveCamera();
    anaglyph = 0;
    dynamic_cast<StereoPerspectiveCamera*>(camera)->stereo = false;
    dynamic_cast<StereoPerspectiveCamera*>(camera)->setNearFar(0.25, 3000);
    dynamic_cast<StereoPerspectiveCamera*>(camera)->setFOV(120);
    cout<<"camera dimensions: "<<dynamic_cast<StereoPerspectiveCamera*>(camera)->width<<" "<< dynamic_cast<PerspectiveCamera*>(camera)->height<<endl;
    dynamic_cast<StereoPerspectiveCamera*>(camera)->setPosSameLook(Vec3(-8.2,-7.8,0));
    dynamic_cast<StereoPerspectiveCamera*>(camera)->setTarget(Vec3(-7.2,-7.8,0));
    GameManager::activeCamera = dynamic_cast<StereoPerspectiveCamera*>(camera);
    GameManager::registerGameObject(dynamic_cast<StereoPerspectiveCamera*>(camera));
    cout<<"camera id = "<<dynamic_cast<StereoPerspectiveCamera*>(camera)->id<<endl;
    window.resize(640,480);
    if (false)
    {
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        try {
            testTexture = TextureManager::loadTexture("./resources/terrain_textures/terrain10partscube.png");
        }catch(bool err){
            cout<<"texture non caricata"<<endl;
        }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        cout<<"testTexture texID = "<<testTexture->texId<<" isTexture: "<<(glIsTexture(testTexture->texId)?"yes":"no")<<endl;
            cout<<"DBG0"<<endl;
        try{
            testModel.load("./resources/3ds/samples/house/medieval_house.3ds");
            testModel.translate(Vec3(0,0,-20));
            testModel.updateVArray();
    //        cout<<"the model of the house has "<<(testModel.hasTexture()?"":"no ")<<"texture"<<endl;
        } catch (bool r)
        {
            if (!r)
            {
                cout<<"WARNING!    -   model not loaded"<<endl;
            }
        }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        try{
            wagenModel.load("./resources/3ds/samples/w/wagen1_Lp_End.3ds");
        } catch(bool r)
        {
            if (!r)
            {
                cout<<"WARNING!    -   wagen model not loaded"<<endl;
            }
        }
    }

    if (!text.loadFontTexture("./resources/fonts/fontHand16_debug.png"))
    {
        cout<<"font non caricato!"<<std::endl;
    } else
    {
        text.BuildFont();
        cout<<"font texID = "<<text.font->texId<<" isTexture: "<<(glIsTexture(text.font->texId)?"yes":"no")<<" type "<<(text.font->type==GL_RGB?"GL_RGB":"GL_RGBA")<<endl;
    }

    try {
    pointLight = new OGLLocalLight();
	pointLight->setPosition(Vec3(1,-2,1).getNormalized());
	pointLight->is_directional = true;
	pointLight->setActive(true);
    pointLight->setAttenuated(false);
    pointLight->ambient[0]=pointLight->ambient[1]=pointLight->ambient[2]=0.5;
    pointLight->specular[0]=pointLight->specular[1]=pointLight->specular[2]=0.1;
    pointLight->diffuse[0]=pointLight->diffuse[1]=pointLight->diffuse[2]=0.4;
    pointLight->diffuse[3]=pointLight->specular[3]=pointLight->ambient[3]=1;
//
//    spherelLight = new OGLLocalLight(Vec3(6,1,-2), true, GL_LINEAR_ATTENUATION, 0.75);
//
//    spotLight = new OGLSpotLight(Vec3(4,1,-4), Vec3(0,1,-1).getNormalized(), 20, 2);

    cameraLight = new OGLSpotLight();
    cameraLight->setAttenuated(true);
    cameraLight->setAttenuationType(GL_LINEAR_ATTENUATION);
    cameraLight->setAttenuationParameter(0.05);
    cameraLight->setCutoff(30);
    cameraLight->setExponent(2);
    cameraLight->ambient[0]=cameraLight->ambient[1]=cameraLight->ambient[2]=0.2;
    cameraLight->specular[0]=cameraLight->specular[1]=cameraLight->specular[2]=0.2;
    cameraLight->diffuse[0]=cameraLight->diffuse[1]=cameraLight->diffuse[2]=0.2;
    cameraLight->diffuse[3]=cameraLight->specular[3]=cameraLight->ambient[3]=1;
    } catch (bool err)
    {
        cout<<"cazzo, troppe luci??"<<endl;
    }
    if (false)
    {
        StaticObject * SOhouse = new StaticObject();
        SOhouse->setModel(&testModel);
    //    GameManager::registerGameObject(SOhouse);
    //    GameManager::registerRenderable(SOhouse);
        StaticObject * SOwagen = new StaticObject();
        SOwagen->setModel(&wagenModel);
        wagenModel.translate(Vec3(20,0,0));
        wagenModel.scale(Vec3(0.25,0.25,0.25));
    }
//    GameManager::registerGameObject(SOwagen);
//    GameManager::registerRenderable(SOwagen);

//    StaticObject * cube1 = new StaticObject();
//    cube1->setModel(Model::getCube(0.1));
//    cube1->getModel()->translate(Vec3(0,0,50));
//
//    GameManager::registerGameObject(cube1);
//    GameManager::registerRenderable(cube1);
//
//
//    for(int i=0 ; i<10 ; i++ )
//    {
//        for (int j=0 ; j<10 ; j++)
//        {
//            for (int k=0 ; k<10 ; k++)
//            {
//                StaticObject * cube1 = new StaticObject();
//                cube1->setModel(Model::getCube(0.1));
//                cube1->getModel()->translate(Vec3(0.05+i*0.1,0.05+j*0.1,30.05+k*0.1));
//                cube1->getModel()->setColor(blue);
//                GameManager::registerGameObject(cube1);
//                GameManager::registerRenderable(cube1);
//            }
//        }
//    }


//    VoxelCube * vc;
//    float side = 0.1;
//    int iim = 10;
//    int jjm = 10;
//    int kkm = 1;
//    int im = 10;
//    int jm = 10;
//    int km = 1;
//    for(int ii=0 ; ii<iim ; ii++ )
//    {
//        for (int jj=0 ; jj<jjm ; jj++)
//        {
//            for (int kk=0 ; kk<kkm ; kk++)
//            {
//                for(int i=0 ; i<im ; i++ )
//                {
//                    for (int j=0 ; j<jm ; j++)
//                    {
//                        for (int k=0 ; k<km ; k++)
//                        {
//                            vc = new VoxelCube(Vec3(side/2.0+i*side+ii*im*side,side/2.0+j*side+jj*jm*side,30+side/2.0+k*side+kk*km*side), side, green);
////                            cout<<"cube at "<<side/2.0+i*side+ii*im*side<<","<<side/2.0+j*side+jj*jm*side<<","<<30+side/2.0+k*side+kk*km*side<<endl;
//                            GameManager::registerGameObject(vc);
//                            GameManager::registerRenderable(vc);
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    VoxelCube * bvc = new VoxelCube(Vec3(), 30, blue);
//    GameManager::registerGameObject(bvc);
//    GameManager::registerRenderable(bvc);
#if 0
        VoxelTerrainWrapper::initialize();
        std::cout<<"master father: "<<VoxelTerrainChunkOfChunks::getMasterFather()<<std::endl;
        VoxelTerrainChunkOfChunks* tmpVTCC = new VoxelTerrainChunkOfChunks();
        std::cout<<"tmpVTCC: "<<tmpVTCC<<std::endl;
        std::cout<<"master father: "<<VoxelTerrainChunkOfChunks::getMasterFather()<<std::endl;
    //    VoxelTerrainChunkOfChunks::getMasterFather()->setOrigin(Vec3(VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin().x,VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin().y,30));
        for(int i=0 ; i<150 ; i++ )
        {
            for (int j=0 ; j<150 ; j++)
            {
                for (int k=0 ; k<150 ; k++)
                {
                    if (true)//|| ((i+j+k)%2==1)
                    {
                        VoxelTerrainChunkOfChunks::getMasterFather()->addChunk(new VoxelAtom(
                                                Vec3(  VoxelAtom::side/2.0+i*VoxelAtom::side,
                                                       VoxelAtom::side/2.0+j*VoxelAtom::side,
                                                       VoxelAtom::side/2.0+k*VoxelAtom::side)
                                                ));
                    }
                }
            }
        }
    //    VoxelTerrainChunkOfChunks::getMasterFather()->initializeStructure();
            VoxelTerrainChunkOfChunks::getMasterFather()->active = VoxelTerrainChunkOfChunks::getMasterFather()->visible = VoxelTerrainChunkOfChunks::getMasterFather()->intersectable = true;
        GameManager::registerGameObject(VoxelTerrainWrapper::instance);
        GameManager::registerRenderable(VoxelTerrainWrapper::instance);
#else
        aabb.center = Vec3(0,10,0);
        aabb.side = 3;
        aabb.side = OctTree::atom_side;
        
        OctTree::tex_atom_face = TextureManager::loadTexture("./resources/terrain_textures/terrain_atom.png");
        if (OctTree::tex_atom_face==NULL)
        {
            std::cerr<<"terrain texture == NULL!!!"<<std::endl;

        } else
        {
            std::cout<<"terrain texture id = "<<OctTree::tex_atom_face->texId<<std::endl;
        }
        OctTree::root = new OctTree();
        if (IS_ATOM(OctTree::root))
        {
            std::cerr<<"OctTree::root node pointer would be considered an atom! "<<OctTree::root<<std::endl;
        }
        std::cout<<"root size: "<<sizeof(OctTree::root)<<std::endl;
        std::cout<<"CHAR_BIT: "<<CHAR_BIT<<std::endl;
        std::cout<<"OctTree* size: "<<sizeof(OctTree*)<<std::endl;
        std::cout<<"root: "<<OctTree::root<<std::endl;
        std::cout<<"root->childs: "<<OctTree::root->childs<<std::endl;
		 std::cout<<"IS_ATOM(OctTree::root):"<<(bool)IS_ATOM(OctTree::root)<<std::endl;
        for (int i=0; i<8 ; i++)
        {
            std::cout<<"root->childs["<<(int)i<<"]: "<<OctTree::root->childs[i]<<std::endl;
        }
        OctTree::root_center = Vec3_int();
        OctTree::root_half_side = 2;
        OctTree::root_level = 1;
        OctTree::render_structure = false;
        
        OctTree* test = NULL;
        SET_ATOM(test);
        std::cout<<"test: "<<test<<" IS_ATOM(test):"<<(bool)IS_ATOM(test)<<std::endl;
        std::cout<<"mask: "<<((unsigned long int)0x1<<(sizeof(OctTreeNode*)*CHAR_BIT-1))<<std::endl;
//        PerlinNoise pn(0.02,20);
//        pn.seed = 1564;
        Vec3 tmppos;
        float tmppn;
        unsigned long int num=0;
        int sideC = 50, tmph;
        perlinModule.SetPersistence(0.05);
        perlinModule.SetOctaveCount(4);
#ifdef DEBUG
#if 0
    running = true;
#endif
#endif
        for(int i=-sideC ; i<sideC ; i++ )
        {
            for (int k=-sideC ; k<sideC ; k++)
            {
#ifdef DEBUG
#if 0
				onRender();Sleep(2000);
#endif
#endif
                perlinModule.SetOctaveCount(4);
                tmppn = perlinModule.GetValue (i*0.05,0,k*0.05)+perlinModule.GetValue (i*0.05,k*0.05,0);
                tmppn = (tmppn/2.0+1)*0.25;
                tmppn += (perlinModule.GetValue(tmppn,i*0.005,k*0.005)+1)*0.5*(perlinModule.GetValue(i*0.0005,tmppn,k*0.0005)+1);
                tmph = 50*tmppn;
                for (int j=-64 ; j<tmph-64 ; j++)
                //for (int j=-sideC ; j<sideC ; j++ )
                {
                    tmppos = Vec3( 2*i+1,
                                   2*j+1,
                                   2*k+1);
                    //tmppos.print();std::cout<<std::endl;
                    //tmppn = perlinModule.GetValue(tmppos.x, tmppos.y, tmppos.z);
                    if (true)//|| ((i+j+k)%2==1)
                    {
//                        std::cout<<"\t\t\t-i "<<i<<" -j "<<j<<" -k "<<k<<std::endl;
#ifdef DEBUG
                        main_tt.check(START_CHECK);
#endif
                        if (OctTree::add_to_root(tmppos))
                        {
                            num++;
                        }
						if (i==-sideC&&j==-64&&k==-sideC)
						{
							OctTree::remove_from_root(Vec3_int());
						}
#ifdef DEBUG
                        main_tt.check(STOP_CHECK);
//                        std::cout<<"add delta: "<<main_tt.last_delta<<std::endl;
#endif
                    }
                }
            }
        }
#ifdef DEBUG
        std::cout<<"add average: "<<main_tt.all_average<<std::endl;
        std::cout<<"num: "<<num<<std::endl;
#endif
//        OctTree::remove_from_root(Vec3(-1,-1,-1)); // TODO corregge un errore strano che si vede solo con la render e l'importance limit
//        if (OctTree::add_to_root(Vec3(-0.1,-0.1,-0.1)))
//        {
//            std::cout<<"Inserito correttamente atomo in (-0.1,-0.1,-0.1)"<<std::endl;
//        }
#endif
//    std::cout<<"tmpVTCC: "<<(void*)tmpVTCC<<std::endl;
//    std::cout<<"master father: "<<(void*)VoxelTerrainChunkOfChunks::getMasterFather()<<std::endl;
//    std::cout<<"renderables[0]: "<<(void*)GameManager::renderables[0]<<std::endl;
//    GameManager::renderables[0]=VoxelTerrainChunkOfChunks::getMasterFather();
//    GameManager::renderables[0]->render();
//    std::cout<<"renderables[0]: "<<(void*)GameManager::renderables[0]<<std::endl;

//    VoxelCube * bvc = new VoxelCube(VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin()+Vec3( VoxelTerrainChunk::chunkSide*VoxelAtom::side*3.0/2.0,
//                                                            (VoxelTerrainChunk::chunkSide*VoxelAtom::side/2.0),
//                                                            (VoxelTerrainChunk::chunkSide*VoxelAtom::side/2.0)), VoxelTerrainChunk::chunkSide*VoxelAtom::side, white);
//	bvc->active = bvc->visible = bvc->intersectable = true;
//    GameManager::registerGameObject(bvc);
//    GameManager::registerRenderable(bvc);

//	std::cout<<"\t\tVoxelTerrainChunkOfChunks::chunkSide*VoxelAtom::side: "<<(float)(VoxelTerrainChunkOfChunks::chunkSide*VoxelAtom::side)<<std::endl;
//        std::cout<<"sizeof(ChunkFace): "<<sizeof(ChunkFace)<<std::endl;
//        std::cout<<"sizeof(VoxelAtom): "<<sizeof(VoxelAtom)<<std::endl;
//        std::cout<<"sizeof(VoxelTerrainChunkOfChunks): "<<sizeof(VoxelTerrainChunkOfChunks)<<std::endl;
//        std::cout<<"sizeof(*new ChunkFace()): "<<sizeof(*new ChunkFace())<<std::endl;
//        std::cout<<"sizeof(*new VoxelAtom()): "<<sizeof(*new VoxelAtom())<<std::endl;
        //std::cout<<"sizeof(*tmpVTCC): "<<sizeof(*tmpVTCC)<<std::endl;
        std::cout<<"sizeof(bool): "<<sizeof(bool)<<std::endl;
        std::cout<<"sizeof(OrientationType): "<<sizeof(OrientationType)<<std::endl;
        std::cout<<"sizeof(float): "<<sizeof(float)<<std::endl;
        std::cout<<"sizeof(double): "<<sizeof(double)<<std::endl;
        std::cout<<"sizeof(unsigned int): "<<sizeof(unsigned int)<<std::endl;
        std::cout<<"sizeof(OctTree): "<<sizeof(OctTree)<<std::endl;
        std::cout<<"sizeof(OctTree*): "<<sizeof(OctTree*)<<std::endl;

//    pcamera_test = new PerspectiveCamera();
//    pcamera_test->setFOV(120);
//    pcamera_test->setNearFar(0.01,150);
//    pcamera_test->setPosSameLook(Vec3(0,0,50));
//    pcamera_test->setLook(Vec3(-0.9,0.0,-0.4));
//    GameManager::registerRenderable(&(pcamera_test->frustum));

//    if (false)
//    {
//        SOwagen->visible = SOhouse->visible = false;
//    }

    eventHandler = new TestEvent();
    dynamic_cast<TestEvent*>(eventHandler)->holder = this;
    running = true;
}

void LoopableVoxelTest::onRender()
{
    bool stereo = false;
    if (anaglyph==1 && dynamic_cast<StereoPerspectiveCamera*>(camera))
    {
        stereo = true;
        dynamic_cast<StereoPerspectiveCamera*> (camera)->setEye(GL_LEFT);
    }
    if ( dynamic_cast<PerspectiveCamera*> (camera) )
    {
        dynamic_cast<PerspectiveCamera*> (camera)->setGLCamera();
    }

//    glShadeModel(GL_SMOOTH);
    //glEnable(GL_NORMALIZE);
    //glEnable(GL_COLOR_MATERIAL);
    //glColorMaterial(GL_FRONT, GL_DIFFUSE);
    GLfloat ambient[4] = {0.9,0.9,0.9, 1};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    //glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
//    cameraLight->setPosition(camera->pos);
//    cameraLight->setDirection(camera->getDirection());
//    cameraLight->glSet();
    pointLight->glSet();
//    spherelLight->glSet();
//    spotLight->glSet();
    glMateriali(GL_FRONT, GL_SHININESS, 25.0);
    GLfloat arr[4] = {0.5,0.5,0.5,1};
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, arr);
     // Material parameters:
     GLfloat material_Ka[] = {0.6f, 0.6f, 0.6f, 1.0f};
     GLfloat material_Kd[] = {0.2f, 0.2f, 0.2f, 1.0f};
     GLfloat material_Ks[] = {0.1f, 0.1f, 0.1f, 1.0f};
     GLfloat material_Ke[] = {0.0f, 0.0f, 0.0f, 0.0f};
     GLfloat material_Se = 20.0f;
     glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_Ka);
     glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_Kd);
     glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_Ks);
     glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, material_Ke);
     glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material_Se);
     //---------------

    glEnable(GL_LIGHTING);
    glShadeModel(GL_FLAT);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glEnable(GL_POLYGON_SMOOTH);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //    glutSolidCube(5);
//    glPolygonMode(GL_FRONT, GL_FILL);
//    glPolygonMode(GL_BACK, GL_LINE);
//    glDisable(GL_POLYGON_OFFSET_FILL);

    Vec3 cinter;
    float tmp_d;
    Vec3 inter;
    Vec3_int center;
    if (true)
    {
        if (stereo)
        {
            glColorMask(GL_TRUE, GL_FALSE, GL_FALSE, GL_TRUE);
            glDisable(GL_BLEND);
        }
        
        BoundingSphere bs = BoundingSphere(Vec3(1,10,1), 1);
        bs.render();
        BoundingBox aabb = BoundingBox(Vec3(10,10,10), 1);
        aabb.render();
        Vec3 iOnSphere, iOnAABB;
        float dist = bs.distanceFrom(&aabb, &iOnSphere, &iOnAABB);
//        cout<<"dist: "<<dist<<endl;
        glPointSize(5);
        glBegin(GL_POINTS);
                glColor3f(0,1,0);
                glVertex3f(1,10,1);
                glColor3f(1,0,0);
                glVertex3f(iOnSphere.x, iOnSphere.y,iOnSphere.z);
                glColor3f(1,1,0);
                glVertex3f(iOnAABB.x, iOnAABB.y, iOnAABB.z);
        glEnd();
        glBegin(GL_LINE);
                glColor3f(1,0,0);
                glVertex3f(iOnSphere.x, iOnSphere.y,iOnSphere.z);
                glColor3f(1,1,0);
                glVertex3f(iOnAABB.x, iOnAABB.y, iOnAABB.z);
        glEnd();
        
//        VoxelTerrainChunkOfChunks::totalActiveFaces = 0;
//        GameManager::render();
        OctTree::render_root();
        glPushAttrib(GL_ENABLE_BIT);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_ALPHA_TEST);
        
#ifdef DEBUG
        main_tt.check(START_CHECK);
#endif
        OctTreeLeaf* tmp = OctTree::select_from_root(camera->pos, camera->getDirection(), &tmp_d, &inter, &center);
#ifdef DEBUG
        main_tt.check(STOP_CHECK);
#endif
        
        if (tmp!=NULL)
        {
            aabb.side = OctTreeNode::atom_side;
            aabb.center = center;
            aabb.render();
            glPointSize(5);
            glBegin(GL_POINT);
                glColor3f(1,0,0);
                glVertex3f(inter.x, inter.y,inter.z);
            glEnd();
            glPointSize(1);
            
            inter=inter-center;
            cinter = inter;
            if ((ABS(inter.x)>ABS(inter.y)) && (ABS(inter.x)>ABS(inter.z)))
            {
                if (inter.x>0)
                {
                    center.x+=OctTreeNode::atom_side;
                } else
                {
                    center.x-=OctTreeNode::atom_side;
                }
            } else
            if ((ABS(inter.y)>ABS(inter.x)) && (ABS(inter.y)>ABS(inter.z)))
            {
                if (inter.y>0)
                {
                    center.y+=OctTreeNode::atom_side;
                } else
                {
                    center.y-=OctTreeNode::atom_side;
                }
            } else
            if ((ABS(inter.z)>ABS(inter.y)) && (ABS(inter.z)>ABS(inter.x)))
            {
                if (inter.z>0)
                {
                    center.z+=OctTreeNode::atom_side;
                } else
                {
                    center.z-=OctTreeNode::atom_side;
                }
            }
            aabb.center = center;
            aabb.render();
        }
        
        
        glBegin(GL_LINES);
        glColor4fv(white);
            glColor3ub(255,0,0);
            glVertex3f(0,0,0);
            glVertex3f(1,0,0);
            glColor3ub(0,255,0);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
            glColor3ub(0,0,255);
            glVertex3f(0,0,0);
            glVertex3f(0,0,1);
        glEnd();
        glColor4fv(white);
        glPopAttrib();
        
//        Vec3 inter;
//        aabb.render();
//        if (aabb.rayIntersect(camera->pos, camera->look, &inter)>0)
//        {
//            glPointSize(5);
//            glBegin(GL_POINT);
//                glColor3f(1,0,0);
//                glVertex3f(inter.x, inter.y,inter.z);
//            glEnd();
//            glPointSize(1);
//        }

        if (stereo)
        {
            dynamic_cast<StereoPerspectiveCamera*> (camera)->setEye(GL_RIGHT);
            dynamic_cast<PerspectiveCamera*> (camera)->setGLCamera();
            glClear(GL_DEPTH_BUFFER_BIT);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glColorMask(GL_FALSE, GL_TRUE, GL_TRUE, 0.5);
//            VoxelTerrainChunkOfChunks::totalActiveFaces = 0;
            GameManager::render();
            OctTree::render_root();
            glDisable(GL_TEXTURE_2D);
            glDisable(GL_BLEND);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_ALPHA_TEST);
            glBegin(GL_LINES);
                glColor3i(1,0,0);
                glVertex3f(0,0,0);
                glVertex3f(1,0,0);
                glColor3i(0,1,0);
                glVertex3f(0,0,0);
                glVertex3f(0,1,0);
                glColor3i(0,0,1);
                glVertex3f(0,0,0);
                glVertex3f(0,0,1);
            glEnd();
            glColor4fv(white);
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_ALPHA_TEST);
            glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        }
        /*glBegin(GL_POINT);
            glColor3f(1,0,0);
            glVertex3d(VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin().x, VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin().y, VoxelTerrainChunkOfChunks::getMasterFather()->getOrigin().z);
            glColor3f(1,1,1);
        glEnd();*/
//        glPushAttrib(GL_ENABLE_BIT);
//        glShadeModel(GL_SMOOTH);
//        glPolygonMode(GL_FRONT, GL_FILL);
//        glPolygonMode(GL_BACK, GL_LINE);
//        glEnable(GL_TEXTURE_2D);
//        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//        glBindTexture(GL_TEXTURE_2D, ChunkFace::terrain_atlas->texId);
//        glBegin(GL_QUADS);
//            glColor3f(1,1,1);
//            glTexCoord2d(0,3);
//            glVertex3f(1, 1, 1);
//            glColor3f(1,1,0);
//            glTexCoord2d(0,0);
//            glVertex3f(1, 1, -1);
//            glColor3f(1,0,0);
//            glTexCoord2d(3,0);
//            glVertex3f(1, -1, -1);
//            glColor3f(1,0,1);
//            glTexCoord2d(3,3);
//            glVertex3f(1, -1, 1);
//        glEnd();
//        glShadeModel(GL_FLAT);
//        glPopAttrib();
//        glColor3f(1,1,1);
    } else
    {
		std::cout<<"\t\t########"<<std::endl;
        glPushAttrib(GL_ENABLE_BIT);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_LINE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, ChunkFace::terrain_atlas->texId);
        glBegin(GL_QUADS);
            glColor3f(1,1,1);
            glTexCoord2d(0,1);
            glVertex3f(1, 1, 1);
            glColor3f(1,1,0);
            glTexCoord2d(0,1-0.0625);
            glVertex3f(1, 1, -1);
            glColor3f(1,0,0);
            glTexCoord2d(0.0625,1-0.0625);
            glVertex3f(1, -1, -1);
            glColor3f(1,0,1);
            glTexCoord2d(0.0625,1);
            glVertex3f(1, -1, 1);
        glEnd();
        glShadeModel(GL_FLAT);
        glPopAttrib();
    }
    
//    glPolygonMode(GL_FRONT, GL_FILL);
//
//#ifdef DEBUG_GL
//    CheckGLError(__FILE__, __LINE__);
//#endif
//    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//    glBindTexture(GL_TEXTURE_2D, text.font->texId);
//    glBegin(GL_QUADS);
//        glColor4f(1,1,1,1);
//        glNormal3i(0, 0, 1);
//        glTexCoord2d(0, 0);
//        glVertex3f(-1, 1, -1);
//        glTexCoord2d(1, 0);
//        glVertex3f(1, 1, -1);
//        glTexCoord2d(1, 1);
//        glVertex3f(1, -1, -1);
//        glTexCoord2d(0, 1);
//        glVertex3f(-1, -1, -1);
//    glEnd();
//
////    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
//    glBindTexture(GL_TEXTURE_2D, testTexture->texId);
//    glBegin(GL_QUADS);
//        glNormal3i(0, 0, 1);
//        glTexCoord2d(0, 0);
//        glVertex3f(0, 0, 0);
//        glTexCoord2d(1, 0);
//        glVertex3f(100, 0, 0);
//        glTexCoord2d(1, 1);
//        glVertex3f(100, 100, 0);
//        glTexCoord2d(0, 1);
//        glVertex3f(0, 100, 0);
//    glEnd();

#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif

//    wagenModel.draw();
//    testModel.setHasTexture(false);
//    testModel.drawImmediate();
//    glUseProgram(0);
//    text.print(8,window.getHeight()-64, "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm!@#$%&/()=?',;.:-_+*[]{}", 0.7, (float) window.getWidth(), (float) window.getHeight());
//    char *stringa = "Hey, 1 stringa!";
//    for (int i=0 ; stringa[i]!=0 ; i++ )
//    {
//        cout<<"stringa["<<i<<"] = "<<stringa[i]<<" = "<<(int)stringa[i]<<endl;
//    }
//    string str = stringa;
//    text.print((float) (window.getWidth() / 2 - str.length()*(text.fontSize)), (float) (window.getHeight() / 2 - (text.fontSize)), str, 2.0, (float) window.getWidth(), (float) window.getHeight());
    FPS_tt.check(MIDDLE_CHECK);
    deltaTimeFPSCalc += FPS_tt.last_delta;
    if ( deltaTimeFPSCalc >= 1000000)
    {
        rendered_frames_in_last_sec++;
        displayedFPS = (float)(rendered_frames_in_last_sec*1000000.0)/deltaTimeFPSCalc;
        rendered_frames_in_last_sec=0;
        deltaTimeFPSCalc = 0;
    } else
    {
        rendered_frames_in_last_sec++;
    }
    char fps_c[200];
//    sprintf(fps_c, "FPS:%.3f active faces:%d", displayedFPS, VoxelTerrainChunkOfChunks::getMasterFather()->nActiveFaces);
//    sprintf(fps_c, "FPS:%.3f active faces:%d", displayedFPS, VoxelTerrainChunkOfChunks::totalActiveFaces);
    sprintf(fps_c, "FPS:%.3f active_faces:%ld root level:%d", displayedFPS, OctTree::active_faces, OctTree::root_level);
    string fps = fps_c;
//    cout<<fps<<std::endl;
    text.print(16,window.getHeight()-32, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    fps_c[0]=0;
    sprintf(fps_c, "cameraPos:(%.1f,%.1f,%.1f)", camera->pos.x, camera->pos.y, camera->pos.z);
    fps = fps_c;
    text.print(16,window.getHeight()-52, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    fps_c[0]=0;
    sprintf(fps_c, "cameraUp:(%.1f,%.1f,%.1f)", camera->vertical.x, camera->vertical.y, camera->vertical.z);
    fps = fps_c;
    text.print(16,window.getHeight()-72, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    fps_c[0]=0;
    Vec3 look = camera->getDirection();
    sprintf(fps_c, "cameraLook:(%.1f,%.1f,%.1f)", look.x, look.y, look.z);
    fps = fps_c;
    text.print(16,window.getHeight()-92, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    look = camera->target;
    sprintf(fps_c, "cameraTarget:(%.2f,%.2f,%.2f)", look.x, look.y, look.z);
    fps = fps_c;
    text.print(16,window.getHeight()-112, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "intersection:(%.2f,%.2f,%.2f) distance: %.2f", inter.x, inter.y, inter.z, tmp_d);
    fps = fps_c;
    text.print(16,window.getHeight()-132, fps, 1, (float) window.getWidth(), (float) window.getHeight());

#ifdef DEBUG
    sprintf(fps_c, "render_array average time: %ld", OctTree::render_array_tt.all_average);
    fps = fps_c;
    text.print(16,window.getHeight()-152, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render_array last_delta  : %ld", OctTree::render_array_tt.last_delta);
    fps = fps_c;
    text.print(16,window.getHeight()-172, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render average time: %ld", OctTree::render_tt.all_average);
    fps = fps_c;
    text.print(16,window.getHeight()-192, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render last_delta  : %ld render_calls  : %u", OctTree::render_tt.last_delta, OctTree::render_calls);
    fps = fps_c;
    text.print(16,window.getHeight()-212, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render_single average time: %ld", OctTree::render_single_tt.all_average);
    fps = fps_c;
    text.print(16,window.getHeight()-232, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render_single last_delta  : %ld", OctTree::render_single_tt.last_delta);
    fps = fps_c;
    text.print(16,window.getHeight()-252, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render_root average time: %ld", OctTree::casual_tt.all_average);
    fps = fps_c;
    text.print(16,window.getHeight()-272, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "render_root last_delta  : %ld", OctTree::casual_tt.last_delta);
    fps = fps_c;
    text.print(16,window.getHeight()-292, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "select_from_root average time: %ld", main_tt.all_average);
    fps = fps_c;
    text.print(16,window.getHeight()-312, fps, 1, (float) window.getWidth(), (float) window.getHeight());
    sprintf(fps_c, "select_from_root last_delta  : %ld", main_tt.last_delta);
    fps = fps_c;
    text.print(16,window.getHeight()-332, fps, 1, (float) window.getWidth(), (float) window.getHeight());
#endif
    SDL_GL_SwapBuffers();
}

void LoopableVoxelTest::onResize(int w, int h)
{
    //    h = (h == 0) ? 1 : h;
//    glViewport(0, 0, w, h);

//	glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluOrtho2D(0.0, (GLfloat)w, 0.0, (GLfloat)h);
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();

	width=MAX(w, 1);
	height=MAX(h, 1);
    if ( dynamic_cast<PerspectiveCamera*>(camera) )
    {
        cout<<"siiiii"<<endl;
        dynamic_cast<PerspectiveCamera*>(camera)->setAspectRatio((float)width/(float)height);
    } else
    {
        cout<<"nooooo"<<endl;
    }
}

void LoopableVoxelTest::onEvent(SDL_Event* event)
{
    eventHandler->onEvent(event);
    switch ( event->type )
    {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_VIDEORESIZE:
            if ( !window.resize(event->resize.w, event->resize.h) )
            {
                cout<<"Oh no!"<<endl;
            }
//            if ( dynamic_cast<PerspectiveCamera*>(camera) )
//            {
//                dynamic_cast<PerspectiveCamera*>(camera)->setAspectRatio(event->resize.w/event->resize.h);
//            }
            break;
//        case SDL_MOUSEBUTTONDOWN:
//            printf("Mouse button %d pressed at (%d,%d)\n",
//                   event->button.button, event->button.x, event->button.y);
    }
}

void LoopableVoxelTest::onLoop(Uint32 elapsedTime)
{
//    std::cout<<"update.."<<std::endl;
    GameManager::update(elapsedTime);
//    cout<<"target: "; GameManager::activeCamera->target.print();cout<<endl;
    //cout<<"dist: "<<(GameManager::activeCamera->target-GameManager::activeCamera->pos).getModule()<<endl;
}

void LoopableVoxelTest::cleanUp()
{
    SDL_Quit();
}
const Uint8* LoopableVoxelTest::getKeysMap_const(void) const
{
    return keys;
}
Uint8* LoopableVoxelTest::getKeysMap(void)
{
    return keys;
}
void LoopableVoxelTest::setKeysMap(Uint8* keys)
{
    this->keys = keys;
}
bool LoopableVoxelTest::isLoopable(void)/// if false this won't loop any more
{
    return running;
}

 
 void LoopableVoxelTest::TestEvent::onExit()
 {
//         running=false;
 }
void LoopableVoxelTest::TestEvent::onResize(int w, int h)
{
//        if ( !window.resize(w,h) )
//        {
//            cout<<"Oh no! Resize failed!"<<endl;
//        }
//        if ( dynamic_cast<PerspectiveCamera*>(camera) )
//        {
//            dynamic_cast<PerspectiveCamera*>(camera)->setAspectRatio(w/(float)h);
//        }
}
 void LoopableVoxelTest::TestEvent::onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
 {
//         Vec3 delta;
//         switch (sym)
//         {
//            case SDLK_SPACE:
//                dynamic_cast<PerspectiveCamera*>(camera)->setPosSameLook(camera->pos+camera->vertical/10.0);
//                break;
//        }
 }
 void LoopableVoxelTest::TestEvent::onMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle)
 {
     if ( SDL_GetModState()&KMOD_CAPS )
     {
         int hh = holder->window.getHeight()/2;
         int hw = holder->window.getWidth()/2;
         dynamic_cast<PerspectiveCamera*>(holder->camera)->rotateY((hw-mX)*0.01);
         dynamic_cast<PerspectiveCamera*>(holder->camera)->rotateX((hh-mY)*0.01);
         SDL_WarpMouse(hw,hh);
     }
 }
 void LoopableVoxelTest::TestEvent::onLButtonDown(int mX, int mY)
 {
     float tmp_d;
     Vec3 inter;
     Vec3_int center;
//        ChunkFace* tmp = VoxelTerrainChunkOfChunks::getMasterFather()->selection(camera->pos, camera->getDirection(), &tmp_d);
     OctTreeLeaf* tmp = OctTree::select_from_root(holder->camera->pos, holder->camera->look, &tmp_d, &inter, &center);
    if (tmp!=NULL)
    {
//            VoxelTerrainChunkOfChunks::getMasterFather()->removeChunk(tmp->owner);
        std::cout<<"selezionato atomo con centro ";center.print();
        OctTree::remove_from_root(center);
    }
 }
 void LoopableVoxelTest::TestEvent::onRButtonDown(int mX, int mY)
 {
    float tmp_d;
     Vec3 inter;
     Vec3_int center;
    OctTreeLeaf* tmp = OctTree::select_from_root(holder->camera->pos, holder->camera->getDirection(), &tmp_d, &inter, &center);

    if (tmp!=NULL)
    {
        inter=inter-center;
        if ((ABS(inter.x)>ABS(inter.y)) && (ABS(inter.x)>ABS(inter.z)))
        {
            if (inter.x>0)
            {
                center.x+=OctTree::atom_side;
            } else
            {
                center.x-=OctTree::atom_side;
            }
        } else
        if ((ABS(inter.y)>ABS(inter.x)) && (ABS(inter.y)>ABS(inter.z)))
        {
            if (inter.y>0)
            {
                center.y+=OctTree::atom_side;
            } else
            {
                center.y-=OctTree::atom_side;
            }
        } else
        if ((ABS(inter.z)>ABS(inter.y)) && (ABS(inter.z)>ABS(inter.x)))
        {
            if (inter.z>0)
            {
                center.z+=OctTree::atom_side;
            } else
            {
                center.z-=OctTree::atom_side;
            }
        }
        OctTree::add_to_root(center);
    }
//            switch (tmp->orientation)
//            {
//                case PX:
//                    pos = tmp->owner->getPos()+Vec3(VoxelAtom::side, 0,0);
//                    break;
//                case MX:
//                    pos = tmp->owner->getPos()+Vec3(-VoxelAtom::side, 0,0);
//                    break;
//                case PY:
//                    pos = tmp->owner->getPos()+Vec3(0, VoxelAtom::side, 0);
//                    break;
//                case MY:
//                    pos = tmp->owner->getPos()+Vec3(0, -VoxelAtom::side, 0);
//                    break;
//                case PZ:
//                    pos = tmp->owner->getPos()+Vec3(0, 0, VoxelAtom::side);
//                    break;
//                case MZ:
//                    pos = tmp->owner->getPos()+Vec3(0, 0, -VoxelAtom::side);
//                    break;
//            }
//            VoxelTerrainChunkOfChunks::getMasterFather()->addChunk(new VoxelAtom(pos));
//            std::cout<<"post add"<<std::endl<<std::endl;
 }