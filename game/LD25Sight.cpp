#include "LD25Sight.h"
#include "LD25MOB.h"
#include "../game/StoringCollider.h"
#include <cfloat>

const int LD25Sight::SIDES_OF_CIRCLE = 20;
RegularPolygon2D LD25Sight::circle(SIDES_OF_CIRCLE);

LD25Sight::LD25Sight(float sight, PhysicalObject2D* owner)
{
    // 
    active = check_collisions = intersectable = true;
    visible = false;
    //
    collider = new StoringCollider();
    /*RegularPolygonBounder* b = new RegularPolygonBounder(calcSides(sight));
    b->setScale(Vec2(sight,sight));
    this->bounder = b;*/
    BoundingCircle* bc = new BoundingCircle(sight);
    this->bounder = bc;
    this->owner = owner;
    color[0] = color[1] = color[2] = 0.3;
    color[3] = 0.1;
}
Renderable2D** LD25Sight::getInSightThings(void)
{
    StoringCollider* sc = reinterpret_cast<StoringCollider*>(collider);
    return sc->collided_things;
}
void LD25Sight::calcDistances()
{
    StoringCollider* sc = reinterpret_cast<StoringCollider*>(collider);
    int count = sc->getCountCollidedThings();
    for (int i=0 ; i<count ; i++)
    {
        CollisionSolver2D::collide(*owner->bounder, *sc->collided_things[i]->bounder, distances[i]);
    }
}
int LD25Sight::getCountThingsSeen(void)
{
    return reinterpret_cast<StoringCollider*>(collider)->getCountCollidedThings();
}
void LD25Sight::render(void)
{
    float radius = reinterpret_cast<BoundingCircle*>(bounder)->getRadius_const();
    circle.setColor(color);
    circle.Tline_Ffill = true;
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslatef(pos.x,pos.y,0);
        glScalef(scale.x*radius, scale.y*radius, 1);
        circle.render();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
float LD25Sight::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    return FLT_MAX;
}