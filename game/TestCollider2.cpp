#include "TestCollider2D.h"
#include "GameManager2D.h"
#include <iostream>

#ifndef DEBUG_COLLISION_MANAGEMENT
//#define DEBUG_COLLISION_MANAGEMENT
#endif

TestCollider2D::TestCollider2D(GLfloat color[4])
{
    memcpy(&color_collision[0], &color[0], sizeof(color));
    swapped = false;
}
void TestCollider2D::setOwner(Renderable2D* owner)
{
    this->owner = owner;
}
void TestCollider2D::onCollision(uint8_t* states, Renderable2D** renderables, int num)
{
    bool none = true;
    for (int i=0 ; i<num ; i++)
    {
#ifdef DEBUG_COLLISION_MANAGEMENT
        std::cout<<dynamic_cast<GameObject*>(owner)->id<<" test collision with "<<dynamic_cast<GameObject*>(renderables[i])->id<<" ="<<(int)states[i]<<std::endl;
#endif
        if (!swapped && states[i]==COLLISION_ENTER)
        {
            owner->bounder->getColor(&color_orig[0]);
            owner->bounder->setColor(color_collision);
            swapped = true;
            return;
        }
        if (states[i]==COLLISION_STAY || states[i]==COLLISION_ENTER)
        {
            none = false;
        }
    }
    if (none && swapped)
    {
        owner->bounder->setColor(color_orig);
        swapped = false;
    }
}