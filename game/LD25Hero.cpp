#define __STDC_LIMIT_MACROS
#include <cstdint>
#include <limits>


#include "LD25Hero.h"
#include "LD25World.h"
#include "LD25MOB.h"
#include "../graphics/OGLColors.h"

#include <iostream>

const uint32_t LD25Hero::TIME_HERO_PERIOD_MS = 120000;
const float LD25Hero::coming_velocity = 20.0f;
const Vec2 LD25Hero::away_pos = Vec2(-50.0f, 40.0f);

LD25Hero::LD25Hero()
{
    // 
    active = check_collisions = intersectable = visible = true;
    //
    visible = false;
    setPos(away_pos);
    setVel(Vec2(.0f,.0f));
    setAcc(Vec2(.0f,.0f));
    max_evilness = 1;
    for (int i=0 ; i<(int)LD25World::num_players ; i++)
    {
        villains[i] = -1;
    }
    villains_count = 0;
    villain_coming_to_id = -1;
    purifying = waiting = fighting = going_away = coming = going_center = false;
    time_until_come_ms = TIME_HERO_PERIOD_MS;
    time_to_wait = time_to_fight = 0;
    after_wait_func = NULL;
    away_bounder = new BoundingCircle(1.0f, away_pos);
    center_bounder = new BoundingCircle(.5f);
    setColor(OGLColors::gold);
    color[3] = 0.5f;
}
LD25Hero::~LD25Hero()
{
    delete away_bounder;
    delete center_bounder;
}

void LD25Hero::targetVillain(void)
{
    coming = true;
    Vec2 v = LD25World::players[villains[villain_coming_to_id]].getPos() - getPos();
    v.normalize();
    v *= coming_velocity;
    setVel(v);
    std::cout<<"targeting "<<villain_coming_to_id<<std::endl;
}
void LD25Hero::goToCenter(void)
{
    coming = true;
    going_center = true;
    Vec2 v = -getPos();
    v.normalize();
    v *= coming_velocity;
    setVel(v);
}
void LD25Hero::goAway(void)
{
    Vec2 v = away_pos - getPos();
    v.normalize();
    v *= coming_velocity;
    setVel(v);
    setAcc(Vec2(.0f,.0f));
    going_away = true;
    std::cout<<"going away"<<std::endl;
}

void LD25Hero::update(Uint32 elapsedTime)
{
    if (waiting)
    {
        if (time_to_wait<elapsedTime)
        {
            waiting = false;
            time_to_wait = 0;
            if (after_wait_func!=NULL)
            {
                (this->*after_wait_func)();
            }
        } else
        {
            time_to_wait-=(int)elapsedTime;
        }
    } else
    if (purifying)
    {
        /**
         * Purify anybody
         */
        bool ga = true;
        for (int i=0 ; i<LD25World::num_players ; i++)
        {
            if (LD25World::players[i].evilness!=0)
            {
                ga = false;
                if (LD25World::players[i].evilness>0)
                {
                    if (LD25World::players[i].evilness>elapsedTime)
                    {
                        LD25World::players[i].evilness-=(int)elapsedTime;
                        LD25World::players[i].points += (int)elapsedTime;
                    } else
                    {
                        LD25World::players[i].points += LD25World::players[i].evilness;
                        LD25World::players[i].evilness = 0;
                    }
                } else
                {
                    if (-LD25World::players[i].evilness>elapsedTime)
                    {
                        LD25World::players[i].evilness+=(int)elapsedTime;
                        LD25World::players[i].points -= (int)elapsedTime;
                    } else
                    {
                        LD25World::players[i].points += LD25World::players[i].evilness;
                        LD25World::players[i].evilness = 0;
                    }
                }
            }
        }
        if (ga)
        {
            purifying = false;
            after_wait_func = &LD25Hero::goAway;
            waiting = true;
            time_to_wait = 1000;
            setVel(Vec2(.0f,.0f));
            setAcc(Vec2(.0f,.0f));
        }
    } else
    if (fighting)
    {
        /**
         * Fight the villains
         */
        if (time_to_fight<elapsedTime)
        {
            LD25World::players[villains[villain_coming_to_id]].points += time_to_fight;
            time_to_fight = 0;
            fighting = false;
            villain_coming_to_id++;
            if (villain_coming_to_id<villains_count)
            {
                targetVillain();
            } else
            {
                goToCenter();
            }
        } else
        {
            time_to_fight -= elapsedTime;
            if (LD25World::players[villains[villain_coming_to_id]].evilness>elapsedTime)
            {
                LD25World::players[villains[villain_coming_to_id]].evilness-=(int)elapsedTime;
            } else
            {
                LD25World::players[villains[villain_coming_to_id]].evilness = 0;
            }
            LD25World::players[villains[villain_coming_to_id]].points += elapsedTime;
        }
    } else
    if (coming)
    {
        /**
         * Go fight the villains
         */
        if (going_center)
        {
            float distance=0.0f;
            if (CollisionSolver2D::collide(*bounder, *center_bounder, distance)==COLLISION_COLLIDE)
            {
                purifying = true;
                coming = false;
                going_center = false;
                setVel(Vec2(.0f,.0f));
                setAcc(Vec2(.0f,.0f));
                setPos(center_bounder->getPos());
            }
//            else
//            if (distance>0 && distance<=10.0f)
//            {
//                if ((center_bounder->getPos()-getPos()).getNormalized().isEqual(getVel().getNormalized(), 1e-5f))
//                {
//                    Vec2 a = -getVel().getNormalized()*distance/5.0f;
//                    setAcc(a);
//                } else
//                {
//                    setVel(-vel);
//                }
//            }
        } else
        if (villain_coming_to_id<villains_count)
        {
            float distance=0.0f;
            if (CollisionSolver2D::collide(*bounder, *LD25World::players[villains[villain_coming_to_id]].bounder, distance)==COLLISION_COLLIDE)
            {
                coming = false;
                fighting = true;
                setVel(Vec2(.0f,.0f));
                time_to_fight = 2000+LD25World::players[villain_coming_to_id].getEvilness();
            }
        } else
        {
            goToCenter();
        }
    } else
    if (going_away)
    {
        float distance=0.0f;
        if (CollisionSolver2D::collide(*bounder, *away_bounder, distance)==COLLISION_COLLIDE)
        {
            setPos(away_pos);
            setVel(Vec2(.0f,.0f));
            going_away = visible = false;
            villains_count = 0;
            max_evilness = 1;
            time_until_come_ms = TIME_HERO_PERIOD_MS;
            
            /**
             * Unstun everybody
             */
            for (int i=0 ; i<(int)LD25World::num_players ; i++)
            {
                LD25World::players[i].beStunned(2000);
                if (LD25World::players[i].evilness!=0)
                {
                    LD25World::players[i].points += LD25World::players[i].evilness;
                    LD25World::players[i].evilness = 0;
                }
                LD25World::players[i].setPos(LD25World::startingPos[i]);
            }
            for (int i=0 ; i<(int)LD25World::mobs_count ; i++)
            {
                LD25World::mobs[i]->beStunned(0);
                LD25World::mobs[i]->acted = false;
                LD25World::mobs[i]->has_lollypop = true;
                LD25World::mobs[i]->throwed=false;
            }
            waiting = true;
            time_to_wait = 2000;
            after_wait_func = NULL;
        }
    } else
    if (time_until_come_ms<elapsedTime)
    {
        /**
         * It's time to come.
         */
        coming = true;
        time_until_come_ms=0;
        visible = true;
        
        /**
         * Detect the villains
         */
        for (int i=0 ; i<(int)LD25World::num_players ; i++)
        {
            if (LD25World::players[i].getEvilness()>=max_evilness)
            {
                if (LD25World::players[i].getEvilness()!=max_evilness)
                {
                    for (int j=0 ; j<villains_count ; j++)
                    {
                        villains[j] = -1;
                    }
                    villains_count = 0;
                    max_evilness = LD25World::players[i].getEvilness();
                }
                
                villains[villains_count++] = i;
            }
        }
        
        /**
         * DEBUG
         */
        for (int i=0 ; i<villains_count ; i++)
        {
            std::cout<<"Villain "<<villains[i]<<std::endl;
        }
        
        /**
         * Stun everybody
         */
        for (int i=0 ; i<(int)LD25World::num_players ; i++)
        {
            LD25World::players[i].beStunned(UINT32_MAX);
        }
        for (int i=0 ; i<(int)LD25World::mobs_count ; i++)
        {
            LD25World::mobs[i]->beStunned(UINT32_MAX);
        }
        
        if (villains_count!=0)
        /**
         * target first villain
         */
        {
            villain_coming_to_id = 0;
            targetVillain();
        } else
        {
            goToCenter();
        }
        
    } else
    {
        time_until_come_ms -=elapsedTime;
    }
}

void LD25Hero::render(void)
{
    LD25Player::render();
}
