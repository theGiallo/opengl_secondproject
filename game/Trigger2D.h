/* 
 * File:   Trigger2D.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 25 December 2012
 */

#ifndef TRIGGER2D_H
#define	TRIGGER2D_H

#include "../graphics/Renderables2D.h"
#include "GameObject.h"

class Trigger2D : public PositionableObject2D, public GameObject
{
public:
    Trigger2D(Bounder2D* bounder=NULL, Collider2D* collider=NULL);
    ~Trigger2D();
    
    void setBounder(Bounder2D* bounder);
    
    /**
     * @override from PositionableObject2D
     */
    virtual void setPos(const Vec2& pos){PositionableObject2D::setPos(pos);};
    virtual void setUp(const Vec2& up){PositionableObject2D::setUp(up);};
    virtual void setScale(const Vec2& scale){PositionableObject2D::setScale(scale);};
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime){};
    virtual void lateUpdate(Uint32 elapsedTime){};
    
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection){throw false;};
};

#endif /* TRIGGER2D_H */