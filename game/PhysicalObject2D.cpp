#include "PhysicalObject2D.h"
#include <cfloat>

PhysicalObject2D::PhysicalObject2D()
{
    appearence = NULL;
    vel = acc = Vec2();
    rot_vel = rot_acc = 0;
}
PhysicalObject2D::PhysicalObject2D(Renderable2D* appearence)
{
    this->appearence = appearence;
    vel = acc = Vec2();
    rot_vel = rot_acc = 0;
}
Renderable2D* PhysicalObject2D::getAppearence(void)
{
    return appearence;
}
void PhysicalObject2D::setAppearence(Renderable2D* appearence)
{
    this->appearence = appearence;
}
Vec2 PhysicalObject2D::getVel(void)
{
    return vel;
}
void PhysicalObject2D::setVel(Vec2 vel)
{
    this->vel = vel;
}
Vec2 PhysicalObject2D::getAcc(void)
{
    return acc;
}
void PhysicalObject2D::setAcc(Vec2 acc)
{
    this->acc = acc;
}
float PhysicalObject2D::getRotVel(void)
{
    return rot_vel;
}
void PhysicalObject2D::setRotVel(float rot_vel)
{
    this->rot_vel = rot_vel;
}
float PhysicalObject2D::getRotAcc(void)
{
    return rot_acc;
}
void PhysicalObject2D::setRotAcc(float rot_acc)
{
    this->rot_acc = rot_acc;
}

void PhysicalObject2D::updatePhysics(Uint32 elapsedTime)
{
    float ts = elapsedTime/1000.0f;
    vel += acc*ts;
    setPos(pos+vel*ts);
    rot_vel += rot_acc*ts;
    setRot(rot+rot_vel*ts);
}
void PhysicalObject2D::update(Uint32 elapsedTime)
{
}
void PhysicalObject2D::lateUpdate(Uint32 elapsedTime)
{

}
void PhysicalObject2D::render(void)
{
    if (appearence != NULL)
    {
        GLfloat rot_matrix[16];
        buildMatrixRotFromUpVector(up, rot_matrix);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
            glTranslatef(pos.x,pos.y,0);
            glScalef(scale.x,scale.y,1);
            glMultMatrixf(rot_matrix);
            appearence->render();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
    } else
    {
        glPushAttrib(GL_CURRENT_BIT);
            glColor3f(1,0,0);
            glBegin(GL_POINTS);
                glVertex2f(pos.x,pos.y);
            glEnd();
        glPopAttrib();
    }
}
float PhysicalObject2D::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    if (appearence!=NULL)
    {
        return appearence->rayIntersect(start, dir, intersection);
    }
    return FLT_MAX;
}