#ifndef IGAMELOOP_H
#define IGAMELOOP_H

#include "ILoopable.h"

class IGameLoop
{
public:
    virtual void setLoopable(ILoopable& loopable) =0;
    
    virtual void init(void) =0;
    virtual void run(void) =0;
};

#endif /* IGAMELOOP_H */