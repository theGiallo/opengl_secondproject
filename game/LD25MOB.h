/** 
 * File:   LD25MOB.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 15 December 2012
 * 
 */
#ifndef LD25MOB_H
#define	LD25MOB_H

#include "LD25Player.h"

class LD25MOB : public LD25Player
{
protected:
    bool still;
    float rot_vel_max;
    float max_acc_module;
    uint8_t type;
public:
    Uint32 time_to_mantain_movement;
    bool throwed;
    bool acted;
    uint32_t time_until_lollypop;
    bool has_lollypop;
    
    enum en_type
    {
        GRANNY,
        LOLLYPOPCHILDM,
        LOLLYPOPCHILDF,
        MAN,
        WOMAN,
        NUMTYPES
    };
    static char* type_names[];
    static float max_velocities_per_type[];
    static GLfloat* colors_per_type[];
    
    
    LD25MOB(Vec2 pos=Vec2(), uint8_t type=GRANNY);
    ~LD25MOB(void);
    
    void resetAccAndRot(void);
    
    uint8_t getType(void);
    char* getTypeName(void);
    
    /**
     * @override from PhysicalObject2D
     */ 
    virtual void updatePhysics(Uint32 elapsedTime);
    
    /**
     * @override from PositionableObject2D
     */
    virtual void setPos(const Vec2& pos){PositionableObject2D::setPos(pos);};
    virtual void setUp(const Vec2& up){PositionableObject2D::setUp(up);};
    virtual void setScale(const Vec2& scale){PositionableObject2D::setScale(scale);};
    
    /**
     * @override from GameObject
     */ 
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime){};
    
    /**
     * @override from Renderable2D
     */
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection){throw false;};

};

#endif /* LD25MOB_H */