#include "StoringCollider.h"

StoringCollider::StoringCollider()
{
    count_collided_things = 0;
}
int StoringCollider::getCountCollidedThings(void)
{
    return count_collided_things;
}
void StoringCollider::onCollision(uint8_t* states, Renderable2D** renderables, int num)
{
    for (int i=0 ; i<num ; i++)
    {
        collided_things[i] = renderables[i];
        collision_states[i] = states[i];
    }
    count_collided_things = num;
}