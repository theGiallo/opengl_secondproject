#ifndef LOOPABLE_VOXELTEST_H
#define LOOPABLE_VOXELTEST_H

#include "ILoopable.h"

#include <cstdlib>
#include <iostream>
#include <string>
//#include <stdlib.h>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL/SDL.h>
#include <list>


#include <libnoise/noise.h>



#include "../game/GameManager.h"
#include "../game/GameObject.h"
#include "../graphics/Renderable.h"
#include "../game/StaticObject.h"
//#include "../game/ChunkFace_VoxelAtom.h"
//#include "../game/VoxelCube.h"
//#include "../game/VoxelTerrainChunk.h"
#include "../game/VoxelTerrainChunkOfChunks.h"
#include "../game/OctTree.h"
#include "../IO/Event.h"
#include "../maths/Quaternion.h"
//#include "../maths/PerlinNoise.h"
#include "../graphics/Camera.h"
#include "../graphics/Window.h"
#include "../graphics/Model.h"
#include "../graphics/Cube.h"
#include "../graphics/TextureManager.h"
#include "../graphics/Texture.h"
#include "../graphics/Text.h"
#include "../graphics/OGLLight.h"
#include "../graphics/OGLLocalLight.h"
#include "../graphics/OGLSpotLight.h"
#include "../graphics/GLDebug.h"
#include "../graphics/BoundingSphere.h"
#include "../graphics/IResizableWindowCallbackHolder.h"
#include "../tools/TimeTool.h"

class LoopableVoxelTest : public ILoopable, IWindowResizeHandler
{
protected:
    Text text;
    Camera * camera;
    uint anaglyph;
    int width,height;
    Window window;
    bool running;
    Uint8* keys;
    Event* eventHandler;
    Model testModel, wagenModel;

    BoundingBox aabb;

    int64_t deltaTimeFPSCalc;
    int rendered_frames_in_last_sec;
    float displayedFPS;
    Texture *testTexture;
    OGLLocalLight *pointLight;
    OGLLocalLight *spherelLight;
    OGLSpotLight *spotLight;
    OGLSpotLight *cameraLight;

    PerspectiveCamera * pcamera_test;

#ifdef DEBUG
    TimeTool main_tt;
#endif
    TimeTool FPS_tt;

    noise::module::Perlin perlinModule;

    GLfloat blue[4];// = {0,0,1,1};
    GLfloat green[4];// = {0,1,0,1};
    GLfloat white[4];// = {1,1,1,1};
    
    void myGlewInit();
public:
    /**
     * @override from IWindowResizeHandler
     **/
    void onResize(int w, int h);
    
    /**
     * keys pointer has not to be allocated
     **/
    const Uint8* getKeysMap_const(void) const;
    Uint8* getKeysMap(void);
    void setKeysMap(Uint8* keys);

    void onEvent(SDL_Event* event);
    void keyChecks(void); /// checks the keymap
    void onLoop(Uint32 elapsedTime);
    void onRender(void);
    void init(void);
    void cleanUp(void);
    bool isLoopable(void); /// if false this won't loop any more
    ~LoopableVoxelTest(){};
    
    class TestEvent : public Event
    {
    public:
        void onExit();
        void onResize(int w, int h);
        void onKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
        void onMouseMove(int mX, int mY, int relX, int relY, bool Left, bool Right, bool Middle);
        void onLButtonDown(int mX, int mY);
        void onRButtonDown(int mX, int mY);
        
        LoopableVoxelTest* holder;
    };
};

#endif /* LOOPABLE_VOXELTEST_H */
