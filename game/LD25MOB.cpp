#include "LD25World.h"
#include "LD25MOB.h"
#include "../graphics/OGLColors.h"
#include <iostream>
#include <ctime>
#define _USE_MATH_DEFINES
#include <cmath>
#include <cassert>


float LD25MOB::max_velocities_per_type[] = {
                                            3.0f, //GRANNY
                                            4.0f, //LOLLYPOPCHILDM
                                            4.0f, //LOLLYPOPCHILDF
                                            5.0f, //MAN
                                            5.0f, //WOMAN
                                           };
GLfloat* LD25MOB::colors_per_type[] = {
                                    OGLColors::gray, //GRANNY
                                    OGLColors::cyan, //LOLLYPOPCHILDM
                                    OGLColors::magenta, //LOLLYPOPCHILDF
                                    OGLColors::brown, //MAN
                                    OGLColors::blue, //WOMAN
                                   };
char* LD25MOB::type_names[] = {"Granny", "Lollypop boy", "Lollypop girl", "Man", "Woman"};

LD25MOB::LD25MOB(Vec2 pos, uint8_t type) : type(type)
{
//    LD25Player::LD25Player();
    bounce_on_world_bounders = true;
    this->pos = pos;
    velocity_module = max_velocities_per_type[type];
    resetAccAndRot();
    rot_vel_max = 1.0f;
    max_acc_module = 1.0f;
    // rendered green
    color[2] = 0; color[3] = 0.23f;
    this->setColor(colors_per_type[type]);
    stunned = throwed = has_lollypop = false;
    color[3] = 0.3f;
}

LD25MOB::~LD25MOB()
{
    
}

void LD25MOB::resetAccAndRot()
{
    acc = Vec2(1.0f,.0f)*max_acc_module;
	rot = 2.0f*M_PI*(rand()/(float)RAND_MAX);
    acc.rotate(rot);
}
uint8_t LD25MOB::getType(void)
{
    return type;
}
char* LD25MOB::getTypeName(void)
{
    return type_names[type];
}

/**
 * @override from PhysicalObject2D
 */ 
void LD25MOB::updatePhysics(Uint32 elapsedTime)
{
    float ts = elapsedTime/1000.0f;
    
    rot_vel += rot_acc*ts;
    setRot(rot+rot_vel*ts);
    
    float r = rot_vel*(ts);
    acc.rotate(r);
    
    vel += acc*ts;
    if (!throwed && vel.getModule()>velocity_module)
    {
        vel.normalize();
        vel*=velocity_module;
    }
    setPos(pos+vel*ts);
}

void LD25MOB::update(Uint32 elapsedTime)
{
    desired_vel = vel;
    LD25Player::update(elapsedTime);
    
    if (stunned)
    {
        return;
    }
    
    if ((type==LOLLYPOPCHILDF||type==LOLLYPOPCHILDM) && !has_lollypop)
    {
        if (time_until_lollypop<elapsedTime)
        {
            has_lollypop = true;
        } else
        {
            time_until_lollypop -= elapsedTime;
        }
    }
    if (acted)
    {
        return;
    }
    if (time_to_mantain_movement<elapsedTime)
    {
        float p = rand()/(float)RAND_MAX*0.5f + 0.5f;
        switch(rand()%7)
        {
            case 0:
            case 3:
                if (still)
                {
                    resetAccAndRot();
                    still=false;
                }
                rot_vel = -rot_vel_max*p;
                break;
            case 1:
            case 4:
                if (still)
                {
                    resetAccAndRot();
                    still=false;
                }
                rot_vel = rot_vel_max*p;
                break;
            case 2:
            case 5:
                if (still)
                {
                    resetAccAndRot();
                    still=false;
                }
                rot_vel = 0.0f;
                break;
            case 6:
                still = true;
                acc = vel.getNormalized()*-max_acc_module;
                rot_vel = 0;
                break;
            default:
                assert(false);
                break;
        }
        time_to_mantain_movement = 500 + (!still&&rot_vel==0? (1000+rand()%500) : 0.0f);
    } else
    {
        if (still && acc.getNormalized().isEqual(vel.getNormalized()))
        {
            acc = vel = Vec2(.0f,.0f);
        }
        time_to_mantain_movement -= elapsedTime;
    }
}


/**
 * @override from Renderable2D
 */
void LD25MOB::render(void)
{
    LD25Player::render();
    if ((type==LOLLYPOPCHILDF||type==LOLLYPOPCHILDM) && has_lollypop)
    {
        Vec2 lp = Vec2(.0f,.5f).getRotated(vel.getAngle(Vec2(1.0f,.0f)))+pos;
        glPushAttrib(GL_ENABLE_BIT);
        glPushAttrib(GL_CURRENT_BIT);
        glPushAttrib(GL_POINT_BIT);
            glEnable(GL_BLEND);
            glPointSize(10);
            glColor4fv(OGLColors::blue);
                glBegin(GL_POINTS);
                    glVertex2f(lp.getX(),lp.getY());
                glEnd();
        glPopAttrib();
        glPopAttrib();
        glPopAttrib();
    }
}