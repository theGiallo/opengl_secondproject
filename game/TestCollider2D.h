/* 
 * File:   TestCollider2D.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 25 August 2012
 */

#ifndef TESTCOLLIDER2D_H
#define	TESTCOLLIDER2D_H

#include "../graphics/Renderables2D.h"

class TestCollider2D : public Collider2D
{
protected:
    Renderable2D* owner;
    GLfloat color_collision[4], color_orig[4];
    bool swapped;
public:
    TestCollider2D(GLfloat color[4]);
    void setOwner(Renderable2D* owner);
    void onCollision(uint8_t* states, Renderable2D** renderables, int num);
};

#endif /* TESTCOLLIDER2D_H */