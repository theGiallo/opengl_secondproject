#include "StaticObject.h"
#include <iostream>

StaticObject::StaticObject()
{
    this->visible = true;
    this->pos = Vec3();
    this->rot = Vec3();
    this->model = NULL; // TODO usare un modello di default
}

Model* StaticObject::getModel()
{
    return model;
}

Vec3 StaticObject::getPos()
{
    return pos;
}

Vec3 StaticObject::getRot()
{
    return rot;
}

void StaticObject::setModel(Model* m)
{
    model=m;
}

void StaticObject::setPos(Vec3 p)
{
    pos = p;
}
// TODO setPos e setRot ora non hanno senso
void StaticObject::setRot(Vec3 r)
{
    rot = r;
}

void StaticObject::update(Uint32 elapsedTime)
{

}
void StaticObject::lateUpdate(Uint32 elapsedTime)
{

}

void StaticObject::render()
{
    model->draw();
}