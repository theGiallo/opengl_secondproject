/* 
 * File:   VoxelTerrainChunk.h
 * Author: thegiallo
 * Directory: game
 *
 * Created on 19 marzo 2011, 17.34
 */

#ifndef VOXELTERRAINCHUNK_H
#define	VOXELTERRAINCHUNK_H

#include "../game/GameObject.h"
#include "../graphics/Renderable.h"
//#include "VoxelAtom.h"
#include "AtomFace_VoxelAtom.h"
#include "../maths/Vec3.h"
#include "../graphics/Frustum.h"

#include <list>

class VoxelTerrainChunk : public GameObject, public Renderable
{
protected:
    Vec3 origin;
    
public:
    const static Uint32 chunkSide = 10;
    VoxelAtom* matrix[chunkSide][chunkSide][chunkSide];
    std::list<AtomFace*> activeFaces[6];
    int nActiveFaces;
    VoxelTerrainChunk* adjacents[6];

    /**
     * testing - OK!
     */
    GLfloat *vArray;

    VoxelTerrainChunk();

    void setOrigin(Vec3 origin);
    Vec3 getOrigin();

    void initializeStructure();
    AtomFace * selection(Vec3 start, Vec3 dir, float * dist);
    void removeVoxelAtom(VoxelAtom * va);
    void addVoxelAtom(VoxelAtom * va);

//    bool isInFrustum(const Frustum* & frustum);

    virtual void update(Uint32 elapsedTime);
    virtual void render();
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection){float d=-1; selection(start, dir, &d);*intersection = start+dir*d;return d;};
};

#endif	/* VOXELTERRAINCHUNK_H */

