/**
 * File:   Main.cpp
 * Author: thegiallo
 *
 * Created on 28 gennaio 2011, 0.01
 * Modified on 06 November 2011
 * Modified on 05 Deceber 2011
 */

//#define DEBUG_GL

//#include "game/Loopable2DTest.h"
#include "game/Loopable2DLD25.h"
#include "game/LoopableVoxelTest.h"
#include "game/LoopableTest3DModels.h"
#include "game/IGameLoop.h"
#include "game/BasicGameLoop.h"
/*
 * 
 */
int main(int argc, char** argv)
{
    //ILoopable* loopable = new Loopable2DTest();
//    ILoopable* loopable = new LoopableVoxelTest();
	ILoopable* loopable = new LoopableTest3DModels();
    IGameLoop* game_loop = new BasicGameLoop(*loopable);
    game_loop->init();
    game_loop->run();
    return 0;
}
