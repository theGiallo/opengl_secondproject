/**
 * Author: theGiallo
 * Directory: tools
 **/

#ifndef PARALLELMERGESORT_H
#define	PARALLELMERGESORT_H
#include <iostream>

template <class T>
class MergeSort
{
public:
	MergeSort();
	MergeSort(int(*compareFunction)(T el1, T el2));
	/**
	 * returns -1,0,1
	 * -1: el1 will be put left
	 * 0: el1==el2
	 * 1: el2 will be put left
	 **/
	int(*compareFunction)(T el1, T el2);
	void merge(unsigned int count1, T* array1, unsigned int count2, T* array2, T* dest);
	void mergeSort(unsigned int count, T* array);

	void randomizeArray(unsigned int count, T* array, unsigned int npassages=3);
	void printArray(unsigned int count, T* array, std::string(*toString)(T to_print));
	void copyArray(unsigned int srccount, T* src, unsigned int destcount, T* dest);
};
template <class T>
MergeSort<T>::MergeSort()
{
}
template <class T>
MergeSort<T>::MergeSort(int(*compareFunction)(T el1, T el2))
{
	this->compareFunction = compareFunction;
}
template <class T>
void MergeSort<T>::merge(unsigned int count1, T* array1, unsigned int count2, T* array2, T* dest)
{
	unsigned int i=0,j=0;
	while(i<count1 || j<count2)
	{
		if (i<count1 && j<count2)
		{
            if (compareFunction(array1[i], array2[j])!=1)
			{
                dest[i+j] = array1[i];
				i++;
			}
            else
			{
                dest[i+j] = array2[j];
				j++;
			}
		}
        else if (i<count1)
		{
            dest[i+j] = array1[i];
			i++;
		}
        else if (j<count2)
		{
            dest[i+j] = array2[j];
			j++;
		}
	}
}
template <class T>
void MergeSort<T>::mergeSort(unsigned int count, T* array)
{
	if (count <= 1)
	{
		return;
	}
	unsigned int c1 = count/2;
	unsigned int c2 = count-c1;
	mergeSort(c1, array);
	mergeSort(c2, &array[c1]);
	T* tmpa = new T[count];
	merge(c1, array, c2, &array[c1], tmpa);
	for (unsigned int i=0 ; i<count ; i++)
	{
		array[i] = tmpa[i];
	}
	delete [] tmpa;
}

template <class T>
void MergeSort<T>::randomizeArray(unsigned int count, T* array, unsigned int npassages)
{
	T tmp;
	srand(time(NULL));
	unsigned int id;
	for (unsigned int p=0 ; p<npassages ; p++)
	{
		for (unsigned int i=0 ; i<count ; i++)
		{
			id = rand()%count;
			tmp = array[i];
			array[i] = array[id];
			array[id] = tmp;
		}
	}
}

template <class T>
void MergeSort<T>::printArray(unsigned int count, T* array, std::string(*toString)(T to_print))
{
	for (unsigned int i=0 ; i<count ; i++)
	{
		std::cout<<" "<<toString(array[i]);
	}
	std::cout<<" ";
}

template <class T>
void MergeSort<T>::copyArray(unsigned int srccount, T* src, unsigned int destcount, T* dest)
{
	for (unsigned int i=0 ; i<srccount && i<destcount ; i++)
	{
		dest[i] = src[i];
	}
}
#endif /* PARALLELMERGESORT_H */
