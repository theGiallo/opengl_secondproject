/* 
 * File:   FilePathTool.cpp
 * Author: thegiallo
 * 
 * Created on 18 aprile 2013, 20.08
 */

#include "FilePathTool.h"

FilePathTool::FilePathTool()
{
}

FilePathTool::FilePathTool(const FilePathTool& orig)
{
}

FilePathTool::~FilePathTool()
{
}

std::string FilePathTool::getFileExtension(const std::string& file_path)
{
    std::string ext = "";
    wchar_t ext_sep = '.';
    for (std::string::const_reverse_iterator cit = file_path.rbegin(); cit!=file_path.rend() ; cit++)
    {
        if (*cit == ext_sep)
        {
            return ext;
        }
        if (*cit == DIR_SEPARATOR)
        {
            return "";
        }
        std::string tmpstr;
        tmpstr.push_back(*cit);
        ext=tmpstr+ext;
    }
    return ext;
}

std::string FilePathTool::removeFileExtension(const std::string& file_path)
{
    int l = getFileExtension(file_path).length();
    if (l!=0)
    {
        l++;
    }
    return file_path.substr(0,file_path.length()-l);
}

std::string FilePathTool::getFileName(const std::string& file_path)
{
    std::string name = "";
    for (std::string::const_reverse_iterator cit = file_path.rbegin(); cit!=file_path.rend() ; cit++)
    {
        if (*cit == DIR_SEPARATOR)
        {
            return name;
        }
        std::string tmpstr;
        tmpstr.push_back(*cit);
        name=tmpstr+name;
    }
    return name;
}

std::string FilePathTool::removeFileName(const std::string& file_path)
{
    int l = getFileName(file_path).length();
    return file_path.substr(0,file_path.length()-l);
}
