/* 
 * File:   FilePathTool.h
 * Author: thegiallo
 *
 * Created on 18 aprile 2013, 20.08
 */

#include <string>

#ifndef FILEPATHTOOL_H
#define	FILEPATHTOOL_H

class FilePathTool
{
public:
    FilePathTool();
    FilePathTool(const FilePathTool& orig);
    virtual ~FilePathTool();
    static std::string getFileExtension(const std::string& file_path);
    static std::string removeFileExtension(const std::string& file_path);
    static std::string removeFileName(const std::string& file_path);
    static std::string getFileName(const std::string& file_path);
    
    static const char DIR_SEPARATOR = '/';
    
private:

};

#endif	/* FILEPATHTOOL_H */

