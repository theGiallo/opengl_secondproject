// textfile.cpp
//
// simple reading and writing for text files
//
// www.lighthouse3d.com
//
// You may use these functions freely.
// they are provided as is, and no warranties, either implicit,
// or explicit are given
//////////////////////////////////////////////////////////////////////


#include <stdlib.h>
#include <string.h>
#include <istream>
#include <iostream>
#include <fstream>
#include "textfile.h"

char *textFileRead(const char *fn)
{
    int length;
    char * buffer;

    std::ifstream is(fn);

    if ( !is.is_open() )
    {
        throw false;
    }

    // get length of file:
    is.seekg (0, std::ios::end);
    length = is.tellg();
    is.clear();
    is.seekg (0, std::ios::beg);

    // allocate memory:
    buffer = new char [length+1];

    // read data as a block:
    is.read (buffer,length);
    is.close();
    buffer[length]=0;
    textFileWrite("/home/thegiallo/Scrivania/log", buffer);

    return buffer;
}

int textFileWrite(char *fn, char *s) {

	FILE *fp;
	int status = 0;

	if (fn != NULL)
    {
		fp = fopen(fn,"w");
		if (fp != NULL)
        {
			if (fwrite(s,sizeof(char),strlen(s),fp) == strlen(s))
            {
				status = 1;
            }
			fclose(fp);
		} else
        {
            throw false;
        }
	}
	return(status);
}







