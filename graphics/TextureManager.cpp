#include "TextureManager.h"
#include <iostream>
std::list<Texture*> initTL()
{
    std::list<Texture*> tmp;
    return tmp;
}
#ifdef WIN32
std::list<Texture*> TextureManager::textures = initTL();
#else
std::list<Texture*> TextureManager::textures(initTL());
#endif
Texture* TextureManager::loadTexture(std::string file_path)
{
	std::cout<<"loading texture: "<<file_path<<std::endl;
    for (std::list<Texture*>::iterator it=TextureManager::textures.begin(); it!=TextureManager::textures.end(); ++it)
    {
        if ( (*it)->file_path.compare(file_path)==0 )
        {
            return *it;
        }
    }
    std::cout<<"postfor"<<std::endl;
    Texture * tmp_tex = new Texture();
	bool loaded = tmp_tex->load(file_path);
    std::cout<<"post load"<<std::endl;
    if ( !loaded )
    {
        throw false;
    }
    std::cout<<"pre isstored"<<std::endl;
    Texture *texp;
    if ( tmp_tex->isStored(&TextureManager::textures, &texp) )
    {
        delete tmp_tex;
        return texp;
    }
    std::cout<<"prestoreanyway"<<std::endl;
    tmp_tex->storeAnyway(&TextureManager::textures);
    return tmp_tex;
}
