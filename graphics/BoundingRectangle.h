/** 
 * File:   BoundingRectangle.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on August 25 2012
 */

#ifndef BOUNDINGRECTANGLE_H
#define	BOUNDINGRECTANGLE_H

#include "../graphics/Renderables2D.h"

class BoundingRectangle : public Bounder2D
{
protected:
    float width, height;
    float scaled_width, scaled_height;
    GLfloat varray[4*2];
    bool updated;
public:
    BoundingRectangle(const Vec2& pos, const float& width, const float& height, const float& rot = 0.0f, const Vec2& scale = UNITARY_SCALE);
    
    const float& getWidth(void) const;
    const float& getScaledWidth(void) const;
    void setWidth(const float& width);
    const float& getHeight(void) const;
    const float& getScaledHeight(void) const;
    void setHeight(const float& height);
    
    const GLfloat* getVArray(void) const;
    void updateVArray(void);
    
    void setPos(const Vec2& pos);
    const float& getRot(void) const;
    void setRot(const float& rot);
    virtual void setScale(const Vec2& scale);
    
    int collide(const Camera2D &camera);
    void render(void);
    float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};

#endif /* BOUNDINGRECTANGLE_H */