/* 
 * File:   Bounder.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 28 marzo 2011, 9.45
 */

#ifndef BOUNDER_H
#define	BOUNDER_H

#include "../maths/Vec3.h"
#include "../graphics/Renderable.h"

class Frustum;

class Bounder : public Renderable
{
public:
    int test;
    virtual void render(void){};
    /**
     * @return this in relation to the frustum: is out, is inside(contained), collide or
     *         contains the frustum
     *          COLLISION_OUT 0
     *          COLLISION_IN 1
     *          COLLISION_COLLIDE 2
     *          COLLISION_CONTAINS 3
     **/
    virtual int collide(Frustum *frustum){return 0;};
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection){return 0;};
};

#include "Frustum.h"

#endif	/* BOUNDER_H */

