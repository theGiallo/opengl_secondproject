/* 
 * File:   Window.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 29 gennaio 2011, 10.02
 */

#ifndef WINDOW_H
#define	WINDOW_H

#include <string>
#ifdef WIN32
	#include <windows.h>
#endif
#include <SDL/SDL.h>
#include "IResizableWindowCallbackHolder.h"

class Window
{
protected:
    int width, height;
    int bitsPerPixel;
    Uint32 flags;
    SDL_Surface *surface;
    std::string title;
    IWindowResizeHandler* resize_handler;

public:
    Window(void);
    ~Window(void);

    void setTitle(std::string title);
    bool init(int width, int height, int bitsPerPixel, Uint32 flags);
    bool initGL(int width, int height, int bitsPerPixel, Uint32 flags=0);

    bool resize(int width, int height);
    void setResizeCallbackHolder(IWindowResizeHandler* onresize_holder);

    int getWidth(void);
    int getHeight(void);
    int getBitsPerPixel(void);
    Uint32 getFlags(void);
    SDL_Surface* getSurface(void);
    std::string getTitle(void);
};

#endif	/* WINDOW_H */

