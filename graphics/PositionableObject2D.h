/* 
 * File:   StaticObject.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 06 November 2011
 */
#ifndef POSITIONABLEOBJECT2D_H
#define	POSITIONABLEOBJECT2D_H

#error
#include "../graphics/Renderables2D.h"
#include "../maths/Vec2.h"


class PositionableObject2D : public Renderable2D
{
protected:
    Vec2 pos;
    Vec2 up; ///default is (0,1)
    float rot;
public:
    PositionableObject2D();
    const Vec2& getPos(void) const;
    virtual void setPos(const Vec2& pos);
    const Vec2& getUp(void) const;
    virtual void setUp(const Vec2& up);
    float getRot(void) const;
    virtual void setRot(float rot);

    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
};

#endif	/* POSITIONABLEOBJECT2D_H */
