#include "OGLTools.h"

OGLTools::OGLTools()
{
}

void OGLTools::translateVArray(GLfloat* vArray, unsigned int count, const Vec2& t)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        vArray[i] += t.x;
        vArray[i+1] += t.y;
    }
}
void OGLTools::rotateVArray(GLfloat* vArray, unsigned int count, float rot)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        Vec2 res = Vec2(vArray[i],vArray[i+1]).getRotated(rot);
        vArray[i] = res.x;
        vArray[i+1] = res.y;
    }
}
void OGLTools::scaleVArray(GLfloat* vArray, unsigned int count, const Vec2& s)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        vArray[i] *= s.x;
        vArray[i+1] *= s.y;
    }
}