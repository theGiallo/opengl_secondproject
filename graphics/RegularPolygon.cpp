#include "RegularPolygon.h"
#include <cfloat>
#define _USE_MATH_DEFINES
#include <cmath>
GLfloat* RegularPolygon2D::varrays[N_VARRAYS];
void RegularPolygon2D::initVArrays(void)
{
	for (int i=0 ; i<N_VARRAYS ; i++)
	{
		varrays[i] = NULL;
	}
}
void RegularPolygon2D::initVArray(unsigned int nsides)
{
	if (varrays[nsides-3]==NULL)
	{
		varrays[nsides-3] = new GLfloat[nsides*2];
	}
	for (unsigned int i=0 ; i<nsides ; i++)
	{
		varrays[nsides-3][i*2] = -sinf(M_PI*2.0f*(float)i/(float)nsides);
		varrays[nsides-3][i*2+1] = cosf(M_PI*2.0f*(float)i/(float)nsides);
	}
}
RegularPolygon2D::RegularPolygon2D()
{
	setNSides(DEFAULT_NSIDES);
	Tline_Ffill = DEFAULT_TLINEFFILL;
}
RegularPolygon2D::RegularPolygon2D(unsigned int nsides)
{
	setNSides(nsides);
	Tline_Ffill = DEFAULT_TLINEFFILL;
}
RegularPolygon2D::RegularPolygon2D(unsigned int nsides, bool Tline_Ffill)
{
	setNSides(nsides);
	this->Tline_Ffill = Tline_Ffill;
}
void RegularPolygon2D::setNSides(unsigned int nsides)
{
	this->nsides = nsides;
	if (varrays[nsides-3]==NULL)
	{
		initVArray(nsides);
	}
}
unsigned int RegularPolygon2D::getNSides(void) const
{
	return nsides;
}
void RegularPolygon2D::render(void)
{
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_ENABLE_BIT);
    glPushAttrib(GL_POLYGON_BIT);
		if (Tline_Ffill)
		{
			glPolygonMode(GL_FRONT, GL_LINE);
		} else
		{
			glPolygonMode(GL_FRONT, GL_FILL);
		}
		glPolygonMode(GL_BACK, GL_NONE);
		glEnableClientState(GL_VERTEX_ARRAY);
		glInterleavedArrays( GL_V2F,2*sizeof(GLfloat),&varrays[nsides-3][0]);
                glColor4fv(&color[0]);
		glDrawArrays(GL_POLYGON, 0, nsides);
		glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
    glPopAttrib();
    glPopAttrib();
    glPopAttrib();
}
float RegularPolygon2D::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
	return FLT_MAX;
}