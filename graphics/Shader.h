/* 
 * File:   Shader.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 7 febbraio 2011, 15.21
 */

#ifndef SHADER_H
#define	SHADER_H

#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <string>

class Shader
{
public:
    GLuint vertex, fragment;
    std::string path_name;

    bool load(std::string pathName="");
    Shader(void);
    char* getCompilerLog(bool vertOrFrag = true);
};

#endif	/* SHADER_H */

