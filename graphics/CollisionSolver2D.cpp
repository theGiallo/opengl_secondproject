#include "CollisionSolver2D.h"
#include "../graphics/GJK2D.h"
#include "BoundingBox.h"

enum CollisionSolver2D::rel_1d CollisionSolver2D::getRel1D(float c, float r, float w)
{
    if (c+r <= w) // leftOfR
    {
        if (c-r >= 0) // rightOfL
        {
            return INSIDE;// inside W
        } else
        {
            if (c+r <= 0)
            {
                return LEFTOF;// left of W
            } else
            {
                return INTERSECTSLEFT;// intersects left W
            }
        }
    } else
    {
        if (c-r >= w)
        {
            return RIGHTOF;// right of W
        } else
        {
            if (c-r >= 0) // rightOfL
            {
                return INTERSECTSRIGHT;// intersects right W
            } else
            {
                return INTERSECTSBOTH;// intersects both R&L
            }
        }
    }
}

int CollisionSolver2D::collide(const BoundingCircle& c1, const BoundingCircle& c2, float& distance_out)
{
    distance_out = (c1.getPos()-c2.getPos()).getModule();
    if (distance_out+c1.getRadius_const()<=c2.getRadius_const())
    {
        distance_out-=c1.getRadius_const()+c2.getRadius_const();
        return COLLISION_IN;
    }
    if (distance_out+c2.getRadius_const()<=c1.getRadius_const())
    {
        distance_out-=c1.getRadius_const()+c2.getRadius_const();
        return COLLISION_CONTAINS;
    }
    if (distance_out<=c1.getRadius_const()+c2.getRadius_const())
    {
        distance_out-=c1.getRadius_const()+c2.getRadius_const();
        return COLLISION_COLLIDE;
    }
    distance_out-=c1.getRadius_const()+c2.getRadius_const();
    return COLLISION_OUT;
}
int CollisionSolver2D::collide(const BoundingCircle& c1, const BoundingRectangle& r1, float& distance_out)
{
    //TODO debug this
    //TODO implement distance calc
    Vec2 C = c1.getPos();
    float r = c1.getRadius_const();
    const GLfloat* va = r1.getVArray();
    Vec2 tr(va[0],va[1]), tl(va[2],va[3]), bl(va[4],va[5]), br(va[6], va[7]);
//    for (int i=0 ; i<4 ; i++)
//    {
//        std::cout<<va[i*2]<<", "<<va[i*2+1]<<std::endl;
//    }
    //
    distance_out = GJK2D::distance(va,4,C);
//    std::cout<<"distance "<<distance_out<<std::endl;
    if (distance_out>r)
    {
        return COLLISION_OUT;
    }
    if (distance_out==r)
    {
        return COLLISION_COLLIDE;
    }
    int c = 0;
    if ( (c1.getPos()-bl).getModule()<=r )
    {
        c++;
    }
    if ((c1.getPos()-br).getModule()<=r)
    {
        c++;
    }
    if ((c1.getPos()-tr).getModule()<=r)
    {
        c++;
    }
    if ((c1.getPos()-tl).getModule()<=r)
    {
        c++;
    }
    if (c==4)
    {
        return COLLISION_CONTAINS;
    }
    
    return COLLISION_COLLIDE;
    
    //needs more calcs
    if (c==0)
    {
        return COLLISION_IN;
    }
    //
    
    
    
    float w = (bl-br).getModule();
    float h = (tr-br).getModule();
    
    C-=r1.getPos();
    C.rotate(-r1.getRot());
    C+=Vec2(w/2.0f, h/2.0f);
    /**
     * Now C refers to an axis system centered into the bottom left corner of r1
     * and aligned with r1
     **/
    rel_1d Wrel = getRel1D(C.x, r, w);
    rel_1d Hrel = getRel1D(C.y, r, h);
    C.print();c1.getPos().print();
    std::cout<<"r:"<<r<<" rot:"<<r1.getRot()<<std::endl;
    std::cout<<"w:"<<w<<" h:"<<h<<std::endl;
    std::cout<<"Wrel:"<<Wrel<<" Hrel:"<<Hrel<<std::endl;
    tl.print();
    std::cout<<"c tl "<<(c1.getPos()-tl).getModule()<<std::endl;
    if (Wrel == INSIDE && Hrel == INSIDE)
    {
        return COLLISION_IN;
    }
    if (Wrel == INTERSECTSBOTH && Hrel == INTERSECTSBOTH && (c1.getPos()-bl).getModule()<=r &&(c1.getPos()-br).getModule()<=r &&(c1.getPos()-tr).getModule()<=r &&(c1.getPos()-tl).getModule()<=r )
    {
        return COLLISION_CONTAINS;
    }
    if ( (Wrel == INSIDE && Hrel == INTERSECTSBOTH) || (Wrel == INTERSECTSBOTH && Hrel == INSIDE) )
    {
        return COLLISION_COLLIDE;
    }
    if ( ((Wrel == INTERSECTSLEFT || Wrel == INTERSECTSRIGHT) && Hrel == INSIDE) ||
         ((Hrel == INTERSECTSLEFT || Hrel == INTERSECTSRIGHT) && Wrel == INSIDE)
       )
    {
        return COLLISION_COLLIDE;
    }
    if  (Wrel == INTERSECTSLEFT)
    {
        if (Hrel == INTERSECTSLEFT)
        {
            // bottom left
            if ((c1.getPos()-bl).getModule()<=r)
            {
                return COLLISION_COLLIDE;
            }
        } else
        if (Hrel == INTERSECTSRIGHT)
        {
            // top left
            if ((c1.getPos()-tl).getModule()<=r)
            {
                return COLLISION_COLLIDE;
            }
        }
    } else
    if (Wrel == INTERSECTSRIGHT)
    {
        if (Hrel == INTERSECTSLEFT)
        {
            // bottom right
            if ((c1.getPos()-br).getModule()<=r)
            {
                return COLLISION_COLLIDE;
            }
        } else
        if (Hrel == INTERSECTSRIGHT)
        {
            // top right
            if ((c1.getPos()-tr).getModule()<=r)
            {
                return COLLISION_COLLIDE;
            }
        }
    }
    std::cout<<"circle vs rectangle -> COLLISION_OUT"<<std::endl;
    return COLLISION_OUT;
    //return COLLISION_COLLIDE;
}
int CollisionSolver2D::collide(const BoundingCircle& c1, const RegularPolygonBounder& rp1, float& distance_out)
{
    // TODO implement
    return COLLISION_OUT;
}
int CollisionSolver2D::collide(const BoundingRectangle& r1, const BoundingRectangle& r2, float& distance_out)
{
    distance_out = GJK2D::distance(r1.getVArray(), 4, r2.getVArray(), 4);
    if ( distance_out>0 )
    {
            return COLLISION_OUT;
    }
    return COLLISION_COLLIDE;
}
int CollisionSolver2D::collide(const BoundingRectangle& r1, const BoundingCircle& c1, float& distance_out)
{
    int res = collide(c1,r1,distance_out);
    if (res==COLLISION_COLLIDE || res==COLLISION_OUT)
    {
        return res;
    }
    if (res==COLLISION_CONTAINS)
    {
        return COLLISION_IN;
    }
    return COLLISION_CONTAINS;
}
int CollisionSolver2D::collide(const BoundingRectangle& r1, const RegularPolygonBounder& rp1, float& distance_out)
{
    int res = collide(rp1,r1,distance_out);
    if (res==COLLISION_COLLIDE || res==COLLISION_OUT)
    {
        return res;
    }
    if (res==COLLISION_CONTAINS)
    {
        return COLLISION_IN;
    }
    return COLLISION_CONTAINS;
}
int CollisionSolver2D::collide(const RegularPolygonBounder& rp1, const RegularPolygonBounder& rp2, float& distance_out)
{
    float dist = GJK2D::distance(rp1.getVArray(), rp1.getPolygon().getNSides(), rp2.getVArray(), rp2.getPolygon().getNSides());
    if ( dist>0 )
    {
            return COLLISION_OUT;
    }
    return COLLISION_COLLIDE;
}
int CollisionSolver2D::collide(const RegularPolygonBounder& rp1, const BoundingRectangle& r1, float& distance_out)
{
    distance_out = GJK2D::distance(rp1.getVArray(), rp1.getPolygon().getNSides(), r1.getVArray(), 4);
    if ( distance_out>0 )
    {
            return COLLISION_OUT;
    }
    return COLLISION_COLLIDE;
}
int CollisionSolver2D::collide(const RegularPolygonBounder& rp1, const BoundingCircle& c1, float& distance_out)
{
    int res = collide(c1,rp1,distance_out);
    if (res==COLLISION_COLLIDE || res==COLLISION_OUT)
    {
        return res;
    }
    if (res==COLLISION_CONTAINS)
    {
        return COLLISION_IN;
    }
    return COLLISION_CONTAINS;
}

int CollisionSolver2D::collide(const Bounder2D& b1, const Bounder2D& b2, float& distance_out)
{
    const BoundingCircle* bc[2] = {dynamic_cast<const BoundingCircle*>(&b1),
    dynamic_cast<const BoundingCircle*>(&b2)};
    const BoundingRectangle* br[2] = {dynamic_cast<const BoundingRectangle*>(&b1), dynamic_cast<const BoundingRectangle*>(&b2)};
    const RegularPolygonBounder* rpb[2] = {dynamic_cast<const RegularPolygonBounder*>(&b1), dynamic_cast<const RegularPolygonBounder*>(&b2)};
    if (bc[0])
    {
        if (bc[1])
        {
            return collide(*bc[0], *bc[1], distance_out);
        }
        if (br[1])
        {
            return collide(*bc[0], *br[1], distance_out);
        }
        if (rpb[1])
        {
            return collide(*bc[0], *rpb[1], distance_out);
        }
    }
    if (br[0])
    {
        if (bc[1])
        {
            return collide(*br[0], *bc[1], distance_out);
        }
        if (br[1])
        {
            return collide(*br[0], *br[1], distance_out);
        }
        if (rpb[1])
        {
            return collide(*br[0], *rpb[1], distance_out);
        }
    }
    if (rpb[0])
    {
        if (bc[1])
        {
            return collide(*rpb[0], *bc[1], distance_out);
        }
        if (br[1])
        {
            return collide(*rpb[0], *br[1], distance_out);
        }
        if (rpb[1])
        {
            return collide(*rpb[0], *rpb[1], distance_out);
        }
    }
    // this mast not be called.
    std::cerr<<"ERROR! - Method int CollisionSolver2D::collide(const Bounder2D& b1, const Bounder2D& b2) lack of some classes or called with Bounder2D"<<std::endl;
    return COLLISION_OUT;
}