#include "Shader.h"
#include "../IO/textfile.h"
#include "../tools/FilePathTool.h"
#include <iostream>
#include <string.h>

//-----------------------------------------------------------------------------
// Error, Warning and Info Strings
char* aGLSLStrings[] = {
        "[e00] GLSL is not available!",
        "[e01] Not a valid program object!",
        "[e02] Not a valid object!",
        "[e03] Out of memory!",
        "[e04] Unknown compiler error!",
        "[e05] Linker log is not available!",
        "[e06] Compiler log is not available!",
        "[Empty]"
        };
//-----------------------------------------------------------------------------
char* Shader::getCompilerLog(bool vertOrFrag)
{
//if (!useGLSL) return aGLSLStrings[0];

 GLint blen = 0;
 GLsizei slen = 0;

 GLuint ShaderObject = vertOrFrag ? vertex : fragment;

 if (ShaderObject==0) return aGLSLStrings[1]; // not a valid program object

 glGetShaderiv(ShaderObject, GL_INFO_LOG_LENGTH , &blen);
// CHECK_GL_ERROR();

 static GLchar *compiler_log;
 if (blen > 1)
 {
    if ((compiler_log = (GLchar*)malloc(blen)) == NULL)
     {
        std::cout<<"ERROR: Could not allocate compiler_log buffer"<<std::endl;
        return aGLSLStrings[3];
    }

     glGetInfoLogARB(ShaderObject, blen, &slen, compiler_log);
//     CHECK_GL_ERROR();
     //cout << "compiler_log: \n", compiler_log);
 }
 if (compiler_log!=0)
    return (char*) compiler_log;
 else
   return aGLSLStrings[6];

 return aGLSLStrings[4];
}



bool Shader::load(std::string in_path_name)
{
    char *vs = NULL,*fs = NULL,*fs2 = NULL;
    if ( in_path_name.compare("")==0)
    {
        in_path_name = path_name;
    }
    std::string shdr_path = "resources/shaders"+in_path_name;
    GLint compiled=0;
    try {
        shdr_path = FilePathTool::removeFileExtension(shdr_path)+".vert";
        vs = textFileRead(shdr_path.c_str());
        if ( vertex==0 )
        {
            vertex = glCreateShader(GL_VERTEX_SHADER);
        }
        // DEBUG begin
        std::cout<<"Vertex Shader:"<<std::endl<<vs<<std::endl;
        // DEBUG end
        const char * vv = vs;
        glShaderSource(vertex, 1, &vv,NULL);
        free(vs);
        glCompileShader(vertex);
        glGetObjectParameterivARB(vertex, GL_COMPILE_STATUS, &compiled);
        if ( compiled==GL_FALSE)
        {
            std::cout<<"errore durante la compilazione di un vertex shader"<<std::endl;
            std::cout<<getCompilerLog(true);
            std::cout<<"file was:"<<std::endl<<vv<<std::endl<<std::endl;
            throw false;
        }
    } catch (bool err)
    {
        vertex = 0;
    }
    try {
        FilePathTool::removeFileExtension(shdr_path)+".frag";
        fs = textFileRead(shdr_path.c_str());
        if ( fragment==0 )
        {
            fragment = glCreateShader(GL_FRAGMENT_SHADER);
        }
        // DEBUG begin
        std::cout<<"Fragment Shader:"<<std::endl<<fs<<std::endl;
        // DEBUG end
        const char * ff = fs;
        glShaderSource(fragment, 1, &ff,NULL);
        free(fs);
        glCompileShader(fragment);
        compiled=0;
        glGetObjectParameterivARB(fragment, GL_COMPILE_STATUS, &compiled);
        if ( compiled==GL_FALSE)
        {
            std::cout<<"errore durante la compilazione di un fragment shader"<<std::endl;
            std::cout<<getCompilerLog(false);
            throw false;
        }
    } catch (bool err)
    {
        fragment = 0;
    }
    if ( vertex!=0 || fragment!=0 )
    {
        path_name = in_path_name;
        return true;
    }
    return false;
}
Shader::Shader(void)
{
    vertex = fragment = 0;
    path_name = "";
}
