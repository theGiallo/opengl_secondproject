#include "Window.h"
#include <iostream>
#include <GL/gl.h>
Window::Window()
{
    width = height = bitsPerPixel = 1;
    title = "new Window";
    surface = NULL;
    resize_handler = NULL;
}
Window::~Window()
{
    //SDL_FreeSurface(surface);
}
void Window::setTitle(std::string title)
{
    this->title = title;
    SDL_WM_SetCaption(title.c_str(), NULL);
}
bool Window::init(int width, int height, int bitsPerPixel, Uint32 flags)
{
    this->width = width;
    this->height = height;
    this->bitsPerPixel = bitsPerPixel;
    this->flags = flags;
    if ( (surface=SDL_SetVideoMode(width,height,bitsPerPixel,flags))==NULL )
    {
        return false;
    }
    return true;
}
bool Window::initGL(int width, int height, int bitsPerPixel, Uint32 flags)
{
    return this->init(width,height,bitsPerPixel,SDL_HWSURFACE | SDL_GL_DOUBLEBUFFER | SDL_OPENGL | flags);
}
bool Window::resize(int width, int height)
{
    std::cout<<"resize "<<width<<"x"<<height<<std::endl;
    bool r = true;
#ifndef WIN32
    r = init(width, height, bitsPerPixel, flags);
    if (r)
    {
#else
	this->width = width;
	this->height = height;
#endif
        glViewport(0,0,width,height);
        if (resize_handler!=NULL)
        {
            resize_handler->onResize(width,height);
        }
#ifndef WIN32
    }
#endif
    return r;
}
void Window::setResizeCallbackHolder(IWindowResizeHandler* onresize_holder)
{
    this->resize_handler = onresize_holder;
}
int Window::getWidth(void)
{
    return width;
}
int Window::getHeight(void)
{
    return height;
}
int Window::getBitsPerPixel(void)
{
    return bitsPerPixel;
}
Uint32 Window::getFlags(void)
{
    return flags;
}
SDL_Surface* Window::getSurface(void)
{
    return surface;
}
std::string Window::getTitle(void)
{
    return title;
}