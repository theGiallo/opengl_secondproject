/* 
 * File:   Model.h
 * Author: thegiallo
 *
 * Created on 31 gennaio 2011, 0.25
 */

#ifndef MODEL_H
#define	MODEL_H

#include "../graphics/Program.h"
#include <string>
#include <SDL/SDL.h>
#include "../maths/Vec3.h"
#include "../maths/Vec2.h"
#include "../maths/Quaternion.h"
#include <lib3ds.h>
#include "../graphics/Texture.h"

class Triangle
{
public:
    Uint16 vId[3];
    bool hasTex;
    Vec2 texPos[3];
    Vec3 normal;

    void CalculateNormal(Vec3 *vertexes);
};

class Model
{
protected:
    Lib3dsMesh mesh;
    Uint16 n_vertexes;
    Uint16 n_triangles;
    Vec3 *vertexes;
    Vec3 *normals;
    Triangle *triangles;
    GLfloat *vArray;
    Vec3 center;
    GLfloat color[4];
    bool has_texture;
    Texture *texture;
    Program *program;

public:
    Model(void);
    ~Model(void);
    void updateVArray(void);
    void draw(void);
    void drawImmediate(void);
    void load(std::string filePath);
    void unload(void);
    void rotate(Quaternion q); // rotate around the model center
    void rotate(Quaternion q, Vec3 center);
    void translate(Vec3 t);
    void scale(Vec3 s); // scale relatively to the model center
    void scale(Vec3 s, Vec3 center);
    void setCenter(Vec3 center);
    Vec3 getCenter(void);
    void setColor(GLfloat color[4]);
    GLfloat* getColor(void);
    Uint16 getVertexesCount(void);
    Uint16 getTrianglesCount(void);
    void calculateVertexesNormals(void);
    void calculateFacesNormals(void);
    bool hasTexture(void);
    void setHasTexture(bool hasTexture);
    Program getProgram(void);

    static Model* getCube(float side=1.0);
};

#endif	/* MODEL_H */

