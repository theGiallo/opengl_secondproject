/**
 * File:   Camera2D.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 06 November 2011
 */

#ifndef CAMERA2D_H
#define	CAMERA2D_H

#include <stdlib.h>
#ifdef WIN32
	#include <windows.h>
	#undef far
	#undef near
#endif
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "../maths/Vec2.h"
#include "../game/GameObject.h"

#define DEG2RAD(a) (((a)*(float)M_PI)/180.0f)
#define RAD2DEG(a) ((a)*180.0f/(float)M_PI)

//collisions
#ifndef COLLISIONS_DEFINED
#define COLLISIONS_DEFINED
#define COLLISION_OUT 0
#define COLLISION_IN 1
#define COLLISION_COLLIDE 2
#define COLLISION_CONTAINS 3
#endif

//class BoundingRectangle;
#include "BoundingRectangle.h"

// TODO come mai Camera2D non è un renderable nè un posizionable? Dovrebbe esserlo.

class Camera2D
{
protected:
    Vec2 vertical;
    GLfloat rot_mx[16];
    BoundingRectangle& br;
    float zoom;
public:
    virtual void setGLCamera(void);
    ~Camera2D(void);
    Camera2D(void);
    Camera2D(Vec2 pos);
    Camera2D(Vec2 pos, Vec2 vertical);
    Camera2D(Vec2 pos, Vec2 vertical, float width, float height);
    void setBaseWidth(float width);
    void setBaseHeigth(float height);
    void setBaseWidthHeight(float width, float height);
    void setPos(Vec2 pos);
    void setRot(float rot);
    void setVertical(Vec2 vertical);
    void setZoom(float zoom); /// sets the zoom value updating the dimensions
    void updateRotMx(void);
    void updateData(void);
    
    const BoundingRectangle& getBoundingRectangle(void) const;
    const Vec2& getPos(void) const;
    const float& getActualWidth(void) const;
    const float& getActualHeight(void) const;
    const float& getBaseWidth(void) const;
    const float& getBaseHeight(void) const;
    const float& getRot(void) const;
    const Vec2& getVertical(void) const;
    const GLfloat* getVArray() const;
    const GLfloat* getRotMx() const;
    const float& getZoom() const;
};


#endif
