/* 
 * File:   BoundingSphere.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 28 marzo 2011, 9.57
 */

#ifndef BOUNDINGSPHERE_H
#define	BOUNDINGSPHERE_H

#include <GL/glew.h>
#include <GL/gl.h>
#include "../graphics/Bounder.h"
#include "../graphics/Sphere.h"
#include "../maths/Vec3.h"
#include "../graphics/BoundingBox.h"
//#include <boost/math/special_functions/pow.hpp>

class BoundingSphere : public Bounder
{
public:
    const static unsigned sphere_model_iterations = 3; //=3
    const static unsigned nfaces = 4*4*4*8; //=4^sphere_model_iterations *8
//    const static unsigned nfaces = boost::math::pow<sphere_model_iterations, unsigned>(4)*8;
    static GLfloat vArray[];//[nfaces*Sphere::sizeofface];
    Vec3 center;
    float radius;

    BoundingSphere();
    BoundingSphere(Vec3 center, float radius=0.5);

    float distanceFrom(BoundingBox* aabb, Vec3* pointOnMe, Vec3* pointOnAABB);
    int collide(BoundingBox* aabb);
    
    virtual int collide(Frustum * frustum);
    virtual void render(void);
private:
    static GLfloat* init_vArray();
    static GLfloat* tmppointerescamotage;
};

#endif	/* BOUNDINGSPHERE_H */

