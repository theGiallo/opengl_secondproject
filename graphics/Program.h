/* 
 * File:   Program.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 7 febbraio 2011, 16.12
 */

#ifndef PROGRAM_H
#define	PROGRAM_H

#include <list>
#include "../graphics/Shader.h"

class Program
{
public:
    GLuint id;
    std::list<Shader*> attached_shaders;

    Program(void);
    bool hasShader(Shader *shader);
    bool hasShader(std::string path_name);
    void attachShader(Shader* shader);
    void detachShader(Shader* shader);
    void create(void);
};

#endif	/* PROGRAM_H */

