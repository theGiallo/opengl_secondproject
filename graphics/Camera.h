/* 
 * File:   Camera.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 28 gennaio 2011, 0.02
 */

#ifndef CAMERA_H
#define	CAMERA_H

#include <stdlib.h>
#ifdef WIN32
	#include <windows.h>
	#undef far
	#undef near
#endif
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include "../maths/Vec3.h"
#include "../maths/Vec2.h"
#include "../graphics/Frustum.h"
#include "../game/GameObject.h"

#define DEG2RAD(a) (((a)*(float)M_PI)/180.0f)
#define RAD2DEG(a) ((a)*180.0f/(float)M_PI)


class Camera
{
public:
    Vec3 pos;
    Vec3 target;
    Vec3 look;
    Vec3 vertical;
    Vec3 getDirection(void);
    virtual void setGLCamera(void)=0;
    Camera(void);
    virtual void setPosSameLook(Vec3 pos)=0;
    virtual void setPosSameTarget(Vec3 pos)=0;
    virtual void setTarget(Vec3 target)=0;
    virtual void setLook(Vec3 look)=0;
};

class PerspectiveCamera : public Camera
{
protected:

public:
    float near, far;
    float width, height;
    Vec2 vanishingPoint; // 0.0 - 1.0
    float FOV;
    float ratio;
    Frustum frustum;
    PerspectiveCamera(void);
    virtual void setGLCamera(void);
    void setFOV(float FOV);
    void setAspectRatio(float ratio);
    void setVanishingPoint(Vec2 vp);
    void setNear(float near);
    void setFar(float far);
    void setNearFar(float near, float far);
    virtual void setPosSameLook(Vec3 pos);
    virtual void setPosSameTarget(Vec3 pos);
    virtual void setTarget(Vec3 target);
    virtual void setLook(Vec3 look);
    virtual void setPosTarget(Vec3 pos, Vec3 target);
    void moveX(float deltax);
    void moveY(float deltay);
    void moveZ(float deltaz);
    void move(Vec3 v);
    void rotateX(float alphax);
    void rotateY(float alphay);
};

class StereoPerspectiveCamera : public PerspectiveCamera, public GameObject
{
private:
    Uint32 timeBuffer;
    Uint32 period;
    float eyeDist;
    int r;
public:
    bool stereo;
    StereoPerspectiveCamera();
    virtual void update(Uint32 elapsedTime);
    virtual void lateUpdate(Uint32 elapsedTime);
    virtual void setGLCamera(void);
    void setEyeDist(float eyeDist);
    void setPeriod(Uint32 period);
    void setEye(int eye);
};



#endif	/* CAMERA_H */

