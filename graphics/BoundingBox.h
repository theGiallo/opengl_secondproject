/* 
 * File:   BoundingBox.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 28 marzo 2011, 15.22
 */

#ifndef BOUNDINGBOX_H
#define	BOUNDINGBOX_H

#include "../graphics/Bounder.h"
#include "../maths/Vec3.h"

class BoundingSphere;

class BoundingBox : public Bounder
{
public:
    Vec3 center;
    float side;

    BoundingBox();
    BoundingBox(Vec3 center, float side=1.0);
    int collide(BoundingSphere * bs);
    
    virtual int collide(Frustum * frustum);
    virtual void render(void);
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection);
};

#endif	/* BOUNDINGBOX_H */

