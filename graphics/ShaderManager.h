/* 
 * File:   ShaderManager.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 7 febbraio 2011, 15.10
 */

#ifndef SHADERMANAGER_H
#define	SHADERMANAGER_H

#include <string>
#include <list>
#include "../graphics/Shader.h"

class ShaderManager
{
protected:
    static std::list<Shader*> shaders;
public:
    static Shader* loadShader(std::string pathName);
};

#endif	/* SHADERMANAGER_H */

