/* 
 * File:   PointLight.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 4 febbraio 2011, 23.23
 */

#ifndef OGLLOCALLIGHT_H
#define	OGLLOCALLIGHT_H

#include "../maths/Vec3.h"
#include "../graphics/OGLLight.h"

class OGLLocalLight : public OGLLight
{
protected:
    Vec3 position;
    bool is_attenuated;
    GLenum attenuationType;
    GLfloat attenuationParameter;
public:
    bool is_directional;

    void setDirection(Vec3 dir);
    Vec3 getPosition(void);
    void setPosition(Vec3 pos);
    bool isAttenuated(void);
    void setAttenuated(bool attenuated);
    void setAttenuationType(GLenum attenuationType);
    GLenum getAttenuationType(void);
    void setAttenuationParameter(GLfloat attenuationParameter);
    GLfloat getAttenuationParameter(void);
    virtual void glSet(void);
    void setParameters(Vec3 pos, bool is_attenuated, GLenum attenuationType, GLfloat attenuationParameter, bool directional=false);
    OGLLocalLight(Vec3 pos, bool is_attenuated, GLenum attenuationType, GLfloat attenuationParameter, bool directional=false);
    OGLLocalLight(void);
};

#endif	/* OGLLOCALLIGHT_H */

