/** 
 * File:   Renderables2D.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 25 August 2012
 * 
 * Contains class headers for:
 *      Renderable2D
 *      PositionableObject2D
 *      Bounder2D
 */
#ifndef RENDERABLES2D_H
#define	RENDERABLES2D_H


//#include "../graphics/Camera2D.h"
class Camera2D;
#include "../game/Collider2D.h"

#include "../maths/Vec2.h"
#include <cstdlib>
/*
	TODO: la propriet� della collisione deve essere associata ad un PositionableObject2D non ad un Renderable2D
	quindi dovrebbe(o � meglio scorrere una delle 2 gi� esistemti?) essere aggiunta una lista di PositionableObject2D e modificato il ciclo di controllo delle collisioni di conseguenza.
*/
class Renderable2D;
class PositionableObject2D;
class Bounder2D;

#define UNITARY_SCALE Vec2(1.0f,1.0f)

class Renderable2D
{
protected:
    GLfloat color[4];
public:
    bool visible;
    Bounder2D * bounder;
    Collider2D* collider;
    bool intersectable;
    bool check_collisions;
    Renderable2D()
    {
        bounder = NULL;
        collider = NULL;
        visible = true;
        intersectable = check_collisions = false;
        color[0] = color[1] = color[2] = 0.5;
        color[3] = 1;
    }
    
    GLfloat* getColor(GLfloat* color_out)
    {
        memcpy(&color_out[0], &color[0], sizeof(color));
        return color_out;
    }
    void setColor(GLfloat color[4])
    {
        memcpy(&this->color[0], &color[0], sizeof(GLfloat)*4);
    };
    
    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
};

class PositionableObject2D : public Renderable2D
{
protected:
    Vec2 pos;
    Vec2 up; ///default is (0,1)
    Vec2 scale;
    float rot;
public:
    PositionableObject2D();
    const Vec2& getPos(void) const;
    virtual void setPos(const Vec2& pos);
    const Vec2& getUp(void) const;
    virtual void setUp(const Vec2& up);
    const Vec2& getScale(void) const;
    virtual void setScale(const Vec2& scale);
    float getRot(void) const;
    virtual void setRot(float rot);

    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
};

class Bounder2D : public PositionableObject2D
{
public:
    /**
     * @return this in relation to the frustum: is out, is inside(contained), collide or
     *         contains the frustum
     *          COLLISION_OUT 0
     *          COLLISION_IN 1
     *          COLLISION_COLLIDE 2
     *          COLLISION_CONTAINS 3
     **/
    virtual int collide(const Camera2D &camera)=0;
    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
};

#endif /* RENDERABLES2D_H */