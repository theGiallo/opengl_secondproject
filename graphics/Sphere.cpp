/* 
 * File:   Sphere.cpp
 * Author: thegiallo
 * 
 * Created on 12 ottobre 2011, 22.30
 *
 * NOT WORKING!
 */
#include "Sphere.h"

#ifndef SQRT2
#define SQRT2 1.414213562f
#endif

Sphere::Sphere(Vec3 center, float radius)
{
    this->center = center;
    this->radius = radius;
}

/**
 * Creates a triangular facet approximation of a sphere centered in (0,0,0)
 * @return The number of triangles, that will be (4^iterations) * 8
 */
unsigned Sphere::createSphereVertexArray(GLfloat *f, unsigned iterations)
{
    int i,j,it;
    Vec3 p[6] = {Vec3(0,0,1), Vec3(0,0,-1), Vec3(-1,-1,0), Vec3(1,-1,0), Vec3(1,1,0), Vec3(-1,1,0)};
    Vec3 pa,pb,pc;
    unsigned nt = 0,ntold;

    /**
     * Create the level 0 object
     **/
    float r = 1 / SQRT2;
    for (i=0 ; i<6 ; i++)
    {
        p[i].x *= r;
        p[i].y *= r;
    }
    /* OMG it's owful! :D*/
    j=0;
    f[0*sizeofface+(j++)] = p[0].get(0);
    f[0*sizeofface+(j++)] = p[0].get(1);
    f[0*sizeofface+(j++)] = p[0].get(2);
    f[0*sizeofface+(j++)] = p[3].get(0);
    f[0*sizeofface+(j++)] = p[3].get(1);
    f[0*sizeofface+(j++)] = p[3].get(2);
    f[0*sizeofface+(j++)] = p[4].get(0);
    f[0*sizeofface+(j++)] = p[4].get(1);
    f[0*sizeofface+(j++)] = p[4].get(2);
    
    f[1*sizeofface+(j++)] = p[0].get(0);
    f[1*sizeofface+(j++)] = p[0].get(1);
    f[1*sizeofface+(j++)] = p[0].get(2);
    f[1*sizeofface+(j++)] = p[4].get(0);
    f[1*sizeofface+(j++)] = p[4].get(1);
    f[1*sizeofface+(j++)] = p[4].get(2);
    f[1*sizeofface+(j++)] = p[5].get(0);
    f[1*sizeofface+(j++)] = p[5].get(1);
    f[1*sizeofface+(j++)] = p[5].get(2);
    
    f[2*sizeofface+(j++)] = p[0].get(0);
    f[2*sizeofface+(j++)] = p[0].get(1);
    f[2*sizeofface+(j++)] = p[0].get(2);
    f[2*sizeofface+(j++)] = p[5].get(0);
    f[2*sizeofface+(j++)] = p[5].get(1);
    f[2*sizeofface+(j++)] = p[5].get(2);
    f[2*sizeofface+(j++)] = p[2].get(0);
    f[2*sizeofface+(j++)] = p[2].get(1);
    f[2*sizeofface+(j++)] = p[2].get(2);
    
    f[3*sizeofface+(j++)] = p[0].get(0);
    f[3*sizeofface+(j++)] = p[0].get(1);
    f[3*sizeofface+(j++)] = p[0].get(2);
    f[3*sizeofface+(j++)] = p[2].get(0);
    f[3*sizeofface+(j++)] = p[2].get(1);
    f[3*sizeofface+(j++)] = p[2].get(2);
    f[3*sizeofface+(j++)] = p[3].get(0);
    f[3*sizeofface+(j++)] = p[3].get(1);
    
    f[3*sizeofface+(j++)] = p[3].get(2);
    f[4*sizeofface+(j++)] = p[1].get(0);
    f[4*sizeofface+(j++)] = p[1].get(1);
    f[4*sizeofface+(j++)] = p[1].get(2);
    f[4*sizeofface+(j++)] = p[4].get(0);
    f[4*sizeofface+(j++)] = p[4].get(1);
    f[4*sizeofface+(j++)] = p[4].get(2);
    f[4*sizeofface+(j++)] = p[3].get(0);
    f[4*sizeofface+(j++)] = p[3].get(1);
    f[4*sizeofface+(j++)] = p[3].get(2);
    
    f[5*sizeofface+(j++)] = p[1].get(0);
    f[5*sizeofface+(j++)] = p[1].get(1);
    f[5*sizeofface+(j++)] = p[1].get(2);
    f[5*sizeofface+(j++)] = p[5].get(0);
    f[5*sizeofface+(j++)] = p[5].get(1);
    f[5*sizeofface+(j++)] = p[5].get(2);
    f[5*sizeofface+(j++)] = p[4].get(0);
    f[5*sizeofface+(j++)] = p[4].get(1);
    f[5*sizeofface+(j++)] = p[4].get(2);
    
    f[6*sizeofface+(j++)] = p[1].get(0);
    f[6*sizeofface+(j++)] = p[1].get(1);
    f[6*sizeofface+(j++)] = p[1].get(2);
    f[6*sizeofface+(j++)] = p[2].get(0);
    f[6*sizeofface+(j++)] = p[2].get(1);
    f[6*sizeofface+(j++)] = p[2].get(2);
    f[6*sizeofface+(j++)] = p[5].get(0);
    f[6*sizeofface+(j++)] = p[5].get(1);
    f[6*sizeofface+(j++)] = p[5].get(2);
    
    f[7*sizeofface+(j++)] = p[1].get(0);
    f[7*sizeofface+(j++)] = p[1].get(1);
    f[7*sizeofface+(j++)] = p[1].get(2);
    f[7*sizeofface+(j++)] = p[3].get(0);
    f[7*sizeofface+(j++)] = p[3].get(1);
    f[7*sizeofface+(j++)] = p[3].get(2);
    f[7*sizeofface+(j++)] = p[2].get(0);
    f[7*sizeofface+(j++)] = p[2].get(1);
    f[7*sizeofface+(j++)] = p[2].get(2);
    nt = 8;

    if (iterations < 1)
    {
        return(nt);
    }

   /**
    * Bisect each edge and move to the surface of a unit sphere
    **/
   for (it=0 ; it<iterations ; it++)
   {
      ntold = nt;
      for (i=0 ; i<ntold ; i++)
      {
            pa.x = (f[i*sizeofface+sizeofpoint*0+X_COMPONENT] + f[i*sizeofface+sizeofpoint*1]+X_COMPONENT) / 2;
            pa.y = (f[i*sizeofface+sizeofpoint*0+Y_COMPONENT] + f[i*sizeofface+sizeofpoint*1]+Y_COMPONENT) / 2;
            pa.z = (f[i*sizeofface+sizeofpoint*0+Z_COMPONENT] + f[i*sizeofface+sizeofpoint*1]+Z_COMPONENT) / 2;
            pb.x = (f[i*sizeofface+sizeofpoint*1+X_COMPONENT] + f[i*sizeofface+sizeofpoint*2]+X_COMPONENT) / 2;
            pb.y = (f[i*sizeofface+sizeofpoint*1+Y_COMPONENT] + f[i*sizeofface+sizeofpoint*2]+Y_COMPONENT) / 2;
            pb.z = (f[i*sizeofface+sizeofpoint*1+Z_COMPONENT] + f[i*sizeofface+sizeofpoint*2]+Z_COMPONENT) / 2;
            pc.x = (f[i*sizeofface+sizeofpoint*2+X_COMPONENT] + f[i*sizeofface+sizeofpoint*0]+X_COMPONENT) / 2;
            pc.y = (f[i*sizeofface+sizeofpoint*2+Y_COMPONENT] + f[i*sizeofface+sizeofpoint*0]+Y_COMPONENT) / 2;
            pc.z = (f[i*sizeofface+sizeofpoint*2+Z_COMPONENT] + f[i*sizeofface+sizeofpoint*0]+Z_COMPONENT) / 2;
            pa.normalize();
            pb.normalize();
            pc.normalize();

            f[nt*sizeofface+sizeofpoint*0+X_COMPONENT] = f[i*sizeofface+sizeofpoint*0+X_COMPONENT];
            f[nt*sizeofface+sizeofpoint*0+Y_COMPONENT] = f[i*sizeofface+sizeofpoint*0+Y_COMPONENT];
            f[nt*sizeofface+sizeofpoint*0+Z_COMPONENT] = f[i*sizeofface+sizeofpoint*0+Z_COMPONENT];
            f[nt*sizeofface+sizeofpoint*1+X_COMPONENT] = pa.x;
            f[nt*sizeofface+sizeofpoint*1+Y_COMPONENT] = pa.y;
            f[nt*sizeofface+sizeofpoint*1+Z_COMPONENT] = pa.z;
            f[nt*sizeofface+sizeofpoint*2+X_COMPONENT] = pc.x;
            f[nt*sizeofface+sizeofpoint*2+Y_COMPONENT] = pc.y;
            f[nt*sizeofface+sizeofpoint*2+Z_COMPONENT] = pc.z;
            nt++;
            f[nt*sizeofface+sizeofpoint*0+X_COMPONENT] = pa.x;
            f[nt*sizeofface+sizeofpoint*0+Y_COMPONENT] = pa.y;
            f[nt*sizeofface+sizeofpoint*0+Z_COMPONENT] = pa.z;
            f[nt*sizeofface+sizeofpoint*1+X_COMPONENT] = f[i*sizeofface+sizeofpoint*1+X_COMPONENT];
            f[nt*sizeofface+sizeofpoint*1+Y_COMPONENT] = f[i*sizeofface+sizeofpoint*1+Y_COMPONENT];
            f[nt*sizeofface+sizeofpoint*1+Z_COMPONENT] = f[i*sizeofface+sizeofpoint*1+Z_COMPONENT];
            f[nt*sizeofface+sizeofpoint*2+X_COMPONENT] = pb.x;
            f[nt*sizeofface+sizeofpoint*2+Y_COMPONENT] = pb.y;
            f[nt*sizeofface+sizeofpoint*2+Z_COMPONENT] = pb.z;
            nt++;
            f[nt*sizeofface+sizeofpoint*0+X_COMPONENT] = pb.x;
            f[nt*sizeofface+sizeofpoint*0+Y_COMPONENT] = pb.y;
            f[nt*sizeofface+sizeofpoint*0+Z_COMPONENT] = pb.z;
            f[nt*sizeofface+sizeofpoint*1+X_COMPONENT] = f[i*sizeofface+sizeofpoint*2+X_COMPONENT];
            f[nt*sizeofface+sizeofpoint*1+Y_COMPONENT] = f[i*sizeofface+sizeofpoint*2+Y_COMPONENT];
            f[nt*sizeofface+sizeofpoint*1+Z_COMPONENT] = f[i*sizeofface+sizeofpoint*2+Z_COMPONENT];
            f[nt*sizeofface+sizeofpoint*2+X_COMPONENT] = pc.x;
            f[nt*sizeofface+sizeofpoint*2+Y_COMPONENT] = pc.y;
            f[nt*sizeofface+sizeofpoint*2+Z_COMPONENT] = pc.z;
            nt++;

            f[i*sizeofface+sizeofpoint*0+X_COMPONENT] = pa.x;
            f[i*sizeofface+sizeofpoint*0+Y_COMPONENT] = pa.y;
            f[i*sizeofface+sizeofpoint*0+Z_COMPONENT] = pa.z;
            f[i*sizeofface+sizeofpoint*1+X_COMPONENT] = pb.x;
            f[i*sizeofface+sizeofpoint*1+Y_COMPONENT] = pb.y;
            f[i*sizeofface+sizeofpoint*1+Z_COMPONENT] = pb.z;
            f[i*sizeofface+sizeofpoint*2+X_COMPONENT] = pc.x;
            f[i*sizeofface+sizeofpoint*2+Y_COMPONENT] = pc.y;
            f[i*sizeofface+sizeofpoint*2+Z_COMPONENT] = pc.z;
        }
    }
    return nt;
}
//const unsigned Sphere::sizeofpoint;
//const unsigned Sphere::sizeofface;
