#include "BoundingRectangle.h"
#include "../graphics/CollisionSolver2D.h"
#include "../graphics/Camera2D.h"

BoundingRectangle::BoundingRectangle(const Vec2& pos, const float& width, const float& height, const float& rot/*=0.0f*/, const Vec2& scale/*=Vec2(1.0f,1.0f)*/)
{
    setPos(pos);
    setScale(scale);
    setWidth(width);
    setHeight(height);
    setRot(rot);
    updateVArray();
}

void BoundingRectangle::setPos(const Vec2& pos)
{
    this->pos = pos;
    updated = false;
}
void BoundingRectangle::setScale(const Vec2& scale)
{
    this->scale = scale;
    scaled_width = scale.getX()*width;
    scaled_height = scale.getY()*height;
    updated = false;
}
const float& BoundingRectangle::getWidth(void) const
{
    return width;
}
const float& BoundingRectangle::getScaledWidth(void) const
{
    return scaled_width;
}
void BoundingRectangle::setWidth(const float& width)
{
    this->width = width;
    scaled_width = scale.getX()*width;
    updated = false;
}
const float& BoundingRectangle::getHeight(void) const
{
    return height;
}
const float& BoundingRectangle::getScaledHeight(void) const
{
    return scaled_height;
}
void BoundingRectangle::setHeight(const float& height)
{
    this->height = height;
    scaled_height = scale.getY()*height;
    updated = false;
}
const float& BoundingRectangle::getRot(void) const
{
    return rot;
}
void BoundingRectangle::setRot(const float& rot)
{
    this->rot = rot;
    updated = false;    
}

const GLfloat* BoundingRectangle::getVArray(void) const
{
    return varray;
}
void BoundingRectangle::updateVArray(void)
{
    int i=0;
    float hwidth = scaled_width/2.0f;
    float hheight = scaled_height/2.0f;
    
    varray[i++] = hwidth;
    varray[i++] = hheight;
    
    varray[i++] = -hwidth;
    varray[i++] = hheight;
    
    varray[i++] = -hwidth;
    varray[i++] = -hheight;
    
    varray[i++] = hwidth;
    varray[i++] = -hheight;
    
//    OGLTools::scaleVArray(varray, 4, scale);
//    OGLTools::rotateVArray(varray, 4, rot);
    OGLTools::translateVArray(varray, 4, pos);
    
    updated = true;
}

int BoundingRectangle::collide(const Camera2D &camera)
{
    if (!updated)
    {
        updateVArray();
    }
    float dist;
    return CollisionSolver2D::collide(*this, camera.getBoundingRectangle(), dist);
}
void BoundingRectangle::render(void)
{
    if (!updated)
    {
        updateVArray();
    }
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_ENABLE_BIT);
        glColor4fv(&color[0]);
        glEnableClientState(GL_VERTEX_ARRAY);
        glInterleavedArrays( GL_V2F,2*sizeof(GLfloat),&varray[0]);
        glDrawArrays(GL_LINE_LOOP, 0, 4);
    glPopAttrib();
    glPopAttrib();
}
float BoundingRectangle::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    if (!updated)
    {
        updateVArray();
    }
    // TODO implement
	throw false;
	return 0.0f;
}