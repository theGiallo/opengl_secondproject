#include "Program.h"
#include <iostream>

#include "../graphics/GLDebug.h"

Program::Program()
{
    id=0;
}
void Program::create(void)
{
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    id = glCreateProgram();
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    GL_INVALID_VALUE;
    GLenum err = glGetError();
    if ( err )
    {
        std::cerr<<"ERRORE DURANTE LA CREAZIONE DI UN PROGRAMMA:"<<std::endl;
        std::cerr<<(char*)glGetString(err)<<std::endl;
        std::cerr<<"-------------------------------"<<std::endl;
    }
}
bool Program::hasShader(Shader *shader)
{
    for (std::list<Shader*>::iterator it=attached_shaders.begin(); it!=attached_shaders.end(); ++it)
    {
        if ( *it==shader )
        {
            return true;
        }
    }
    return false;
}
bool Program::hasShader(std::string path_name)
{
    for (std::list<Shader*>::iterator it=attached_shaders.begin(); it!=attached_shaders.end(); ++it)
    {
        if ( (*it)->path_name.compare(path_name)==0 )
        {
            return true;
        }
    }
    return false;
}
void Program::attachShader(Shader* shader)
{
    if ( hasShader(shader) )
    {
        return;
    }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    attached_shaders.push_front(shader);
    glAttachShader(id, shader->vertex);
    glAttachShader(id, shader->fragment);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
}
void Program::detachShader(Shader* shader)
{
    if ( hasShader(shader) )
    {
        attached_shaders.remove(shader);
        glDetachShader(id, shader->vertex);
        glDetachShader(id, shader->fragment);
        return;
    }
}
