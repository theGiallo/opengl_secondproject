/* 
 * File:   TextureManager.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 2 febbraio 2011, 22.07
 */

#ifndef TEXTUREMANAGER_H
#define	TEXTUREMANAGER_H

#include <string>
#include <list>
#include "../graphics/Texture.h"

class TextureManager
{
protected:
    static std::list<Texture*> textures;
public:
    static Texture* loadTexture(std::string file_path);
};

#endif	/* TEXTUREMANAGER_H */

