/* 
 * File:   Texture.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 31 gennaio 2011, 11.24
 */

#ifndef TEXTURE_H
#define	TEXTURE_H

#include <locale>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL/SDL.h>
#include <string>
#include <list>

class Texture
{
protected:
    //locale loc;
public:
    //long hash;
    std::string file_path;
    GLubyte *texels;
    Uint16 width, height;
    GLuint type; //RGA or RGBA
    GLuint texId;

    Texture(void);
    ~Texture(void);
    bool load(std::string file_path);
    void unloadTexelsArray(void);
    void calcHash(void);
    void store(std::list<Texture*> *texList); // if !isStored then storeAnyway
    void storeAnyway(std::list<Texture*> *texList);
    bool isStored(std::list<Texture*> *texList, GLuint *texId);
    bool isStored(std::list<Texture*> *texList, Texture **stored);
};

#endif	/* TEXTURE_H */

