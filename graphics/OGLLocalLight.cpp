#include "OGLLocalLight.h"

Vec3 OGLLocalLight::getPosition(void)
{
    return position;
}
void OGLLocalLight::setPosition(Vec3 pos)
{
    this->position=pos;
}
bool OGLLocalLight::isAttenuated(void)
{
    return is_attenuated;
}
void OGLLocalLight::setAttenuated(bool attenuated)
{
    is_attenuated = attenuated;
}
void OGLLocalLight::setAttenuationType(GLenum attenuationType)
{
    this->attenuationType = attenuationType;
}
GLenum OGLLocalLight::getAttenuationType(void)
{
    return attenuationType;
}
void OGLLocalLight::setAttenuationParameter(GLfloat attenuationParameter)
{
    this->attenuationParameter = attenuationParameter;
}
GLfloat OGLLocalLight::getAttenuationParameter(void)
{
    return attenuationParameter;
}
void OGLLocalLight::glSet(void)
{
    if (!active)
        return;
    GLfloat posv[4];
    posv[0]=position.x;
    posv[1]=position.y;
    posv[2]=position.z;
    posv[3]=is_directional?0:1;
    glLightfv(id, GL_POSITION, posv);
    if (is_attenuated)
    {
        glLightf(id , attenuationType, attenuationParameter);
    }
    OGLLight::glSet();
}
void OGLLocalLight::setDirection(Vec3 dir)
{
    is_directional = true;
    position = dir*(-1);
}
void OGLLocalLight::setParameters(Vec3 pos, bool is_attenuated, GLenum attenuationType, GLfloat attenuationParameter, bool directional)
{
    position = pos;
    this->is_attenuated = is_attenuated;
    this->attenuationType = attenuationType;
    this->attenuationParameter = attenuationParameter;
    is_directional = directional;
}
OGLLocalLight::OGLLocalLight(Vec3 pos, bool is_attenuated, GLenum attenuationType, GLfloat attenuationParameter, bool directional)
{
    if (!init())
    {
        throw false;
    }
    initDefault();
    setParameters(pos, is_attenuated, attenuationType, attenuationParameter, directional);
    this->is_attenuated = is_attenuated;
}
OGLLocalLight::OGLLocalLight(void)
{
    position = Vec3();
    attenuationType = GL_CONSTANT_ATTENUATION;
    attenuationParameter = 1;
    is_directional = false;
    setAttenuated(false);
}