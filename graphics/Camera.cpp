#include "../graphics/Camera.h"
#include "../maths/Quaternion.h"
#include <iostream>
#include "../game/GameManager.h"

/**TODO integrare in GameManager con un wrapper*/
#include "../game/OctTree.h"

#include <iostream>

Camera::Camera(void)
{
    pos = Vec3(0,0,1);
    target = Vec3(0,0,0);
    vertical = Vec3(0,1,0);
}

Vec3 Camera::getDirection()
{
    return look;
}

PerspectiveCamera::PerspectiveCamera()
{
    far = 65536;
    near = 1;
    ratio = 1;
    setFOV(90);
    pos = Vec3(0,0,1);
    target = Vec3();
    vanishingPoint = Vec2(0.5,0.5);
    vertical = Vec3(0,1,0);
    frustum = Frustum();
    frustum.updateNF(near,far);
    frustum.updatePosTargetUp(pos, target, vertical, ratio);
}
void PerspectiveCamera::setGLCamera()
{
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(pos.x, pos.y, pos.z, target.x, target.y, target.z, vertical.x, vertical.y, vertical.z);
    glMatrixMode(GL_PROJECTION);
    glFrustum(-vanishingPoint.x*width, (1-vanishingPoint.x)*width, -vanishingPoint.y*height, (1-vanishingPoint.y)*height, near, far);
}
void PerspectiveCamera::setFOV(float FOV)
{
    this->FOV = FOV;
    this->width = near*tan(DEG2RAD(FOV/2.0f))*2.0f;
    this->height = width/ratio;
    frustum.updateFOV(FOV);
}
void PerspectiveCamera::setAspectRatio(float ratio)
{
    this->ratio = ratio;
    this->height = width/ratio;
    frustum.updateRatio(ratio);
}
void PerspectiveCamera::setVanishingPoint(Vec2 vp)
{
    vanishingPoint = vp;
}
void PerspectiveCamera::setNear(float near)
{
    this->near = near;
    this->width = near*tan(DEG2RAD(FOV/2.0f))*2.0f;
    this->height = width/ratio;
    frustum.updateNF(near, far);
}
void PerspectiveCamera::setFar(float far)
{
    this->far = far;
    frustum.updateNF(near, far);
}
void PerspectiveCamera::setNearFar(float near, float far)
{
    this->near = near;
    this->width = near*tan(DEG2RAD(FOV));
    this->height = width/ratio;
    this->far = far;
    frustum.updateNF(near, far);
//    std::cout<<"#############SetNearFar###############"<<std::endl;
}
void PerspectiveCamera::setPosSameLook(Vec3 pos)
{
    Vec3 tmp = this->pos;
    this->pos = pos;
    this->target = pos+look*(target-tmp).getModule();
    frustum.updatePosLookUp(pos, look, vertical, ratio);
}
void PerspectiveCamera::setPosSameTarget(Vec3 pos)
{
    Vec3 tmp = this->pos;
    this->pos = pos;
    this->look = (target-tmp).getNormalized();
    frustum.updatePosTargetUp(pos, target, vertical, ratio);
}
void PerspectiveCamera::setTarget(Vec3 target)
{
    this->target = target;
    this->look = (target-pos).getNormalized();
    frustum.updatePosTargetUp(pos, target, vertical, ratio);
}
void PerspectiveCamera::setLook(Vec3 look)
{
    this->look = look;
    this->target = pos+look*(target-pos).getModule();
    frustum.updatePosLookUp(pos, look, vertical, ratio);
}
void PerspectiveCamera::setPosTarget(Vec3 pos, Vec3 target)
{
    this->pos = pos;
    this->target = target;
    frustum.updatePosTargetUp(pos, target, vertical, ratio);
}
void PerspectiveCamera::moveX(float deltax)
{
    setPosSameLook(pos+(look^vertical)*deltax);
}
void PerspectiveCamera::moveY(float deltay)
{
    setPosSameLook(pos+vertical*deltay);
}
void PerspectiveCamera::moveZ(float deltaz)
{
    setPosSameLook(pos+look*deltaz);
}
void PerspectiveCamera::move(Vec3 v)
{
    setPosSameLook(pos+v);
}
void PerspectiveCamera::rotateX(float alphax)
{
    Quaternion nrot;
    nrot.FromAxis(look^vertical, DEG2RAD(alphax));
    vertical = nrot*vertical;
    vertical.normalize();
    setTarget(nrot*(target-pos)+pos);
}
void PerspectiveCamera::rotateY(float alphay)
{
    Quaternion nrot;
    nrot.FromAxis(vertical, DEG2RAD(alphay));
    setTarget(nrot*(target-pos)+pos);
}


StereoPerspectiveCamera::StereoPerspectiveCamera()
{
    timeBuffer = 0;
    eyeDist = 0.0635f;
    period = 500;
    r = 1;
    active = true;
    stereo = true;
}
void StereoPerspectiveCamera::update(Uint32 elapsedTime)
{
    if (!stereo)
    {
        return;
    }
    Vec3 new_target;
    Renderable* tmp_r;
    if (GameManager::rayCast(pos, look, &new_target, &tmp_r))
    {
        this->target = new_target;
        //std::cout<<"sto guardando qualcosa!!"<<std::endl;
    } else
    {
        this->target = this->pos+look*far;
    }
    /**TODO integrate into GameManager with a wrapper*/
    Vec3 voxel_target;
    Vec3_int voxel_ac;
    float voxel_dist;
    if (OctTree::select_from_root(pos, look, &voxel_dist, &voxel_target, &voxel_ac))
    {
        if (voxel_dist<(target-pos).getModule())
        {
            this->target = voxel_target;
        }
    }
    timeBuffer += elapsedTime;
    if (timeBuffer>=period)
    {
        if ( (timeBuffer/period)%2 == 1 )
        {
            r *= -1;
        }
        timeBuffer %= period;
    }
}
void StereoPerspectiveCamera::lateUpdate(Uint32 elapsedTime)
{

}
void StereoPerspectiveCamera::setGLCamera(void)
{
    PerspectiveCamera tmpCamera = *this;
    Vec3 right = look^vertical;
    right.normalize();
    Vec3 t;
    if (stereo)
    {
        t = pos+right*(r*eyeDist/2.0f);
    } else
    {
        t = pos;
    }
    tmpCamera.setPosSameTarget(t);
    tmpCamera.setGLCamera();
}
void StereoPerspectiveCamera::setEyeDist(float eyeDist)
{
    this->eyeDist = eyeDist;
}
void StereoPerspectiveCamera::setPeriod(Uint32 period)
{
    this->period = period;
}
void StereoPerspectiveCamera::setEye(int eye)
{
    if (eye==GL_LEFT && r>0)
    {
        r*=-1;
    }
    if (eye==GL_RIGHT && r<0)
    {
        r*=-1;
    }
}
