/**
 * Author: theGiallo
 * Directory: graphics
 **/

#ifndef REGULARPOLYGON2D_H
#define	REGULARPOLYGON2D_H
#include "../graphics/Renderables2D.h"

#define N_VARRAYS 500
#define DEFAULT_NSIDES 3
#define DEFAULT_TLINEFFILL true
class RegularPolygon2D : public Renderable2D
{
protected:
    //<static>
    static void initVArray(unsigned int nsides);
    //</static>

    unsigned int nsides;
public:
    //<static>
    static GLfloat* varrays[N_VARRAYS];
    static void initVArrays(void);
    //</static>

    bool Tline_Ffill;
    RegularPolygon2D();
    RegularPolygon2D(unsigned int nsides);
    RegularPolygon2D(unsigned int nsides, bool Tline_Ffill);
    void setNSides(unsigned int nsides);
    unsigned int getNSides(void) const;

    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};
#endif /* REGULARPOLYGON2D_H */