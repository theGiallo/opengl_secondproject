#include <GL/glew.h>

#include "../graphics/BoundingBox.h"
#include "../graphics/BoundingSphere.h"
#include <iostream>

BoundingBox::BoundingBox()
{
    this->center = Vec3();
    this->side = 1;
}
BoundingBox::BoundingBox(Vec3 center, float side)
{
    this->center = center;
    this->side = side;
}

int BoundingBox::collide(Frustum * frustum)
{
//    std::cout<<"bounding_box.side:   "<<side<<std::endl;
//    std::cout<<"bounding_box.center: ";center.print();
    return frustum->collideBox(center, side);
}

void BoundingBox::render(void)
{
    float hs = side / 2.0f;
//                    std::cout<<"hs:                  "<<hs<<std::endl;
//    glPushAttrib(GL_CURRENT_BIT);
//    glPushAttrib(GL_ENABLE_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glColor4f(1,1,1,1);
    glDisable(GL_TEXTURE_2D);
//    glDisable(GL_LIGHTING);
//    glBegin(GL_LINE_STRIP);
//        glVertex3d(center.x-hs, center.y-hs, center.z-hs);
//        glVertex3d(center.x+hs, center.y-hs, center.z-hs);
//        glVertex3d(center.x+hs, center.y+hs, center.z-hs);
//        glVertex3d(center.x-hs, center.y+hs, center.z-hs);
//        glVertex3d(center.x-hs, center.y-hs, center.z-hs);
//        glVertex3d(center.x-hs, center.y-hs, center.z+hs);
//        glVertex3d(center.x-hs, center.y+hs, center.z+hs);
//        glVertex3d(center.x+hs, center.y+hs, center.z+hs);
//        glVertex3d(center.x+hs, center.y-hs, center.z+hs);
//        glVertex3d(center.x-hs, center.y-hs, center.z+hs);
//    glEnd();
//    glBegin(GL_LINES);
//        glVertex3d(center.x+hs, center.y-hs, center.z-hs);
//        glVertex3d(center.x+hs, center.y-hs, center.z+hs);
//        glVertex3d(center.x+hs, center.y+hs, center.z-hs);
//        glVertex3d(center.x+hs, center.y+hs, center.z+hs);
//        glVertex3d(center.x-hs, center.y+hs, center.z-hs);
//        glVertex3d(center.x-hs, center.y+hs, center.z+hs);
//    glEnd();
        glBegin(GL_QUADS);
            glVertex3f(center.x+hs, center.y+hs, center.z+hs);
            glVertex3f(center.x+hs, center.y+hs, center.z-hs);
            glVertex3f(center.x-hs, center.y+hs, center.z-hs);
            glVertex3f(center.x-hs, center.y+hs, center.z+hs);

            glVertex3f(center.x+hs, center.y-hs, center.z+hs);
            glVertex3f(center.x+hs, center.y-hs, center.z-hs);
            glVertex3f(center.x-hs, center.y-hs, center.z-hs);
            glVertex3f(center.x-hs, center.y-hs, center.z+hs);

            glVertex3f(center.x+hs, center.y+hs, center.z+hs);
            glVertex3f(center.x+hs, center.y+hs, center.z-hs);
            glVertex3f(center.x+hs, center.y-hs, center.z-hs);
            glVertex3f(center.x+hs, center.y-hs, center.z+hs);

            glVertex3f(center.x-hs, center.y+hs, center.z+hs);
            glVertex3f(center.x-hs, center.y+hs, center.z-hs);
            glVertex3f(center.x-hs, center.y-hs, center.z-hs);
            glVertex3f(center.x-hs, center.y-hs, center.z+hs);

            glVertex3f(center.x+hs, center.y+hs, center.z+hs);
            glVertex3f(center.x+hs, center.y-hs, center.z+hs);
            glVertex3f(center.x-hs, center.y-hs, center.z+hs);
            glVertex3f(center.x-hs, center.y+hs, center.z+hs);

            glVertex3f(center.x+hs, center.y+hs, center.z-hs);
            glVertex3f(center.x+hs, center.y-hs, center.z-hs);
            glVertex3f(center.x-hs, center.y-hs, center.z-hs);
            glVertex3f(center.x-hs, center.y+hs, center.z-hs);
        glEnd();
//    glPopAttrib();
//    glPopAttrib();
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_LINE);
        //TODO usare una pop e push per qua sopra
}
float BoundingBox::rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection)
{
//    std::cout<<"aabb center: ";center.print();
//    std::cout<<"aabb side: "<<side<<std::endl;
//    std::cout<<"start: ";start.print();
//    std::cout<<"dir: ";dir.print();
    //TODO implement! questo non funziona!
    float dist=-1, tmp_d=-1;
    Vec3 tmp_inter;
    float d = side/2.0f;
    // X normal faces
    if (dir.x > 0)
    {
        if (start.x < center.x-d)// else start is ahead of the face
        {
            tmp_inter = start+dir*((center.x-d-start.x)/dir.x);
            if (tmp_inter.y<=center.y+d && tmp_inter.y>center.y-d &&
                tmp_inter.z<=center.z+d && tmp_inter.z>center.z-d)
            {
                tmp_d = (tmp_inter-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (-1,0,0)
//        float x = center.x-side/2.0;
//        if (start.x > x)// else start is ahead of the face
//        {
//            tmp_inter = start+dir*((x-start.x)/dir.x);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    } else
    if (dir.x < 0)
    {
        if (start.x > center.x+d)// else start is ahead of the face
        {
            tmp_inter = start+dir*((center.x+d-start.x)/dir.x);
            if (tmp_inter.y<center.y+d && tmp_inter.y>=center.y-d &&
                tmp_inter.z<center.z+d && tmp_inter.z>=center.z-d)
            {
                tmp_d = (tmp_inter-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (1,0,0)
//        float x = center.x+side/2.0;
//        if (start.x < x)//else start is ahead of the face
//        {
//            tmp_inter = start+dir*((start.x-x)/dir.x);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    }
    // Y normal faces
    if (dir.y > 0)
    {
        if (start.y < center.y-d)//else start is ahead of the face
        {
            tmp_inter = start+dir*((center.y-d-start.y)/dir.y);
            if (tmp_inter.x<center.x+d && tmp_inter.x>= center.x-d &&
                tmp_inter.z<=center.z+d && tmp_inter.z>center.z-d)
            {
                tmp_d = (tmp_inter-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (0,-1,0)
//        float y = center.y-side/2.0;
//        if (start.y > y)//else start is ahead of the face
//        {
//            tmp_inter = start+dir*((y-start.y)/dir.y);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    } else
    if (dir.y < 0)
    {
        if (start.y > center.y+d)//else start is ahead of the face
        {
            tmp_inter = start+dir*((center.y+d-start.y)/dir.y);
            if (tmp_inter.x<=center.x+d && tmp_inter.x> center.x-d &&
                tmp_inter.z<center.z+d && tmp_inter.z>=center.z-d)
            {
                tmp_d = (tmp_inter-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (0,1,0)
//        float y = center.y+side/2.0;
//        if (start.y < y)//else start is ahead of the face
//        {
//            tmp_inter = start+dir*((start.y-y)/dir.y);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    }
    // Z normal faces
    if (dir.z > 0)
    {
        if (start.z < center.z-d)//else start is ahead of the face
        {
            tmp_inter = start+dir*((center.z-d-start.z)/dir.z);
            if (tmp_inter.x<center.x+d && tmp_inter.x>=center.x-d &&
                tmp_inter.y<center.y+d && tmp_inter.y>=center.y-d)
            {
                tmp_d = (center-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (0,0,-1)
//        float z = center.z-side/2.0;
//        if (start.z > z)//else start is ahead of the face
//        {
//            tmp_inter = start+dir*((z-start.z)/dir.z);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    } else
    if (dir.z < 0)
    {
        if (start.z > center.z+d)//else start is ahead of the face
        {
            tmp_inter = start+dir*((center.z+d-start.z)/dir.z);
            if (tmp_inter.x<=center.x+d && tmp_inter.x>center.x-d &&
                tmp_inter.y<=center.y+d && tmp_inter.y>center.y-d)
            {
                tmp_d = (center-start).getModule();
                if (dist==-1||tmp_d<dist)
                {
                    dist = tmp_d;
                    *intersection = tmp_inter;
                }
            }
        }
//        //intersect with face with normal (0,0,1)
//        float z = center.z+side/2.0;
//        if (start.z < z)//else start is ahead of the face
//        {
//            tmp_inter = start+dir*((start.z-z)/dir.z);
//            tmp_d = (tmp_inter-start).getModule();
//            if (tmp_d<dist)
//            {
//                dist = tmp_d;
//                *intersection = tmp_inter;
//            }
//        }
    }
    
    return dist;
}
int BoundingBox::collide(BoundingSphere * bs)
{
    int res = bs->collide(this);
    return INVERT_COLLISION_RELATION(res);
}
