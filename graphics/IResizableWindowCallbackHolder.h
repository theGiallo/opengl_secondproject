#ifndef IRESIZABLEWINDOW_H
#define IRESIZABLEWINDOW_H

class IWindowResizeHandler
{
public:
    virtual void onResize(int width, int height) =0;
};


#endif /* IRESIZABLEWINDOW_H */