#include "BoundingCircle.h"
#include "../graphics/CollisionSolver2D.h"
#include "../graphics/Camera2D.h"

#include <iostream>

RegularPolygon2D BoundingCircle::circle_appearence(50);

BoundingCircle::BoundingCircle(const float& radius/*=1.0f*/, Vec2 center /* = Vec2() */)
{
    // 
    check_collisions = intersectable = visible = true;
    //
    this->radius = radius;
    this->pos = center;
    setScale(UNITARY_SCALE);
}

const float& BoundingCircle::getRadius_const(void) const
{
    return radius;
}
float& BoundingCircle::getRadius_r(void)
{
    return radius;
}
void BoundingCircle::setRadius(const float& radius)
{
    this->radius = radius;
}

int BoundingCircle::collide(const Camera2D &camera)
{
    float dist;
    return CollisionSolver2D::collide(*this, camera.getBoundingRectangle(), dist);
}
void BoundingCircle::render(void)
{
    circle_appearence.setColor(color);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glTranslatef(pos.x,pos.y,0);
        glScalef(scale.x*radius, scale.y*radius, 1);
        circle_appearence.render();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
float BoundingCircle::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
    /* circumpherence*/
    float a = -2.0f*pos.x;
    float b = -2.0f*pos.y;
    float c = pos.x*pos.x + pos.y*pos.y -radius;
    /*rect*/
    float m = dir.y/dir.x;
    
    float A = 1+m*m;
    float B = a-2*start.y*m-m*m*2*start.x-b*m;
    float C = c+start.y*start.y+2*start.x*start.y*m+m*m-m*m*start.x+b*start.y-b*m*start.x;
    
    float D = B*B-4*A*C;
    
    if (D<0)
    {
        return -1;
    }
    if (D==0)
    {
        intersection->x = -B/(2*A);
        intersection->y = -m*(intersection->x-start.x)+start.y;
        return (start-*intersection).getModule();
    }
    float dist1, dist2;
    Vec2 inter;
    intersection->x = (-B-D)/(-2*A);
    inter.x = (-B-D)/(-2*A);
    intersection->y = -m*(intersection->x-start.x)+start.y;
    inter.y = -m*(inter.x-start.x)+start.y;
    dist1 = (start-*intersection).getModule();
    dist2 = (start-inter).getModule();
    if (dist1<dist2)
    {
        return dist1;
    }
    *intersection = inter;
    return dist2;
}