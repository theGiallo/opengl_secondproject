#include "../graphics/ProgramManager.h"
#include "../graphics/ShaderManager.h"
#include <iostream>

#include "../graphics/GLDebug.h"

Program* initDP()
{
    Program* tmp = new Program();
    return tmp;
}
Program* ProgramManager::defaultProgram=NULL;

std::list<Program*> initPs()
{
    std::list<Program*> tmp;
    return tmp;
}
std::list<Program*> ProgramManager::programs(initPs());

Program* ProgramManager::getProgram(int n, std::string *pathNames)
{
    int corr;
    if ( n==0 )
    {
        std::cout<<"default program returned because of empty file shader"<<std::endl;
        return getDefaultProgram();
    }
    for (std::list<Program*>::iterator it=ProgramManager::programs.begin(); it!=ProgramManager::programs.end(); ++it)
    {
        if ( (*it)->attached_shaders.size() == n )
        {
            corr=0;
            for (int i=0 ; i<n ; i++ )
            {
                if ( (*it)->hasShader(pathNames[i]) )
                {
                    corr++;
                }
            }
            if ( corr==n )
            {
                return *it;
            }
        }
    }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    //DEBUG begin
    std::cout<<"creating a new program with "<<n<<" shaders..."<<std::endl;
    for (int i = 0; i < n; i++)
    {
        std::cout<<"\tpathNames["<<i<<"]= "<<pathNames[i]<<std::endl;
    }

    //DEBUG end
    Program* tmp_p= new Program();
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    tmp_p->create();
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    for (int i=0 ; i<n ; i++ )
    {
        //DEBUG begin
        std::cout<<"\tattaching the "<<i<<" shader...\t";
        //DEBUG end
        try{tmp_p->attachShader(ShaderManager::loadShader(pathNames[i]));}
        catch(bool err){ std::cerr<<"Error loading shader '"<<pathNames[i]<<"'"<<std::endl;}
        //DEBUG begin
        std::cout<<"\tdone"<<std::endl;
        //DEBUG end
    }
    GLint linked;
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    glLinkProgram(tmp_p->id);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    glGetProgramiv(tmp_p->id, GL_LINK_STATUS, &linked);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    if (linked==GL_TRUE)
    {
        std::cout<<" linkato correttamente"<<std::endl;
    } else
    {
        std::cout << " **linker error** "<<linked<<"\n"<<std::endl;
    }
    //DEBUG begin
    std::cout<<"done"<<std::endl;
    //DEBUG end
    programs.push_front(tmp_p);
    return tmp_p;
}

Program* ProgramManager::getDefaultProgram(void)
{
    if ( defaultProgram == NULL)
    {
        defaultProgram = new Program();
        std::cout<<defaultProgram<<std::endl;
        std::cout<<"default program id "<<defaultProgram->id<<std::endl;
        programs.push_front(defaultProgram);
    }
    return defaultProgram;
}
