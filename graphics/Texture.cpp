#include <string.h>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include "../graphics/Texture.h"
#include "../graphics/TextureManager.h"
#include <FreeImagePlus.h>
#include <FreeImage.h>
#include <iostream>

Texture::Texture(void)
{
    texId = type = 0;
    texels = NULL;
}
Texture::~Texture(void)
{
    unloadTexelsArray();
}
/*void Texture::calcHash(void)
{
    hash = Texture::loc.hash((char*)texels,((char*)texels)+sizeof(GLubyte)*width*height*(type==GL_RGBA?4:3));
}*/
bool Texture::load(std::string file_path)
{
	std::cout<<"Texture::load file_path: "<<file_path<<std::endl;
    const char *path = file_path.c_str();
    this->file_path = file_path;
    FIBITMAP*bitmap;
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    fif = FreeImage_GetFileType(path, 0);
    if(fif == FIF_UNKNOWN)
    {
        fif = FreeImage_GetFIFFromFilename(path);
    }
    if((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif))
    {
        bitmap = FreeImage_Load(fif, path, 0);
    } else
    {
        return false;
    }
   FREE_IMAGE_TYPE image_type = FreeImage_GetImageType(bitmap);
   GLuint bpp = FreeImage_GetBPP(bitmap);
   width = FreeImage_GetWidth(bitmap);
   height = FreeImage_GetHeight(bitmap);
   GLuint pitch = FreeImage_GetPitch(bitmap);
   GLuint ctype = FreeImage_GetColorType(bitmap);
   void*bits = FreeImage_GetBits(bitmap);
   if (bits==NULL)
   {
	   std::cerr<<"The image file does not contain pixel data"<<std::endl;
   }
#ifdef DEBUG
   std::cout<<"\twidth: "<<width<<std::endl<<"\theight: "<<height<<std::endl<<"\tpitch: "<<pitch<<std::endl<<"\tbpp: "<<bpp<<std::endl;
#endif
   if ( image_type != FIT_BITMAP || (bpp != 24 && bpp!=32) || (ctype!=FIC_RGB && ctype!=FIC_RGBALPHA) )
   {
       FreeImage_Unload(bitmap);
       return false;
   }
   if (ctype==FIC_RGB)
   {
       type = GL_RGB;
       texels = (GLubyte*)malloc(width*height*3);
#ifdef DEBUG
		std::cout<<"\ttype: FIC_RGB"<<std::endl;
#endif
   } else if (ctype==FIC_RGBALPHA)
   {
       type = GL_RGBA;
       texels = (GLubyte*)malloc(width*height*4);
#ifdef DEBUG
		std::cout<<"\ttype: FIC_RGBALPHA"<<std::endl;
#endif
   } else
   {
	   throw false;
   }
   if (texels==NULL)
   {
	   std::cerr<<"Error allocating memory for a texture"<<std::endl;
	   throw false;
   }
    int red,green,blue,alpha;
    int offset = 0;
    int offset_img = 0;
	// Calculate the number of bytes per pixel (3 for 24-bit or 4 for 32-bit)
	int bytespp = FreeImage_GetLine(bitmap) / FreeImage_GetWidth(bitmap);
    if( ctype==FIC_RGB /*bpp == 24*/)
    {
#ifdef OS_MACOSX
#ifdef CWC_BIG_ENDIAN
      red = 0; green = 1; blue = 2; alpha = 3;
#else
      red = 2; green = 1; blue = 0; alpha = 3;
#endif
#else
      red = 0; green = 1; blue = 2; alpha = 3;
#endif
      for (unsigned int y=0;y<height;y++)
      {
         offset_img = y*pitch;
         for (unsigned int x=0;x<width;x++)
         {
            texels[offset+0]    = ((unsigned char*)bits)[offset_img+FI_RGBA_RED];
            texels[offset+1]  = ((unsigned char*)bits)[offset_img+FI_RGBA_GREEN];
            texels[offset+2]   = ((unsigned char*)bits)[offset_img+FI_RGBA_BLUE];
            // DEBUG std::cout<<(unsigned short int)texels[offset+red]<<" "<<(unsigned short int)texels[offset+green]<<" "<<(unsigned short int)texels[offset+blue]<<std::endl;
            offset += 3;
            offset_img += bytespp;
         }
      }
   } else if ( ctype==FIC_RGBALPHA /*bpp==32*/)
   {
#ifdef OS_MACOSX
#ifdef CWC_BIG_ENDIAN
      red = 0; green = 1; blue = 2; alpha = 3;
#else
      red = 2; green = 1; blue = 0; alpha = 3;
#endif
#else
      red = 0; green = 1; blue = 2; alpha = 3;
#endif
      for (unsigned int y=0;y<height;y++)
      {
         offset_img = y*pitch;
         for (unsigned int x=0;x<width;x++)
         {
            texels[offset+0]    = ((unsigned char*)bits)[offset_img+FI_RGBA_RED];
            texels[offset+1]  = ((unsigned char*)bits)[offset_img+FI_RGBA_GREEN];
            texels[offset+2]   = ((unsigned char*)bits)[offset_img+FI_RGBA_BLUE];
            texels[offset+3]  = ((unsigned char*)bits)[offset_img+FI_RGBA_ALPHA];
            // DEBUG std::cout<<(unsigned short int)texels[offset+red]<<" "<<(unsigned short int)texels[offset+green]<<" "<<(unsigned short int)texels[offset+blue]<<" "<<(unsigned short int)texels[offset+alpha]<<std::endl;
            offset += 4;
            offset_img +=bytespp;
         }
      }
   }
    std::cout<<"preunload"<<std::endl;
   FreeImage_Unload(bitmap);
   return true;
}
void Texture::unloadTexelsArray(void)
{
    delete [] texels;
}
void Texture::store(std::list<Texture*> *texList) // if !isStored then storeAnyway
{
    if (texId!=0)
    {
        return;
    }
    if ( !isStored(texList, &texId) )
    {
        storeAnyway(texList);
    }
}
void Texture::storeAnyway(std::list<Texture*> *texList)
{
    if (texId==0)
    {
        glGenTextures(1, &texId);
    }
    glBindTexture( GL_TEXTURE_2D, texId);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    GLint ret = gluBuild2DMipmaps(GL_TEXTURE_2D, type, width, height, type, GL_UNSIGNED_BYTE, texels);
    std::cout<<(char*)gluErrorString(ret)<<std::endl;;
}
bool Texture::isStored(std::list<Texture*> *texList, GLuint *texId)
{
    for (std::list<Texture*>::iterator it=texList->begin(); it!=texList->end(); ++it)
    {
        if ((*it)->file_path.compare(this->file_path)==0 || ((*it)->width==width && (*it)->height==height && (*it)->type==type && memcmp((*it)->texels, texels, sizeof(GLubyte)*width*height*(type==GL_RGBA?4:3))==0) )
        {
            *texId = (*it)->texId;
            return true;
        }
    }
    return false;
}
bool Texture::isStored(std::list<Texture*>* texList, Texture** stored)
{
    for (std::list<Texture*>::iterator it=texList->begin(); it!=texList->end(); ++it)
    {
        if ((*it)->file_path.compare(this->file_path)==0 || ((*it)->width==width && (*it)->height==height && (*it)->type==type && memcmp((*it)->texels, texels, sizeof(GLubyte)*width*height*(type==GL_RGBA?4:3))==0) )
        {
            *stored = *it;
            return true;
        }
    }
    return false;
}
