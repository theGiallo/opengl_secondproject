/**
 * Author: theGiallo
 * Directory: graphics
 **/
 
#ifndef OGLTOOLS_H
#define	OGLTOOLS_H

#include <GL/glew.h>
#include "../maths/Vec2.h"

class OGLTools
{
private:
	OGLTools();
public:
	static void translateVArray(GLfloat* vArray, unsigned int count, const Vec2& t);
	static void rotateVArray(GLfloat* vArray, unsigned int count, float rot);
	static void scaleVArray(GLfloat* vArray, unsigned int count, const Vec2& s);
};
#endif	/* OGLTOOLS_H */
