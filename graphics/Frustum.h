/* 
 * File:   Frustum.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 24 marzo 2011, 14.59
 */

#ifndef FRUSTUM_H
#define	FRUSTUM_H

#include "../maths/Vec3.h"
#include "../maths/Plane.h"
#include "../graphics/Renderable.h"

// planes
#define NEAR_PL 0
#define FAR_PL 1
#define LEFT_PL 2
#define RIGHT_PL 3
#define TOP_PL 4
#define BOTTOM_PL 5

// collision //inverted first 2
#define COLLISION_OUT 0
#define COLLISION_IN 1
#define COLLISION_COLLIDE 2
#define COLLISION_CONTAINS 3

#define INVERT_COLLISION_RELATION(c) ((c)==COLLISION_IN?COLLISION_CONTAINS:((c)==COLLISION_CONTAINS?COLLISION_IN:(c)))

// boundPoints
#define NTL 0
#define NTR 1
#define NBL 2
#define NBR 3
#define FTL 4
#define FTR 5
#define FBL 6
#define FBR 7

#ifdef WIN32
	#undef far
	#undef near
#endif

class Frustum : public Renderable
{
public:
	float far; float near;
    float FOV, ratio;
    Vec3 pos, look, up, right;
    Vec3 boundPoints[8];
    Plane planes[6];

    Frustum();

    void updatePosLookUp(Vec3 pos, Vec3 look, Vec3 up, float ratio=0);
    void updatePosTargetUp(Vec3 pos, Vec3 target, Vec3 up, float ratio=0);
    void updateNF(float near, float far);
    void updateFOV(float FOV);
    void updateRatio(float ratio);
    
    bool isInside(Vec3 point);
    int collideSphere(Vec3 center, float radius);
    int collideBox(Vec3 center, float side);

    void updatePlanes();
    void updateNears();
    void updateFars();
    void updatePoints();

    virtual void render(void);

    void calcBoundingSphere();
};

#endif	/* FRUSTUM_H */

