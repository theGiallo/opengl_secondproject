/* 
 * File:   ProgramManager.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 7 febbraio 2011, 16.09
 */

#ifndef PROGRAMMANAGER_H
#define	PROGRAMMANAGER_H

#include <string>
#include <list>
#include "../graphics/ShaderManager.h"
#include "../graphics/Program.h"

class ProgramManager
{
protected:
    static std::list<Program*> programs;
    static Program* defaultProgram;
public:
    static Program* getProgram(int n, std::string *pathNames);
    static Program* getDefaultProgram(void);

};

#endif	/* PROGRAMMANAGER_H */

