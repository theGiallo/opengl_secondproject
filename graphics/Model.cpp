#include <iostream>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include "../graphics/Model.h"
#include "../graphics/Texture.h"
#include "../graphics/TextureManager.h"
#include "../maths/Vec2.h"
#include "../graphics/ProgramManager.h"
#include "../tools/FilePathTool.h"
#include <fstream>
#include <istream>
#include <iostream>
#include <string>
#include "../graphics/GLDebug.h"

Model::Model(void)
{
    n_vertexes = n_triangles = 0;
    vertexes = NULL;
    vArray = NULL;
    triangles = NULL;
    texture = NULL;
    has_texture = false;
    color[0] = color[1] = color[2] = color[3] = 1;
}
Model::~Model(void)
{
    unload();
}
void Model::updateVArray(void)
{
    int j,d;
    for (int i=0 ; i<n_triangles ; i++ )
    {
        j = i*(has_texture?24:18);
        d=0;
        if (has_texture)
        {
            vArray[j+d++]=triangles[i].texPos[0].x;
            vArray[j+d++]=triangles[i].texPos[0].y;
        }
        vArray[j+d++]=normals[triangles[i].vId[0]].x;
        vArray[j+d++]=normals[triangles[i].vId[0]].y;
        vArray[j+d++]=normals[triangles[i].vId[0]].z;
        vArray[j+d++]=vertexes[triangles[i].vId[0]].x;
        vArray[j+d++]=vertexes[triangles[i].vId[0]].y;
        vArray[j+d++]=vertexes[triangles[i].vId[0]].z;

        if (has_texture)
        {
            vArray[j+d++]=triangles[i].texPos[1].x;
            vArray[j+d++]=triangles[i].texPos[1].y;
        }
        vArray[j+d++]=normals[triangles[i].vId[1]].x;
        vArray[j+d++]=normals[triangles[i].vId[1]].y;
        vArray[j+d++]=normals[triangles[i].vId[1]].z;
        vArray[j+d++]=vertexes[triangles[i].vId[1]].x;
        vArray[j+d++]=vertexes[triangles[i].vId[1]].y;
        vArray[j+d++]=vertexes[triangles[i].vId[1]].z;

        if (has_texture)
        {
            vArray[j+d++]=triangles[i].texPos[2].x;
            vArray[j+d++]=triangles[i].texPos[2].y;
        }
        vArray[j+d++]=normals[triangles[i].vId[2]].x;
        vArray[j+d++]=normals[triangles[i].vId[2]].y;
        vArray[j+d++]=normals[triangles[i].vId[2]].z;
        vArray[j+d++]=vertexes[triangles[i].vId[2]].x;
        vArray[j+d++]=vertexes[triangles[i].vId[2]].y;
        vArray[j+d++]=vertexes[triangles[i].vId[2]].z;
    }
}
void Model::draw(void)
{
    glUseProgram(program->id);

    if (has_texture)
    {
        glBindTexture(GL_TEXTURE_2D, texture->texId);
        GLint loc;
        loc = glGetUniformLocation(program->id, "myTexture");
        if (loc!=-1)
        {
            //return false;  // can't find variable / invalid index
            glUniform1i(loc, texture->texId);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        } else
        {
            std::cerr<<"Error in initializing GLSL variable myTexture'"<<std::endl;
        }
    }

	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glInterleavedArrays( has_texture?GL_T2F_N3F_V3F:GL_N3F_V3F,(has_texture?8:6)*sizeof(GLfloat),&vArray[0]);
    glDrawArrays(GL_TRIANGLES, 0, n_triangles*3);
	glDisableClientState(GL_VERTEX_ARRAY);
}
void Model::drawImmediate(void)
{
    glUseProgram(program->id);
    if (has_texture)
    {
        glBindTexture(GL_TEXTURE_2D, texture->texId);
        GLint loc;
        loc = glGetUniformLocation(program->id, "myTexture");
        if (loc!=-1)
        {
            //return false;  // can't find variable / invalid index
            glUniform1i(loc, texture->texId);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        } else
        {
            std::cerr<<"Error in initializing GLSL variable myTexture'"<<std::endl;
        }
    }


    glColor4fv(this->color);
    glPushAttrib(GL_COLOR);
    glBegin(GL_TRIANGLES);
    for (int i=0 ; i<n_triangles ; i++ )
    {
        if (has_texture) glTexCoord2f(triangles[i].texPos[0].x, triangles[i].texPos[0].y );
        glNormal3f(normals[triangles[i].vId[0]].x, normals[triangles[i].vId[0]].y, normals[triangles[i].vId[0]].z);
        glVertex3f(vertexes[triangles[i].vId[0]].x, vertexes[triangles[i].vId[0]].y, vertexes[triangles[i].vId[0]].z);
        if (has_texture) glTexCoord2f(triangles[i].texPos[1].x, triangles[i].texPos[1].y );
        glNormal3f(normals[triangles[i].vId[1]].x, normals[triangles[i].vId[1]].y, normals[triangles[i].vId[1]].z);
        glVertex3f(vertexes[triangles[i].vId[1]].x, vertexes[triangles[i].vId[1]].y, vertexes[triangles[i].vId[1]].z);
        if (has_texture) glTexCoord2f(triangles[i].texPos[2].x, triangles[i].texPos[2].y );
        glNormal3f(normals[triangles[i].vId[2]].x, normals[triangles[i].vId[2]].y, normals[triangles[i].vId[2]].z);
        glVertex3f(vertexes[triangles[i].vId[2]].x, vertexes[triangles[i].vId[2]].y, vertexes[triangles[i].vId[2]].z);
    }
    glEnd();
    glPopAttrib();
}
void Model::load(std::string file_path)
{
	std::cout<<"DBG2.0"<<std::endl;
	std::cout<<"DBG2.1: "<<file_path<<std::endl;
    Lib3dsFile * file = lib3ds_file_open(file_path.c_str());
	std::cout<<"DBG2.2"<<std::endl;
    if ( !file )
    {
		std::cerr<<"DBG2.e"<<std::endl;
        throw false;
    }
    if ( file->nmeshes == 0 )
    {
        return;
    }
    mesh = *(file->meshes[0]);
    int i;
    // TODO : know when a mesh has no texture
    n_vertexes = mesh.nvertices;
    vertexes = new Vec3[n_vertexes];
    normals = new Vec3[n_vertexes];
    for ( i=0 ; i<n_vertexes ; i++ )
    {
        vertexes[i] = Vec3(mesh.vertices[i][0], mesh.vertices[i][1], mesh.vertices[i][2]);
    }
    calculateVertexesNormals();
    n_triangles = mesh.nfaces;
    triangles = new Triangle[n_triangles];
    for ( i=0 ; i<n_triangles ; i++ )
    {
        triangles[i].vId[0] = mesh.faces[i].index[0];
        triangles[i].vId[1] = mesh.faces[i].index[1];
        triangles[i].vId[2] = mesh.faces[i].index[2];

        triangles[i].texPos[0] = Vec2(mesh.texcos[triangles[i].vId[0]][0], mesh.texcos[triangles[i].vId[0]][1]);
        triangles[i].texPos[1] = Vec2(mesh.texcos[triangles[i].vId[1]][0], mesh.texcos[triangles[i].vId[1]][1]);
        triangles[i].texPos[2] = Vec2(mesh.texcos[triangles[i].vId[2]][0], mesh.texcos[triangles[i].vId[2]][1]);
    }
    std::string tex_path(file->materials[mesh.faces[0].material]->texture1_map.name);
    std::string tex_base_path = FilePathTool::removeFileName(file_path);
    tex_path = tex_base_path + tex_path;
    has_texture = true;
    try {
//        std::cout<<"path of the model texure "<<texPath.string()<<std::endl;
        texture = TextureManager::loadTexture(tex_path);
//        std::cout<<"id texture "<<texture->texId<<std::endl;
    } catch (bool /*err*/)
    {
        has_texture = false;
    }
    vArray = new GLfloat[n_triangles*(has_texture?24:18)];
    this->updateVArray();
    
    // read the file with the shaders used to render this model
    std::string shaders_file_path = FilePathTool::removeFileExtension(file_path)+".shaders";
    //std::cout<<shaders_file_path<<std::endl;
    std::ifstream shaders_file(shaders_file_path.c_str());
    std::string tmp_shdr;
    int nlines=0;
    if ( shaders_file.is_open() )
    {
        shaders_file.seekg(0, shaders_file.beg);
        while ( !shaders_file.eof() )
        {
            std::getline(shaders_file, tmp_shdr);
            std::cout<<"test line "<<nlines<<": "<<tmp_shdr<<std::endl;
            nlines++;
        }
        shaders_file.clear();
        shaders_file.seekg(0, shaders_file.beg);
        std::string * shaders = new std::string[nlines];
        int i=0;
//        std::basic_string<char> tmp_str;
//        std::cout<<"nlines= "<<nlines<<std::endl;
        for ( i=0 ; i<nlines ; i++ )
        {
            std::getline(shaders_file, shaders[i]);
            std::cout<<"line "<<i<<": "<<shaders[i]<<std::endl;
        }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        program = ProgramManager::getProgram(nlines, shaders);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        delete [] shaders;
        shaders_file.close();
    } else
    {
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        program = ProgramManager::getDefaultProgram();
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
        std::cout<<"default program "<<(void*)program<<std::endl;
    }
}
void Model::unload(void)
{
    n_vertexes = n_triangles = 0;
    if ( vertexes != NULL)
    {
        delete [] vertexes;
    }
    if ( normals != NULL)
    {
        delete [] normals;
    }
    if ( triangles != NULL )
    {
        delete [] triangles;
    }
    if ( vArray != NULL )
    {
        delete [] vArray;
    }
}
void Model::rotate(Quaternion q) // rotate around the model center
{
    this->rotate(q, center);
}
void Model::rotate(Quaternion q, Vec3 center)
{
    for (int i=0 ; i<n_vertexes ; i++ )
    {
        vertexes[i].rotate(q, center);
    }
    this->center.rotate(q,center);
    updateVArray();
}
void Model::translate(Vec3 t)
{
    for (int i=0 ; i<n_vertexes ; i++ )
    {
        vertexes[i]+=t;
    }
    this->center+=t;
    updateVArray();
}
void Model::scale(Vec3 s) // scale relatively to the model center
{
    this->scale(s, center);
}
void Model::scale(Vec3 s, Vec3 center)
{
    for (int i=0 ; i<n_vertexes ; i++ )
    {
        vertexes[i].scale(s, center);
    }
    this->center.scale(s, center);
    updateVArray();
}
void Model::setCenter(Vec3 center)
{
    this->translate(center-this->center);
}
Vec3 Model::getCenter(void)
{
    return center;
}
void Model::setColor(GLfloat color[4])
{
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
}
GLfloat* Model::getColor(void)
{
    GLfloat *ret = new GLfloat[4];
    ret[0] = color[0];
    ret[1] = color[1];
    ret[2] = color[2];
    ret[3] = color[3];
    return ret;
}
Uint16 Model::getVertexesCount(void)
{
    return n_vertexes;
}
Uint16 Model::getTrianglesCount(void)
{
    return n_triangles;
}
void Model::calculateFacesNormals(void)
{
    for (int i=0 ; i<n_triangles ; i++ )
    {
        triangles[i].CalculateNormal(vertexes);
    }
}
void Model::calculateVertexesNormals(void)
{
    calculateFacesNormals();
    Vec3 tmp_normal;
    int adj;
    for (int i=0 ; i<n_vertexes ; i++ )
    {
        adj = 0;
        for ( int j=0 ; j<n_triangles ; j++ )
        {
            if ( triangles[j].vId[0]==i || triangles[j].vId[1]==i || triangles[j].vId[2]==i )
            {
                adj++;
                tmp_normal+=triangles[j].normal;
            }
        }
        normals[i] = tmp_normal/(float)adj;
    }
}
void Triangle::CalculateNormal(Vec3 *vertexes)
{
    normal = (vertexes[vId[0]]-vertexes[vId[1]])^(vertexes[vId[2]]-vertexes[vId[1]]);
    normal.normalize();
}
bool Model::hasTexture(void)
{
    return has_texture;
}

void Model::setHasTexture(bool hasTexture)
{
    this->has_texture = hasTexture;
}
Program Model::getProgram(void)
{
    return *program;
}

Model* Model::getCube(float side)
{
    Model *cube = new Model();
    cube->n_vertexes = 8;
    cube->n_triangles = 12;
    cube->normals = new Vec3[8];
    cube->vertexes = new Vec3[8];
    cube->vArray = new GLfloat[288];
    cube->triangles = new Triangle[12];
    cube->texture = NULL;
    cube->has_texture = false;
    cube->color[0] = cube->color[1] = cube->color[2] = cube->color[3] = 1;
    cube->center = Vec3();
    cube->program = ProgramManager::getDefaultProgram();

    float halfSide = side/2.0f;

    cube->vertexes[0].x = cube->vertexes[4].x = halfSide;
    cube->vertexes[0].y = cube->vertexes[4].y = halfSide;
    cube->vertexes[1].x = cube->vertexes[5].x = -halfSide;
    cube->vertexes[1].y = cube->vertexes[5].y = halfSide;
    cube->vertexes[2].x = cube->vertexes[6].x = -halfSide;
    cube->vertexes[2].y = cube->vertexes[6].y = -halfSide;
    cube->vertexes[3].x = cube->vertexes[7].x = halfSide;
    cube->vertexes[3].y = cube->vertexes[7].y = -halfSide;
    cube->vertexes[0].z = cube->vertexes[1].z = cube->vertexes[2].z = cube->vertexes[3].z = -halfSide;
    cube->vertexes[4].z = cube->vertexes[5].z = cube->vertexes[6].z = cube->vertexes[7].z = halfSide;

    // bottom face
    cube->triangles[0].vId[0] = 3;
    cube->triangles[0].vId[1] = 1;
    cube->triangles[0].vId[2] = 0;
    cube->triangles[1].vId[0] = 3;
    cube->triangles[1].vId[1] = 2;
    cube->triangles[1].vId[2] = 1;
    cube->triangles[0].normal.z = cube->triangles[1].normal.z = -1;
    cube->triangles[0].normal.y = cube->triangles[0].normal.x = cube->triangles[1].normal.x = cube->triangles[1].normal.z = 0;

    //top face
    cube->triangles[2].vId[0] = 4;
    cube->triangles[2].vId[1] = 5;
    cube->triangles[2].vId[2] = 7;
    cube->triangles[3].vId[0] = 5;
    cube->triangles[3].vId[1] = 6;
    cube->triangles[3].vId[2] = 7;
    cube->triangles[2].normal.z = cube->triangles[3].normal.z = 1;
    cube->triangles[2].normal.y = cube->triangles[2].normal.x = cube->triangles[3].normal.x = cube->triangles[3].normal.z = 0;

    //side face 1
    cube->triangles[4].vId[0] = 0;
    cube->triangles[4].vId[1] = 1;
    cube->triangles[4].vId[2] = 4;
    cube->triangles[5].vId[0] = 4;
    cube->triangles[5].vId[1] = 1;
    cube->triangles[5].vId[2] = 5;
    cube->triangles[4].normal.y = cube->triangles[5].normal.y = 1;
    cube->triangles[4].normal.x = cube->triangles[4].normal.z = cube->triangles[5].normal.x = cube->triangles[5].normal.z = 0;

    //side face 2
    cube->triangles[6].vId[0] = 1;
    cube->triangles[6].vId[1] = 2;
    cube->triangles[6].vId[2] = 5;
    cube->triangles[7].vId[0] = 5;
    cube->triangles[7].vId[1] = 2;
    cube->triangles[7].vId[2] = 6;
    cube->triangles[6].normal.x = cube->triangles[7].normal.x = -1;
    cube->triangles[6].normal.y = cube->triangles[6].normal.z = cube->triangles[7].normal.y = cube->triangles[7].normal.z = 0;

    //side face 3
    cube->triangles[8].vId[0] = 2;
    cube->triangles[8].vId[1] = 3;
    cube->triangles[8].vId[2] = 6;
    cube->triangles[9].vId[0] = 6;
    cube->triangles[9].vId[1] = 3;
    cube->triangles[9].vId[2] = 7;
    cube->triangles[8].normal.y = cube->triangles[9].normal.y = -1;
    cube->triangles[8].normal.x = cube->triangles[8].normal.z = cube->triangles[9].normal.x = cube->triangles[9].normal.z = 0;

    //side face 4
    cube->triangles[10].vId[0] = 3;
    cube->triangles[10].vId[1] = 0;
    cube->triangles[10].vId[2] = 7;
    cube->triangles[11].vId[0] = 7;
    cube->triangles[11].vId[1] = 0;
    cube->triangles[11].vId[2] = 4;
    cube->triangles[10].normal.x = cube->triangles[11].normal.x = 1;
    cube->triangles[10].normal.y = cube->triangles[10].normal.z = cube->triangles[11].normal.y = cube->triangles[11].normal.z = 0;


    cube->calculateVertexesNormals();
    cube->updateVArray();

    return cube;
}
