/* 
 * File:   Renderable.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 17 marzo 2011, 21.36
 */

#ifndef RENDERABLE_H
#define	RENDERABLE_H

#include "../maths/Vec3.h"

class Renderable;
class Bounder;

class Renderable
{
public:
    bool visible;
    Bounder * bounder;
    virtual void render(void){};
    bool intersectable;
	Renderable()
	{
		bounder = NULL;
	}
    virtual float rayIntersect(Vec3 start, Vec3 dir, Vec3 * intersection){return 0;};
};

#include "../graphics/Bounder.h"

#endif	/* RENDERABLE_H */
