/**
 * Author: theGiallo
 * Directory: graphics
 **/

#ifndef GJK2D_H
#define	GJK2D_H
#include <GL/glew.h>
#include "../maths/Vec2.h"
#include "../tools/DoubleLinkedCircularList.h"
#include <utility>

class ConvexHull;
class GJK2D;
class GLfloat_Vec2;

int compareFunction(GLfloat_Vec2 el1, GLfloat_Vec2 el2);

class GJK2D {
private:
    GJK2D();
    static void calcScore(Vec2 v0X, Vec2 v0v1, Vec2 v0v2, int* score1, int* score2);
public:
    static bool useBaseCase;
    
    static const float epsilon;

    static Vec2 supportMap(Vec2 origin, Vec2 dir, const GLfloat* vArray, unsigned int nVert, int* out_idV = NULL);
    static float distance(const GLfloat* vArray1, unsigned int nVert1, const GLfloat* vArray2, unsigned int nVert2);
    static float distance(const GLfloat* vArray1, unsigned int nVert1, Vec2 point, Vec2* out_min_dist_P = NULL, int* out_edge_ids = NULL);
    static float trianglePointDistance(Vec2 tr[3], Vec2 p_start, Vec2* p_on_tr, unsigned int short* id0, unsigned int short* id1);
    static float distFromSegment(const Vec2& v0X, const Vec2& v0v1, const Vec2& v0, const Vec2& X, Vec2* out_P_on_seg);
    static ConvexHull& getConvexHull(GLfloat* vArray, unsigned int nVert, ConvexHull& retvalue);
    static ConvexHull& mergeHulls( ConvexHull& left,  ConvexHull& right, ConvexHull& retvalue); /// points array must be contiguous
};

class GLfloat_Vec2 {
public:
    bool operator ==(GLfloat_Vec2 v);
    bool operator !=(GLfloat_Vec2 v);
    GLfloat_Vec2();
    GLfloat_Vec2(GLfloat x, GLfloat y);
    GLfloat x, y;
};

class ConvexHull {
public:
    ConvexHull();
    ConvexHull(const ConvexHull& ch);
    ConvexHull(GLfloat_Vec2* points, unsigned int points_count);
    GLfloat_Vec2* points;
    unsigned int points_count;
    DoubleLinkedCircularList<unsigned int> hull_pids;
};
#endif /* GJK2D_H */
