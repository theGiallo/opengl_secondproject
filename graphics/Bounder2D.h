/** 
 * File:   Bounder2D.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 06 November 2011
 */
#error

#ifndef BOUNDER2D_H
#define	BOUNDER2D_H

#include "../maths/Vec2.h"

//class Renderable2D;
#include "../graphics/Renderable2D.h"
#include "../graphics/PositionableObject2D.h"
class Camera2D;
class Bounder2D;

class Bounder2D : public PositionableObject2D
{
protected:
    GLfloat color[4];
public:
    /**
     * @return this in relation to the frustum: is out, is inside(contained), collide or
     *         contains the frustum
     *          COLLISION_OUT 0
     *          COLLISION_IN 1
     *          COLLISION_COLLIDE 2
     *          COLLISION_CONTAINS 3
     **/
    virtual int collide(const Camera2D &camera)=0;
    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
    
    GLfloat* getColor(GLfloat* color_out)
    {
        return color;
    }
    void setColor(GLfloat color[4])
    {
        memcpy(&this->color[0], &color[0], sizeof(color));
    };
};
#include "../graphics/Camera2D.h"

#endif	/* BOUNDER_H */
