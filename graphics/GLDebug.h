/* 
 * File:   GLDebug.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 16 febbraio 2011, 1.48
 */

#ifndef GLDEBUG_H
#define	GLDEBUG_H
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>
int CheckGLError(char *file, int line);


#endif	/* GLDEBUG_H */

