#include <string.h>

#include "OGLLight.h"
#include <iostream>
GLchar OGLLight::enumToNum(GLenum en)
{
    switch(en)
    {
        case GL_LIGHT0:
            return 0;
        case GL_LIGHT1:
            return 1;
        case GL_LIGHT2:
            return 2;
        case GL_LIGHT3:
            return 3;
        case GL_LIGHT4:
            return 4;
        case GL_LIGHT5:
            return 5;
        case GL_LIGHT6:
            return 6;
        case GL_LIGHT7:
            return 7;
        default:
            throw false;
    }
}
GLenum OGLLight::numToEum(GLchar num)
{
    switch (num)
    {
        case 0:
            return GL_LIGHT0;
        case 1:
            return GL_LIGHT1;
        case 2:
            return GL_LIGHT2;
        case 3:
            return GL_LIGHT3;
        case 4:
            return GL_LIGHT4;
        case 5:
            return GL_LIGHT5;
        case 6:
            return GL_LIGHT6;
        case 7:
            return GL_LIGHT7;
        default:
            throw false;
    }
}
bool OGLLight::usedLights[8] = {false, false, false, false, false, false, false, false};

bool OGLLight::init(void)
{
    for (int i=0 ; i<8 ; i++ )
    {
        if ( !OGLLight::usedLights[i] )
        {
            id = OGLLight::numToEum(i);
            OGLLight::usedLights[i] = true;
            return true;
        }
    }
    return false;
}
void OGLLight::setActive(bool active)
{
        this->active = active;
}
bool OGLLight::isActive(void)
{
    return active;
}
void OGLLight::setAmbient(GLfloat ambient[4])
{
    memcpy((void*)this->ambient, (void*)ambient, 4*sizeof(GLfloat));
}
void OGLLight::setDiffuse(GLfloat diffuse[4])
{
    memcpy((void*)this->diffuse, (void*)diffuse, 4*sizeof(GLfloat));
}
void OGLLight::setSpecular(GLfloat specular[4])
{
    memcpy((void*)this->specular, (void*)specular, 4*sizeof(GLfloat));
}
void OGLLight::setParameters(bool active, GLfloat ambient[4], GLfloat diffuse[4], GLfloat specular[4])
{
    this->active = active;
    memcpy(this->ambient, ambient, sizeof(GLfloat)*4);
    memcpy(this->diffuse, diffuse, sizeof(GLfloat)*4);
    memcpy(this->specular, specular, sizeof(GLfloat)*4);
}
void OGLLight::initDefault()
{
    ambient[0]=ambient[1]=ambient[2]=ambient[3]=diffuse[0]=diffuse[1]=diffuse[2]=diffuse[3]=specular[0]=specular[1]=specular[2]=specular[3]=1;
    active = true;
}
void OGLLight::glSet(void)
{
    if (active)
    {
        glLightfv(id, GL_AMBIENT, ambient);
        glLightfv(id, GL_DIFFUSE, diffuse);
        glLightfv(id, GL_SPECULAR, specular);
        glEnable(id);
    } else
    {
        glDisable(id);
        return;
    }
}
OGLLight::OGLLight(bool active, GLfloat ambient[4], GLfloat diffuse[4], GLfloat specular[4])
{
    if (!init())
    {
        throw false;
    }
    setParameters(active, ambient, diffuse, specular);
}
OGLLight::OGLLight(void)
{
    if (!init())
    {
        throw false;
    }
    initDefault();
}
OGLLight::~OGLLight(void)
{
    destroy();
}
void OGLLight::destroy(void)
{
    OGLLight::usedLights[OGLLight::enumToNum(id)] = false;
}