#include <GL/glew.h>
#include "../graphics/Cube.h"
#include "../graphics/GLDebug.h"
#include "../graphics/Texture.h"
#include "../graphics/TextureManager.h"
#include <iostream>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/gl.h>

void Cube::initCubeList()
{
    Cube::list = glGenLists(1);
    if (Cube::list==0)
    {
        std::cerr<<"WARNING!!! - an error occurred creating the list for rendering Cube"<<std::endl;
    }
    glNewList(Cube::list, GL_COMPILE);
    //bottom face
        glNormal3f(0,0,-1);
        glTexCoord2d(0, 1);
        glVertex3f(0.5,0.5,-0.5);
        glTexCoord2d(0, 0);
        glVertex3f(0.5,-0.5,-0.5);
        glTexCoord2d(1, 0);
        glVertex3f(-0.5,-0.5,-0.5);
        glTexCoord2d(1, 1);
        glVertex3f(-0.5,0.5,-0.5);
    //top face
        glNormal3f(0,0,1);
        glTexCoord2d(0, 1);
        glVertex3f(0.5,0.5,0.5);
        glTexCoord2d(0, 0);
        glVertex3f(-0.5,0.5,0.5);
        glTexCoord2d(1, 0);
        glVertex3f(-0.5,-0.5,0.5);
        glTexCoord2d(1, 1);
        glVertex3f(0.5,-0.5,0.5);
    //side face 1
        glNormal3f(0,1,0);
        glTexCoord2d(0, 1);
        glVertex3f(0.5,0.5,0.5);
        glTexCoord2d(0, 0);
        glVertex3f(0.5,0.5,-0.5);
        glTexCoord2d(1, 0);
        glVertex3f(-0.5,0.5,-0.5);
        glTexCoord2d(1, 1);
        glVertex3f(-0.5,0.5,0.5);
    //side face 2
        glNormal3f(-1,0,0);
        glTexCoord2d(0, 1);
        glVertex3f(-0.5,0.5,0.5);
        glTexCoord2d(0, 0);
        glVertex3f(-0.5,0.5,-0.5);
        glTexCoord2d(1, 0);
        glVertex3f(-0.5,-0.5,-0.5);
        glTexCoord2d(1, 1);
        glVertex3f(-0.5,-0.5,0.5);
    //side face 3
        glNormal3f(0,-1,0);
        glTexCoord2d(0, 1);
        glVertex3f(-0.5,-0.5,0.5);
        glTexCoord2d(0, 0);
        glVertex3f(-0.5,-0.5,-0.5);
        glTexCoord2d(1, 0);
        glVertex3f(0.5,-0.5,-0.5);
        glTexCoord2d(1, 1);
        glVertex3f(0.5,-0.5,0.5);
    //side face 4
        glNormal3f(1,0,0);
        glTexCoord2d(0, 1);
        glVertex3f(0.5,-0.5,0.5);
        glTexCoord2d(0, 0);
        glVertex3f(0.5,-0.5,-0.5);
        glTexCoord2d(1, 0);
        glVertex3f(0.5,0.5,-0.5);
        glTexCoord2d(1, 1);
        glVertex3f(0.5,0.5,0.5);
    glEndList();
}
GLint Cube::list=0;

Cube::Cube()
{
    GLfloat color[4] = {1,1,1,1};
    *this = Cube(Vec3(), 1, color);
}

Cube::Cube(Vec3 pos, float side, GLfloat color[4])
{
    this->pos = pos;
    this->side = side;
    this->color[0] = color[0];
    this->color[1] = color[1];
    this->color[2] = color[2];
    this->color[3] = color[3];
}
Texture* Cube::texture = NULL;
void Cube::initializeTexture()
{
    Cube::texture = TextureManager::loadTexture("./resources/terrain_textures/terrain10partscube.png");
    if (Cube::texture==NULL)
    {
        std::cerr<<"texture of cube == NULL!!!"<<std::endl;

    } else
    {
        std::cout<<"texture of cube txt id = "<<Cube::texture->texId<<std::endl;
    }
}

void Cube::render()
{
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    glPushAttrib(GL_ENABLE_BIT);
    glPushAttrib(GL_COLOR_BUFFER_BIT);
    glPushAttrib(GL_COLOR);
    glPolygonMode(GL_FRONT, GL_FILL);
    glEnable(GL_POLYGON_OFFSET_LINE); // Avoid Stitching!
    glPolygonOffset(1,1);
    glPushAttrib(GL_CURRENT_BIT);
//    glDisable(GL_TEXTURE_2D);
   glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
   glBindTexture(GL_TEXTURE_2D, texture->texId);
    glMatrixMode(GL_MODELVIEW);
//    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
//        glLoadIdentity();
        glTranslated(pos.x,pos.y,pos.z);
        glScaled(side,side,side);
        glBegin(GL_QUADS);
            //glColor4fv(color);
            glCallList(Cube::list);
        glEnd();
//    glMatrixMode(GL_MODELVIEW);
//    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
//    glEnable(GL_TEXTURE_2D);
    glPopAttrib();
    glPopAttrib();
    glPopAttrib();
//    glDisable(GL_POLYGON_OFFSET_FILL);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
}
