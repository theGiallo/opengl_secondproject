#include <GL/glew.h>
#include "../graphics/BoundingSphere.h"
#include "../graphics/Frustum.h"
#include "../graphics/Camera.h"
#include <math.h>
#include <iostream>
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/gl.h>

Frustum::Frustum()
{
    pos = Vec3();
    look = Vec3();
    FOV = near = far = 0;
    ratio = 1;
    bounder = NULL;
}

void Frustum::calcBoundingSphere()
{
    if (bounder==NULL)
    {
        bounder = new BoundingSphere();
    }
    Vec3 sum = Vec3();
    for (int i=0 ; i<6 ; i++)
    {
        sum += boundPoints[i];
    }
    dynamic_cast<BoundingSphere*>(bounder)->center = sum / 6.0;
    dynamic_cast<BoundingSphere*>(bounder)->radius = ((sum / 6.0)-boundPoints[0]).getModule();
}

void Frustum::render()
{
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0);
    glEnable(GL_BLEND);
    glColor4f(1,1,1,0.5);
    glBegin(GL_QUADS);
    //BOTTOM
        glVertex3d(boundPoints[NBL].x,boundPoints[NBL].y,boundPoints[NBL].z);
        glVertex3d(boundPoints[FBL].x,boundPoints[FBL].y,boundPoints[FBL].z);
        glVertex3d(boundPoints[FBR].x,boundPoints[FBR].y,boundPoints[FBR].z);
        glVertex3d(boundPoints[NBR].x,boundPoints[NBR].y,boundPoints[NBR].z);
    //TOP
        glVertex3d(boundPoints[NTL].x,boundPoints[NTL].y,boundPoints[NTL].z);
        glVertex3d(boundPoints[NTR].x,boundPoints[NTR].y,boundPoints[NTR].z);
        glVertex3d(boundPoints[FTR].x,boundPoints[FTR].y,boundPoints[FTR].z);
        glVertex3d(boundPoints[FTL].x,boundPoints[FTL].y,boundPoints[FTL].z);
    //LEFT
        glVertex3d(boundPoints[NTL].x,boundPoints[NTL].y,boundPoints[NTL].z);
        glVertex3d(boundPoints[FTL].x,boundPoints[FTL].y,boundPoints[FTL].z);
        glVertex3d(boundPoints[FBL].x,boundPoints[FBL].y,boundPoints[FBL].z);
        glVertex3d(boundPoints[NBL].x,boundPoints[NBL].y,boundPoints[NBL].z);
    //RIGHT
        glVertex3d(boundPoints[FTR].x,boundPoints[FTR].y,boundPoints[FTR].z);
        glVertex3d(boundPoints[NTR].x,boundPoints[NTR].y,boundPoints[NTR].z);
        glVertex3d(boundPoints[NBR].x,boundPoints[NBR].y,boundPoints[NBR].z);
        glVertex3d(boundPoints[FBR].x,boundPoints[FBR].y,boundPoints[FBR].z);
    //NEAR
        glVertex3d(boundPoints[NTR].x,boundPoints[NTR].y,boundPoints[NTR].z);
        glVertex3d(boundPoints[NTL].x,boundPoints[NTL].y,boundPoints[NTL].z);
        glVertex3d(boundPoints[NBL].x,boundPoints[NBL].y,boundPoints[NBL].z);
        glVertex3d(boundPoints[NBR].x,boundPoints[NBR].y,boundPoints[NBR].z);
    //FAR
        glVertex3d(boundPoints[FTR].x,boundPoints[FTR].y,boundPoints[FTR].z);
        glVertex3d(boundPoints[FBR].x,boundPoints[FBR].y,boundPoints[FBR].z);
        glVertex3d(boundPoints[FBL].x,boundPoints[FBL].y,boundPoints[FBL].z);
        glVertex3d(boundPoints[FTL].x,boundPoints[FTL].y,boundPoints[FTL].z);
    glEnd();
//    glBegin(GL_LINE_LOOP);
//        glVertex3d(boundPoints[NBL].x,boundPoints[NBL].y,boundPoints[NBL].z);
//        glVertex3d(boundPoints[FBL].x,boundPoints[FBL].y,boundPoints[FBL].z);
//        glVertex3d(boundPoints[FBR].x,boundPoints[FBR].y,boundPoints[FBR].z);
//        glVertex3d(boundPoints[NBR].x,boundPoints[NBR].y,boundPoints[NBR].z);
//    glEnd();
//    glBegin(GL_LINE_LOOP);
//        glVertex3d(boundPoints[NTL].x,boundPoints[NTL].y,boundPoints[NTL].z);
//        glVertex3d(boundPoints[NTR].x,boundPoints[NTR].y,boundPoints[NTR].z);
//        glVertex3d(boundPoints[FTR].x,boundPoints[FTR].y,boundPoints[FTR].z);
//        glVertex3d(boundPoints[FTL].x,boundPoints[FTL].y,boundPoints[FTL].z);
//    glEnd();
//    glColor4f(1,0,0,1);
//    glBegin(GL_LINES);
//        glVertex3d(boundPoints[FTL].x,boundPoints[FTL].y,boundPoints[FTL].z);
//        glVertex3d(boundPoints[FBL].x,boundPoints[FBL].y,boundPoints[FBL].z);
//        glVertex3d(boundPoints[FTR].x,boundPoints[FTR].y,boundPoints[FTR].z);
//        glVertex3d(boundPoints[FBR].x,boundPoints[FBR].y,boundPoints[FBR].z);
//        glVertex3d(boundPoints[NTL].x,boundPoints[NTL].y,boundPoints[NTL].z);
//        glVertex3d(boundPoints[NBL].x,boundPoints[NBL].y,boundPoints[NBL].z);
//        glVertex3d(boundPoints[NTR].x,boundPoints[NTR].y,boundPoints[NTR].z);
//        glVertex3d(boundPoints[NBR].x,boundPoints[NBR].y,boundPoints[NBR].z);
//    glEnd();
    glPopAttrib();
    glPopAttrib();
}

void Frustum::updatePosLookUp(Vec3 pos, Vec3 look, Vec3 up, float ratio)
{
    if (this->pos==pos && this->look==look && this->up==up)
    {
        return;
    }
    if (ratio!=0)
    {
        this->ratio = ratio;
    }
    this->pos = pos;
    this->look = look;
    this->up = up;
    this->right = look^up;
    updatePoints();
    updatePlanes();
    calcBoundingSphere();
}

void Frustum::updatePosTargetUp(Vec3 pos, Vec3 target, Vec3 up, float ratio)
{
    Vec3 look = (target-pos).getNormalized();
    if (this->pos==pos && this->look==look && this->up==up)
    {
        return;
    }
    if (ratio!=0)
    {
        this->ratio = ratio;
    }
    this->pos = pos;
    this->look = look;
    this->up = up;
    this->right = look^up;
    updatePoints();
    updatePlanes();
    calcBoundingSphere();
}

void Frustum::updateFOV(float FOV)
{
    if (this->FOV == FOV)
    {
        return;
    }
    this->FOV=FOV;
    updatePoints();
    updatePlanes();
    calcBoundingSphere();
}

void Frustum::updateNF(float near, float far)
{
    if (this->near==near && this->far==far)
    {
        return;
    }
    if (this->near!=near)
    {
        this->near = near;
        updateNears();
    }
    if (this->far!=far)
    {
        this->far = far;
        updateFars();
    }
    updatePlanes();
    calcBoundingSphere();
}

void Frustum::updateNears()
{
    Vec3 nc = pos+look*near;
//    std::cout<<"nc: ";nc.print();
//    std::cout<<"pos: ";pos.print();
//    std::cout<<"look: ";look.print();
//    std::cout<<"far: "<<far<<std::endl;
    float hnw = tan(DEG2RAD(FOV/2.0f))*near;
    float hnh = hnw/ratio;
    boundPoints[NTL] = nc-right*hnw+up*hnh;
    boundPoints[NTR] = nc+right*hnw+up*hnh;
    boundPoints[NBL] = nc-right*hnw-up*hnh;
    boundPoints[NBR] = nc+right*hnw-up*hnh;
}

void Frustum::updateFars()
{
    Vec3 fc = pos+look*far;
//    std::cout<<"fc: ";fc.print();
//    std::cout<<"look: ";fc.print();
//    std::cout<<"far: "<<far<<std::endl;
    float hfw = tan(DEG2RAD(FOV/2.0f))*far;
    float hfh = hfw/ratio;
    boundPoints[FTL] = fc-right*hfw+up*hfh;
    boundPoints[FTR] = fc+right*hfw+up*hfh;
    boundPoints[FBL] = fc-right*hfw-up*hfh;
    boundPoints[FBR] = fc+right*hfw-up*hfh;
}

void Frustum::updateRatio(float ratio)
{
    if (this->ratio==ratio)
    {
        return;
    }
    this->ratio = ratio;
    updatePoints();
    updatePlanes();
    calcBoundingSphere();
}

void Frustum::updatePoints()
{
    updateNears();
    updateFars();
//    std::cout<<"---- updatePoints: ----"<<std::endl;
//    std::cout<<"FOV = "<<FOV<<std::endl;
//    for (int i=0 ; i<6 ; i++)
//    {
//        boundPoints[i].print();
//    }
}

void Frustum::updatePlanes()
{
//    float hnw = tan(DEG2RAD(FOV/2.0))*near;
//    float hnh = hnw/ratio;
//    float hfw = tan(DEG2RAD(FOV/2.0))*far;
//    float hfh = hfw/ratio;
    planes[NEAR_PL] = Plane(boundPoints[NBR],boundPoints[NBL],boundPoints[NTL]);
    planes[FAR_PL] = Plane(boundPoints[FBL],boundPoints[FBR],boundPoints[FTR]);
    planes[LEFT_PL] = Plane(boundPoints[NBL], boundPoints[FBL], boundPoints[FTL]);
    planes[RIGHT_PL] = Plane(boundPoints[FBR], boundPoints[NBR], boundPoints[NTR]);
    planes[TOP_PL] = Plane(boundPoints[NTR], boundPoints[NTL], boundPoints[FTL]);
    planes[BOTTOM_PL] = Plane(boundPoints[FBL], boundPoints[NBL], boundPoints[NBR]);
}

bool Frustum::isInside(Vec3 point)
{
    for (int i=0 ; i<6 ; i++ )
    {
        if (planes[i].dist(point)>0)
        {
//            std::cout<<"------"<<std::endl;
//            point.print();
//            std::cout<<"out"<<std::endl;
            return false;
        }
//        else
//        {
//            point.print();
//            std::cout<<"dist: "<<planes[i].dist(point)<<std::endl;
//        }
    }
    return true;
}

int Frustum::collideSphere(Vec3 center, float radius)
{
    int n=0,m=0;
    float d;
    for (int i=0 ; i<6 ; i++ )
    {
        d=planes[i].dist(center);
        if (d>radius)
        {
            return COLLISION_OUT;
        }
        if (d<=0)
        {
            m++;
        }
        if (ABS(d)<radius)
        {
            n++;
        }
    }
    if (n==0)
    {
        if (m==6)
        {
            return COLLISION_IN;
        }
        return COLLISION_OUT;
    }
    if (n==6)
    {
        return COLLISION_CONTAINS;
    }
    return COLLISION_COLLIDE;
}

int Frustum::collideBox(Vec3 center, float side)
{
//    std::cout<<"side: "<<side<<std::endl;
    int in=0,out=0;
    
    float hs=side/2.0f;
    int allInC=0;
    for (int i=0 ; i<6 ; i++ )
    {
        int allIn=1;
        int inC = 8;
        if (planes[i].dist(center+Vec3(hs,hs,hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(-hs,-hs,-hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(hs,hs,-hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(-hs,-hs,hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(-hs,hs,-hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(hs,-hs,hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(-hs,hs,hs))>0)
        {
            inC--;
            allIn=0;
        }
        if (planes[i].dist(center+Vec3(hs,-hs,-hs))>0)
        {
            inC--;
            allIn=0;
        }
        
        if (inC==0)
        {
            return COLLISION_OUT;
        }
        allInC+=allIn;
    }
    if (allInC==6)
    {
        return COLLISION_IN;
    }
    return COLLISION_COLLIDE; //equal to COLLISION_CONTAINS in this case
    
    /**
     * old method: lack of one case
     */
    
    if ( isInside(center+Vec3(hs,hs,hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(hs,hs,-hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(hs,-hs,hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(hs,-hs,-hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(-hs,hs,hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(-hs,hs,-hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(-hs,-hs,hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if ( isInside(center+Vec3(-hs,-hs,-hs)) )
    {
        in++;
    } else
    {
        out++;
    }
    if (in!=0 && out!=0)
    {
        return COLLISION_COLLIDE;
    }
    if (in==8)
    {
        return COLLISION_IN;
    }
    float diag = SQRT2*hs;
    if (collideSphere(center, diag)==COLLISION_CONTAINS)
    {
        return COLLISION_CONTAINS;
    }
//    std::cout<<"in: "<<in<<" out:"<<out<<std::endl;
    return COLLISION_OUT;
}
