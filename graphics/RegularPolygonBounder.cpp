#include "../graphics/RegularPolygonBounder.h"
#include "../graphics/GJK2D.h"
#include "../graphics/CollisionSolver2D.h"
#include "../graphics/Camera2D.h"

/// TODO dovrebbe esserci il problema del diamante... anche se compila... 8)

RegularPolygonBounder::RegularPolygonBounder(unsigned int n)
{
    polygon = RegularPolygon2D(n, true);
    vArray = new GLfloat[n*2];
    setScale(UNITARY_SCALE);
    memcpy(vArray,polygon.varrays[n-3], sizeof(GLfloat)*n*2);
}
	
void RegularPolygonBounder::setPos(const Vec2& pos)
{
    int n = polygon.getNSides();
    //OGLTools::translateVArray(vArray, polygon.getNSides(), -this->pos);
    memcpy(vArray,polygon.varrays[n-3], sizeof(GLfloat)*n*2);
    PositionableObject2D::setPos(pos);
    OGLTools::rotateVArray(vArray, n, this->rot);
    OGLTools::scaleVArray(vArray, n, this->scale);
    OGLTools::translateVArray(vArray, n, this->pos);
}
void RegularPolygonBounder::setUp(const Vec2& up)
{
    int n = polygon.getNSides();
    //OGLTools::rotateVArray(vArray, polygon.getNSides(), -this->rot);
    memcpy(vArray,polygon.varrays[n-3], sizeof(GLfloat)*n*2);
    PositionableObject2D::setUp(up);
    OGLTools::rotateVArray(vArray, n, this->rot);
    OGLTools::scaleVArray(vArray, n, this->scale);
    OGLTools::translateVArray(vArray, n, this->pos);
}
void RegularPolygonBounder::setScale(const Vec2& scale)
{
    int n = polygon.getNSides();
    //OGLTools::rotateVArray(vArray, polygon.getNSides(), -this->rot);
    memcpy(vArray,polygon.varrays[n-3], sizeof(GLfloat)*n*2);
    PositionableObject2D::setScale(scale);
    OGLTools::rotateVArray(vArray, n, this->rot);
    OGLTools::scaleVArray(vArray, n, this->scale);
    OGLTools::translateVArray(vArray, n, this->pos);
}
void RegularPolygonBounder::setRot(float rot)
{
    int n = polygon.getNSides();
    //OGLTools::rotateVArray(vArray, polygon.getNSides(), -this->rot);
    memcpy(vArray,polygon.varrays[n-3], sizeof(GLfloat)*n*2);
    PositionableObject2D::setRot(rot);
    OGLTools::rotateVArray(vArray, n, this->rot);
    OGLTools::scaleVArray(vArray, n, this->scale);
    OGLTools::translateVArray(vArray, n, this->pos);
}
const GLfloat* RegularPolygonBounder::getVArray(void) const
{
    return vArray;
}
const RegularPolygon2D& RegularPolygonBounder::getPolygon(void) const
{
    return polygon;
}

int RegularPolygonBounder::collide(RegularPolygonBounder &rpb)
{
    float dist;
    return CollisionSolver2D::collide(*this, rpb, dist);
}

int RegularPolygonBounder::collide(const Camera2D &camera)
{
    float dist;
    return CollisionSolver2D::collide(*this, camera.getBoundingRectangle(), dist);
}
void RegularPolygonBounder::render(void)
{
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_ENABLE_BIT);
    glPushAttrib(GL_POLYGON_BIT);
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_BACK, GL_NONE);
		glEnableClientState(GL_VERTEX_ARRAY);
		glInterleavedArrays( GL_V2F,2*sizeof(GLfloat),&vArray[0]);
                glColor4fv(&color[0]);
		glDrawArrays(GL_POLYGON, 0, polygon.getNSides());
		glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
    glPopAttrib();
    glPopAttrib();
    glPopAttrib();
}
float RegularPolygonBounder::rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)
{
	throw "not yet implemented";
	return 0;
}
