/* 
 * File:   Cube.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 18 marzo 2011, 21.08
 */

#ifndef CUBE_H
#define	CUBE_H

#include "../maths/Vec3.h"
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include "../graphics/Texture.h"

class Cube
{
public:
    Vec3 pos;
    float side;
    GLfloat color[4];
    
    Cube();
    Cube(Vec3 pos, float side, GLfloat color[4]);
    static void initializeTexture(void);

    void render(void);

    static void initCubeList();
    static Texture* texture;
private:
    static GLint list;
};

#endif	/* CUBE_H */

