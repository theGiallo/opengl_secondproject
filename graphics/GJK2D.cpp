#include "GJK2D.h"
#include "../maths/Vec3.h"
#include "../maths/Vec2.h"
#include "../tools/MergeSort.h"
#include <cfloat>
#include <utility>
#include <cassert>

const float GJK2D::epsilon = 8e-6;

Vec2 GJK2D::supportMap(Vec2 origin, Vec2 dir, const GLfloat* vArray, unsigned int nVert, int* out_idV/*=NULL*/)
{
	float minval = FLT_MAX, tmpval;
	Vec2 s, tmps;
        int idV;
	for (unsigned int i=0 ; i<nVert ; i++)
	{
		tmps.x = vArray[i*2];
		tmps.y = vArray[i*2+1];
		if ((tmpval = dir*(tmps-origin))<minval)
		{
			s = tmps;
			minval = tmpval;
                        idV = i;
#ifdef DEBUG_SUPPORTMAP
                        std::cout<<"found one"<<std::endl;
#endif
		} else
                {
#ifdef DEBUG_SUPPORTMAP
                    std::cout<<"tmps:";tmps.print();
                    std::cout<<"tmpval:"<<tmpval<<std::endl;
                    std::cout<<"min:"<<minval<<std::endl;
#endif
                }
	}
        if (out_idV!=NULL)
        {
            *out_idV = idV;
        }
	return s;
}
float GJK2D::distFromSegment(const Vec2& v0X, const Vec2& v0v1, const Vec2& v0, const Vec2& X, Vec2* out_P_on_seg)
{
    float t = (v0X*v0v1)/(v0v1.x*v0v1.x+v0v1.y*v0v1.y);
    *out_P_on_seg = v0+v0v1*t;
    return (X-*out_P_on_seg).getModule();
}
void GJK2D::calcScore(Vec2 v0X, Vec2 v0v1, Vec2 v0v2, int* score1, int* score2)
{
    float xv0v2 = v0X*v0v2;
    if (xv0v2<0)
    {
        (*score1)++;
    } else
    {
        float xv0v1 = v0X*v0v1;
        if (xv0v1<0)
        {
            (*score2)++;
        } else
        {
            if (xv0v1<xv0v2)
            {
                (*score1)++;
            } else
            if (xv0v1>xv0v2)
            {
                (*score2)++;
            } else
            {
                (*score1)++;
                (*score2)++;
            }
        }
    }
}
float GJK2D::trianglePointDistance(Vec2 tr[3], Vec2 p_start, Vec2* p_on_tr, unsigned int short* id0, unsigned int short* id1)
{
	Vec2 AC = tr[2]-tr[0];
	Vec2 AB = tr[1]-tr[0];
	Vec2 BC = tr[2]-tr[1];

	Vec2 AX = p_start-tr[0];
	if (AX*AB<=0 && AX*AC<=0)
	{
		*p_on_tr = tr[0];
                *id0 = *id1 = 0;
		return AX.getModule();
	}
	Vec2 BX = p_start-tr[1];
	if (BX*(-AB)<=0 && BX*BC<=0)
	{
		*p_on_tr = tr[1];
                *id0 = *id1 = 1;
		return BX.getModule();
	}
	Vec2 CX = p_start-tr[2];
	if (CX*(-AC)<=0 && CX*(-BC)<=0)
	{
		*p_on_tr = tr[2];
                *id0 = *id1 = 2;
		return CX.getModule();
	}
	//AB
	Vec3 tmpv3 = (BC^(-AB))^Vec3(-AB.x,-AB.y,0);
	Vec2 tmpv2 = Vec2(tmpv3.x, tmpv3.y);
	if (tmpv2*BX>=0 && AX*AB>=0 && BX*(-AB)>=0)
        {
            float d = distFromSegment(AX, AB, tr[0], p_start, p_on_tr);
            /*
            float t = (AX*AB)/(AB.x*AB.x+AB.y*AB.y);
            *p_on_tr = tr[0]+AB*t;
            */
            *id0 = 0;
            *id1 = 1;
	//  return (p_start-*p_on_tr).getModule();
            return d;
	}
	//BC
	tmpv3 = ((-AB)^BC)^Vec3(BC.x,BC.y,0);
	tmpv2 = Vec2(tmpv3.x, tmpv3.y);
	if (tmpv2*BX>=0 && CX*(-BC)>=0 && BX*BC>=0)
	{
            float d = distFromSegment(BX, BC, tr[1], p_start, p_on_tr);
            /*
            float t = (BX*BC)/(BC.x*BC.x+BC.y*BC.y);
            *p_on_tr = tr[1]+BC*t;
            */
            *id0 = 1;
            *id1 = 2;
            //return (p_start-*p_on_tr).getModule();
            return d;
	}
	//AC
	tmpv3 = (AB^AC)^Vec3(AC.x,AC.y,0);
	tmpv2 = Vec2(tmpv3.x, tmpv3.y);
	if (tmpv2*AX>=0 && AX*AC>=0 && CX*(-AC)>=0)
	{
            float d = distFromSegment(AX, AC, tr[0], p_start, p_on_tr);
            /*
            float t = (AX*AC)/(AC.x*AC.x+AC.y*AC.y);
            *p_on_tr = tr[0]+AC*t;
            */
            *id0 = 2;
            *id1 = 0;
            //return (p_start-*p_on_tr).getModule();
            return d;
	}
        
        
        
        //Inside
        int scores[3] = {0,0,0};
        calcScore(AX,AB,AC,&scores[1], &scores[2]);
        calcScore(BX,-AB,BC,&scores[0], &scores[2]);
        calcScore(CX,-AC,-BC,&scores[0], &scores[1]);
        float d;
        if (scores[0]>=scores[1])
        {
            if (scores[1]>=scores[2]) //AB
            {
                d = distFromSegment(AX, AB, tr[0], p_start, p_on_tr);
                *id0 = 0;
                *id1 = 1;
            } else //AC
            {
                d = distFromSegment(AX, AC, tr[0], p_start, p_on_tr);
                *id0 = 0;
                *id1 = 2;
            }
        } else
        {
            if (scores[2]>=scores[1]) //BC
            {
                d = distFromSegment(BX, BC, tr[1], p_start, p_on_tr);
                *id0 = 1;
                *id1 = 2;
            } else
            if (scores[0]>=scores[2]) //AB
            {
                d = distFromSegment(AX, AB, tr[0], p_start, p_on_tr);
                *id0 = 0;
                *id1 = 1;
            } else //BC
            {
                d = distFromSegment(BX, BC, tr[1], p_start, p_on_tr);
                *id0 = 1;
                *id1 = 2;
            }
        }
        return -d;
}
float GJK2D::distance(const GLfloat* vArray, unsigned int nVert, Vec2 point, Vec2* out_min_dist_P/*=NULL*/, int* out_edge_ids/*=NULL*/)
{
#ifdef DEBUG_GJK2D
    std::cout<<"nVert:"<<nVert<<std::endl;
    for (int i=0 ; i<nVert ; i++)
    {
        std::cout<<vArray[i*2]<<", "<<vArray[i*2+1]<<std::endl;
    }
    std::cout<<"--------------------------------------------------------------------------------"<<std::endl<<std::endl;
#endif
    /*
            1. Initialize the simplex set Q with up to d+1 
            points from C (in d dimensions)
    */
    Vec2 tr[3];
    int ids[3];
    int idV;
    for (unsigned int j=0, i=0, jump=nVert/3 ; j<nVert && i<3 ; j+=jump, i++)
    {
        tr[i].x = vArray[j*2+0];
        tr[i].y = vArray[j*2+1];
        ids[i] = j;
    }
    Vec2 P, V, q0_V, q0_q1, P_point;
    unsigned short int id0, id1;
    float dist;
    while (true)
    {
        /*
                2. Compute point P of minimum norm in CH(Q)
        */
#ifdef DEBUG_GJK2D
        std::cout<<"tr:"<<tr[0].x<<","<<tr[0].y<<" "<<tr[1].x<<","<<tr[1].y<<" "<<tr[2].x<<","<<tr[2].y<<std::endl;
#endif
        dist = GJK2D::trianglePointDistance(tr, point, &P, &id0, &id1);
#ifdef DEBUG_GJK2D
        std::cout<<"dist:"<<dist<<" id0:"<<id0<<" id1:"<<id1<<" P:";P.print();
        std::cout<<"ids[id0]:"<<ids[id0]<<" ids[id1]:"<<ids[id1]<<std::endl;
        int tmp;
//                std::cin>>tmp;
#endif
        /**************************************************************/
        /***********  TEMPORARY BEFORE IMPLEMENTING EPA  **************/
        /**************************************************************/
        if (dist<0)
        {
            if (out_min_dist_P!=NULL)
            {
                *out_min_dist_P = P;
            }
            if (out_edge_ids!=NULL)
            {
                out_edge_ids[0] = ids[id0];
                out_edge_ids[1] = ids[id1];
            }
            return dist;
        }
        /*
                3. If P is the origin, exit; return 0
        */
//        if (P==point) // this sometimes fails because of machine precision error
        if (P.isEqual(point,epsilon))
        {
            return 0;
        }
        /*
                4. Reduce Q to the smallest subset Q' of Q, such 
                that P in CH(Q')
        */
        /*
                5. Let V=SC(-P) be a supporting point in direction -P
        */
        P_point = point-P;
        V = GJK2D::supportMap(P, -P_point, vArray, nVert, &idV);
#ifdef DEBUG_GJK2D
        std::cout<<"V:";V.print();
        std::cout<<"idV:"<<idV<<std::endl;
#endif
        /*
                6. If V no more extreme in direction -P than P
                itself, exit; return ||P||
        */
        /*q0_V = (V-tr[id0]).getNormalized();
        q0_q1 = (tr[id1]-tr[id0]);*/
        if (idV==ids[id0] || idV==ids[id1])
        {
            if (out_min_dist_P!=NULL)
            {
                *out_min_dist_P = P;
            }
            if (out_edge_ids!=NULL)
            {
                out_edge_ids[0] = ids[id0];
                out_edge_ids[1] = ids[id1];
            }
#ifdef DEBUG_GJK2D
            std::cout<<"return P"<<std::endl;
#endif
            return P_point.getModule();
        }
        /*
                7. Add V to Q. Go to step 2
        */
        switch(id0+id1)
        {
        case 0: // 0 0
        case 1: // 0 1
            tr[2] = V;
            ids[2] = idV;
            break;
        case 2:
            if (id0==id1) // 1 1
            {
                tr[0] = V;
                ids[0] = idV;
            } else // 0 2
            {
                tr[1] = V;
                ids[1] = idV;
            }
            break;
        case 3: // 1 2
        case 4: // 2 2
            tr[0] = V;
            ids[0] = idV;
            break;
        default:
            break;
        }
    }
    assert(false);
    return 0.0f;
}

float GJK2D::distance(const GLfloat* vArray1, unsigned int nVert1, const GLfloat* vArray2, unsigned int nVert2)
{
    /*
      <C=A-B>
    */
    unsigned int nVertC = nVert1*nVert2;
    GLfloat* C = new GLfloat[nVertC*2];
    unsigned int k=0;
    for (unsigned int i=0 ; i<nVert1 ; i++)
    {
        for (unsigned int j=0 ; j<nVert2 ; j++)
        {
            C[k++] = vArray1[i*2]-vArray2[j*2];
            C[k++] = vArray1[i*2+1]-vArray2[j*2+1];
        }
    }
    static MergeSort<GLfloat_Vec2> ms(compareFunction);
    ms.mergeSort(nVertC, (GLfloat_Vec2*)C);
    /*
      return distance(C,nC,O)
    */
    ConvexHull CH;
    getConvexHull(C, nVertC, CH);
    GLfloat* CCH = new GLfloat[CH.hull_pids.getCount()*2];
    int i=0, round=0;
    for (DoubleLinkedCircularList<unsigned int>::iterator it = CH.hull_pids.head() ; ; it++)
    {
        if (it==CH.hull_pids.head())
        {
            if (round==1)
            {
                break;
            }
            round++;
        }
        try
        {
        CCH[i] = CH.points[*it].x;
        i++;
        CCH[i] = CH.points[*it].y;
        i++;
        } catch (bool)
        {
                std::cerr<<"Meeeeerda!!!"<<std::endl;
                throw false;
        }
    }
#ifdef DEBUG_GJK2D
    for (unsigned int i=0 ; i<nVertC ; i++)
    {
        std::cout<<C[i*2]<<","<<C[i*2+1]<<" "<<std::endl;
    }
    std::cout<<std::endl;
    for (unsigned int i=0 ; i<CH.hull_pids.getCount() ; i++)
    {
        std::cout<<CCH[i*2]<<","<<CCH[i*2+1]<<" "<<std::endl;
    }
    std::cout<<std::endl;
#endif
    delete [] C;
    float dist =  GJK2D::distance(CCH, CH.hull_pids.getCount(), Vec2(0,0));
    delete [] CCH;
    return dist;
}

ConvexHull& GJK2D::getConvexHull(GLfloat* vArray, unsigned int nVert, ConvexHull& retvalue)
{
    static MergeSort<GLfloat_Vec2> ms(compareFunction);
    if (nVert<=2)
    {	
        /*GLfloat* res = new GLfloat[nVert*2];
        ms.copyArray(nVert, (GLfloat_Vec2*)vArray, nVert, (GLfloat_Vec2*)res);
        ConvexHull ch =  ConvexHull((GLfloat_Vec2*)res,nVert);*/
        retvalue.hull_pids.clear();
        retvalue.points = (GLfloat_Vec2*)vArray;
        retvalue.points_count = nVert;
        DoubleLinkedCircularList<unsigned int>::iterator it = retvalue.hull_pids.head();
        for (unsigned int i=0 ; i<nVert ; i++)
        {
            it=it.insertPrev(i);
        }
        return retvalue;
    }
    //int h = floor(n/2);
    unsigned int h = (nVert+1)/2;
    //m = n-h;
    unsigned int m = nVert-h;
    //point LH[], RH[]; //left and right hulls
    //LH = findHullDC(h, S[1..h]);
    ConvexHull LH, RH;
    getConvexHull(vArray, h, LH);
    //RH = findHullDC(m, S[h+1..n]);
    getConvexHull(&vArray[h*2], m, RH);
    //return mergeHulls(LH.size(), RH.size(), LH, HR);
    return mergeHulls(LH, RH, retvalue);
}
ConvexHull& GJK2D::mergeHulls(ConvexHull& left, ConvexHull& right, ConvexHull& retvalue)
{
    retvalue.hull_pids.clear();
    retvalue.points = left.points;
    retvalue.points_count = left.points_count+right.points_count;

	DoubleLinkedCircularList<unsigned int>::iterator tmp_it;
        DoubleLinkedCircularList<unsigned int>::iterator it_pul, it_pur, it_pll, it_plr;
	Vec2 v0, v1, v2, v3; /// v1 is p*l, v2 is p*r
	it_pul = left.hull_pids.const_head();
	it_pll = left.hull_pids.const_head();
	it_pur = right.hull_pids.const_head();
	it_plr = right.hull_pids.const_head();
	
#ifdef DEBUG_CONVEXHULL
	std::cout<<"left count:"<<left.hull_pids.getCount()<<" right count:"<<right.hull_pids.getCount()<<std::endl;
#endif
	if (GJK2D::useBaseCase)
	{
		int case12_21 = 0;
		if (left.hull_pids.getCount()==2 && right.hull_pids.getCount()==1)
		{
			if (*it_pul>*(it_pul+1))
			{
				++it_pul;
			}
			v0.x = left.points[*it_pul].x;
			v0.y = left.points[*it_pul].y;
			v1.x = right.points[*it_pur].x;
			v1.y = right.points[*it_pur].y;
			v2.x = left.points[(*it_pul)+1].x;
			v2.y = left.points[(*it_pul)+1].y;
			case12_21 = 2;
		} else if (left.hull_pids.getCount()==1 && right.hull_pids.getCount()==2)
		{
			if (*it_pur>*(it_pur+1))
			{
				++it_pur;
			}
			v0.x = right.points[*it_pur].x;
			v0.y = right.points[*it_pur].y;
			v1.x = left.points[*it_pul].x;
			v1.y = left.points[*it_pul].y;
			v2.x = right.points[(*it_pur)+1].x;
			v2.y = right.points[(*it_pur)+1].y;
			case12_21 = 1;
		}
		Vec3 vn = (v0-v1).getNormalized()^((v2-v1).getNormalized());
#ifdef DEBUG_CONVEXHULL
		(v0-v1).print();
		(v2-v1).print();
		vn.print();
#endif
		tmp_it = retvalue.hull_pids.head();
		if (case12_21!=0)
		{
			tmp_it = retvalue.hull_pids.head();
			/**
			 * By default, counterclockwise polygons are taken to be front-facing. (http://www.opengl.org/sdk/docs/man/xhtml/glFrontFace.xml)
			 **/
			if (vn.z<0)
			{
				//insert 0 1 2
				if (case12_21==2)
				{
					tmp_it.insertPrev(*it_pul);
					tmp_it.insertPrev(left.points_count+(*it_pur));
					tmp_it.insertPrev(*(it_pul+1));
				} else
				{
					tmp_it.insertPrev(left.points_count+(*it_pur));
					tmp_it.insertPrev(*it_pul);
					tmp_it.insertPrev(left.points_count+(*(it_pur+1)));
				}
			} else
			if (vn.z>0)
			{
				//insert 2 1 0
				if (case12_21==2)
				{
					tmp_it.insertPrev(*(it_pul+1));
					tmp_it.insertPrev(left.points_count+(*it_pur));
					tmp_it.insertPrev(*it_pul);
				} else
				{
					tmp_it.insertPrev(left.points_count+(*(it_pur+1)));
					tmp_it.insertPrev(*it_pul);
					tmp_it.insertPrev(left.points_count+(*it_pur));
				}
			} else // return 2 or 1 point
			{
				if (case12_21==2)
				{
					if (v0!=v1)
					{
						if (*it_pul<left.points_count+(*it_pur))
						{
							tmp_it.insertPrev(*it_pul);
							tmp_it.insertPrev(left.points_count+(*it_pur));
						} else
						{
							tmp_it.insertPrev(left.points_count+(*it_pur));
							tmp_it.insertPrev(*it_pul);
						}
					} else
					{
						if (v0!=v2)
						{
							if (v0.y<=v2.y)
							{
								tmp_it.insertPrev(*it_pul);
								tmp_it.insertPrev(*(it_pul+1));
							} else
							{
								tmp_it.insertPrev(*(it_pul+1));
								tmp_it.insertPrev(*it_pul);
							}
						} else
						{
							tmp_it.insertPrev(*it_pul);
						}
					}
				} else
				{
					if (v1!=v2)
					{
						if (*it_pul<left.points_count+(*it_pur))
						{	
							tmp_it.insertPrev(*it_pul);
							tmp_it.insertPrev(left.points_count+(*(it_pur+1)));
						} else
						{
							tmp_it.insertPrev(left.points_count+(*(it_pur+1)));
							tmp_it.insertPrev(*it_pul);
						}
					} else
					{
						if (v1!=v0)
						{	
							if (v1.y<=v0.y)
							{
								tmp_it.insertPrev(*it_pul);
								tmp_it.insertPrev(left.points_count+(*it_pur));
							} else
							{
								tmp_it.insertPrev(left.points_count+(*it_pur));
								tmp_it.insertPrev(*it_pul);
							}
						} else
						{
							tmp_it.insertPrev(*it_pul);
						}
					}
				}
			}
			return retvalue;
		}
	} // if GJK2D::baseCase

	/**
	 * choose the rightmost and the leftmost points of the left and right hulls
	 **/
	while (left.points[*it_pul] != left.points[left.points_count-1])
	{
		++it_pul;
	}
	while (right.points[*it_pur] != right.points[0])
	{
		++it_pur;
	}
	it_pll = it_pul;
	it_plr = it_pur;
	
	v0.x = left.points[*(it_pul+1)].x;
	v0.y = left.points[*(it_pul+1)].y;
	v1.x = left.points[*it_pul].x;
	v1.y = left.points[*it_pul].y;
	v2.x = right.points[*it_pur].x;
	v2.y = right.points[*it_pur].y;
	v3.x = right.points[*(it_pur-1)].x;
	v3.y = right.points[*(it_pur-1)].y;
	unsigned int old_l, old_r;
	old_l = *it_pul;
	old_r = *it_pur;
	while(true)
	{
		if (/*!uppertangent l*/((v0-v1).getNormalized()^((v2-v1).getNormalized())).z<=0) // sin of internal angle must be less then 180�
		{
			++it_pul;
			if (old_l<*it_pul || left.hull_pids.getCount()==1)
			{
				--it_pul;
			} else
			{
				old_l = *it_pul;
				v0.x = left.points[*(it_pul+1)].x;
				v0.y = left.points[*(it_pul+1)].y;
				v1.x = left.points[*it_pul].x;
				v1.y = left.points[*it_pul].y;
				continue;
			}
		}
		if (/*!uppertangent r*/((v1-v2).getNormalized()^((v3-v2).getNormalized())).z<=0) // sin of internal angle must be less then 180�
		{
			--it_pur;
			if (old_r>*it_pur || right.hull_pids.getCount()==1)
			{
				++it_pur;
			} else
			{
				old_r = *it_pur;
				v2.x = right.points[*it_pur].x;
				v2.y = right.points[*it_pur].y;
				v3.x = right.points[*(it_pur-1)].x;
				v3.y = right.points[*(it_pur-1)].y;
				continue;
			}
		}
		break;
	}
	v0.x = left.points[*(it_pll-1)].x;
	v0.y = left.points[*(it_pll-1)].y;
	v1.x = left.points[*it_pll].x;
	v1.y = left.points[*it_pll].y;
	v2.x = right.points[*it_plr].x;
	v2.y = right.points[*it_plr].y;
	v3.x = right.points[*(it_plr+1)].x;
	v3.y = right.points[*(it_plr+1)].y;
	old_l = *it_pll;
	old_r = *it_plr;
	while(true)
	{
		if (/*!lowertangent l*/((v0-v1).getNormalized()^((v2-v1).getNormalized())).z>=0) // sin of external angle must be less then 180�
		{
			--it_pll;
			if (old_l<*it_pll || left.hull_pids.getCount()==1)
			{
				++it_pll;
			} else
			{
				old_l = *it_pll;
				v0.x = left.points[*(it_pll-1)].x;
				v0.y = left.points[*(it_pll-1)].y;
				v1.x = left.points[*it_pll].x;
				v1.y = left.points[*it_pll].y;
				continue;
			}
		}
		if (/*!lowertangent r*/((v1-v2).getNormalized()^((v3-v2).getNormalized())).z>=0) // sin of external angle must be less then 180�
		{
			++it_plr;
			if (old_r>*it_plr || right.hull_pids.getCount()==1)
			{
				--it_plr;
			} else
			{
				old_r = *it_plr;
				v2.x = right.points[*it_plr].x;
				v2.y = right.points[*it_plr].y;
				v3.x = right.points[*(it_plr+1)].x;
				v3.y = right.points[*(it_plr+1)].y;
				continue;
			}
		}
		break;
	}
	/**
	* By default, counterclockwise polygons are taken to be front-facing. (http://www.opengl.org/sdk/docs/man/xhtml/glFrontFace.xml)
	**/
	tmp_it = it_pll+1;
	while (tmp_it!=it_pul)
	{
		tmp_it.deleteAndNext();
	}
	tmp_it = it_pur+1;
	while (tmp_it!=it_plr)
	{
		tmp_it.deleteAndNext();
	}
	
	/**
	 * join lists
	 **/
	tmp_it = retvalue.hull_pids.head();
	for (bool end=false ; !end ; it_pul.deleteAndNext() )
	{
		if (it_pul==it_pll)
		{
			end = true;
		}
		tmp_it.insertPrev(*it_pul);
	}
	for (bool end=false ; !end ; it_plr.deleteAndNext() )
	{
		if (it_plr==it_pur)
		{
			end = true;
		}
		tmp_it.insertPrev(left.points_count+(*it_plr));
	}

	return retvalue;
}
int compareFunction(GLfloat_Vec2 el1, GLfloat_Vec2 el2)
{
	if (el1.x<el2.x)
	{
		return -1;
	}
	if (el1.x>el2.x)
	{
		return 1;
	}
	if (el1.y<el2.y)
	{
		return -1;
	}
	if (el1.y>el2.y)
	{
		return 1;
	}
	return 0;
}


ConvexHull::ConvexHull()
{
	this->points = NULL;
	this->points_count = 0;
}
ConvexHull::ConvexHull(const ConvexHull& ch)
{
    this->points = ch.points;
    this->points_count = ch.points_count;
    this->hull_pids = DoubleLinkedCircularList<unsigned int>(ch.hull_pids);
}
ConvexHull::ConvexHull(GLfloat_Vec2* points, unsigned int points_count)
{
	this->points = points;
	this->points_count = points_count;
}


bool GLfloat_Vec2::operator == (GLfloat_Vec2 v)
{
	return x==v.x && y==v.y;
}
bool GLfloat_Vec2::operator != (GLfloat_Vec2 v)
{
	return !(*this==v);
}
GLfloat_Vec2::GLfloat_Vec2()
{
	x=y=0;
}
GLfloat_Vec2::GLfloat_Vec2(GLfloat x, GLfloat y)
{
	this->x = x;
	this->y = y;
}

bool GJK2D::useBaseCase=false;
