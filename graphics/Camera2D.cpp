#include "Camera2D.h"

#include "BoundingRectangle.h"

#define DEFAULT_POS Vec2()
#define DEFAULT_VERTICAL Vec2(0.0f,1.0f)
#define DEFAULT_WIDTH 100
#define DEFAULT_HEIGHT 100


Camera2D::~Camera2D(void)
{
    delete &br;
}

void Camera2D::updateRotMx()
{
    buildMatrixRotFromUpVector(vertical, rot_mx);
}
void Camera2D::setGLCamera(void)
{
    Vec2 pos = getPos();
    float width = getActualWidth();
    float height = getActualHeight();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-width/2.0f,width/2.0f,-height/2.0f,+height/2.0f,-1,1);
	glMultMatrixf(rot_mx);
	glTranslatef(-pos.x,-pos.y,0);
}
Camera2D::Camera2D(void) : br(*new BoundingRectangle(DEFAULT_POS, DEFAULT_WIDTH, DEFAULT_HEIGHT))
{
    vertical = DEFAULT_VERTICAL;
    updateRotMx();
    zoom = 1.0f;
}
Camera2D::Camera2D(Vec2 pos) : br(*new BoundingRectangle(pos, DEFAULT_WIDTH, DEFAULT_HEIGHT))
{
    this->vertical = DEFAULT_VERTICAL;
    updateRotMx();
    zoom = 1.0f;
}
Camera2D::Camera2D(Vec2 pos, Vec2 vertical) : br(*new BoundingRectangle(pos, DEFAULT_WIDTH, DEFAULT_HEIGHT))
{
    this->vertical = vertical.getNormalized();
    setBaseWidthHeight(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    updateRotMx();
    zoom = 1.0f;
}
Camera2D::Camera2D(Vec2 pos, Vec2 vertical, float width, float height) : br(*new BoundingRectangle(pos, width, height))
{
    this->vertical = vertical.getNormalized();
    updateRotMx();
    zoom = 1.0f;
}
void Camera2D::setBaseWidth(float width)
{
    br.setWidth(width);
}
void Camera2D::setBaseHeigth(float height)
{
    br.setHeight(height);
}
void Camera2D::setBaseWidthHeight(float width, float height)
{
    br.setWidth(width);
    br.setHeight(height);
}
void Camera2D::setPos(Vec2 pos)
{
    br.setPos(pos);
}
void Camera2D::setRot(float rot)
{
    vertical = DEFAULT_VERTICAL;
    vertical.rotate(rot);
    br.setRot(rot);
}
void Camera2D::setVertical(Vec2 vertical)
{
    this->vertical = vertical.getNormalized();
    br.setRot(this->vertical.getAngle(DEFAULT_VERTICAL));
}
void Camera2D::setZoom(float zoom)
{
    this->zoom=zoom;
    float scale = 1.0f/zoom;
    br.setScale(Vec2(scale,scale));
}


void Camera2D::updateData(void)
{
    br.updateVArray();
    updateRotMx();
}


const BoundingRectangle& Camera2D::getBoundingRectangle(void) const
{
    return br;
}
const Vec2& Camera2D::getPos(void) const
{
    return br.getPos();
}
const float& Camera2D::getActualWidth(void) const
{
    return br.getScaledWidth();
}
const float& Camera2D::getActualHeight(void) const
{
    return br.getScaledHeight();
}
const float& Camera2D::getBaseWidth(void) const
{
    return br.getWidth();
}
const float& Camera2D::getBaseHeight(void) const
{
    return br.getHeight();
}
const float& Camera2D::getRot(void) const
{
    return br.getRot();
}
const Vec2& Camera2D::getVertical() const
{
    return vertical;
}
const GLfloat* Camera2D::getVArray() const
{
    return br.getVArray();
}
const GLfloat* Camera2D::getRotMx() const
{
    return &rot_mx[0];
}
const float& Camera2D::getZoom(void) const
{
    return zoom;
}