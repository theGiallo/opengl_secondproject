/* 
 * File:   Sphere.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 12 ottobre 2011, 22.30
 */

#ifndef SPHERE_H
#define	SPHERE_H

#include <GL/glew.h>
#include <GL/gl.h>
#include "../maths/Vec3.h"

class Sphere {
public:
    const static unsigned sizeofpoint = 3;
    const static unsigned sizeofface = 3*sizeofpoint;
    
    Sphere(Vec3 center = Vec3(), float radius = 1);
    float radius;
    Vec3 center;
    
    static unsigned createSphereVertexArray(GLfloat *f, unsigned iterations);
private:

};

#endif	/* SPHERE_H */

