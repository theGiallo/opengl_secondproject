#include "../graphics/BoundingSphere.h"
#include "../maths/Plane.h"
#include "../graphics/Sphere.h"
#include <cfloat>

BoundingSphere::BoundingSphere()
{
    this->center = Vec3();
    this->radius = 0.5;
}
BoundingSphere::BoundingSphere(Vec3 center, float radius)
{
    this->center = center;
    this->radius = radius;
}

int BoundingSphere::collide(Frustum * frustum)
{
    return frustum->collideSphere(center,radius);
}

int BoundingSphere::collide(BoundingBox* aabb)
{
    float aabb_hs = aabb->side/2.0f;
    float centers_min_dist = aabb_hs+radius;
    float dist_to_be_inside  = aabb_hs-radius;
//    int n;
    float dx,dy,dz;
    Vec3 centers_dist = aabb->center-center;
    if ((centers_dist+Vec3( aabb_hs, aabb_hs, aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3( aabb_hs, aabb_hs,-aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3(-aabb_hs,-aabb_hs, aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3( aabb_hs,-aabb_hs,-aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3(-aabb_hs, aabb_hs, aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3(-aabb_hs, aabb_hs, aabb_hs)).getModule()<radius &&
        (centers_dist+Vec3( aabb_hs,-aabb_hs,-aabb_hs)).getModule()<radius
            )
    {
        return COLLISION_CONTAINS;
    }
    dx = ABS(aabb->center.x-center.x);
    dy = ABS(aabb->center.y-center.y);
    dz = ABS(aabb->center.z-center.z);
    if (dx<centers_min_dist &&
        dy<centers_min_dist &&
        dz<centers_min_dist)
    {
        if (dist_to_be_inside>0)
        {
            if (dx<dist_to_be_inside &&
                dy<dist_to_be_inside &&
                dz<dist_to_be_inside)
            {
                return COLLISION_IN;
            }
        }
        return COLLISION_COLLIDE;
    }
    return COLLISION_OUT;
}

float BoundingSphere::distanceFrom(BoundingBox* aabb, Vec3* pointOnMe, Vec3* pointOnAABB)
{
    Plane plane;
    Vec3 projection, axis, result, possible_result;
    float c_dist;
    float aabb_hs = aabb->side/2.0f;
    uint8_t checks, dir_checks, i;
    for (i = 0 ; i<6 ; i++)
    {
        axis = Vec3();
        axis.set(i/2,i%2?1:-1);
        plane = Plane(axis, aabb->center+axis*aabb_hs);
        c_dist = plane.dist(center);
        checks=0;
        if (c_dist<0) // TODO is correct?
        {
            continue;
        }
        projection = center+axis*c_dist; // TODO or plus?
        if (projection.get(i/2+1)>aabb->center.get(i/2+1)+aabb_hs && projection.get(i/2+1)<aabb->center.get(i/2+1)-aabb_hs)
        {
            result.set(i/2, projection.get(i/2));
            checks++;
            dir_checks = 2;
        }
        if (projection.get(i/2+2)>aabb->center.get(i/2+2)+aabb_hs && projection.get(i/2+2)<aabb->center.get(i/2+2)-aabb_hs)
        {
            if (checks++)
            {
                result = projection;
                break;
            } else
            {
                result.set(i/2, projection.get(i/2));
                dir_checks = 1;
            }
        }
        if (checks)
        {
            float tmpc = aabb->center.get(i/2+dir_checks);
            projection.set(i/2+dir_checks, tmpc+ (tmpc>projection.get(i/2+dir_checks)?-aabb_hs:aabb_hs) );
            result = projection;
            break;
//            if(!(i%2)) /// if it's not behind a plane of a face, it'll be behind its opposite face
//            {
//                i++;
//            }
        } else
        {
            Vec3 tmpv;
            float mind=FLT_MAX,tmpd;
            // <result may be the minumum distant point of the face>
            for (uint8_t j=0 ; j<4 ; j++)
            {
                tmpv = center+axis*aabb_hs;
                tmpv.add(i/2+1,j%2?aabb_hs:-aabb_hs); // 0 1 0 1
                tmpv.add(i/2+2,j%3?aabb_hs:-aabb_hs); // 0 1 2 0
                if ((tmpd=(tmpv-center).getModule())<mind)
                {
                    possible_result = tmpv;
                }
            }
        }
    }
    if (i==6)
    {
        result = possible_result;
    }
    *pointOnAABB = result;
    *pointOnMe = (result-center).getNormalized()*radius+center;
    return (*pointOnMe-*pointOnAABB).getModule();
}
void BoundingSphere::render(void)
{
    glPushAttrib(GL_CURRENT_BIT);
        glPushAttrib(GL_ENABLE_BIT);
            glColor4f(1,1,1,1);
            glPolygonMode(GL_FRONT, GL_LINE);
            glPolygonMode(GL_BACK, GL_NONE);
            glDisable(GL_TEXTURE_2D);
            glEnableClientState(GL_VERTEX_ARRAY);
            glInterleavedArrays(GL_V3F, Sphere::sizeofpoint*sizeof(GLfloat), &vArray[0]);
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
                glLoadIdentity();
                glTranslated(center.x,center.y,center.z);
                glScaled(radius,radius,radius);
                glMatrixMode(GL_MODELVIEW);
            glPopMatrix();
            glDrawArrays(GL_TRIANGLES, 0, nfaces*3);
            glDisableClientState(GL_VERTEX_ARRAY);///potrebbe essere inutile
        glPopAttrib();
    glPopAttrib();
}
GLfloat* BoundingSphere::init_vArray()
{
    Sphere::createSphereVertexArray(BoundingSphere::vArray, sphere_model_iterations);
    return BoundingSphere::vArray;
}
//const unsigned BoundingSphere::sphere_model_iterations = 3; //=3
//const unsigned BoundingSphere::nfaces = 4*4*4*8; //=4^sphere_model_iterations *8
GLfloat BoundingSphere::vArray[nfaces*Sphere::sizeofface];
GLfloat* BoundingSphere::tmppointerescamotage = BoundingSphere::init_vArray();
