/* 
 * File:   Renderable2D.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 06 November 2011
 */
#error

#ifndef RENDERABLE2D_H
#define	RENDERABLE2D_H

#include "../maths/Vec2.h"
#include <cstdlib>

class Bounder2D;
class Collider2D;

/*
	TODO: la propriet� della collisione deve essere associata ad un PositionableObject2D non ad un Renderable2D
	quindi dovrebbe(o � meglio scorrere una delle 2 gi� esistemti?) essere aggiunta una lista di PositionableObject2D e modificato il ciclo di controllo delle collisioni di conseguenza.
*/

class Renderable2D
{
public:
    bool visible;
    Bounder2D * bounder;
    Collider2D* collider;
    bool intersectable;
    bool check_collisions;
    Renderable2D()
    {
        bounder = NULL;
        collider = NULL;
        visible = true;
        intersectable = check_collisions = false;
    }
    virtual void render(void)=0;
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection)=0;
};
#include "../game/Collider2D.h"

#endif	/* RENDERABLE2D_H */
