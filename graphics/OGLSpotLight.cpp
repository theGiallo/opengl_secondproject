#ifdef __linux__
	#include <X11/X.h>
#endif

#include "../graphics/OGLSpotLight.h"

void OGLSpotLight::setDirection(Vec3 direction)
{
    this->direction = direction;
}
Vec3 OGLSpotLight::getDirection(void)
{
    return direction;
}
void OGLSpotLight::setCutoff(GLfloat cutoff)
{
    this->cutoff = cutoff;
}
GLfloat OGLSpotLight::getCutoff(void)
{
    return cutoff;
}
void OGLSpotLight::setExponent(GLfloat exponent)
{
    this->exponent = exponent;
}
GLfloat OGLSpotLight::getExponent(void)
{
    return exponent;
}
void OGLSpotLight::glSet(void)
{
    if (!active)
        return;
    glLightf(id, GL_SPOT_CUTOFF, cutoff);
    glLightf(id, GL_SPOT_EXPONENT, exponent);
    GLfloat dirv[3];
    dirv[0] = direction.x;
    dirv[1] = direction.y;
    dirv[2] = direction.z;
    glLightfv(id, GL_SPOT_DIRECTION, dirv);
    OGLLocalLight::glSet();
}
void OGLSpotLight::setSpotProp(Vec3 direction, GLfloat cutoff, GLfloat exponent)
{
    setDirection(direction);
    setCutoff(cutoff);
    setExponent(exponent);
}
OGLSpotLight::OGLSpotLight(Vec3 pos, Vec3 direction, GLfloat cutoff, GLfloat exponent)
{
    if (!init())
    {
        throw false;
    }
    initDefault();
    setPosition(pos);
    setDirection(direction);
    setCutoff(cutoff);
    setExponent(exponent);
}
OGLSpotLight::OGLSpotLight(void)
{
    setDirection(Vec3(0,0,1));
    setCutoff(90);
    setExponent(2);
}
