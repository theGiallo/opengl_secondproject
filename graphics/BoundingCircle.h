/** 
 * File:   BoundingCircle.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on August 25 2012
 */

#ifndef BOUNDINGCIRCLE_H
#define	BOUNDINGCIRCLE_H

#include "../graphics/Renderables2D.h"
#include "../graphics/RegularPolygon.h"

class BoundingCircle : public Bounder2D
{
protected:
    float radius;
    static RegularPolygon2D circle_appearence;
public:
    BoundingCircle(const float& = 1.0f, Vec2 center = Vec2());
    
    const float& getRadius_const(void) const;
    float& getRadius_r(void);
    void setRadius(const float& radius);
    
    virtual int collide(const Camera2D &camera);
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};

#endif /* BOUNDINGCIRCLE_H */