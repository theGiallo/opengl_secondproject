#include "../graphics/Text.h"
#include "../graphics/TextureManager.h"
#include <string>
#include "../graphics/GLDebug.h"
#include "iostream"
#include "OGLColors.h"

Text::Text(void)
{
    base = fontSize = 0;
    color = OGLColors::white;
}
Text::~Text(void)
{
}
bool Text::loadFontTexture(std::string file_path)
{
    try{
        font = TextureManager::loadTexture(file_path);
    } catch (bool err)
    {
        return false;
    }
    return true;
}
void Text::BuildFont(void)
{
    int i;
    float cx,cy;

    base = glGenLists(256);
    std::cout<<"base font list = "<<base<<std::endl;
    fontSize = font->width/16;
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, font->texId);
    for ( i=0 ; i<256 ; i++ )
    {
        cx = float(i%16)/16.0f;
        cy = float(i/16)/16.0f;

        glNewList(base+i, GL_COMPILE);
        glBegin(GL_QUADS);
            glTexCoord2d(cx, 1-cy-.0625f);
            glVertex2i(0,0);
            glTexCoord2d(cx+0.0625f, 1-cy-.0625f);
            glVertex2i(fontSize,0);
            glTexCoord2d(cx+0.0625, 1-cy);
            glVertex2i(fontSize,fontSize);
            glTexCoord2d(cx, 1-cy);
            glVertex2i(0,fontSize);
        glEnd();
        glTranslated(fontSize,0,0);
        glEndList();
    }
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
}
void Text::print(float x, float y, std::string str, float size, float width, float height, int variant)
{
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    if ( variant!=0 )
    {
        variant = 1;
    }
//    GLint depth;
//    glGetIntegerv(GL_MODELVIEW_STACK_DEPTH, &depth);
//    std::cout<<"MODEL_VIEW stack size is "<<depth<<std::endl;
//    glGetIntegerv(GL_PROJECTION_STACK_DEPTH, &depth);
//    std::cout<<"PROJECTION stack size is "<<depth<<std::endl;
//    std::cout<<(unsigned char*)str.c_str()<<std::endl<<"is long "<<strlen(str.c_str())<<std::endl;
    glPushAttrib(GL_ENABLE_BIT);
    glPushAttrib(GL_CURRENT_BIT);
    glPushAttrib(GL_COLOR_BUFFER_BIT);
    glPushAttrib(GL_POLYGON_BIT);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_NONE);
		glEnable(GL_TEXTURE_2D);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glBindTexture(GL_TEXTURE_2D,font->texId);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
			glLoadIdentity();
			glOrtho(0,width,0,height,-100,100);
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
				glLoadIdentity();
				glTranslated(x,y,0);
				glScaled(size,size,1);
				glListBase(base+128*variant);
				glColor4fv(color);
				glCallLists(str.length(), GL_BYTE, str.c_str());
			glMatrixMode(GL_MODELVIEW);
			glPopMatrix();
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glPopAttrib();
    glPopAttrib();
    glPopAttrib();
    glPopAttrib();
    //TODO patch temporanea //rimossa
        //glPolygonMode(GL_FRONT, GL_FILL);
        //glPolygonMode(GL_BACK, GL_LINE);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
}
