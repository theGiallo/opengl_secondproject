/* 
 * File:   OGLSpotLight.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 4 febbraio 2011, 23.58
 */

#ifndef OGLSPOTLIGHT_H
#define	OGLSPOTLIGHT_H

#include "../graphics/OGLLocalLight.h"

class OGLSpotLight : public OGLLocalLight
{
public:
    Vec3 direction;
    GLfloat cutoff;
    GLfloat exponent;

    void setDirection(Vec3 direction);
    Vec3 getDirection(void);
    void setCutoff(GLfloat cutoff);
    GLfloat getCutoff(void);
    void setExponent(GLfloat exponent);
    GLfloat getExponent(void);
    virtual void glSet(void);
    void setSpotProp(Vec3 direction, GLfloat cutoff, GLfloat exponent);
    OGLSpotLight(Vec3 pos, Vec3 direction, GLfloat cutoff, GLfloat exponent);
    OGLSpotLight();

};

#endif	/* OGLSPOTLIGHT_H */

