/** 
 * File:   CollisionSolver.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on August 25 2012
 */

#ifndef COLLISIONSOLVER_H
#define	COLLISIONSOLVER_H

#include "../graphics/Renderables2D.h"
#include "../graphics/BoundingCircle.h"
#include "../graphics/BoundingRectangle.h"
#include "../graphics/RegularPolygonBounder.h"


#define COLLISION_OUT 0
#define COLLISION_IN 1 //(the ist is inside the 2nd)
#define COLLISION_COLLIDE 2
#define COLLISION_CONTAINS 3 //(the 1st contains the 2nd)

/**
 * the collide functions returns an integer expressing the relation of the first
 * argument to the second
 *      COLLISION_OUT 0
 *      COLLISION_IN 1 (the ist is inside the 2nd)
 *      COLLISION_COLLIDE 2
 *      COLLISION_CONTAINS 3 (the 1st contains the 2nd)
 **/
class CollisionSolver2D
{
protected:
    enum rel_1d
    {
        LEFTOF,
        RIGHTOF,
        INTERSECTSLEFT,
        INTERSECTSRIGHT,
        INTERSECTSBOTH,
        INSIDE
    };
    static rel_1d getRel1D(float c, float r, float w);
public:
    static int collide(const BoundingCircle& c1, const BoundingCircle& c2, float& distance_out);
    static int collide(const BoundingCircle& c1, const BoundingRectangle& r1, float& distance_out);
    static int collide(const BoundingCircle& c1, const RegularPolygonBounder& rp1, float& distance_out);
    static int collide(const BoundingRectangle& r1, const BoundingRectangle& r2, float& distance_out);
    static int collide(const BoundingRectangle& r1, const BoundingCircle& c1, float& distance_out);
    static int collide(const BoundingRectangle& r1, const RegularPolygonBounder& rp1, float& distance_out);
    static int collide(const RegularPolygonBounder& rp1, const RegularPolygonBounder& rp2, float& distance_out);
    static int collide(const RegularPolygonBounder& rp1, const BoundingRectangle& r1, float& distance_out);
    static int collide(const RegularPolygonBounder& rp1, const BoundingCircle& c1, float& distance_out);
    
    /**
     * This is meant to be the last one. It's a placeholder to let code compile
     * @param b1
     * @param b2
     * @return 
     */
    static int collide(const Bounder2D& b1, const Bounder2D& b2, float& distance_out); // placeholder
};

#endif /* COLLISIONSOLVER_H */