/* 
 * File:   OGLLight.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 4 febbraio 2011, 22.57
 */

#ifndef OGLLIGHT_H
#define	OGLLIGHT_H

#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>

class OGLLight
{
protected:
    bool active;
    bool init(void);
    static bool usedLights[8];
    static GLchar enumToNum(GLenum en);
    static GLenum numToEum(GLchar num);
public:
    GLenum id;
    GLfloat ambient[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    
    void setActive(bool active);
    bool isActive(void);
    void setAmbient(GLfloat ambient[4]);
    void setDiffuse(GLfloat diffuse[4]);
    void setSpecular(GLfloat specular[4]);
    void setParameters(bool active, GLfloat ambient[4], GLfloat diffuse[4], GLfloat specular[4]);
    void initDefault();
    virtual void glSet(void);
    OGLLight(bool active, GLfloat ambient[4], GLfloat diffuse[4], GLfloat specular[4]);
    void destroy(void);
    OGLLight(void);
    ~OGLLight(void);
};

#endif	/* OGLLIGHT_H */

