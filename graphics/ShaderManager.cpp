#include "ShaderManager.h"
#include "../graphics/GLDebug.h"
#include <iostream>

std::list<Shader*> initSL()
{
    std::list<Shader*> tmp;
    return tmp;
}
std::list<Shader*> ShaderManager::shaders(initSL());

Shader* ShaderManager::loadShader(std::string pathName)
{
    for (std::list<Shader*>::iterator it=ShaderManager::shaders.begin(); it!=ShaderManager::shaders.end(); ++it)
    {
        if ( (*it)->path_name.compare(pathName)==0 )
        {
            return *it;
        }
    }
    //DEBUG begin
    std::cout<<"\t\tcreating a new shader from file '"<<pathName<<"'...";
    //DEBUG end
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    Shader * tmp_shdr = new Shader();
    if ( !tmp_shdr->load(pathName) )
    {
        std::cerr<<std::endl<<"Error loading the shader '"<<pathName<<"'"<<std::endl;
        throw false;
    }
    ShaderManager::shaders.push_front(tmp_shdr);
#ifdef DEBUG_GL
    CheckGLError(__FILE__, __LINE__);
#endif
    //DEBUG begin
    std::cout<<"\t\tdone"<<std::endl;
    //DEBUG end
    return tmp_shdr;
}
