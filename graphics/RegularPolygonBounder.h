/**
 * Author: theGiallo
 * Directory: graphics
 **/

#ifndef REGULARPOLYGONBOUNDER_H
#define	REGULARPOLYGONBOUNDER_H
#include "../graphics/Renderables2D.h"
#include "../graphics/RegularPolygon.h"
#include "../graphics/OGLTools.h"

class RegularPolygonBounder : public Bounder2D
{
protected:
    GLfloat* vArray;
    RegularPolygon2D polygon;
public:

    RegularPolygonBounder(unsigned int n);

    virtual int collide(RegularPolygonBounder &rpb);
	
    virtual void setPos(const Vec2& pos);
    virtual void setUp(const Vec2& up);
    virtual void setScale(const Vec2& scale);
    virtual void setRot(float rot);
    const GLfloat* getVArray(void) const;
    const RegularPolygon2D& getPolygon(void) const;

    virtual int collide(const Camera2D &camera);
    virtual void render(void);
    virtual float rayIntersect(Vec2 start, Vec2 dir, Vec2 * intersection);
};
#endif /* REGULARPOLYGONBOUNDER_H */
