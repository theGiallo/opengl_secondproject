/* 
 * File:   Text.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 31 gennaio 2011, 17.54
 */

#ifndef TEXT_H
#define	TEXT_H

#include <string>
#include "../graphics/Texture.h"

class Text
{
public:
    Texture* font;
    GLuint base;
    GLuint fontSize; // fonts used are monospace and square
    GLfloat* color;

    Text(void);
    ~Text(void);
    bool loadFontTexture(std::string file_path);
    void BuildFont(void);
    //ASCII code is composed by 128 characters that is 2^7. Textures should be squared, so we use a variant in order to use all the texture space
    void print(float x, float y, std::string, float size, float width, float height, int variant=0);
};

#endif	/* TEXT_H */

