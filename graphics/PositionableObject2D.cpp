#define _USE_MATH_DEFINES
#include <cmath>
#include "Renderables2D.h"


#define DEFAULT_UP Vec2(0.0f,1.0f)
#define DEFAULT_ROT 0.0f

PositionableObject2D::PositionableObject2D()
{
    scale = UNITARY_SCALE;
    pos = Vec2();
    up = DEFAULT_UP;
    rot = DEFAULT_ROT;
}
const Vec2& PositionableObject2D::getPos(void) const
{
    return pos;
}
const Vec2& PositionableObject2D::getUp(void) const
{
    return up;
}
const Vec2& PositionableObject2D::getScale(void) const
{
    return scale;
}
void PositionableObject2D::setPos(const Vec2& pos)
{
    this->pos = pos;
    if (bounder)
    {
        bounder->setPos(pos);
    }
}
void PositionableObject2D::setUp(const Vec2& up)
{
    setRot(up.getAngle(DEFAULT_UP));
    if (bounder)
    {
        bounder->setRot(rot);
    }
    this->up = up;
}
void PositionableObject2D::setScale(const Vec2& scale)
{
    this->scale = scale;
    if (bounder)
    {
        bounder->setScale(scale);
    }
}
float PositionableObject2D::getRot(void) const
{
    return rot;
}
void PositionableObject2D::setRot(float rot)
{
    float _2pi = M_PI*2.0f;
    if (rot >= _2pi || rot<0)
    {
            float n = rot / (_2pi), ni, nf;
            nf = std::modf (n , &ni);
            rot = nf*_2pi;
    }
    up.x = sinf(rot);
    up.y = cosf(rot);
    this->rot = rot;
    if (bounder)
    {
        bounder->setRot(rot);
    }
}