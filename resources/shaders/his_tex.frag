uniform sampler2D myTexture;
varying vec2 vTexCoord;

void main (void)  
{
    vec4 color = texture2D(myTexture, vTexCoord.st);

    if (color.rgb == vec3(1.0,0.0,0.0))
    {
        // discard;
    }
    if (color.rgb == vec3(0,0,0))
    {
        color.rgb = vec3(1,0,0);
    }

    gl_FragColor = color;
}
