uniform vec3 CamPos, CamDir;
uniform float DepthNear, DepthFar;
varying float CameraDepth;

void main (void)  
{
	vec3 offset = (gl_Vertex.xyz / gl_Vertex.w) - CamPos;
	float z = -dot(offset, CamDir);
	CameraDepth = (z - DepthNear) / (DepthFar - DepthNear);
}
