varying vec3 N;
varying vec3 v;    

void main (void)  
{  
	vec4 Iamb;
	vec4 Idiff;
	vec4 Ispec;
	int i=0;
	vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  
	for ( i=0 ; i<2 ; i++ )
	{
		vec3 L = normalize(gl_LightSource[i].position.xyz - v);   
		vec3 R = normalize(-reflect(L,N));  

		//calculate Ambient Term:  
		Iamb = Iamb+gl_FrontLightProduct[i].ambient;    

		//calculate Diffuse Term:  
		Idiff = Idiff+gl_FrontLightProduct[i].diffuse * max(dot(N,L), 0.0);    

		// calculate Specular Term:
		Ispec = Ispec+gl_FrontLightProduct[i].specular 
				    * pow(max(dot(R,E),0.0),0.3*gl_FrontMaterial.shininess);
    }

   // write Total Color:  
   gl_FragColor = gl_FrontLightModelProduct.sceneColor * (Iamb + Idiff + Ispec);   
   //gl_FragColor = vec4(1,1,1,1);
}
