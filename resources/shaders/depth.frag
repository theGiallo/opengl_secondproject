varying float CameraDepth;
uniform sampler2D myTexture;

void main (void)  
{
	gl_FragColor.rgb = vec3(CameraDepth);
	gl_FragColor.a = 1.0;
}
