varying vec2 vTexCoord;

void main(void)
{
   vTexCoord = gl_MultiTexCoord0.xy;
   //vTexCoord = gl_TexCoord[0].st;
   gl_Position = ftransform();
}
