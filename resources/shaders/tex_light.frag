uniform sampler2D myTexture;
varying vec2 vTexCoord;
varying vec3 N;
varying vec3 v;

void main (void)  
{  vec4 color = texture2D(myTexture, vTexCoord);       
   
	if (color.rgb == vec3(1.0,0.0,0.0))
		discard; 

	vec3 L = normalize(gl_LightSource[0].position.xyz - v);   
	vec3 E = normalize(-v); // we are in Eye Coordinates, so EyePos is (0,0,0)  
	vec3 R = normalize(-reflect(L,N));  

	//calculate Ambient Term:  
	vec4 Iamb = gl_FrontLightProduct[0].ambient;    

	//calculate Diffuse Term:  
	vec4 Idiff = gl_FrontLightProduct[0].diffuse * max(dot(N,L), 0.0);    

	// calculate Specular Term:
	vec4 Ispec = gl_FrontLightProduct[0].specular 
		        * pow(max(dot(R,E),0.0),0.3*gl_FrontMaterial.shininess);
                
	color.r = color.r*(Iamb.r+Idiff.r+Ispec.r);
	color.g = color.g*(Iamb.g+Idiff.g+Ispec.g);
	color.b = color.b*(Iamb.b+Idiff.b+Ispec.b);
	gl_FragColor = color;
}